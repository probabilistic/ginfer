open Lp_common

module Clp = struct
  include Stubs

  let force_sync _ = ()

  let solve_with_log_level t l =
    set_log_level t l;
    initial_solve t
  ;;
end

let () = register_lp_backend_std "clp" (module Clp)
