module C = Configurator.V1

let () =
  C.main ~name:"lp-config" (fun _ ->
      let empty_flags = { C.Pkg_config.cflags = []; libs = [] } in
      let get_dirs ?(inc = "include") ?(lib = "lib") prefix =
        Filename.concat prefix inc, Filename.concat prefix lib
      in
      let config_glp prefix =
        let include_dir, lib_dir = get_dirs prefix in
        let cflags = [ Printf.sprintf "-I%s" include_dir ] in
        let libs =
          [ Printf.sprintf "-Wl,-rpath,%s" lib_dir
          ; Printf.sprintf "-L%s" lib_dir
          ; "-lglpk"
          ]
        in
        { C.Pkg_config.cflags; libs }
      in
      let config_grb prefix =
        let include_dir, lib_dir = get_dirs prefix in
        let cflags = [ Printf.sprintf "-I%s" include_dir ] in
        let libs =
          [ Printf.sprintf "-Wl,-rpath,%s" lib_dir
          ; Printf.sprintf "-L%s" lib_dir
          ; "-lgurobi95"
          ]
        in
        { C.Pkg_config.cflags; libs }
      in
      let config_clp prefix =
        let include_dir, lib_dir = get_dirs prefix in
        let cflags = [ Printf.sprintf "-I%s" include_dir ] in
        let libs =
          [ Printf.sprintf "-Wl,-rpath,%s" lib_dir
          ; Printf.sprintf "-L%s" lib_dir
          ; "-lClp"
          ; "-lCoinUtils"
          ]
        in
        { C.Pkg_config.cflags; libs }
      in
      let config_msk prefix =
        let include_dir, lib_dir = get_dirs ~inc:"h" ~lib:"bin" prefix in
        let cflags = [ Printf.sprintf "-I%s" include_dir ] in
        let libs =
          [ Printf.sprintf "-Wl,-rpath,%s" lib_dir
          ; Printf.sprintf "-L%s" lib_dir
          ; "-lmosek64"
          ]
        in
        { C.Pkg_config.cflags; libs }
      in
      let config_spx prefix =
        let include_dir, lib_dir = get_dirs prefix in
        let cflags = [ Printf.sprintf "-I%s" include_dir ] in
        let libs =
          [ Printf.sprintf "-Wl,-rpath,%s" lib_dir
          ; Printf.sprintf "-L%s" lib_dir
          ; "-lsoplex"
          ; "-lgmp"
          ; "-lz"
          ]
        in
        { C.Pkg_config.cflags; libs }
      in
      let glp_conf =
        match Sys.getenv_opt "LIBGLP" with
        | Some l when String.length l > 0 -> config_glp l
        | _ -> empty_flags
      in
      C.Flags.write_sexp "glp_c_flags.sexp" glp_conf.cflags;
      C.Flags.write_sexp "glp_c_library_flags.sexp" glp_conf.libs;
      let grb_conf =
        match Sys.getenv_opt "LIBGRB" with
        | Some l when String.length l > 0 -> config_grb l
        | _ -> empty_flags
      in
      C.Flags.write_sexp "grb_c_flags.sexp" grb_conf.cflags;
      C.Flags.write_sexp "grb_c_library_flags.sexp" grb_conf.libs;
      let clp_conf =
        match Sys.getenv_opt "LIBCLP" with
        | Some l when String.length l > 0 -> config_clp l
        | _ -> empty_flags
      in
      C.Flags.write_sexp "clp_c_flags.sexp" clp_conf.cflags;
      C.Flags.write_sexp "clp_c_library_flags.sexp" clp_conf.libs;
      let msk_conf =
        match Sys.getenv_opt "LIBMSK" with
        | Some l when String.length l > 0 -> config_msk l
        | _ -> empty_flags
      in
      C.Flags.write_sexp "msk_c_flags.sexp" msk_conf.cflags;
      C.Flags.write_sexp "msk_c_library_flags.sexp" msk_conf.libs;
      let spx_conf =
        match Sys.getenv_opt "LIBSPX" with
        | Some l when String.length l > 0 -> config_spx l
        | _ -> empty_flags
      in
      C.Flags.write_sexp "spx_c_flags.sexp" spx_conf.cflags;
      C.Flags.write_sexp "spx_c_library_flags.sexp" spx_conf.libs)
;;
