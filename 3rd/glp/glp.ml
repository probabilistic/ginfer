open Lp_common

module Glp = struct
  include Stubs

  let force_sync _ = ()
  let solve_with_log_level t l = simplex t l
end

let () = register_lp_backend_std "glp" (module Glp)
