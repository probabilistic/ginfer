/*
 * Copyright (C) 2020 Di Wang
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <glpk.h>
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>

#define Model_val(v) (*((glp_prob **) Data_custom_val(v)))

static void finalize_model(value v) {
    glp_delete_prob(Model_val(v));
}

static struct custom_operations glp_model_ops = {
    "edu.cmu.ocaml-glp.0",
    finalize_model,
    custom_compare_default,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
};

static value alloc_model(glp_prob *c_model) {
    value block = caml_alloc_custom(&glp_model_ops, sizeof(glp_prob *), 0, 1);
    Model_val(block) = c_model;
    return block;
}

/*
 * Creating model
 */

/*
 * val create : unit -> t
 */
CAMLprim value ocaml_glp_create(value unit) {
    CAMLparam1(unit);

    CAMLreturn(alloc_model(glp_create_prob()));
}

/*
 * Getters and setters of problem parameters
 */

/*
 * val number_rows : t -> int
 */
CAMLprim value ocaml_glp_number_rows(value model) {
    CAMLparam1(model);

    glp_prob *c_model = Model_val(model);
    int c_num_rows = glp_get_num_rows(c_model);

    CAMLreturn(Val_int(c_num_rows));
}

/*
 * val number_columns : t -> int
 */
CAMLprim value ocaml_glp_number_columns(value model) {
    CAMLparam1(model);

    glp_prob *c_model = Model_val(model);
    int c_num_columns = glp_get_num_cols(c_model);

    CAMLreturn(Val_int(c_num_columns));
}

/*
 * val number_elements : t -> int
 */
CAMLprim value ocaml_glp_number_elements(value model) {
    CAMLparam1(model);

    glp_prob *c_model = Model_val(model);
    int c_num_nzs = glp_get_num_nz(c_model);

    CAMLreturn(Val_int(c_num_nzs));
}

/*
 * val direction : t -> direction
 */
CAMLprim value ocaml_glp_direction(value model) {
    CAMLparam1(model);

    glp_prob *c_model = Model_val(model);
    int c_dir = glp_get_obj_dir(c_model);
    c_dir = c_dir == GLP_MAX ? 0 : 1;

    CAMLreturn(Val_int(c_dir));
}

/*
 * val set_direction : t -> direction -> unit
 */
CAMLprim value ocaml_glp_set_direction(value model, value direction) {
    CAMLparam2(model, direction);

    glp_prob *c_model = Model_val(model);
    int c_dir = Int_val(direction) == 0 ? GLP_MAX : GLP_MIN;
    glp_set_obj_dir(c_model, c_dir);

    CAMLreturn(Val_unit);
}

/*
 * val add_rows : t -> row array -> int -> unit
 */
CAMLprim value ocaml_glp_add_rows(value model, value rows, value nrows) {
    CAMLparam3(model, rows, nrows);
    CAMLlocal3(row, elements, element);

    glp_prob *c_model = Model_val(model);
    const int c_row_count = Int_val(nrows);
    int c_row_start = glp_get_num_rows(c_model);
    glp_add_rows(c_model, c_row_count);
    int i, j, c_row_size;
    int *c_inds;
    double *c_vals;
    double c_lb, c_ub;

    for (i = 0; i < c_row_count; ++i) {
        row = Field(rows, i);
        elements = Field(row, 2);
        c_row_size = Wosize_val(elements);
        c_inds = (int *)malloc(sizeof(int) * (c_row_size + 1));
        c_vals = (double *)malloc(sizeof(double) * (c_row_size + 1));
        for (j = 0; j < c_row_size; ++j) {
            element = Field(elements, j);
            c_inds[j + 1] = Int_val(Field(element, 0)) + 1;
            c_vals[j + 1] = Double_val(Field(element, 1));
        }
        glp_set_mat_row(c_model, c_row_start + i + 1, c_row_size, c_inds, c_vals);
        c_lb = Double_val(Field(row, 0));
        c_ub = Double_val(Field(row, 1));
        int kind;
        if (isinf(c_lb) && isinf(c_ub)) {
            kind = GLP_FR;
        } else if (isinf(c_ub)) {
            kind = GLP_LO;
        } else if (isinf(c_lb)) {
            kind = GLP_UP;
        } else if (c_lb == c_ub) {
            kind = GLP_FX;
        } else {
            kind = GLP_DB;
        }
        glp_set_row_bnds(c_model, c_row_start + i + 1, kind, c_lb, c_ub);
        free(c_inds);
        free(c_vals);
    }

    CAMLreturn(Val_unit);
}

/*
 * val delete_rows : t -> int array -> unit
 */
CAMLprim value ocaml_glp_delete_rows(value model, value which) {
    CAMLparam2(model, which);

    glp_prob *c_model = Model_val(model);
    const int c_number = Wosize_val(which);
    int *c_which = (int *)malloc(sizeof(int) * c_number);
    int i;

    for (i = 0; i < c_number; ++i) {
        c_which[i] = Int_val(Field(which, i)) + 1;
    }
    glp_del_rows(c_model, c_number, c_which);
    free(c_which);

    CAMLreturn(Val_unit);
}

/*
 * val add_columns : t -> column array -> int -> unit
 */
CAMLprim value ocaml_glp_add_columns(value model, value columns, value ncolumns) {
    CAMLparam3(model, columns, ncolumns);
    CAMLlocal3(column, elements, element);

    glp_prob *c_model = Model_val(model);
    const int c_column_count = Int_val(ncolumns);
    int c_column_start = glp_get_num_cols(c_model);
    glp_add_cols(c_model, c_column_count);
    int i, j, c_column_size;
    int *c_inds;
    double *c_vals;
    double c_lb, c_ub, c_obj;

    for (i = 0; i < c_column_count; ++i) {
        column = Field(columns, i);
        elements = Field(column, 3);
        c_column_size = Wosize_val(elements);
        c_inds = (int *)malloc(sizeof(int) * (c_column_size + 1));
        c_vals = (double *)malloc(sizeof(double) * (c_column_size + 1));
        for (j = 0; j < c_column_size; ++j) {
            element = Field(elements, j);
            c_inds[j + 1] = Int_val(Field(element, 0)) + 1;
            c_vals[j + 1] = Double_val(Field(element, 1));
        }
        glp_set_mat_col(c_model, c_column_start + i + 1, c_column_size, c_inds, c_vals);
        c_lb = Double_val(Field(column, 1));
        c_ub = Double_val(Field(column, 2));
        int kind;
        if (isinf(c_lb) && isinf(c_ub)) {
            kind = GLP_FR;
        } else if (isinf(c_ub)) {
            kind = GLP_LO;
        } else if (isinf(c_lb)) {
            kind = GLP_UP;
        } else if (c_lb == c_ub) {
            kind = GLP_FX;
        } else {
            kind = GLP_DB;
        }
        c_obj = Double_val(Field(column, 0));
        glp_set_col_bnds(c_model, c_column_start + i + 1, kind, c_lb, c_ub);
        glp_set_col_kind(c_model, c_column_start + i + 1, GLP_CV);
        glp_set_obj_coef(c_model, c_column_start + i + 1, c_obj);
        free(c_inds);
        free(c_vals);
    }

    CAMLreturn(Val_unit);
}

/*
 * val delete_columns : t -> int array -> unit
 */
CAMLprim value ocaml_glp_delete_columns(value model, value which) {
    CAMLparam2(model, which);

    glp_prob *c_model = Model_val(model);
    const int c_number = Wosize_val(which);
    int *c_which = (int *)malloc(sizeof(int) * c_number);
    int i;

    for (i = 0; i < c_number; ++i) {
        c_which[i] = Int_val(Field(which, i)) + 1;
    }
    glp_del_cols(c_model, c_number, c_which);
    free(c_which);

    CAMLreturn(Val_unit);
}

/*
 * val objective_coefficients : t -> float array
 */
CAMLprim value ocaml_glp_objective_coefficients(value model) {
    CAMLparam1(model);
    CAMLlocal1(objective_coefficients);

    glp_prob *c_model = Model_val(model);
    int c_num_columns = glp_get_num_cols(c_model);
    int i;

    objective_coefficients = caml_alloc(c_num_columns, Double_array_tag);
    for (i = 0; i < c_num_columns; ++i) {
        Store_double_field(objective_coefficients, i, glp_get_obj_coef(c_model, i + 1));
    }

    CAMLreturn(objective_coefficients);
}

/*
 * val change_objective_coefficients : t -> float array -> unit
 */
CAMLprim value ocaml_glp_change_objective_coefficients(value model, value objective_coefficients) {
    CAMLparam2(model, objective_coefficients);

    glp_prob *c_model = Model_val(model);
    int c_num_columns = glp_get_num_cols(c_model);
    int i;

    for (i = 0; i < c_num_columns; ++i) {
        glp_set_obj_coef(c_model, i + 1, Double_field(objective_coefficients, i));
    }

    CAMLreturn(Val_unit);
}

/*
 * Solver oprations
 */

 /*
  * val simplex : t -> int -> unit
  */
CAMLprim value ocaml_glp_simplex(value model, value log_level) {
    CAMLparam2(model, log_level);

    glp_prob *c_model = Model_val(model);
    glp_smcp c_param;
    glp_init_smcp(&c_param);
    c_param.msg_lev = Int_val(log_level);
    c_param.presolve = 1;
    glp_simplex(c_model, &c_param);

    CAMLreturn(Val_unit);
}

/*
 * val exact : t -> int -> unit
 */
CAMLprim value ocaml_glp_exact(value model, value log_level) {
    CAMLparam2(model, log_level);

    glp_prob *c_model = Model_val(model);
    glp_smcp c_param;
    glp_init_smcp(&c_param);
    c_param.msg_lev = Int_val(log_level);
    glp_exact(c_model, &c_param);

    CAMLreturn(Val_unit);
}

/*
 * val interior : t -> int -> unit
 */
CAMLprim value ocaml_glp_interior(value model, value log_level) {
    CAMLparam2(model, log_level);

    glp_prob *c_model = Model_val(model);
    glp_iptcp c_param;
    glp_init_iptcp(&c_param);
    c_param.msg_lev = Int_val(log_level);
    glp_interior(c_model, &c_param);

    CAMLreturn(Val_unit);
}

/*
 * Retrieving solutions
 */

/*
 * val objective_value : t -> float
 */
CAMLprim value ocaml_glp_objective_value(value model) {
    CAMLparam1(model);

    glp_prob *c_model = Model_val(model);

    CAMLreturn(caml_copy_double(glp_get_obj_val(c_model)));
}

/*
 * val primal_row_solution : t -> float array
 */
CAMLprim value ocaml_glp_primal_row_solution(value model) {
    CAMLparam1(model);
    CAMLlocal1(solution);

    glp_prob *c_model = Model_val(model);
    int c_num_rows = glp_get_num_rows(c_model);
    int i;

    solution = caml_alloc(c_num_rows, Double_array_tag);
    for (i = 0; i < c_num_rows; ++i) {
        Store_double_field(solution, i, glp_get_row_prim(c_model, i + 1));
    }

    CAMLreturn(solution);
}

/*
 * val primal_column_solution : t -> float array
 */
CAMLprim value ocaml_glp_primal_column_solution(value model) {
    CAMLparam1(model);
    CAMLlocal1(solution);

    glp_prob *c_model = Model_val(model);
    int c_num_columns = glp_get_num_cols(c_model);
    int i;

    solution = caml_alloc(c_num_columns, Double_array_tag);
    for (i = 0; i < c_num_columns; ++i) {
        Store_double_field(solution, i, glp_get_col_prim(c_model, i + 1));
    }

    CAMLreturn(solution);
}

/*
 * val dual_row_solution : t -> float array
 */
CAMLprim value ocaml_glp_dual_row_solution(value model) {
    CAMLparam1(model);
    CAMLlocal1(solution);

    glp_prob *c_model = Model_val(model);
    int c_num_rows = glp_get_num_rows(c_model);
    int i;

    solution = caml_alloc(c_num_rows, Double_array_tag);
    for (i = 0; i < c_num_rows; ++i) {
        Store_double_field(solution, i, glp_get_row_dual(c_model, i + 1));
    }

    CAMLreturn(solution);
}

/*
 * val dual_column_solution : t -> float array
 */
CAMLprim value ocaml_glp_dual_column_solution(value model) {
    CAMLparam1(model);
    CAMLlocal1(solution);

    glp_prob *c_model = Model_val(model);
    int c_num_columns = glp_get_num_cols(c_model);
    int i;

    solution = caml_alloc(c_num_columns, Double_array_tag);
    for (i = 0; i < c_num_columns; ++i) {
        Store_double_field(solution, i, glp_get_col_dual(c_model, i + 1));
    }

    CAMLreturn(solution);
}

/*
 * val status : t -> status
 */
CAMLprim value ocaml_glp_status(value model) {
    CAMLparam1(model);

    glp_prob *c_model = Model_val(model);
    int c_status = glp_get_status(c_model);
    if (c_status == GLP_OPT) {
        c_status = 0;
    } else if (c_status == GLP_INFEAS || c_status == GLP_NOFEAS) {
        c_status = 1;
    } else if (c_status == GLP_UNBND) {
        c_status = 2;
    } else if (c_status == GLP_FEAS) {
        c_status = 4;
    } else {
        c_status = 5;
    }

    CAMLreturn(Val_int(c_status));
}

/*
 * MPS operations
 */

/*
 * val read_mps : t -> string -> unit
 */
CAMLprim value ocaml_glp_read_mps(value model, value filename) {
    CAMLparam2(model, filename);

    glp_prob *c_model = Model_val(model);
    const char *c_filename = String_val(filename);
    glp_read_mps(c_model, GLP_MPS_FILE, NULL, c_filename);

    CAMLreturn(Val_unit);
}

/*
 * val write_mps : t -> string -> unit
 */
CAMLprim value ocaml_glp_write_mps(value model, value filename) {
    CAMLparam2(model, filename);

    glp_prob *c_model = Model_val(model);
    const char *c_filename = String_val(filename);
    glp_write_mps(c_model, GLP_MPS_FILE, NULL, c_filename);

    CAMLreturn(Val_unit);
}
