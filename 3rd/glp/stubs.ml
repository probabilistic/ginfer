(*
 * Copyright (C) 2020 Di Wang
 *)

open Lp_common

(* Types *)

type t
type row = float lprow
type column = float lpcolumn

(* Creating model *)
external create : unit -> t = "ocaml_glp_create"

(* Getters and setters of problem parameters *)
external number_rows : t -> int = "ocaml_glp_number_rows"
external number_columns : t -> int = "ocaml_glp_number_columns"
external number_elements : t -> int = "ocaml_glp_number_elements"
external direction : t -> direction = "ocaml_glp_direction"
external set_direction : t -> direction -> unit = "ocaml_glp_set_direction"
external add_rows : t -> row array -> int -> unit = "ocaml_glp_add_rows"
external delete_rows : t -> int array -> unit = "ocaml_glp_delete_rows"
external add_columns : t -> column array -> int -> unit = "ocaml_glp_add_columns"
external delete_columns : t -> int array -> unit = "ocaml_glp_delete_columns"
external objective_coefficients : t -> float array = "ocaml_glp_objective_coefficients"

external change_objective_coefficients
  :  t
  -> float array
  -> unit
  = "ocaml_glp_change_objective_coefficients"

(* Solver operations *)
external simplex : t -> int -> unit = "ocaml_glp_simplex"
external exact : t -> int -> unit = "ocaml_glp_exact"
external interior : t -> int -> unit = "ocaml_glp_interior"

(* Retrieving solutions *)
external objective_value : t -> float = "ocaml_glp_objective_value"
external primal_row_solution : t -> float array = "ocaml_glp_primal_row_solution"
external primal_column_solution : t -> float array = "ocaml_glp_primal_column_solution"
external dual_row_solution : t -> float array = "ocaml_glp_dual_row_solution"
external dual_column_solution : t -> float array = "ocaml_glp_dual_column_solution"
external status : t -> status = "ocaml_glp_status"

(* MPS operations *)
external read_mps : t -> string -> unit = "ocaml_glp_read_mps"
external write_mps : t -> string -> unit = "ocaml_glp_write_mps"
