open Lp_common

module Grb = struct
  include Stubs

  let force_sync t = update t

  let solve_with_log_level t l =
    set_log_level t l;
    solve t
  ;;
end

let () = register_lp_backend_std "grb" (module Grb)
