/*
 * Copyright (C) 2020 Di Wang
 */

#include <stdio.h>
#include <stdlib.h>
#include <gurobi_c.h>
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>

#define Model_val(v) (*((GRBmodel **) Data_custom_val(v)))

static void finalize_model(value v) {
    GRBmodel *model = Model_val(v);
    GRBfreemodel(model);
}

static struct custom_operations grb_model_ops = {
    "edu.cmu.ocaml-grb.0",
    finalize_model,
    custom_compare_default,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
};

static value alloc_model(GRBmodel *g_model) {
    value block = caml_alloc_custom(&grb_model_ops, sizeof(GRBmodel *), 0, 1);
    Model_val(block) = g_model;
    return block;
}

static GRBenv *g_env = NULL;

/*
 * val create : unit -> t
 */
CAMLprim value grb_create(value unit) {
    CAMLparam1(unit);

    if (g_env == NULL) {
        GRBemptyenv(&g_env);
        GRBsetintparam(g_env, GRB_INT_PAR_OUTPUTFLAG, 0);
        GRBstartenv(g_env);
    }
    GRBmodel *g_model;
    GRBnewmodel(g_env, &g_model, "model", 0, NULL, NULL, NULL, NULL, NULL);

    CAMLreturn(alloc_model(g_model));
}

/*
 * Getters and setters of problem parameters
 */

 /*
 * val update : t -> unit;;
 */
CAMLprim value grb_update(value model)
{
    CAMLparam1(model);

    GRBmodel *g_model = Model_val(model);
    GRBupdatemodel(g_model);

    CAMLreturn(Val_unit);
}

/*
 * val number_rows : t -> int
 */
CAMLprim value grb_number_rows(value model) {
    CAMLparam1(model);

    GRBmodel *g_model = Model_val(model);
    int g_num_rows;
    GRBgetintattr(g_model, GRB_INT_ATTR_NUMCONSTRS, &g_num_rows);

    CAMLreturn(Val_int(g_num_rows));
}

/*
 * val number_columns : t -> int
 */
CAMLprim value grb_number_columns(value model) {
    CAMLparam1(model);

    GRBmodel *g_model = Model_val(model);
    int g_num_columns;
    GRBgetintattr(g_model, GRB_INT_ATTR_NUMVARS, &g_num_columns);

    CAMLreturn(Val_int(g_num_columns));
}

/*
 * val number_elements : t -> int
 */
CAMLprim value grb_number_elements(value model) {
    CAMLparam1(model);

    GRBmodel *g_model = Model_val(model);
    int g_num_elements;
    GRBgetintattr(g_model, GRB_INT_ATTR_NUMNZS, &g_num_elements);

    CAMLreturn(Val_int(g_num_elements));
}

/*
 * val direction : t -> direction
 */
CAMLprim value grb_direction(value model) {
    CAMLparam1(model);

    GRBmodel *g_model = Model_val(model);
    int g_direction;
    GRBgetintattr(g_model, GRB_INT_ATTR_MODELSENSE, &g_direction);
    g_direction = g_direction < 0 ? 0 : 1;

    CAMLreturn(Val_int(g_direction));
}

/*
 * val set_direction : t -> direction -> unit
 */
CAMLprim value grb_set_direction(value model, value direction) {
    CAMLparam2(model, direction);

    GRBmodel *g_model = Model_val(model);
    int g_direction = Int_val(direction) == 0 ? -1 : 1;
    GRBsetintattr(g_model, GRB_INT_ATTR_MODELSENSE, g_direction);

    CAMLreturn(Val_unit);
}

/*
 * val add_rows : t -> row array -> int -> unit
 */
CAMLprim value grb_add_rows(value model, value rows, value nrows) {
    CAMLparam3(model, rows, nrows);
    CAMLlocal3(row, elements, element);

    GRBmodel *g_model = Model_val(model);
    const int g_row_count = Int_val(nrows);
    double *g_row_lower = (double *)malloc(sizeof(double) * g_row_count);
    double *g_row_upper = (double *)malloc(sizeof(double) * g_row_count);
    char *g_sense_ge = (char *)malloc(sizeof(char) * g_row_count);
    char *g_sense_le = (char *)malloc(sizeof(char) * g_row_count);
    int *g_row_starts = (int *)malloc(sizeof(int) * (g_row_count + 1));
    int *g_columns;
    double *g_elements;
    int i, j, k, g_row_size;

    g_row_starts[0] = 0;
    for (i = 0; i < g_row_count; ++i) {
        row = Field(rows, i);
        g_row_lower[i] = Double_val(Field(row, 0));
        g_row_upper[i] = Double_val(Field(row, 1));
        g_sense_ge[i] = GRB_GREATER_EQUAL;
        g_sense_le[i] = GRB_LESS_EQUAL;
        elements = Field(row, 2);
        g_row_size = Wosize_val(elements);
        g_row_starts[i + 1] = g_row_starts[i] + g_row_size;
    }

    g_columns = (int *)malloc(sizeof(int) * g_row_starts[g_row_count]);
    g_elements = (double *)malloc(sizeof(double) * g_row_starts[g_row_count]);

    k = 0;
    for (i = 0; i < g_row_count; ++i) {
        row = Field(rows, i);
        elements = Field(row, 2);
        g_row_size = Wosize_val(elements);
        for (j = 0; j < g_row_size; ++j) {
            element = Field(elements, j);
            g_columns[k] = Int_val(Field(element, 0));
            g_elements[k] = Double_val(Field(element, 1));
            ++k;
        }
    }

    // GRBaddconstrs(g_model, g_row_count, g_row_starts[g_row_count], g_row_starts, g_columns, g_elements, g_sense_ge, g_row_lower, NULL);
    // GRBaddconstrs(g_model, g_row_count, g_row_starts[g_row_count], g_row_starts, g_columns, g_elements, g_sense_le, g_row_upper, NULL);
    // XXX: The code below would possibly introduce new variables.
    GRBaddrangeconstrs(
        g_model, g_row_count,
        g_row_starts[g_row_count], g_row_starts,
        g_columns, g_elements,
        g_row_lower, g_row_upper, NULL
    );

    free(g_row_lower);
    free(g_row_upper);
    free(g_sense_ge);
    free(g_sense_le);
    free(g_row_starts);
    free(g_columns);
    free(g_elements);

    CAMLreturn(Val_unit);
}

/*
 * val delete_rows : t -> int array -> unit
 */
CAMLprim value grb_delete_rows(value model, value which) {
    CAMLparam2(model, which);

    GRBmodel *g_model = Model_val(model);
    const int g_number = Wosize_val(which);
    int *g_which = (int *)malloc(sizeof(int) * g_number);
    int i;

    for (i = 0; i < g_number; ++i) {
        g_which[i] = Int_val(Field(which, i));
    }
    GRBdelconstrs(g_model, g_number, g_which);
    free(g_which);

    CAMLreturn(Val_unit);
}

/*
 * val add_columns : t -> column array -> int -> unit
 */
CAMLprim value grb_add_columns(value model, value columns, value ncolumns) {
    CAMLparam3(model, columns, ncolumns);
    CAMLlocal3(column, elements, element);

    GRBmodel *g_model = Model_val(model);
    const int g_column_count = Int_val(ncolumns);
    double *g_column_lower = (double *)malloc(sizeof(double) * g_column_count);
    double *g_column_upper = (double *)malloc(sizeof(double) * g_column_count);
    double *g_objective = (double *)malloc(sizeof(double) * g_column_count);
    int *g_column_starts = (int *)malloc(sizeof(int) * (g_column_count + 1));
    int *g_rows;
    double *g_elements;
    int i, j, k, g_column_size;

    g_column_starts[0] = 0;
    for (i = 0; i < g_column_count; ++i) {
        column = Field(columns, i);
        g_objective[i] = Double_val(Field(column, 0));
        g_column_lower[i] = Double_val(Field(column, 1));
        g_column_upper[i] = Double_val(Field(column, 2));
        elements = Field(column, 3);
        g_column_size = Wosize_val(elements);
        g_column_starts[i + 1] = g_column_starts[i] + g_column_size;
    }

    g_rows = (int *)malloc(sizeof(int) * g_column_starts[g_column_count]);
    g_elements = (double *)malloc(sizeof(double) * g_column_starts[g_column_count]);

    k = 0;
    for (i = 0; i < g_column_count; ++i) {
        column = Field(columns, i);
        elements = Field(column, 3);
        g_column_size = Wosize_val(elements);
        for (j = 0; j < g_column_size; ++j) {
            element = Field(elements, j);
            g_rows[k] = Int_val(Field(element, 0));
            g_elements[k] = Double_val(Field(element, 1));
            ++k;
        }
    }

    GRBaddvars(
        g_model, g_column_count,
        g_column_starts[g_column_count], g_column_starts,
        g_rows, g_elements,
        g_objective, g_column_lower, g_column_upper, NULL, NULL
    );

    free(g_column_lower);
    free(g_column_upper);
    free(g_objective);
    free(g_column_starts);
    free(g_rows);
    free(g_elements);

    CAMLreturn(Val_unit);
}

/*
 * val delete_columns : t -> int array -> unit
 */
CAMLprim value grb_delete_columns(value model, value which) {
    CAMLparam2(model, which);

    GRBmodel *g_model = Model_val(model);
    const int g_number = Wosize_val(which);
    int *g_which = (int *)malloc(sizeof(int) * g_number);
    int i;

    for (i = 0; i < g_number; ++i) {
        g_which[i] = Int_val(Field(g_which, i));
    }
    GRBdelvars(g_model, g_number, g_which);
    free(g_which);

    CAMLreturn(Val_unit);
}

/*
 * val column_lower : t -> float array
 */
CAMLprim value grb_column_lower(value model) {
    CAMLparam1(model);
    CAMLlocal1(column_lower);

    GRBmodel *g_model = Model_val(model);
    int g_num_columns;
    GRBgetintattr(g_model, GRB_INT_ATTR_NUMVARS, &g_num_columns);
    double *g_column_lower = (double *)malloc(sizeof(double) * g_num_columns);
    GRBgetdblattrarray(g_model, "LB", 0, g_num_columns, g_column_lower);
    int i;

    column_lower = caml_alloc(g_num_columns, Double_array_tag);
    for (i = 0; i < g_num_columns; ++i) {
        Store_double_field(column_lower, i, g_column_lower[i]);
    }
    free(g_column_lower);

    CAMLreturn(column_lower);
}

/*
 * val change_column_lower : t -> float array -> unit
 */
CAMLprim value grb_change_column_lower(value model, value column_lower) {
    CAMLparam2(model, column_lower);

    GRBmodel *g_model = Model_val(model);
    int g_num_columns;
    GRBgetintattr(g_model, GRB_INT_ATTR_NUMVARS, &g_num_columns);
    double *g_column_lower = (double *)malloc(sizeof(double) * g_num_columns);
    int i;

    for (i = 0; i < g_num_columns; ++i) {
        g_column_lower[i] = Double_field(column_lower, i);
    }
    GRBsetdblattrarray(g_model, "LB", 0, g_num_columns, g_column_lower);
    free(g_column_lower);

    CAMLreturn(Val_unit);
}

/*
 * val column_upper : t -> float array
 */
CAMLprim value grb_column_upper(value model) {
    CAMLparam1(model);
    CAMLlocal1(column_upper);

    GRBmodel *g_model = Model_val(model);
    int g_num_columns;
    GRBgetintattr(g_model, GRB_INT_ATTR_NUMVARS, &g_num_columns);
    double *g_column_upper = (double *)malloc(sizeof(double) * g_num_columns);
    GRBgetdblattrarray(g_model, "UB", 0, g_num_columns, g_column_upper);
    int i;

    column_upper = caml_alloc(g_num_columns, Double_array_tag);
    for (i = 0; i < g_num_columns; ++i) {
        Store_double_field(column_upper, i, g_column_upper[i]);
    }
    free(g_column_upper);

    CAMLreturn(column_upper);
}

/*
 * val change_column_upper : t -> float array -> unit
 */
CAMLprim value grb_change_column_upper(value model, value column_upper) {
    CAMLparam2(model, column_upper);

    GRBmodel *g_model = Model_val(model);
    int g_num_columns;
    GRBgetintattr(g_model, GRB_INT_ATTR_NUMVARS, &g_num_columns);
    double *g_column_upper = (double *)malloc(sizeof(double) * g_num_columns);
    int i;

    for (i = 0; i < g_num_columns; ++i) {
        g_column_upper[i] = Double_field(column_upper, i);
    }
    GRBsetdblattrarray(g_model, "UB", 0, g_num_columns, g_column_upper);
    free(g_column_upper);

    CAMLreturn(Val_unit);
}

/*
 * val objective_coefficients : t -> float array
 */
CAMLprim value grb_objective_coefficients(value model) {
    CAMLparam1(model);
    CAMLlocal1(objective_coefficients);

    GRBmodel *g_model = Model_val(model);
    int g_num_columns;
    GRBgetintattr(g_model, GRB_INT_ATTR_NUMVARS, &g_num_columns);
    double *g_objective_coefficients = (double *)malloc(sizeof(double) * g_num_columns);

    GRBgetdblattrarray(g_model, GRB_DBL_ATTR_OBJ, 0, g_num_columns, g_objective_coefficients);

    objective_coefficients = caml_alloc(g_num_columns, Double_array_tag);
    for (int i = 0; i < g_num_columns; ++i) {
        Store_double_field(objective_coefficients, i, g_objective_coefficients[i]);
    }

    free(g_objective_coefficients);

    CAMLreturn(objective_coefficients);
}

/*
 * val change_objective_coefficients : t -> float array -> unit
 */
CAMLprim value grb_change_objective_coefficients(value model, value objective_coefficients) {
    CAMLparam2(model, objective_coefficients);

    GRBmodel *g_model = Model_val(model);
    int g_num_columns;
    GRBgetintattr(g_model, GRB_INT_ATTR_NUMVARS, &g_num_columns);
    double *g_objective_coefficients = (double *)malloc(sizeof(double) * g_num_columns);

    for (int i = 0; i < g_num_columns; ++i) {
        g_objective_coefficients[i] = Double_field(objective_coefficients, i);
    }
    GRBsetdblattrarray(g_model, GRB_DBL_ATTR_OBJ, 0, g_num_columns, g_objective_coefficients);

    free(g_objective_coefficients);

    CAMLreturn(Val_unit);
}

/*
 * Getters and setters of solver parameters
 */

/*
 * val log_level : t -> int
 */
CAMLprim value grb_log_level(value model) {
    CAMLparam1(model);

    GRBmodel *g_model = Model_val(model);
    GRBenv *g_env = GRBgetenv(g_model);
    int g_log_level;
    GRBgetintparam(g_env, GRB_INT_PAR_OUTPUTFLAG, &g_log_level);

    CAMLreturn(Val_int(g_log_level));
}

/*
 * val set_log_level : t -> int -> unit
 */
CAMLprim value grb_set_log_level(value model, value log_level) {
    CAMLparam1(model);

    GRBmodel *g_model = Model_val(model);
    GRBenv *g_env = GRBgetenv(g_model);
    int g_log_level = Int_val(log_level);
    g_log_level = g_log_level >= 1 ? 1 : 0;
    GRBsetintparam(g_env, GRB_INT_PAR_OUTPUTFLAG, g_log_level);

    CAMLreturn(Val_unit);
}

/*
 * Solver oprations
 */

/*
 * val solve : t -> unit;;
 */
CAMLprim value grb_solve(value model) {
    CAMLparam1(model);

    GRBmodel *g_model = Model_val(model);
    GRBoptimize(g_model);

    CAMLreturn(Val_unit);
}

/*
 * Retrieving solutions
 */

/*
 * val objective_value : t -> float
 */
CAMLprim value grb_objective_value(value model) {
    CAMLparam1(model);

    GRBmodel *g_model = Model_val(model);
    double g_objval;
    GRBgetdblattr(g_model, GRB_DBL_ATTR_OBJVAL, &g_objval);

    CAMLreturn(caml_copy_double(g_objval));
}

/*
 * val primal_column_solution : t -> float array
 */
CAMLprim value grb_primal_column_solution(value model) {
    CAMLparam1(model);
    CAMLlocal1(solution);

    GRBmodel *g_model = Model_val(model);
    int g_num_columns;
    GRBgetintattr(g_model, GRB_INT_ATTR_NUMVARS, &g_num_columns);

    double *g_solution = (double *)malloc(sizeof(double) * g_num_columns);
    GRBgetdblattrarray(g_model, GRB_DBL_ATTR_X, 0, g_num_columns, g_solution);
    int i;

    solution = caml_alloc(g_num_columns, Double_array_tag);
    for (i = 0; i < g_num_columns; ++i) {
        Store_double_field(solution, i, g_solution[i]);
    }

    free(g_solution);

    CAMLreturn(solution);
}

/*
 * val status : t -> status
 */
CAMLprim value grb_status(value model) {
    CAMLparam1(model);

    GRBmodel *g_model = Model_val(model);
    int g_status;
    GRBgetintattr(g_model, GRB_INT_ATTR_STATUS, &g_status);
    if (g_status == GRB_OPTIMAL) {
        g_status = 0;
    } else if (g_status == GRB_INFEASIBLE) {
        g_status = 1;
    } else if (g_status == GRB_UNBOUNDED) {
        g_status = 2;
    } else if (g_status == GRB_INF_OR_UNBD) {
        g_status = 3;
    } else if (g_status == GRB_SUBOPTIMAL) {
        g_status = 4;
    } else {
        g_status = 5;
    }

    CAMLreturn(Val_int(g_status));
}

/*
 * MPS operations
 */

/*
 * val read_mps : t -> string -> unit
 */
CAMLprim value grb_read_mps(value model, value filename) {
    CAMLparam2(model, filename);

    GRBmodel *g_model = Model_val(model);
    const char *g_filename = String_val(filename);
    GRBread(g_model, g_filename);

    CAMLreturn(Val_unit);
}

/*
 * val write_mps : t -> string -> unit
 */
CAMLprim value grb_write_mps(value model, value filename) {
    CAMLparam2(model, filename);

    GRBmodel *g_model = Model_val(model);
    const char *g_filename = String_val(filename);
    GRBwrite(g_model, g_filename);

    CAMLreturn(Val_unit);
}
