(*
 * Copyright (C) 2020 Di Wang
 *)

open Lp_common

(* Types *)

type t
type row = float lprow
type column = float lpcolumn

(* Creating model *)
external create : unit -> t = "grb_create"

(* Getters and setters of problem parameters *)
external update : t -> unit = "grb_update"
external number_rows : t -> int = "grb_number_rows"
external number_columns : t -> int = "grb_number_columns"
external number_elements : t -> int = "grb_number_elements"
external direction : t -> direction = "grb_direction"
external set_direction : t -> direction -> unit = "grb_set_direction"
external add_rows : t -> row array -> int -> unit = "grb_add_rows"
external delete_rows : t -> int array -> unit = "grb_delete_rows"
external add_columns : t -> column array -> int -> unit = "grb_add_columns"
external delete_columns : t -> int array -> unit = "grb_delete_columns"
external column_lower : t -> float array = "grb_column_lower"
external change_column_lower : t -> float array -> unit = "grb_change_column_lower"
external column_upper : t -> float array = "grb_column_upper"
external change_column_upper : t -> float array -> unit = "grb_change_column_upper"
external objective_coefficients : t -> float array = "grb_objective_coefficients"

external change_objective_coefficients
  :  t
  -> float array
  -> unit
  = "grb_change_objective_coefficients"

(* Getters and setters of solver parameters*)
external log_level : t -> int = "grb_log_level"
external set_log_level : t -> int -> unit = "grb_set_log_level"

(* Solver operations *)
external solve : t -> unit = "grb_solve"

(* Retrieving solutions *)
external objective_value : t -> float = "grb_objective_value"
external primal_column_solution : t -> float array = "grb_primal_column_solution"
external status : t -> status = "grb_status"

(* MPS operations *)
external read_mps : t -> string -> unit = "grb_read_mps"
external write_mps : t -> string -> unit = "grb_write_mps"
