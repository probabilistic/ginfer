type direction =
  | Maximize
  | Minimize

type status =
  | Optimal
  | Infeasible
  | Unbounded
  | Inf_or_unbd
  | Suboptimal
  | Other

type 'a lprow =
  { row_lower : 'a
  ; row_upper : 'a
  ; row_elements : (int * 'a) array
  }

type 'a lpcolumn =
  { column_obj : 'a
  ; column_lower : 'a
  ; column_upper : 'a
  ; column_elements : (int * 'a) array
  }

module type SOLVER = sig
  type t
  type flt
  type row = flt lprow
  type column = flt lpcolumn

  val create : unit -> t
  val number_rows : t -> int
  val number_columns : t -> int
  val number_elements : t -> int
  val add_rows : t -> row array -> int -> unit
  val add_columns : t -> column array -> int -> unit
  val objective_coefficients : t -> flt array
  val change_objective_coefficients : t -> flt array -> unit
  val set_direction : t -> direction -> unit
  val status : t -> status
  val primal_column_solution : t -> flt array
  val force_sync : t -> unit
  val solve_with_log_level : t -> int -> unit
  val read_mps : t -> string -> unit
  val write_mps : t -> string -> unit
end

module type SOLVER_WITH_STD_FLOAT = SOLVER with type flt := float
module type SOLVER_WITH_MPQ_FLOAT = SOLVER with type flt := Mpqf.t

let lp_backends_std = Hashtbl.create 2
let lp_backends_mpq = Hashtbl.create 2

let register_lp_backend_std (name : string) (backend : (module SOLVER_WITH_STD_FLOAT)) =
  Hashtbl.add lp_backends_std name backend
;;

let register_lp_backend_mpq (name : string) (backend : (module SOLVER_WITH_MPQ_FLOAT)) =
  Hashtbl.add lp_backends_mpq name backend
;;

let get_lp_backend_std name = Hashtbl.find_opt lp_backends_std name
let get_lp_backend_mpq name = Hashtbl.find_opt lp_backends_mpq name

let iter_lp_backends_std ~f =
  Hashtbl.iter (fun name backend -> f ~name ~backend) lp_backends_std
;;

let iter_lp_backends_mpq ~f =
  Hashtbl.iter (fun name backend -> f ~name ~backend) lp_backends_mpq
;;
