open Lp_common

module Msk = struct
  include Stubs

  let force_sync _ = ()

  let solve_with_log_level t l =
    set_log_level t l;
    solve t
  ;;
end

let () = register_lp_backend_std "msk" (module Msk)
