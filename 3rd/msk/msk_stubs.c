/*
 * Copyright (C) 2020 Di Wang
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <mosek.h>
#include <caml/alloc.h>
#include <caml/custom.h>
#include <caml/memory.h>
#include <caml/mlvalues.h>

#define Model_val(v) (*((MSKtask_t *) Data_custom_val(v)))

static void finalize_model(value v) {
    MSKtask_t model = Model_val(v);
    MSK_deletetask(&model);
}

static struct custom_operations msk_model_ops = {
    "edu.cmu.ocaml-msk.0",
    finalize_model,
    custom_compare_default,
    custom_hash_default,
    custom_serialize_default,
    custom_deserialize_default
};

static value alloc_model(MSKtask_t g_model) {
    value block = caml_alloc_custom(&msk_model_ops, sizeof(MSKtask_t), 0, 1);
    Model_val(block) = g_model;
    return block;
}

/*
 * val create : unit -> t
 */
CAMLprim value msk_create(value unit) {
    CAMLparam1(unit);

    MSKenv_t g_env;
    MSK_makeenv(&g_env, NULL);
    MSKtask_t g_model;
    MSK_maketask(g_env, 0, 0, &g_model);

    CAMLreturn(alloc_model(g_model));
}

/*
 * Getters and setters of problem parameters
 */

/*
 * val number_rows : t -> int
 */
CAMLprim value msk_number_rows(value model) {
    CAMLparam1(model);

    MSKtask_t g_model = Model_val(model);
    int g_num_rows;
    MSK_getnumcon(g_model, &g_num_rows);

    CAMLreturn(Val_int(g_num_rows));
}

/*
 * val number_columns : t -> int
 */
CAMLprim value msk_number_columns(value model) {
    CAMLparam1(model);

    MSKtask_t g_model = Model_val(model);
    int g_num_columns;
    MSK_getnumvar(g_model, &g_num_columns);

    CAMLreturn(Val_int(g_num_columns));
}

/*
 * val number_elements : t -> int
 */
CAMLprim value msk_number_elements(value model) {
    CAMLparam1(model);

    MSKtask_t g_model = Model_val(model);
    int g_num_elements;
    MSK_getnumanz(g_model, &g_num_elements);

    CAMLreturn(Val_int(g_num_elements));
}

/*
 * val direction : t -> direction
 */
CAMLprim value msk_direction(value model) {
    CAMLparam1(model);

    MSKtask_t g_model = Model_val(model);
    MSKobjsensee g_direction;
    MSK_getobjsense(g_model, &g_direction);
    g_direction = g_direction == MSK_OBJECTIVE_SENSE_MAXIMIZE ? 0 : 1;

    CAMLreturn(Val_int(g_direction));
}

/*
 * val set_direction : t -> direction -> unit
 */
CAMLprim value msk_set_direction(value model, value direction) {
    CAMLparam2(model, direction);

    MSKtask_t g_model = Model_val(model);
    MSKobjsensee g_direction = Int_val(direction) == 0 ? MSK_OBJECTIVE_SENSE_MAXIMIZE : MSK_OBJECTIVE_SENSE_MINIMIZE;
    MSK_putobjsense(g_model, g_direction);

    CAMLreturn(Val_unit);
}

/*
 * val add_rows : t -> row array -> int -> unit
 */
CAMLprim value msk_add_rows(value model, value rows, value nrows) {
    CAMLparam3(model, rows, nrows);
    CAMLlocal3(row, elements, element);

    MSKtask_t g_model = Model_val(model);
    int c_row_start;
    MSK_getnumcon(g_model, &c_row_start);

    const int c_row_count = Int_val(nrows);
    MSK_appendcons(g_model, c_row_count);

    int *c_ptrb = (int *)malloc(sizeof(int) * c_row_count);
    int *c_ptre = (int *)malloc(sizeof(int) * c_row_count);
    MSKboundkeye *c_bkc = (MSKboundkeye *)malloc(sizeof(MSKboundkeye) * c_row_count);
    double *c_blc = (double *)malloc(sizeof(double) * c_row_count);
    double *c_buc = (double *)malloc(sizeof(double) * c_row_count);

    int c_last = 0;
    double c_lb, c_ub;
    for (int i = 0; i < c_row_count; ++i) {
        row = Field(rows, i);
        elements = Field(row, 2);
        int c_row_size = Wosize_val(elements);
        c_ptrb[i] = c_last;
        c_ptre[i] = c_last + c_row_size;
        c_last += c_row_size;
        c_lb = Double_val(Field(row, 0));
        c_ub = Double_val(Field(row, 1));
        c_blc[i] = c_lb;
        c_buc[i] = c_ub;
        if (isinf(c_lb) && isinf(c_ub)) {
            c_bkc[i] = MSK_BK_FR;
        } else if (isinf(c_lb)) {
            c_bkc[i] = MSK_BK_UP;
        } else if (isinf(c_ub)) {
            c_bkc[i] = MSK_BK_LO;
        } else if (c_blc[i] == c_buc[i]) {
            c_bkc[i] = MSK_BK_FX;
        } else {
            c_bkc[i] = MSK_BK_RA;
        }
    }

    int *c_asub = (int *)malloc(sizeof(int) * c_last);
    double *c_aval = (double *)malloc(sizeof(double) * c_last);

    int k = 0;
    for (int i = 0; i < c_row_count; ++i) {
        row = Field(rows, i);
        elements = Field(row, 2);
        int c_row_size = Wosize_val(elements);
        for (int j = 0; j < c_row_size; ++j) {
            element = Field(elements, j);
            c_asub[k] = Int_val(Field(element, 0));
            c_aval[k] = Double_val(Field(element, 1));
            ++k;
        }
    }

    MSK_putarowslice(g_model, c_row_start, c_row_start + c_row_count, c_ptrb, c_ptre, c_asub, c_aval);
    MSK_putconboundslice(g_model, c_row_start, c_row_start + c_row_count, c_bkc, c_blc, c_buc);

    free(c_ptrb);
    free(c_ptre);
    free(c_bkc);
    free(c_blc);
    free(c_buc);
    free(c_asub);
    free(c_aval);

    CAMLreturn(Val_unit);
}

/*
 * val add_columns : t -> column array -> int -> unit
 */
CAMLprim value msk_add_columns(value model, value columns, value ncolumns) {
    CAMLparam3(model, columns, ncolumns);
    CAMLlocal3(column, elements, element);

    MSKtask_t g_model = Model_val(model);
    int c_column_start;
    MSK_getnumvar(g_model, &c_column_start);

    const int c_column_count = Int_val(ncolumns);
    MSK_appendvars(g_model, c_column_count);

    int *c_ptrb = (int *)malloc(sizeof(int) * c_column_count);
    int *c_ptre = (int *)malloc(sizeof(int) * c_column_count);
    MSKboundkeye *c_bkc = (MSKboundkeye *)malloc(sizeof(MSKboundkeye) * c_column_count);
    double *c_blc = (double *)malloc(sizeof(double) * c_column_count);
    double *c_buc = (double *)malloc(sizeof(double) * c_column_count);
    double *c_obj = (double *)malloc(sizeof(double) * c_column_count);

    int c_last = 0;
    double c_lb, c_ub;
    for (int i = 0; i < c_column_count; ++i) {
        column = Field(columns, i);
        elements = Field(column, 3);
        int c_column_size = Wosize_val(elements);
        c_ptrb[i] = c_last;
        c_ptre[i] = c_last + c_column_size;
        c_last += c_column_size;
        c_lb = Double_val(Field(column, 1));
        c_ub = Double_val(Field(column, 2));
        c_obj[i] = Double_val(Field(column, 0));
        c_blc[i] = c_lb;
        c_buc[i] = c_ub;
        if (isinf(c_lb) && isinf(c_ub)) {
            c_bkc[i] = MSK_BK_FR;
        } else if (isinf(c_lb)) {
            c_bkc[i] = MSK_BK_UP;
        } else if (isinf(c_ub)) {
            c_bkc[i] = MSK_BK_LO;
        } else if (c_blc[i] == c_buc[i]) {
            c_bkc[i] = MSK_BK_FX;
        } else {
            c_bkc[i] = MSK_BK_RA;
        }
    }

    int *c_asub = (int *)malloc(sizeof(int) * c_last);
    double *c_aval = (double *)malloc(sizeof(double) * c_last);

    int k = 0;
    for (int i = 0; i < c_column_count; ++i) {
        column = Field(columns, i);
        elements = Field(column, 3);
        int c_column_size = Wosize_val(elements);
        for (int j = 0; j < c_column_size; ++j) {
            element = Field(elements, j);
            c_asub[k] = Int_val(Field(element, 0));
            c_aval[k] = Double_val(Field(element, 1));
            ++k;
        }
    }

    MSK_putacolslice(g_model, c_column_start, c_column_start + c_column_count, c_ptrb, c_ptre, c_asub, c_aval);
    MSK_putvarboundslice(g_model, c_column_start, c_column_start + c_column_count, c_bkc, c_blc, c_buc);
    MSK_putcslice(g_model, c_column_start, c_column_start + c_column_count, c_obj);

    free(c_ptrb);
    free(c_ptre);
    free(c_bkc);
    free(c_obj);
    free(c_blc);
    free(c_buc);
    free(c_asub);
    free(c_aval);

    CAMLreturn(Val_unit);
}

/*
 * val objective_coefficients : t -> float array
 */
CAMLprim value msk_objective_coefficients(value model) {
    CAMLparam1(model);
    CAMLlocal1(objective_coefficients);

    MSKtask_t g_model = Model_val(model);
    int g_num_columns;
    MSK_getnumvar(g_model, &g_num_columns);
    double *g_objective_coefficients = (double *)malloc(sizeof(double) * g_num_columns);

    MSK_getc(g_model, g_objective_coefficients);

    objective_coefficients = caml_alloc(g_num_columns, Double_array_tag);
    for (int i = 0; i < g_num_columns; ++i) {
        Store_double_field(objective_coefficients, i, g_objective_coefficients[i]);
    }

    free(g_objective_coefficients);

    CAMLreturn(objective_coefficients);
}

/*
 * val change_objective_coefficients : t -> float array -> unit
 */
CAMLprim value msk_change_objective_coefficients(value model, value objective_coefficients) {
    CAMLparam2(model, objective_coefficients);

    MSKtask_t g_model = Model_val(model);
    int g_num_columns;
    MSK_getnumvar(g_model, &g_num_columns);
    double *g_objective_coefficients = (double *)malloc(sizeof(double) * g_num_columns);

    for (int i = 0; i < g_num_columns; ++i) {
        g_objective_coefficients[i] = Double_field(objective_coefficients, i);
    }
    MSK_putcslice(g_model, 0, g_num_columns, g_objective_coefficients);

    free(g_objective_coefficients);

    CAMLreturn(Val_unit);
}

/*
 * Getters and setters of solver parameters
 */

/*
 * val log_level : t -> int
 */
CAMLprim value msk_log_level(value model) {
    CAMLparam1(model);

    MSKtask_t g_model = Model_val(model);
    int g_log_level;
    MSK_getintparam(g_model, MSK_IPAR_LOG, &g_log_level);

    CAMLreturn(Val_int(g_log_level));
}

/*
 * val set_log_level : t -> int -> unit
 */
CAMLprim value msk_set_log_level(value model, value log_level) {
    CAMLparam1(model);

    MSKtask_t g_model = Model_val(model);
    int g_log_level = Int_val(log_level);
    MSK_putintparam(g_model, MSK_IPAR_LOG, g_log_level);

    CAMLreturn(Val_unit);
}

/*
 * Solver oprations
 */

/*
 * val solve : t -> unit;;
 */
CAMLprim value msk_solve(value model) {
    CAMLparam1(model);

    MSKtask_t g_model = Model_val(model);
    MSK_optimize(g_model);

    CAMLreturn(Val_unit);
}

/*
 * Retrieving solutions
 */

/*
 * val objective_value : t -> float
 */
CAMLprim value msk_objective_value(value model) {
    CAMLparam1(model);

    MSKtask_t g_model = Model_val(model);
    double g_objval;
    MSK_getprimalobj(g_model, MSK_SOL_BAS, &g_objval);

    CAMLreturn(caml_copy_double(g_objval));
}

/*
 * val primal_column_solution : t -> float array
 */
CAMLprim value msk_primal_column_solution(value model) {
    CAMLparam1(model);
    CAMLlocal1(solution);

    MSKtask_t g_model = Model_val(model);
    int g_num_columns;
    MSK_getnumvar(g_model, &g_num_columns);

    double *g_solution = (double *)malloc(sizeof(double) * g_num_columns);
    MSK_getxx(g_model, MSK_SOL_BAS, g_solution);

    solution = caml_alloc(g_num_columns, Double_array_tag);
    for (int i = 0; i < g_num_columns; ++i) {
        Store_double_field(solution, i, g_solution[i]);
    }

    free(g_solution);

    CAMLreturn(solution);
}

/*
 * val status : t -> status
 */
CAMLprim value msk_status(value model) {
    CAMLparam1(model);

    MSKtask_t g_model = Model_val(model);
    MSKsolstae g_status;
    MSK_getsolsta(g_model, MSK_SOL_BAS, &g_status);
    if (g_status == MSK_SOL_STA_OPTIMAL) {
        g_status = 0;
    } else if (g_status == MSK_SOL_STA_PRIM_INFEAS_CER) {
        g_status = 1;
    } else if (g_status == MSK_SOL_STA_DUAL_INFEAS_CER) {
        g_status = 3;
    } else if (g_status == MSK_SOL_STA_PRIM_AND_DUAL_FEAS || g_status == MSK_SOL_STA_INTEGER_OPTIMAL) {
        g_status = 4;
    } else {
        g_status = 5;
    }

    CAMLreturn(Val_int(g_status));
}

/*
 * MPS operations
 */

/*
 * val read_mps : t -> string -> unit
 */
CAMLprim value msk_read_mps(value model, value filename) {
    CAMLparam2(model, filename);

    MSKtask_t g_model = Model_val(model);
    const char *g_filename = String_val(filename);
    MSK_readdata(g_model, g_filename);

    CAMLreturn(Val_unit);
}

/*
 * val write_mps : t -> string -> unit
 */
CAMLprim value msk_write_mps(value model, value filename) {
    CAMLparam2(model, filename);

    MSKtask_t g_model = Model_val(model);
    const char *g_filename = String_val(filename);
    MSK_writedata(g_model, g_filename);

    CAMLreturn(Val_unit);
}
