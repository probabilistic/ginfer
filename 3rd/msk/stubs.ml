(*
 * Copyright (C) 2020 Di Wang
 *)

open Lp_common

(* Types *)

type t
type row = float lprow
type column = float lpcolumn

(* Creating model *)
external create : unit -> t = "msk_create"

(* Getters and setters of problem parameters *)
external number_rows : t -> int = "msk_number_rows"
external number_columns : t -> int = "msk_number_columns"
external number_elements : t -> int = "msk_number_elements"
external direction : t -> direction = "msk_direction"
external set_direction : t -> direction -> unit = "msk_set_direction"
external add_rows : t -> row array -> int -> unit = "msk_add_rows"
external add_columns : t -> column array -> int -> unit = "msk_add_columns"
external objective_coefficients : t -> float array = "msk_objective_coefficients"

external change_objective_coefficients
  :  t
  -> float array
  -> unit
  = "msk_change_objective_coefficients"

(* Getters and setters of solver parameters*)
external log_level : t -> int = "msk_log_level"
external set_log_level : t -> int -> unit = "msk_set_log_level"

(* Solver operations *)
external solve : t -> unit = "msk_solve"

(* Retrieving solutions *)
external objective_value : t -> float = "msk_objective_value"
external primal_column_solution : t -> float array = "msk_primal_column_solution"
external status : t -> status = "msk_status"

(* MPS operations *)
external read_mps : t -> string -> unit = "msk_read_mps"
external write_mps : t -> string -> unit = "msk_write_mps"
