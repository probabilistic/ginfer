include config.mk

DUNE := LIBCLP="$(LIBCLP)" LIBGLP="$(LIBGLP)" LIBGRB="$(LIBGRB)" LIBMSK="$(LIBMSK)" LIBSPX="$(LIBSPX)" dune
INSTALL_ARGS := $(if $(PREFIX),--prefix $(PREFIX),)

default:
	$(DUNE) build @install

install:
	$(DUNE) install $(INSTALL_ARGS)

uninstall:
	$(DUNE) uninstall $(INSTALL_ARGS)

reinstall: uninstall install

clean:
	$(DUNE) clean

distclean: clean
	rm -f config.mk util/dune

.PHONY: default install uninstall reinstall clean distclean
