ginfer is a static analyzer for higher (raw or central) moments of cost
accumulators (e.g., running time) in probabilistic programs.
ginfer handles imperative arithmetic programs in a C-like syntax.

## INSTALLATION

1) Installing [opam](https://opam.ocaml.org/doc/Install.html):
``` bash
$ sh <(curl -sL https://raw.githubusercontent.com/ocaml/opam/master/shell/install.sh)
$ opam init -c 4.10.2
$ eval $(opam env)
```

2) Installing the required dependencies:

- Ubuntu:
``` bash
$ sudo apt-get install libgmp-dev libmpfr-dev libffi-dev libgsl-dev libglpk-dev ppl-dev
```
- macOS:
``` bash
$ brew install gmp mpfr libffi gsl glpk ppl
```

3) Installing the required packages:
``` bash
$ opam install dune dune-build-info core menhir ocamlgraph ctypes ctypes-foreign conf-ppl apron z3 conf-gsl
```

4) Obtaining [Gurobi](https://www.gurobi.com/) 9.5:

ginfer uses Gurobi as a solver backend to discharge linear constraints generated
from static analysis for moment inference. You can download it from its official
website; however, you need to get a license to use it. If you are a student,
professor, or researcher, you may be able to obtain a free license [here](https://www.gurobi.com/academia/academic-program-and-licenses/).

Note that currently, ginfer only works with Gurobi 9.5.

5) Compiling ginfer:

First run the configure script with the *absolute* path to GLPK and Gurobi:
``` bash
$ ./configure --with-glpk /path/to/glpk --with-gurobi /path/to/gurobi
```
Both `/path/to/glpk` and `path/to/gurobi` should contain `lib/` and `include/`
folder holding the library and header files, respectively. Below is an example
on macOS:
``` bash
$ ./configure --with-glpk /usr/local --with-gurobi /Library/gurobi912/mac64
```
After configuration, simply run `make` to compile the main binary. You may then
also run `make install` to install ginfer as an opam package.

## INPUT FILES

ginfer inputs are in a C-like syntax. There are three types: `void`, `int`, and
`real`. A program consists of a sequence of (possibly mutually recursive)
functions. The *last* function is assumed to be the main function. In a function
body, all local variables must be declared at the beginning, and initialization
is not allowed (local variables are initialized to zeros). Bodies of all
control-flow constructs (e.g., `if` and `while`) must be enclosed by brackets.
Arguments of function calls can only be variables. The `return` statement also
only accepts variables.

For probabilistic behavior and cost analysis, ginfer introduces several new
syntactic constructs:

- `if (prob(a, b)) { ... } else { ... }`

    Make a random branch selection; choose the then-branch with probability
    `a/(a+b)`, and the else-branch with probability `b/(a+b)`. Both `a` and `b`
    are positive literal integers.

- `x = UnifR(0, 1);`

    Sample a random variable from the specified distribution. The example above
    samples a real value from the uniform distribution on the unit interval and
    assigns the value to `x`. Currently, the arguments of distributions can only
    be literals. Other supported distributions are
    `Ber(a, b)` for Bernoulli,
    `Bin(a, b, n)` for Binomial,
    `Geo(a, b)` for Geometric,
    `Nbin(a, b, n)` for Negative Binomial,
    `Pois(a, b)` for Poisson,
    `Hyper(n, r, m)` for Hyper Geometric,
    and `Unif(l, r)` for Discrete Uniform.

- `tick(e);`

    Evaluate the value of `e` and accumulate it to the cost accumulator. This
    command allows users to specify different cost metrics, such as running
    time, memory allocations, or cash flow.

- `assume(phi);`

    Assume a Boolean-valued expression `phi` is true at a program point. This
    command has no operational effect, but ginfer can use it as a hint to
    improve analysis precision.

Examples can be found in the folder `bench`.

## COMMAND LINE USAGE

For a description of the command-line usage options, run `ginfer help`.

ginfer has five modes for cost analysis:

- `run-bi`: Raw-moment bound inference with general costs
- `run-bi-mon`: Raw-moment bound inference with non-negative costs
- `run-vi`: Central-moment bound inference with general costs
- `run-vi-mon`: Central-moment bound inference with non-negative costs
- `run-ti`: Raw-moment upper-bound inference for termination time

For each mode, you can run `ginfer help run-*` for a description of options for
that mode.

Below are several common command-line options for cost analysis:

- `-mom m`: Specify the target moment (integer-valued)
- `-deg d`: Specify the maximal degree for the polynomial templates that express
moment bounds
- `[-init phi]`: Specify an initial condition on the parameters for the main
function (string-valued)
- `[-lower]`: Switch to lower-bound inference (for most of the time, ginfer
performs upper-bound analysis)
- `[-solver (glp|grb)]`: Specify the solver backend for linear constraints (by
default, ginfer uses GLPK (`glp`); Gurobi (`grb`) is preferred if it is
available)

Example usage:
``` bash
$ ginfer run-vi-mon bench/tacas19/rdwalk1d.c -mom 3 -deg 3 -init "x > 1" -solver grb
$ ginfer run-bi bench/pldi19/bcmining.c -m 1 -d 1 -i "x >= 1" -s glp -lower
```
