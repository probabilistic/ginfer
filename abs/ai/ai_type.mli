open Action_types

(** One-vocabulary abstract domain *)
module type DOMAIN0 = sig
  (** Abstract domain element *)
  type t [@@deriving equal]

  (** Hint for widening *)
  type hint

  val bot : (string * Typed.rhs_ty) list -> t
  val top : (string * Typed.rhs_ty) list -> t
  val join : t -> t -> t
  val meet : t -> t -> t
  val extend : t -> Typed.data_action -> [ `Forward | `Backward ] -> t
  val branch : t -> Typed.bool_expr -> [ `Then | `Else ] -> t

  (** [call abs args params callee_scope dir] transforms [abs] from caller to callee. *)
  val call
    :  t
    -> string list
    -> string list
    -> (string * Typed.rhs_ty) list
    -> [ `Forward | `Backward ]
    -> t

  (** [return abs1 args params callee_scope abs2 returns xs caller_scope dir] transforms [abs1] from callee back to
      caller, and combines information from [abs2] about local variables of caller. *)
  val return
    :  t
    -> string list
    -> string list
    -> string list
    -> t
    -> string list
    -> string list
    -> string list
    -> [ `Forward | `Backward ]
    -> t

  val alpha : (string * Typed.rhs_ty) list -> Typed.bool_expr -> t
  val forget : t -> string list -> t
  val decl_vars : t -> (string * Typed.rhs_ty) list -> t
  val dead_vars : t -> string list -> t
  val widen : ?hint:hint -> t -> t -> t
  val print : Format.formatter -> t -> unit
end

(** One-vocabulary data-flow analysis *)
module type CFG_ANALYSIS0 = sig
  (** Calling context abstraction *)
  type context

  (** Abstract domain *)
  type dom

  (** Widening hint *)
  type hint

  val analyze
    :  [ `Forward | `Backward ]
    -> ?init:Typed.bool_expr
    -> ?widen_threshold:int
    -> ?widen_hints:(context * Cfg.vertex -> hint)
    -> Cfg.program
    -> Cfg.vertex
    -> (context * dom) list
end
