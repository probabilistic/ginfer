open Core
open Apron
open Action_types

let classify_ty = function
  | Environment.INT -> Texpr1.Int
  | Environment.REAL -> Texpr1.Real
;;

let join_ty = function
  | Texpr1.Int, Texpr1.Int -> Texpr1.Int
  | _ -> Texpr1.Real
;;

let texpr_of_expr env e =
  let rec inner e =
    match Typed.destruct_arith e with
    | `Var (x, _) ->
      ( Texpr1.var env (Var.of_string x.txt)
      , classify_ty (Environment.typ_of_var env (Var.of_string x.txt)) )
    | `Int n -> Texpr1.cst env (Coeff.s_of_int n), Texpr1.Int
    | `Real r -> Texpr1.cst env (Coeff.s_of_float r), Texpr1.Real
    | `Frac f -> Texpr1.cst env (Coeff.s_of_mpqf f), Texpr1.Real
    | `BinOp (bop, e1, e2) ->
      let t_e1, ty1 = inner e1 in
      let t_e2, ty2 = inner e2 in
      let ty = join_ty (ty1, ty2) in
      (match bop with
      | Add -> Texpr1.binop Texpr1.Add t_e1 t_e2 ty Texpr1.Near, ty
      | Sub -> Texpr1.binop Texpr1.Sub t_e1 t_e2 ty Texpr1.Near, ty
      | Mul -> Texpr1.binop Texpr1.Mul t_e1 t_e2 ty Texpr1.Near, ty)
    | `Negate e0 ->
      let t_e0, ty0 = inner e0 in
      Texpr1.unop Texpr1.Neg t_e0 ty0 Texpr1.Near, ty0
  in
  let t_e, _ = inner e in
  t_e
;;

let tcons_of_relation env e1 op e2 =
  let sub12 () = texpr_of_expr env (Typed.mk_bin_op ~loc:Location.none (Sub, e1, e2)) in
  let sub21 () = texpr_of_expr env (Typed.mk_bin_op ~loc:Location.none (Sub, e2, e1)) in
  match op with
  | GE -> Tcons1.make (sub12 ()) Tcons1.SUPEQ
  | GT -> Tcons1.make (sub12 ()) Tcons1.SUP
  | LE -> Tcons1.make (sub21 ()) Tcons1.SUPEQ
  | LT -> Tcons1.make (sub21 ()) Tcons1.SUP
  | EQ -> Tcons1.make (sub12 ()) Tcons1.EQ
  | NE -> Tcons1.make (sub12 ()) Tcons1.DISEQ
;;

let abs_of_logic man env =
  let rec inner1 l =
    match Typed.destruct_bool l with
    | `Bool true -> Abstract1.top man env
    | `Bool false -> Abstract1.bottom man env
    | `And (l1, l2) ->
      let abs1 = inner1 l1 in
      let abs2 = inner1 l2 in
      Abstract1.meet man abs1 abs2
    | `Or (l1, l2) ->
      let abs1 = inner1 l1 in
      let abs2 = inner1 l2 in
      Abstract1.join man abs1 abs2
    | `RelOp (rop, e1, e2) ->
      let sub12 () =
        texpr_of_expr env (Typed.mk_bin_op ~loc:Location.none (Sub, e1, e2))
      in
      let sub21 () =
        texpr_of_expr env (Typed.mk_bin_op ~loc:Location.none (Sub, e2, e1))
      in
      let cons =
        match rop with
        | GE -> Tcons1.make (sub12 ()) Tcons1.SUPEQ
        | GT -> Tcons1.make (sub12 ()) Tcons1.SUP
        | LE -> Tcons1.make (sub21 ()) Tcons1.SUPEQ
        | LT -> Tcons1.make (sub21 ()) Tcons1.SUP
        | EQ -> Tcons1.make (sub12 ()) Tcons1.EQ
        | NE -> Tcons1.make (sub12 ()) Tcons1.DISEQ
      in
      let arr = Tcons1.array_make env 1 in
      Tcons1.array_set arr 0 cons;
      Abstract1.of_tcons_array man env arr
    | `Not l0 -> inner2 l0
    | `Nondet -> Abstract1.top man env
    | `Var (x, _) ->
      let te =
        texpr_of_expr
          env
          (Typed.mk_bin_op
             ~loc:Location.none
             ( Sub
             , Typed.mk_arith_var ~loc:Location.none x `TyInt
             , Typed.mk_int ~loc:Location.none 1 ))
      in
      let cons = Tcons1.make te Tcons1.EQ in
      let arr = Tcons1.array_make env 1 in
      Tcons1.array_set arr 0 cons;
      Abstract1.of_tcons_array man env arr
  and inner2 l =
    match Typed.destruct_bool l with
    | `Bool true -> Abstract1.bottom man env
    | `Bool false -> Abstract1.top man env
    | `And (l1, l2) ->
      let abs1 = inner2 l1 in
      let abs2 = inner2 l2 in
      Abstract1.join man abs1 abs2
    | `Or (l1, l2) ->
      let abs1 = inner2 l1 in
      let abs2 = inner2 l2 in
      Abstract1.meet man abs1 abs2
    | `RelOp (rop, e1, e2) ->
      let sub12 () =
        texpr_of_expr env (Typed.mk_bin_op ~loc:Location.none (Sub, e1, e2))
      in
      let sub21 () =
        texpr_of_expr env (Typed.mk_bin_op ~loc:Location.none (Sub, e2, e1))
      in
      let cons =
        match rop with
        | GE -> Tcons1.make (sub21 ()) Tcons1.SUP
        | GT -> Tcons1.make (sub21 ()) Tcons1.SUPEQ
        | LE -> Tcons1.make (sub12 ()) Tcons1.SUP
        | LT -> Tcons1.make (sub12 ()) Tcons1.SUPEQ
        | EQ -> Tcons1.make (sub12 ()) Tcons1.DISEQ
        | NE -> Tcons1.make (sub12 ()) Tcons1.EQ
      in
      let arr = Tcons1.array_make env 1 in
      Tcons1.array_set arr 0 cons;
      Abstract1.of_tcons_array man env arr
    | `Not l0 -> inner1 l0
    | `Nondet -> Abstract1.top man env
    | `Var (x, _) ->
      let te = texpr_of_expr env (Typed.mk_arith_var ~loc:Location.none x `TyInt) in
      let cons = Tcons1.make te Tcons1.EQ in
      let arr = Tcons1.array_make env 1 in
      Tcons1.array_set arr 0 cons;
      Abstract1.of_tcons_array man env arr
  in
  fun l -> function
    | `Then -> inner1 l
    | `Else -> inner2 l
;;

let abs_of_assignment man abs x e =
  let env = Abstract1.env abs in
  Abstract1.assign_texpr man abs (Var.of_string x) (texpr_of_expr env e) None
;;

let abs_of_assignments man abs xs es =
  let env = Abstract1.env abs in
  Abstract1.assign_texpr_array
    man
    abs
    (Array.of_list_map xs ~f:Var.of_string)
    (Array.of_list_map es ~f:(texpr_of_expr env))
    None
;;

let abs_of_assignments_with man abs xs es =
  let env = Abstract1.env abs in
  Abstract1.assign_texpr_array_with
    man
    abs
    (Array.of_list_map xs ~f:Var.of_string)
    (Array.of_list_map es ~f:(texpr_of_expr env))
    None
;;

let abs_of_substitution man abs x e =
  let env = Abstract1.env abs in
  Abstract1.substitute_texpr man abs (Var.of_string x) (texpr_of_expr env e) None
;;

let abs_of_substitutions man abs xs es =
  let env = Abstract1.env abs in
  Abstract1.substitute_texpr_array
    man
    abs
    (Array.of_list_map xs ~f:Var.of_string)
    (Array.of_list_map es ~f:(texpr_of_expr env))
    None
;;

let abs_of_substitutions_with man abs xs es =
  let env = Abstract1.env abs in
  Abstract1.substitute_texpr_array_with
    man
    abs
    (Array.of_list_map xs ~f:Var.of_string)
    (Array.of_list_map es ~f:(texpr_of_expr env))
    None
;;

let create_tcons_array env conss =
  let res = Tcons1.array_make env (List.length conss) in
  List.iteri conss ~f:(fun i cons -> Tcons1.array_set res i cons);
  res
;;

let abs_of_relation man abs e1 op e2 =
  let env = Abstract1.env abs in
  Abstract1.meet_tcons_array
    man
    abs
    (create_tcons_array env [ tcons_of_relation env e1 op e2 ])
;;

let abs_of_relations man abs es1 op es2 =
  let env = Abstract1.env abs in
  Abstract1.meet_tcons_array
    man
    abs
    (create_tcons_array
       env
       (List.map2_exn es1 es2 ~f:(fun e1 e2 -> tcons_of_relation env e1 op e2)))
;;

let abs_of_relation_with man abs e1 op e2 =
  let env = Abstract1.env abs in
  Abstract1.meet_tcons_array_with
    man
    abs
    (create_tcons_array env [ tcons_of_relation env e1 op e2 ])
;;

let abs_of_relations_with man abs es1 op es2 =
  let env = Abstract1.env abs in
  Abstract1.meet_tcons_array_with
    man
    abs
    (create_tcons_array
       env
       (List.map2_exn es1 es2 ~f:(fun e1 e2 -> tcons_of_relation env e1 op e2)))
;;
