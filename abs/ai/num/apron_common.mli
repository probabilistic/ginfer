open Apron
open Action_types

val texpr_of_expr : Environment.t -> Typed.arith_expr -> Texpr1.t

val tcons_of_relation
  :  Environment.t
  -> Typed.arith_expr
  -> rel_op
  -> Typed.arith_expr
  -> Tcons1.t

val abs_of_logic
  :  'a Manager.t
  -> Environment.t
  -> Typed.bool_expr
  -> [ `Then | `Else ]
  -> 'a Abstract1.t

val abs_of_assignment
  :  'a Manager.t
  -> 'a Abstract1.t
  -> string
  -> Typed.arith_expr
  -> 'a Abstract1.t

val abs_of_substitution
  :  'a Manager.t
  -> 'a Abstract1.t
  -> string
  -> Typed.arith_expr
  -> 'a Abstract1.t

val abs_of_assignments
  :  'a Manager.t
  -> 'a Abstract1.t
  -> string list
  -> Typed.arith_expr list
  -> 'a Abstract1.t

val abs_of_substitutions
  :  'a Manager.t
  -> 'a Abstract1.t
  -> string list
  -> Typed.arith_expr list
  -> 'a Abstract1.t

val abs_of_assignments_with
  :  'a Manager.t
  -> 'a Abstract1.t
  -> string list
  -> Typed.arith_expr list
  -> unit

val abs_of_substitutions_with
  :  'a Manager.t
  -> 'a Abstract1.t
  -> string list
  -> Typed.arith_expr list
  -> unit

val abs_of_relation
  :  'a Manager.t
  -> 'a Abstract1.t
  -> Typed.arith_expr
  -> rel_op
  -> Typed.arith_expr
  -> 'a Abstract1.t

val abs_of_relations
  :  'a Manager.t
  -> 'a Abstract1.t
  -> Typed.arith_expr list
  -> rel_op
  -> Typed.arith_expr list
  -> 'a Abstract1.t

val abs_of_relation_with
  :  'a Manager.t
  -> 'a Abstract1.t
  -> Typed.arith_expr
  -> rel_op
  -> Typed.arith_expr
  -> unit

val abs_of_relations_with
  :  'a Manager.t
  -> 'a Abstract1.t
  -> Typed.arith_expr list
  -> rel_op
  -> Typed.arith_expr list
  -> unit
