open Core
open Apron
open Action_types
open Ffloat

module Make0 (V : sig
  type abs

  val create : unit -> abs Manager.t
end) =
struct
  let mknoloc_e mk = mk ~loc:Location.none
  let mknoloc = Location.mknoloc

  type t = V.abs Abstract1.t
  type hint = Lincons1.earray

  let g_manager = V.create ()
  let equal oct1 oct2 = Abstract1.is_eq g_manager oct1 oct2
  let print fmt oct = Abstract1.print fmt oct

  let bot scope =
    let ivars, rvars =
      List.fold scope ~init:([], []) ~f:(fun (acc1, acc2) (x, ty) ->
          match ty with
          | `TyInt | `TyBool -> x :: acc1, acc2
          | `TyReal | `TyProb -> acc1, x :: acc2)
    in
    Abstract1.bottom
      g_manager
      (Environment.make
         (Array.of_list_map ivars ~f:Var.of_string)
         (Array.of_list_map rvars ~f:Var.of_string))
  ;;

  let set_bvars_range oct bvars =
    Apron_common.abs_of_relations_with
      g_manager
      oct
      (List.map bvars ~f:(fun x -> mknoloc_e (Typed.mk_arith_var (mknoloc x) `TyInt)))
      GE
      (List.map bvars ~f:(fun _ -> mknoloc_e (Typed.mk_int 0)));
    Apron_common.abs_of_relations_with
      g_manager
      oct
      (List.map bvars ~f:(fun x -> mknoloc_e (Typed.mk_arith_var (mknoloc x) `TyInt)))
      LE
      (List.map bvars ~f:(fun _ -> mknoloc_e (Typed.mk_int 1)))
  ;;

  let set_pvars_range oct pvars =
    Apron_common.abs_of_relations_with
      g_manager
      oct
      (List.map pvars ~f:(fun x -> mknoloc_e (Typed.mk_arith_var (mknoloc x) `TyReal)))
      GE
      (List.map pvars ~f:(fun _ -> mknoloc_e (Typed.mk_int 0)));
    Apron_common.abs_of_relations_with
      g_manager
      oct
      (List.map pvars ~f:(fun x -> mknoloc_e (Typed.mk_arith_var (mknoloc x) `TyReal)))
      LE
      (List.map pvars ~f:(fun _ -> mknoloc_e (Typed.mk_int 1)))
  ;;

  let top scope =
    let ivars, bvars, rvars, pvars =
      List.fold scope ~init:([], [], [], []) ~f:(fun (acc1, acc2, acc3, acc4) (x, ty) ->
          match ty with
          | `TyInt -> x :: acc1, acc2, acc3, acc4
          | `TyBool -> acc1, x :: acc2, acc3, acc4
          | `TyReal -> acc1, acc2, x :: acc3, acc4
          | `TyProb -> acc1, acc2, acc3, x :: acc4)
    in
    let oct =
      Abstract1.top
        g_manager
        (Environment.make
           (Array.of_list_map (ivars @ bvars) ~f:Var.of_string)
           (Array.of_list_map (rvars @ pvars) ~f:Var.of_string))
    in
    set_bvars_range oct bvars;
    set_pvars_range oct pvars;
    oct
  ;;

  let join oct1 oct2 = Abstract1.join g_manager oct1 oct2
  let meet oct1 oct2 = Abstract1.meet g_manager oct1 oct2

  let rename_vars oct ~f =
    let env = Abstract1.env oct in
    let ivars, rvars = Environment.vars env in
    let vars = Array.append ivars rvars in
    let vars' = Array.map vars ~f:(fun x -> Var.of_string (f (Var.to_string x))) in
    Abstract1.rename_array g_manager oct vars vars'
  ;;

  let forget oct xs =
    let env = Abstract1.env oct in
    let xs = List.filter xs ~f:(fun x -> Environment.mem_var env (Var.of_string x)) in
    Abstract1.forget_array g_manager oct (Array.of_list_map xs ~f:Var.of_string) false
  ;;

  let decl_vars oct xts =
    let ivars, bvars, rvars, pvars =
      List.fold xts ~init:([], [], [], []) ~f:(fun (acc1, acc2, acc3, acc4) (x, ty) ->
          match ty with
          | `TyInt -> x :: acc1, acc2, acc3, acc4
          | `TyBool -> acc1, x :: acc2, acc3, acc4
          | `TyReal -> acc1, acc2, x :: acc3, acc4
          | `TyProb -> acc1, acc2, acc3, x :: acc4)
    in
    let env = Abstract1.env oct in
    let env' =
      Environment.add
        env
        (Array.of_list_map (ivars @ bvars) ~f:Var.of_string)
        (Array.of_list_map (rvars @ pvars) ~f:Var.of_string)
    in
    Abstract1.change_environment g_manager oct env' false
  ;;

  let dead_vars oct xs =
    let env = Abstract1.env oct in
    let env' = Environment.remove env (Array.of_list_map xs ~f:Var.of_string) in
    Abstract1.change_environment g_manager oct env' false
  ;;

  let extend oct act dir =
    match act with
    | Typed.DWeaken es ->
      Apron_common.abs_of_relations
        g_manager
        oct
        es
        GE
        (List.map es ~f:(fun _ -> mknoloc_e (Typed.mk_int 0)))
    | DSkip -> oct
    | DScore _ -> oct
    | DAssume phi ->
      meet oct (Apron_common.abs_of_logic g_manager (Abstract1.env oct) phi `Then)
    | DSample (x, `Real d) ->
      let l, r = d.dist_range in
      let lo =
        match l with
        | None -> Scalar.of_infty (-1)
        | Some f -> Scalar.of_float f
      in
      let hi =
        match r with
        | None -> Scalar.of_infty 1
        | Some f -> Scalar.of_float f
      in
      let interval = Interval.of_scalar lo hi in
      (match dir with
      | `Forward ->
        let oct' = forget oct [ x.txt ] in
        Abstract1.meet_with
          g_manager
          oct'
          (Abstract1.of_box
             g_manager
             (Abstract1.env oct')
             [| Var.of_string x.txt |]
             [| interval |]);
        oct'
      | `Backward ->
        let oct' =
          Abstract1.meet
            g_manager
            oct
            (Abstract1.of_box
               g_manager
               (Abstract1.env oct)
               [| Var.of_string x.txt |]
               [| interval |])
        in
        forget oct' [ x.txt ])
    | DSample (x, `Int d) ->
      let l, r = d.dist_range in
      let lo =
        match l with
        | None -> Scalar.of_infty (-1)
        | Some n -> Scalar.of_int n
      in
      let hi =
        match r with
        | None -> Scalar.of_infty 1
        | Some n -> Scalar.of_int n
      in
      let interval = Interval.of_scalar lo hi in
      (match dir with
      | `Forward ->
        let oct' = forget oct [ x.txt ] in
        Abstract1.meet_with
          g_manager
          oct'
          (Abstract1.of_box
             g_manager
             (Abstract1.env oct')
             [| Var.of_string x.txt |]
             [| interval |]);
        oct'
      | `Backward ->
        let oct' =
          Abstract1.meet
            g_manager
            oct
            (Abstract1.of_box
               g_manager
               (Abstract1.env oct)
               [| Var.of_string x.txt |]
               [| interval |])
        in
        forget oct' [ x.txt ])
    | DAssign (x, e) ->
      (match Typed.refine e with
      | `ArithExpr ae ->
        (match dir with
        | `Forward -> Apron_common.abs_of_assignment g_manager oct x.txt ae
        | `Backward -> Apron_common.abs_of_substitution g_manager oct x.txt ae)
      | `BoolExpr be ->
        let then_ = Apron_common.abs_of_logic g_manager (Abstract1.env oct) be `Then in
        let else_ = Apron_common.abs_of_logic g_manager (Abstract1.env oct) be `Else in
        (match dir with
        | `Forward ->
          join
            (Apron_common.abs_of_assignment
               g_manager
               (meet oct then_)
               x.txt
               (Typed.mk_int ~loc:Location.none 1))
            (Apron_common.abs_of_assignment
               g_manager
               (meet oct else_)
               x.txt
               (Typed.mk_int ~loc:Location.none 0))
        | `Backward ->
          join
            (meet
               (Apron_common.abs_of_substitution
                  g_manager
                  oct
                  x.txt
                  (Typed.mk_int ~loc:Location.none 1))
               then_)
            (meet
               (Apron_common.abs_of_assignment
                  g_manager
                  oct
                  x.txt
                  (Typed.mk_int ~loc:Location.none 0))
               else_))
      | _ -> oct)
  ;;

  let branch oct phi tag =
    let env = Abstract1.env oct in
    meet oct (Apron_common.abs_of_logic g_manager env phi tag)
  ;;

  let classify_ty = function
    | Environment.INT -> `TyInt
    | Environment.REAL -> `TyReal
  ;;

  let lookup_ty env x =
    let v = Var.of_string x in
    if Environment.mem_var env v
    then Some (classify_ty @@ Environment.typ_of_var env v)
    else None
  ;;

  let call oct args (* xs *) params (* returns *) callee_scope dir =
    let env = Abstract1.env oct in
    match dir, args, params with
    | `Forward, args, params ->
      let args, params, arg_tys =
        List.unzip3
        @@ List.filter_map (List.zip_exn args params) ~f:(fun (arg, param) ->
               lookup_ty env arg |> Option.map ~f:(fun ty -> arg, param, ty))
      in
      let transfer_params = List.map params ~f:(fun x -> "ARG_" ^ x) in
      let oct' = rename_vars oct ~f:(fun x -> "DIST_" ^ x) in
      let callee_oct = top callee_scope in
      Abstract1.unify_with g_manager oct' callee_oct;
      Apron_common.abs_of_assignments_with
        g_manager
        oct'
        transfer_params
        (List.map2_exn args arg_tys ~f:(fun x ty ->
             mknoloc_e (Typed.mk_arith_var (mknoloc ("DIST_" ^ x)) ty)));
      Apron_common.abs_of_assignments_with
        g_manager
        oct'
        params
        (List.map2_exn transfer_params arg_tys ~f:(fun x ty ->
             mknoloc_e (Typed.mk_arith_var (mknoloc x) ty)));
      Abstract1.change_environment_with g_manager oct' (Abstract1.env callee_oct) false;
      oct'
    | `Backward, xs, returns ->
      let xs, returns, xs_tys =
        List.unzip3
        @@ List.filter_map (List.zip_exn xs returns) ~f:(fun (x, return) ->
               lookup_ty env x |> Option.map ~f:(fun ty -> x, return, ty))
      in
      let transfer_returns = List.map returns ~f:(fun x -> "RET_" ^ x) in
      let oct' = rename_vars oct ~f:(fun x -> "DIST_" ^ x) in
      let callee_oct = top callee_scope in
      Abstract1.unify_with g_manager oct' callee_oct;
      Apron_common.abs_of_substitutions_with
        g_manager
        oct'
        (List.map xs ~f:(fun x -> "DIST_" ^ x))
        (List.map2_exn transfer_returns xs_tys ~f:(fun x ty ->
             mknoloc_e (Typed.mk_arith_var (mknoloc x) ty)));
      Apron_common.abs_of_assignments_with
        g_manager
        oct'
        returns
        (List.map2_exn transfer_returns xs_tys ~f:(fun x ty ->
             mknoloc_e (Typed.mk_arith_var (mknoloc x) ty)));
      Abstract1.change_environment_with g_manager oct' (Abstract1.env callee_oct) false;
      oct'
  ;;

  let return
      caller_oct
      args
      (* xs *) params
      (* returns *) _
      oct
      returns
      (* params *) xs
      (* args *) _
      dir
    =
    let env = Abstract1.env caller_oct in
    match dir, args, params, returns, xs with
    | `Forward, args, params, returns, xs ->
      let args, params, arg_tys =
        List.unzip3
        @@ List.filter_map (List.zip_exn args params) ~f:(fun (arg, param) ->
               lookup_ty env arg |> Option.map ~f:(fun ty -> arg, param, ty))
      in
      let xs, returns, xs_tys =
        List.unzip3
        @@ List.filter_map (List.zip_exn xs returns) ~f:(fun (x, return) ->
               lookup_ty env x |> Option.map ~f:(fun ty -> x, return, ty))
      in
      let transfer_params = List.map params ~f:(fun x -> "ARG_" ^ x) in
      let caller_oct' = rename_vars caller_oct ~f:(fun x -> "DIST_" ^ x) in
      let oct' = Abstract1.unify g_manager caller_oct' oct in
      Apron_common.abs_of_substitutions_with
        g_manager
        oct'
        transfer_params
        (List.map2_exn args arg_tys ~f:(fun x ty ->
             mknoloc_e (Typed.mk_arith_var (mknoloc ("DIST_" ^ x)) ty)));
      let transfer_returns = List.map returns ~f:(fun x -> "RET_" ^ x) in
      Apron_common.abs_of_assignments_with
        g_manager
        oct'
        transfer_returns
        (List.map2_exn returns xs_tys ~f:(fun x ty ->
             mknoloc_e (Typed.mk_arith_var (mknoloc x) ty)));
      Apron_common.abs_of_assignments_with
        g_manager
        oct'
        (List.map xs ~f:(fun x -> "DIST_" ^ x))
        (List.map2_exn transfer_returns xs_tys ~f:(fun x ty ->
             mknoloc_e (Typed.mk_arith_var (mknoloc x) ty)));
      Abstract1.change_environment_with g_manager oct' (Abstract1.env caller_oct') false;
      rename_vars oct' ~f:(fun x -> String.suffix x (String.length x - 5))
    | `Backward, xs, returns, params, args ->
      let xs, returns, xs_tys =
        List.unzip3
        @@ List.filter_map (List.zip_exn xs returns) ~f:(fun (x, return) ->
               lookup_ty env x |> Option.map ~f:(fun ty -> x, return, ty))
      in
      let args, params, arg_tys =
        List.unzip3
        @@ List.filter_map (List.zip_exn args params) ~f:(fun (arg, param) ->
               lookup_ty env arg |> Option.map ~f:(fun ty -> arg, param, ty))
      in
      let transfer_returns = List.map returns ~f:(fun x -> "RET_" ^ x) in
      let caller_oct' = rename_vars caller_oct ~f:(fun x -> "DIST_" ^ x) in
      let oct' = Abstract1.unify g_manager caller_oct' oct in
      Apron_common.abs_of_substitutions_with
        g_manager
        oct'
        (List.map xs ~f:(fun x -> "DIST_" ^ x))
        (List.map2_exn transfer_returns xs_tys ~f:(fun x ty ->
             mknoloc_e (Typed.mk_arith_var (mknoloc x) ty)));
      let transfer_params = List.map params ~f:(fun x -> "ARG_" ^ x) in
      Apron_common.abs_of_substitutions_with
        g_manager
        oct'
        params
        (List.map2_exn transfer_params arg_tys ~f:(fun x ty ->
             mknoloc_e (Typed.mk_arith_var (mknoloc x) ty)));
      Apron_common.abs_of_substitutions_with
        g_manager
        oct'
        transfer_params
        (List.map2_exn args arg_tys ~f:(fun x ty ->
             mknoloc_e (Typed.mk_arith_var (mknoloc ("DIST_" ^ x)) ty)));
      Abstract1.change_environment_with g_manager oct' (Abstract1.env caller_oct') false;
      rename_vars oct' ~f:(fun x -> String.suffix x (String.length x - 5))
  ;;

  let alpha scope phi =
    let ivars, bvars, rvars, pvars =
      List.fold scope ~init:([], [], [], []) ~f:(fun (acc1, acc2, acc3, acc4) (x, ty) ->
          match ty with
          | `TyInt -> x :: acc1, acc2, acc3, acc4
          | `TyBool -> acc1, x :: acc2, acc3, acc4
          | `TyReal -> acc1, acc2, x :: acc3, acc4
          | `TyProb -> acc1, acc2, acc3, x :: acc4)
    in
    let oct =
      Apron_common.abs_of_logic
        g_manager
        (Environment.make
           (Array.of_list_map (ivars @ bvars) ~f:Var.of_string)
           (Array.of_list_map (rvars @ pvars) ~f:Var.of_string))
        phi
        `Then
    in
    set_bvars_range oct bvars;
    set_pvars_range oct pvars;
    oct
  ;;

  let widen ?hint oct1 oct2 =
    match hint with
    | None -> Abstract1.widening g_manager oct1 oct2
    | Some lcs -> Abstract1.widening_threshold g_manager oct1 oct2 lcs
  ;;

  let to_hint oct = Abstract1.to_lincons_array g_manager oct

  let coeff_to_float = function
    | Coeff.Scalar s ->
      (match s with
      | Scalar.Float f -> f
      | Scalar.Mpqf f -> Mpqf.to_float f
      | Scalar.Mpfrf f -> Mpfrf.to_float f)
    | _ -> assert false
  ;;

  let to_smt oct =
    let env = Abstract1.env oct in
    let ivars, rvars = Environment.vars env in
    let arr = Abstract1.to_lincons_array g_manager oct in
    List.fold
      (List.init (Lincons1.array_length arr) ~f:(fun i -> i))
      ~init:[]
      ~f:(fun acc i ->
        let lincons = Lincons1.array_get arr i in
        let p0 = Smt.mk_const_r (Lincons1.get_cst lincons |> coeff_to_float) in
        let p1 =
          Array.fold ivars ~init:p0 ~f:(fun acc x ->
              let v = Var.to_string x in
              if String.is_prefix v ~prefix:"ARG_" || String.is_prefix v ~prefix:"RET_"
              then acc
              else
                Smt.mk_add
                  acc
                  (Smt.mk_mult
                     (Smt.mk_const_r (Lincons1.get_coeff lincons x |> coeff_to_float))
                     (Smt.mk_ivar v)))
        in
        let p2 =
          Array.fold rvars ~init:p1 ~f:(fun acc x ->
              let v = Var.to_string x in
              if String.is_prefix v ~prefix:"ARG_" || String.is_prefix v ~prefix:"RET_"
              then acc
              else
                Smt.mk_add
                  acc
                  (Smt.mk_mult
                     (Smt.mk_const_r (Lincons1.get_coeff lincons x |> coeff_to_float))
                     (Smt.mk_rvar v)))
        in
        match Lincons1.get_typ lincons with
        | Lincons1.EQ -> Smt.mk_eq p2 (Smt.mk_const 0) :: acc
        | Lincons1.SUPEQ -> Smt.mk_ge p2 (Smt.mk_const 0) :: acc
        | Lincons1.SUP -> Smt.mk_gt p2 (Smt.mk_const 0) :: acc
        | _ -> assert false)
  ;;

  module Gen_poly (Flt : FLOAT) = struct
    module Flt_poly = Polynomial.Make_float_poly (Flt)

    let coeff_to_float = function
      | Coeff.Scalar s ->
        (match s with
        | Scalar.Float f -> Flt.of_float f
        | Scalar.Mpqf f -> Flt.of_mpqf f
        | Scalar.Mpfrf f -> Flt.of_mpfrf f)
      | _ -> assert false
    ;;

    let to_polys oct =
      let oct = Abstract1.closure g_manager oct in
      let env = Abstract1.env oct in
      let ivars, rvars = Environment.vars env in
      let arr = Abstract1.to_lincons_array g_manager oct in
      List.fold
        (List.init (Lincons1.array_length arr) ~f:(fun i -> i))
        ~init:[]
        ~f:(fun acc i ->
          let lincons = Lincons1.array_get arr i in
          let p =
            Array.fold
              (Array.append ivars rvars)
              ~init:(Flt_poly.const (Lincons1.get_cst lincons |> coeff_to_float))
              ~f:(fun acc x ->
                let v = Var.to_string x in
                if String.is_prefix v ~prefix:"ARG_" || String.is_prefix v ~prefix:"RET_"
                then acc
                else
                  Flt_poly.add_monom
                    (Polynomial.Monom.of_var v)
                    (Lincons1.get_coeff lincons x |> coeff_to_float)
                    acc)
          in
          match Lincons1.get_typ lincons with
          | Lincons1.EQ -> p :: Flt_poly.scale Flt.minus_one p :: acc
          | Lincons1.SUPEQ -> p :: acc
          | _ -> assert false)
    ;;
  end
end

module Box0 = struct
  type abs = Box.t

  let create () = Box.manager_alloc ()
end

module Oct0 = struct
  type abs = Oct.t

  let create () = Oct.manager_alloc ()
end

module Polka0 = struct
  type abs = Polka.loose Polka.t

  let create () = Polka.manager_alloc_loose ()
end

module Ppl0 = struct
  type abs = Ppl.loose Ppl.t

  let create () = Ppl.manager_alloc_loose ()
end
