open Apron
open Ffloat

module Make0 : functor
  (V : sig
     type abs

     val create : unit -> abs Manager.t
   end)
  -> sig
  include Ai_type.DOMAIN0 with type hint = Lincons1.earray

  val to_hint : t -> hint
  val to_smt : t -> Smt.expr list

  module Gen_poly : functor (Flt : FLOAT) -> sig
    val to_polys : t -> Flt.t Polynomial.Poly.t list
  end
end

module Box0 : sig
  type abs

  val create : unit -> abs Manager.t
end

module Oct0 : sig
  type abs

  val create : unit -> abs Manager.t
end

module Polka0 : sig
  type abs

  val create : unit -> abs Manager.t
end

module Ppl0 : sig
  type abs

  val create : unit -> abs Manager.t
end
