open Core

module Make0 (D : Ai_type.DOMAIN0) (C : Ctxt_type.CFG_CONTEXT) = struct
  type context = C.t [@@deriving sexp, compare, equal, hash]
  type dom = D.t
  type hint = D.hint

  module Config = struct
    type t = context * Cfg.vertex [@@deriving sexp, compare, equal, hash]
  end

  module Vertex_table = Hashtbl.Make (struct
    type t = Cfg.vertex [@@deriving sexp, compare, hash]
  end)

  module Config_table = Hashtbl.Make (Config)
  module DG = Graph.Imperative.Digraph.Concrete (Config)
  module DG_wto = Graph.WeakTopological.Make (DG)

  let rm_loc = List.map ~f:(fun x -> x.Action_types.txt)

  let make_vertex_to_func prog =
    let tbl =
      List.fold prog.Cfg.prg_funcs ~init:[] ~f:(fun acc func ->
          Cfg.fold_vertex (fun acc v -> (v, func) :: acc) acc func.Cfg.fun_cfg)
      |> Vertex_table.of_alist_exn
    in
    fun v -> Vertex_table.find_exn tbl v
  ;;

  let make_scope func =
    let get_ty x = Action_ops.tyctx_get_exn func.Cfg.fun_scope x in
    let res =
      List.concat
        [ List.map func.Cfg.fun_params ~f:(fun x -> "ARG_" ^ x.txt, get_ty x)
        ; List.map func.fun_returns ~f:(fun x -> "RET_" ^ x.txt, get_ty x)
        ; List.map (Action_ops.tyctx_to_alist func.fun_scope) ~f:(fun (x, ty) -> x, ty)
        ]
    in
    res, List.unzip res |> fst
  ;;

  let make_func_to_scope prog =
    let tbl =
      List.fold prog.Cfg.prg_funcs ~init:[] ~f:(fun acc func ->
          (func.fun_name.txt, make_scope func) :: acc)
      |> String.Map.of_alist_exn
    in
    fun name -> Map.find_exn tbl name
  ;;

  let rec dequeue (s1, s2) =
    match s1, s2 with
    | [], [] -> None
    | h :: t, s2 -> Some (h, (t, s2))
    | _ -> dequeue (List.rev s2, [])
  ;;

  let enqueue (s1, s2) v = s1, v :: s2
  let empty_queue = [], []

  let build_cg dir prog =
    let vertex_to_func = make_vertex_to_func prog in
    let main_func = Cfg.main_func prog in
    let dg = DG.create ~size:(Cfg.nb_vertex main_func.fun_cfg) () in
    let wl = ref empty_queue in
    let rec bfs () =
      match dequeue !wl with
      | None -> ()
      | Some ((ctx, v), wl') ->
        wl := wl';
        if not (DG.mem_vertex dg (ctx, v))
        then (
          DG.add_vertex dg (ctx, v);
          Cfg.iter_succ_e
            (fun e ->
              let dsts = Cfg.edge_dsts e in
              let ctrl = Cfg.edge_action e in
              List.iter dsts ~f:(fun dst -> wl := enqueue !wl (ctx, dst));
              match ctrl with
              | Cfg.CCall (name, _, _) ->
                let ctx' = C.extend v ctx in
                wl := enqueue !wl (ctx', (Cfg.find_func prog name).fun_init)
              | _ -> ())
            (vertex_to_func v).fun_cfg
            v);
        bfs ()
    in
    wl := enqueue !wl (C.empty, main_func.fun_init);
    bfs ();
    let add_edge =
      match dir with
      | `Forward -> fun u v -> DG.add_edge dg u v
      | `Backward -> fun u v -> DG.add_edge dg v u
    in
    DG.iter_vertex
      (fun (ctx, v) ->
        Cfg.iter_succ_e
          (fun e ->
            let dsts = Cfg.edge_dsts e in
            let ctrl = Cfg.edge_action e in
            match ctrl with
            | Cfg.CCall (name, _, _) ->
              let ctx' = C.extend v ctx in
              let callee = Cfg.find_func prog name in
              add_edge (ctx, v) (ctx', callee.fun_init);
              add_edge (ctx', callee.fun_exit) (ctx, List.nth_exn dsts 0);
              add_edge (ctx, v) (ctx, List.nth_exn dsts 0)
            | _ -> List.iter dsts ~f:(fun dst -> add_edge (ctx, v) (ctx, dst)))
          (vertex_to_func v).fun_cfg
          v)
      dg;
    dg
  ;;

  let annot_cg dir prog dg =
    let vertex_to_func = make_vertex_to_func prog in
    let func_to_scope = make_func_to_scope prog in
    let trans = Config_table.create () in
    let add_t u v f =
      match dir with
      | `Forward ->
        let record =
          Config_table.find_or_add trans u ~default:(fun () -> Config_table.create ())
        in
        Config_table.set record ~key:v ~data:f
      | `Backward ->
        let record =
          Config_table.find_or_add trans v ~default:(fun () -> Config_table.create ())
        in
        Config_table.set record ~key:u ~data:f
    in
    DG.iter_vertex
      (fun (ctx, v) ->
        let v_func = vertex_to_func v in
        Cfg.iter_succ_e
          (fun e ->
            let dsts = Cfg.edge_dsts e in
            let ctrl = Cfg.edge_action e in
            match ctrl with
            | Cfg.CSeq act ->
              add_t
                (ctx, v)
                (ctx, List.nth_exn dsts 0)
                ( None
                , function
                  | [ abs ] -> D.extend abs act dir
                  | _ -> assert false )
            | Cfg.CProb _ ->
              add_t
                (ctx, v)
                (ctx, List.nth_exn dsts 0)
                ( None
                , function
                  | [ abs ] -> abs
                  | _ -> assert false );
              add_t
                (ctx, v)
                (ctx, List.nth_exn dsts 1)
                ( None
                , function
                  | [ abs ] -> abs
                  | _ -> assert false )
            | Cfg.CCond phi ->
              add_t
                (ctx, v)
                (ctx, List.nth_exn dsts 0)
                ( None
                , function
                  | [ abs ] -> D.branch abs phi `Then
                  | _ -> assert false );
              add_t
                (ctx, v)
                (ctx, List.nth_exn dsts 1)
                ( None
                , function
                  | [ abs ] -> D.branch abs phi `Else
                  | _ -> assert false )
            | Cfg.CCall (name, args, xs) ->
              let ctx' = C.extend v ctx in
              let callee = Cfg.find_func prog name in
              (match dir with
              | `Forward ->
                add_t
                  (ctx, v)
                  (ctx', callee.fun_init)
                  ( None
                  , function
                    | [ abs ] ->
                      D.call
                        abs
                        (rm_loc args)
                        (rm_loc callee.fun_params)
                        (func_to_scope callee.fun_name.txt |> fst)
                        dir
                    | _ -> assert false );
                add_t
                  (ctx', callee.fun_exit)
                  (ctx, List.nth_exn dsts 0)
                  ( Some (ctx, v)
                  , function
                    | [ abs1; abs2 ] ->
                      D.return
                        abs1
                        (rm_loc args)
                        (rm_loc callee.fun_params)
                        (func_to_scope callee.fun_name.txt |> snd)
                        abs2
                        (rm_loc callee.fun_returns)
                        (rm_loc xs)
                        (func_to_scope v_func.fun_name.txt |> snd)
                        dir
                    | _ -> assert false );
                add_t
                  (ctx, v)
                  (ctx, List.nth_exn dsts 0)
                  ( Some (ctx', callee.fun_exit)
                  , function
                    | [ abs2; abs1 ] ->
                      D.return
                        abs1
                        (rm_loc args)
                        (rm_loc callee.fun_params)
                        (func_to_scope callee.fun_name.txt |> snd)
                        abs2
                        (rm_loc callee.fun_returns)
                        (rm_loc xs)
                        (func_to_scope v_func.fun_name.txt |> snd)
                        dir
                    | _ -> assert false )
              | `Backward ->
                add_t
                  (ctx', callee.fun_exit)
                  (ctx, List.nth_exn dsts 0)
                  ( None
                  , function
                    | [ abs ] ->
                      D.call
                        abs
                        (rm_loc xs)
                        (rm_loc callee.fun_returns)
                        (func_to_scope callee.fun_name.txt |> fst)
                        dir
                    | _ -> assert false );
                add_t
                  (ctx, v)
                  (ctx', callee.fun_init)
                  ( Some (ctx, List.nth_exn dsts 0)
                  , function
                    | [ abs1; abs2 ] ->
                      D.return
                        abs1
                        (rm_loc xs)
                        (rm_loc callee.fun_returns)
                        (func_to_scope callee.fun_name.txt |> snd)
                        abs2
                        (rm_loc callee.fun_params)
                        (rm_loc args)
                        (func_to_scope v_func.fun_name.txt |> snd)
                        dir
                    | _ -> assert false );
                add_t
                  (ctx, v)
                  (ctx, List.nth_exn dsts 0)
                  ( Some (ctx', callee.fun_init)
                  , function
                    | [ abs2; abs1 ] ->
                      D.return
                        abs1
                        (rm_loc xs)
                        (rm_loc callee.fun_returns)
                        (func_to_scope callee.fun_name.txt |> snd)
                        abs2
                        (rm_loc callee.fun_params)
                        (rm_loc args)
                        (func_to_scope v_func.fun_name.txt |> snd)
                        dir
                    | _ -> assert false )))
          (vertex_to_func v).fun_cfg
          v)
      dg;
    trans
  ;;

  let interpret_solution prog sol =
    let vertex_to_func = make_vertex_to_func prog in
    let cache =
      Config_table.to_alist sol
      |> List.map ~f:(fun ((ctx, v), abs) ->
             ( v
             , ( ctx
               , let v_func = vertex_to_func v in
                 D.forget
                   abs
                   (List.append
                      (List.map v_func.fun_params ~f:(fun x -> "ARG_" ^ x.txt))
                      (List.map v_func.fun_returns ~f:(fun x -> "RET_" ^ x.txt))) ) ))
      |> Vertex_table.of_alist_multi
    in
    fun v -> Vertex_table.find cache v |> Option.value ~default:[]
  ;;

  let analyze dir ?init:pre ?(widen_threshold = 1) ?widen_hints prog =
    let vertex_to_func = make_vertex_to_func prog in
    let func_to_scope = make_func_to_scope prog in
    let dg = build_cg dir prog in
    let trans = annot_cg dir prog dg in
    let main_func = Cfg.main_func prog in
    let sol = Config_table.create ~size:(Cfg.nb_vertex main_func.fun_cfg) () in
    let init =
      match dir with
      | `Forward -> C.empty, main_func.fun_init
      | `Backward -> C.empty, main_func.fun_exit
    in
    let init_abs =
      match pre with
      | None -> D.top (func_to_scope main_func.fun_name.txt |> fst)
      | Some phi ->
        (match dir with
        | `Forward ->
          D.call
            (D.alpha
               (List.map main_func.fun_params ~f:(fun x ->
                    let ty = Action_ops.tyctx_get_exn main_func.fun_scope x in
                    x.txt, ty))
               phi)
            (rm_loc main_func.fun_params)
            (rm_loc main_func.fun_params)
            (func_to_scope main_func.fun_name.txt |> fst)
            `Forward
        | `Backward ->
          D.call
            (D.alpha
               (List.map main_func.fun_returns ~f:(fun x ->
                    let ty = Action_ops.tyctx_get_exn main_func.fun_scope x in
                    x.txt, ty))
               phi)
            (rm_loc main_func.fun_returns)
            (rm_loc main_func.fun_returns)
            (func_to_scope main_func.fun_name.txt |> fst)
            `Backward)
    in
    Config_table.set sol ~key:init ~data:init_abs;
    let analyze_node ~widen ((_, v) as key) =
      let n_abs =
        DG.fold_pred
          (fun ((_, v') as key') acc ->
            let o_abs' =
              Config_table.find_or_add sol key' ~default:(fun () ->
                  D.bot (func_to_scope (vertex_to_func v').fun_name.txt |> fst))
            in
            let other, tran =
              Config_table.find_exn (Config_table.find_exn trans key') key
            in
            let t_abs' =
              match other with
              | None -> tran [ o_abs' ]
              | Some ((_, other_v) as other) ->
                tran
                  [ Config_table.find_or_add sol other ~default:(fun () ->
                        D.bot (func_to_scope (vertex_to_func other_v).fun_name.txt |> fst))
                  ; o_abs'
                  ]
            in
            D.join acc t_abs')
          dg
          key
          (if Config.equal key init
          then init_abs
          else D.bot (func_to_scope (vertex_to_func v).fun_name.txt |> fst))
      in
      Config_table.set
        sol
        ~key
        ~data:
          (if widen
          then (
            let o_abs = Config_table.find_exn sol key in
            D.widen
              ?hint:(Option.map widen_hints ~f:(fun widen_hints -> widen_hints key))
              o_abs
              (D.join o_abs n_abs))
          else n_abs)
    in
    let wto = DG_wto.recursive_scc dg init in
    let rec f () elem =
      match elem with
      | Graph.WeakTopological.Vertex key -> analyze_node ~widen:false key
      | Graph.WeakTopological.Component (key, cs) ->
        let rec iter n =
          Graph.WeakTopological.fold_left f () cs;
          let ori = Config_table.find_exn sol key in
          analyze_node ~widen:(n >= widen_threshold) key;
          let cur = Config_table.find_exn sol key in
          if D.equal ori cur then () else iter (n + 1)
        in
        analyze_node ~widen:false key;
        iter 0
    in
    Graph.WeakTopological.fold_left f () wto;
    (* one-time narrowing *)
    let rec g () elem =
      match elem with
      | Graph.WeakTopological.Vertex key -> analyze_node ~widen:false key
      | Graph.WeakTopological.Component (key, cs) ->
        analyze_node ~widen:false key;
        Graph.WeakTopological.fold_left g () cs
    in
    Graph.WeakTopological.fold_left g () wto;
    interpret_solution prog sol
  ;;
end
