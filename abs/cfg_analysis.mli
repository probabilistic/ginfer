module Make0 : functor (D : Ai_type.DOMAIN0) (C : Ctxt_type.CFG_CONTEXT) ->
  Ai_type.CFG_ANALYSIS0 with type dom = D.t and type context = C.t and type hint = D.hint
