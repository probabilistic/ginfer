open Core
open Result.Let_syntax

module Make0 (D : Vc_type.VCOND0) (C : Ctxt_type.CFG_CONTEXT) = struct
  type solution = D.solution
  type hint = D.hint
  type context = C.t [@@deriving sexp, compare, hash]

  module Vertex_table = Hashtbl.Make (struct
    type t = Cfg.vertex [@@deriving sexp, compare, hash]
  end)

  module Config_table = Hashtbl.Make (struct
    type t = context * Cfg.vertex [@@deriving sexp, compare, hash]
  end)

  module Prefix_map = Map.Make (struct
    type t = context * string [@@deriving sexp, compare]
  end)

  let rm_loc = List.map ~f:(fun x -> x.Action_types.txt)

  let classify func v =
    if Cfg.equal_vertex v func.Cfg.fun_init
    then rm_loc func.fun_params
    else if Cfg.equal_vertex v func.Cfg.fun_exit
    then rm_loc func.fun_returns
    else Action_ops.tyctx_scope func.Cfg.fun_scope
  ;;

  let annot_cg builder ?hints prog =
    let get_hint key = Option.map hints ~f:(fun hints -> hints key) in
    let get_hints key1 key2 = Option.map hints ~f:(fun hints -> hints key1, hints key2) in
    let get_hintss key1 key2 key3 key4 =
      Option.map hints ~f:(fun hints -> hints key1, hints key2, hints key3, hints key4)
    in
    let gannots = Config_table.create () in
    let rec do_fun is_global ctx stack func lev to_annot =
      if D.is_trivial_level lev
      then to_annot
      else (
        match Prefix_map.find ctx (stack, func.Cfg.fun_name.txt) with
        | Some (mk_from_annot, rec_to_annot) ->
          D.identify_annots builder rec_to_annot to_annot;
          mk_from_annot ()
        | None ->
          let rec_annot = ref None in
          let mk_rec_annot () =
            match !rec_annot with
            | Some annot -> annot
            | None ->
              let annot =
                D.gen_new_annot
                  builder
                  ?hint:(get_hint (stack, func.fun_init))
                  (List.map func.fun_params ~f:(fun param -> param.txt))
                  lev
              in
              rec_annot := Some annot;
              annot
          in
          let ctx =
            Prefix_map.set
              ctx
              ~key:(stack, func.fun_name.txt)
              ~data:(mk_rec_annot, to_annot)
          in
          let pannots = Vertex_table.create ~size:(Cfg.nb_vertex func.fun_cfg) () in
          let rec dfs v =
            let pannot = Vertex_table.find_or_add pannots v ~default:(fun () -> `Todo) in
            match pannot with
            | `Done annot -> annot
            | `Doing ->
              let annot =
                D.gen_new_annot builder ?hint:(get_hint (stack, v)) (classify func v) lev
              in
              Vertex_table.set pannots ~key:v ~data:(`Done annot);
              annot
            | `Todo ->
              Vertex_table.set pannots ~key:v ~data:`Doing;
              let src_annot =
                Cfg.fold_succ_e
                  (fun acc e ->
                    let dsts = Cfg.edge_dsts e in
                    let ctrl = Cfg.edge_action e in
                    let dst_annots = List.map dsts ~f:(fun dst -> dfs dst) in
                    let src_annot =
                      match ctrl with
                      | Cfg.CSeq act ->
                        let dst_annot = List.nth_exn dst_annots 0 in
                        D.extend
                          builder
                          (Action_ops.tyctx_scope func.fun_scope)
                          act
                          ?hint:(get_hint (stack, v))
                          dst_annot
                          lev
                      | Cfg.CProb pe ->
                        let dst1, dst2 = List.nth_exn dsts 0, List.nth_exn dsts 1 in
                        let dst_annot1, dst_annot2 =
                          List.nth_exn dst_annots 0, List.nth_exn dst_annots 1
                        in
                        D.prob_branch
                          builder
                          ?hint:(get_hints (stack, dst1) (stack, dst2))
                          pe
                          (dst_annot1, dst_annot2)
                          lev
                      | Cfg.CCond phi ->
                        let dst1, dst2 = List.nth_exn dsts 0, List.nth_exn dsts 1 in
                        let dst_annot1, dst_annot2 =
                          List.nth_exn dst_annots 0, List.nth_exn dst_annots 1
                        in
                        D.cond_branch
                          builder
                          ?hint:(get_hints (stack, dst1) (stack, dst2))
                          phi
                          (dst_annot1, dst_annot2)
                          lev
                      | Cfg.CCall (name, args, xs) ->
                        let stack' = C.extend v stack in
                        let callee = Cfg.find_func prog name in
                        let dst = List.nth_exn dsts 0 in
                        let dst_annot = List.nth_exn dst_annots 0 in
                        let call_annot, trans_annots =
                          D.call
                            builder
                            ?hint:
                              (get_hintss
                                 (stack, v)
                                 (stack', callee.fun_init)
                                 (stack', callee.fun_exit)
                                 (stack, dst))
                            (rm_loc args)
                            (rm_loc callee.fun_params)
                            (Action_ops.tyctx_scope callee.fun_scope)
                            (rm_loc callee.fun_returns)
                            (rm_loc xs)
                            (Action_ops.tyctx_scope func.fun_scope)
                            dst_annot
                            lev
                        in
                        let called_annot =
                          do_fun is_global ctx stack' callee lev call_annot
                        in
                        let transed_annots =
                          List.map trans_annots ~f:(fun (trans_lev, trans_annot) ->
                              ( trans_lev
                              , do_fun
                                  false
                                  Prefix_map.empty
                                  stack'
                                  callee
                                  trans_lev
                                  trans_annot ))
                        in
                        let src_annot =
                          D.return
                            builder
                            ?hint:
                              (get_hintss
                                 (stack, v)
                                 (stack', callee.fun_init)
                                 (stack', callee.fun_exit)
                                 (stack, dst))
                            (rm_loc args)
                            (rm_loc callee.fun_params)
                            (Action_ops.tyctx_scope callee.fun_scope)
                            (rm_loc callee.fun_returns)
                            (rm_loc xs)
                            (Action_ops.tyctx_scope func.fun_scope)
                            dst_annot
                            called_annot
                            transed_annots
                            lev
                        in
                        src_annot
                    in
                    match acc with
                    | None -> Some src_annot
                    | Some pre_annot ->
                      D.identify_annots builder pre_annot src_annot;
                      Some pre_annot)
                  None
                  func.fun_cfg
                  v
              in
              let src_annot =
                match src_annot with
                | None ->
                  assert (Cfg.equal_vertex v func.fun_exit);
                  to_annot
                | Some annot -> annot
              in
              let annot =
                match Vertex_table.find_exn pannots v with
                | `Done pre_annot ->
                  D.identify_annots builder pre_annot src_annot;
                  pre_annot
                | `Doing -> src_annot
                | `Todo -> assert false
              in
              Vertex_table.set pannots ~key:v ~data:(`Done annot);
              annot
          in
          let from_annot = dfs func.fun_init in
          if is_global
          then
            Cfg.iter_vertex
              (fun v ->
                let opt_annot =
                  Vertex_table.find_and_call
                    pannots
                    v
                    ~if_found:(function
                      | `Done annot -> Some annot
                      | _ -> assert false)
                    ~if_not_found:(fun _ -> None)
                in
                opt_annot
                |> Option.iter ~f:(fun annot ->
                       Config_table.set gannots ~key:(stack, v) ~data:annot))
              func.fun_cfg;
          (match !rec_annot with
          | Some annot -> D.identify_annots builder from_annot annot
          | None -> ());
          from_annot)
    in
    let main_func = Cfg.main_func prog in
    let goal =
      do_fun
        true
        Prefix_map.empty
        C.empty
        main_func
        D.toplevel
        (D.gen_query_annot builder)
    in
    goal, gannots
  ;;

  let analyze ?init ?hints ?(stat = false) prog =
    let main_func = Cfg.main_func prog in
    let builder = D.new_builder () in
    let goal, annots =
      if stat
      then Timer.wrap_duration "vc-generation" (fun () -> annot_cg builder ?hints prog)
      else annot_cg builder ?hints prog
    in
    let%bind () =
      let hint = Option.map hints ~f:(fun hints -> hints (C.empty, main_func.fun_init)) in
      if stat
      then
        Timer.wrap_duration "vc-solving" (fun () ->
            D.solve
              ~stat
              builder
              ?init
              ?hint
              main_func.fun_scope
              (rm_loc main_func.fun_params)
              goal)
      else
        D.solve
          ~stat
          builder
          ?init
          ?hint
          main_func.fun_scope
          (rm_loc main_func.fun_params)
          goal
    in
    let cache = Config_table.create () in
    Ok
      (fun v ->
        match Config_table.find cache v with
        | Some sol -> sol
        | None ->
          let annot = Config_table.find_exn annots v in
          let sol = D.gen_solution builder annot in
          Config_table.set cache ~key:v ~data:sol;
          sol)
  ;;
end
