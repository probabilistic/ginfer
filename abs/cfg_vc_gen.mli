module Make0 : functor (D : Vc_type.VCOND0) (C : Ctxt_type.CFG_CONTEXT) ->
  Vc_type.CFG_VCOND0
    with type solution = D.solution
     and type hint = D.hint
     and type context = C.t
