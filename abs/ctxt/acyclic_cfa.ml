open Core

module Make () : Ctxt_type.CFG_CONTEXT = struct
  type t = Cfg.vertex list [@@deriving sexp, compare, equal, hash]

  let empty = []
  let extend v ctx = v :: List.take_while ctx ~f:(fun u -> not (Cfg.equal_vertex u v))

  let print fmt ctx =
    Format.fprintf fmt "[";
    List.iter ctx ~f:(fun v -> Format.fprintf fmt " %a " Cfg.print_vertex v);
    Format.fprintf fmt "]"
  ;;

  let to_string ctx =
    List.fold_right
      ctx
      ~f:(fun v acc -> "C" ^ Cfg.vertex_to_string v ^ "_" ^ acc)
      ~init:""
  ;;
end
