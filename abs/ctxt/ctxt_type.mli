(** Calling context abstraction *)
module type CFG_CONTEXT = sig
  type t [@@deriving sexp, compare, equal, hash]

  val empty : t
  val extend : Cfg.vertex -> t -> t
  val print : Format.formatter -> t -> unit
  val to_string : t -> string
end
