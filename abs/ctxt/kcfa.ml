open Core

module Make (K : sig
  val k : int
end) : Ctxt_type.CFG_CONTEXT = struct
  type t = Cfg.vertex list [@@deriving sexp, compare, equal, hash]

  let empty = []
  let extend v ctx = List.take (v :: ctx) K.k

  let print fmt ctx =
    Format.fprintf fmt "[";
    List.iter ctx ~f:(fun v -> Format.fprintf fmt " %a " Cfg.print_vertex v);
    Format.fprintf fmt "]"
  ;;

  let to_string ctx =
    List.fold_right
      ctx
      ~f:(fun v acc -> "C" ^ Cfg.vertex_to_string v ^ "_" ^ acc)
      ~init:""
  ;;
end
