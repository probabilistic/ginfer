module Make : functor
  (K : sig
     val k : int
   end)
  -> Ctxt_type.CFG_CONTEXT
