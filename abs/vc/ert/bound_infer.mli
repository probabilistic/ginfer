module Make0 : functor
  (P : Pot.S)
  (V : sig
     val deg : int
     val mom : int
     val obj : [ `Upper | `Lower ]
   end)
  -> sig
  include Vc_type.VCOND0 with type hint = P.Flt_poly.t list and type solution = P.solution
end
