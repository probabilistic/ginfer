open Core
open Action_types
open Action_ops
open Polynomial
open Result.Let_syntax

exception Lp_failure

let () =
  Caml.Printexc.register_printer (function
      | Lp_failure -> Some "analysis failure: LP failed"
      | _ -> None)
;;

module String_list_table = Hashtbl.Make (struct
  type t = string list * int [@@deriving sexp, compare, hash]
end)

module Make0
    (P : Pot.S) (V : sig
      val deg : int
      val mom : int
      val obj : [ `Upper | `Lower ]
    end) =
struct
  module Flt = P.Flt
  module Flt_poly = P.Flt_poly

  type builder = P.lp_manager
  type level = int * int * Monom.t [@@deriving sexp, compare, equal, hash]
  type annotation = P.annotation array * P.annotation array
  type solution = P.solution
  type hint = Flt_poly.t list

  let print_level fmt (cost_free, deg, m) =
    Format.fprintf fmt "<%d, %d, %a>" cost_free deg Monom.print m
  ;;

  let new_builder () = P.create_lp_manager ()
  let toplevel = 0, V.deg, Monom.one
  let is_trivial_level (cost_free, deg, _) = cost_free = V.mom && deg = 0

  let base_monoms =
    let thunk = String_list_table.create () in
    fun ((vars, deg) as key) ->
      match String_list_table.find thunk key with
      | Some result -> result
      | None ->
        let rec iter n last acc =
          if n = 0
          then acc
          else (
            let step =
              List.fold last ~init:[] ~f:(fun acc (monom, k) ->
                  List.foldi vars ~init:acc ~f:(fun i acc x ->
                      if i >= k then (Monom.mul_var x monom, i) :: acc else acc))
            in
            iter (n - 1) step (List.append (List.unzip step |> fst) acc))
        in
        let result = iter deg [ Monom.one, 0 ] [ Monom.one ] |> List.rev in
        String_list_table.set thunk ~key ~data:result;
        result
  ;;

  let infeasible_for focus =
    match focus with
    | None -> false
    | Some focus ->
      List.exists focus ~f:(fun p ->
          match Flt_poly.is_const p with
          | Some f -> Flt.(f < zero && not (is_zero f))
          | None -> false)
  ;;

  let rewrite_for focus deg =
    let rec iter n last acc =
      if n = 0
      then acc
      else (
        let step =
          List.fold last ~init:[] ~f:(fun acc (p, k) ->
              List.foldi focus ~init:acc ~f:(fun i acc f ->
                  if i >= k then (Flt_poly.mul p f, i) :: acc else acc))
        in
        iter (n - 1) step (List.append (List.unzip step |> fst) acc))
    in
    let result =
      iter deg [ Flt_poly.const Flt.one, 0 ] [ Flt_poly.const Flt.one ] |> List.rev
    in
    result
  ;;

  let rec poly_of_arith_expr ae =
    match Typed.destruct_arith ae with
    | `Int n -> Flt_poly.const (Flt.of_int n)
    | `Real r -> Flt_poly.const (Flt.of_float r)
    | `Frac f -> Flt_poly.const (Flt.of_mpqf f)
    | `Var (x, _) -> Flt_poly.of_monom (Monom.of_var x.txt) Flt.one
    | `BinOp (bop, ae1, ae2) ->
      let p1 = poly_of_arith_expr ae1 in
      let p2 = poly_of_arith_expr ae2 in
      (match bop with
      | Add -> Flt_poly.add p1 p2
      | Sub -> Flt_poly.sub p1 p2
      | Mul -> Flt_poly.mul p1 p2)
    | `Negate ae0 ->
      let p0 = poly_of_arith_expr ae0 in
      Flt_poly.scale Flt.minus_one p0
  ;;

  let rec poly_of_bool_expr be =
    let open Option.Let_syntax in
    match Typed.destruct_bool be with
    | `Var (x, _) -> return @@ Flt_poly.of_monom (Monom.of_var x.txt) Flt.one
    | `Bool true -> return @@ Flt_poly.const (Flt.of_int 1)
    | `Bool false -> return @@ Flt_poly.const (Flt.of_int 0)
    | `And (be1, be2) ->
      let%bind p1 = poly_of_bool_expr be1 in
      let%bind p2 = poly_of_bool_expr be2 in
      return @@ Flt_poly.mul p1 p2
    | `Or (be1, be2) ->
      let one = Flt_poly.const (Flt.of_int 1) in
      let%bind p1 = poly_of_bool_expr be1 in
      let%bind p2 = poly_of_bool_expr be2 in
      return
      @@ Flt_poly.sub one (Flt_poly.mul (Flt_poly.sub one p1) (Flt_poly.sub one p2))
    | `Not be0 ->
      let%bind p0 = poly_of_bool_expr be0 in
      return @@ Flt_poly.sub (Flt_poly.const (Flt.of_int 1)) p0
    | `RelOp _ | `Nondet -> None
  ;;

  let poly_of_prob_expr pe =
    match Typed.destruct_prob pe with
    | `Var (x, _) -> Flt_poly.of_monom (Monom.of_var x.txt) Flt.one
    | `Prob (a, b) -> Flt_poly.const (Flt.of_mpqf (Mpqf.of_frac a (a + b)))
  ;;

  let gen_new_annot0 ~cost_free st scope deg =
    Array.init (V.mom + 1) ~f:(fun i ->
        if i < cost_free
        then P.zero_annot st
        else if i = 0
        then P.const_annot st Flt.one
        else P.new_annot st (base_monoms (scope, deg)))
  ;;

  let gen_new_annot st ?hint:_ scope (cost_free, deg, _) =
    gen_new_annot0 ~cost_free st scope deg, gen_new_annot0 ~cost_free st scope deg
  ;;

  let gen_query_annot0 st =
    Array.init (V.mom + 1) ~f:(fun i ->
        P.annot_of_poly st (Flt_poly.const (if i = 0 then Flt.one else Flt.zero)))
  ;;

  let gen_query_annot st = gen_query_annot0 st, gen_query_annot0 st

  let identify_annots0 st annots1 annots2 =
    Array.iter2_exn annots1 annots2 ~f:(fun annot1 annot2 ->
        P.constrain_annot st annot1 `Eq annot2)
  ;;

  let identify_annots st (annots1_lo, annots1_hi) (annots2_lo, annots2_hi) =
    identify_annots0 st annots1_lo annots2_lo;
    identify_annots0 st annots1_hi annots2_hi
  ;;

  let extend
      st
      ?hint
      scope
      act
      (dst_annots_lo, dst_annots_hi)
      ((cost_free, deg, _) as level)
    =
    if infeasible_for hint
    then (
      (* dead code in the analyzed program *)
      let annot = gen_new_annot st scope level in
      annot)
    else (
      let hi =
        Array.mapi dst_annots_hi ~f:(fun i dst_annot ->
            match act with
            | Typed.DSkip | DAssume _ -> dst_annot
            | DScore (tick, sgn) ->
              let sgn = Option.value_exn !sgn in
              let tick_expr = poly_of_arith_expr tick in
              let tick_poly =
                Flt_poly.pow
                  i
                  (Flt_poly.add_monom (Monom.of_var "TICK") Flt.one tick_expr)
              in
              Flt_poly.fold tick_poly ~init:(P.zero_annot st) ~f:(fun ~mono ~coef acc ->
                  let this, that = Monom.split_on [ "TICK" ] mono in
                  P.add_annot
                    acc
                    (P.scale_annot
                       coef
                       (P.annot_mul_monom
                          st
                          (if sgn || (i - Monom.degree this) mod 2 = 0
                          then dst_annots_hi.(Monom.degree this)
                          else dst_annots_lo.(Monom.degree this))
                          that)))
            | DSample (x, `Int d) ->
              let moments = d.dist_mom in
              let annot = ref dst_annot in
              for k = deg downto 1 do
                let moment = moments k in
                annot
                  := P.subst_var_expo_annot
                       k
                       ( x.txt
                       , Flt_poly.const
                           (Either.value_map
                              ~first:Flt.of_float
                              ~second:Flt.of_mpqf
                              moment) )
                       !annot
              done;
              !annot
            | DSample (x, `Real d) ->
              let moments = d.dist_mom in
              let annot = ref dst_annot in
              for k = deg downto 1 do
                let moment = moments k in
                annot
                  := P.subst_var_expo_annot
                       k
                       ( x.txt
                       , Flt_poly.const
                           (Either.value_map
                              ~first:Flt.of_float
                              ~second:Flt.of_mpqf
                              moment) )
                       !annot
              done;
              !annot
            | DAssign (x, e) ->
              (match Typed.refine e with
              | `ArithExpr ae ->
                let annot = P.subst_var_annot (x.txt, poly_of_arith_expr ae) dst_annot in
                annot
              | `BoolExpr be ->
                (match poly_of_bool_expr be with
                | Some p -> P.subst_var_annot (x.txt, p) dst_annot
                | None -> P.forget_var st x.txt dst_annot)
              | `ProbExpr pe -> P.subst_var_annot (x.txt, poly_of_prob_expr pe) dst_annot)
            | DWeaken es ->
              if i > cost_free
              then (
                let hint = Option.value hint ~default:[] in
                let es = List.map es ~f:poly_of_arith_expr in
                let rewrite = rewrite_for (es @ hint) deg in
                let annot = P.weaken_annot st dst_annot rewrite in
                annot)
              else dst_annot)
      in
      let lo =
        Array.mapi dst_annots_lo ~f:(fun i dst_annot ->
            match act with
            | DSkip | DAssume _ -> dst_annot
            | DScore (tick, sgn) ->
              let sgn = Option.value_exn !sgn in
              let tick_expr = poly_of_arith_expr tick in
              let tick_poly =
                Flt_poly.pow
                  i
                  (Flt_poly.add_monom (Monom.of_var "TICK") Flt.one tick_expr)
              in
              Flt_poly.fold tick_poly ~init:(P.zero_annot st) ~f:(fun ~mono ~coef acc ->
                  let this, that = Monom.split_on [ "TICK" ] mono in
                  P.add_annot
                    acc
                    (P.scale_annot
                       coef
                       (P.annot_mul_monom
                          st
                          (if sgn || (i - Monom.degree this) mod 2 = 0
                          then dst_annots_lo.(Monom.degree this)
                          else dst_annots_hi.(Monom.degree this))
                          that)))
            | DSample (x, `Int d) ->
              let moments = d.dist_mom in
              let annot = ref dst_annot in
              for k = deg downto 1 do
                let moment = moments k in
                annot
                  := P.subst_var_expo_annot
                       k
                       ( x.txt
                       , Flt_poly.const
                           (Either.value_map
                              ~first:Flt.of_float
                              ~second:Flt.of_mpqf
                              moment) )
                       !annot
              done;
              !annot
            | DSample (x, `Real d) ->
              let moments = d.dist_mom in
              let annot = ref dst_annot in
              for k = deg downto 1 do
                let moment = moments k in
                annot
                  := P.subst_var_expo_annot
                       k
                       ( x.txt
                       , Flt_poly.const
                           (Either.value_map
                              ~first:Flt.of_float
                              ~second:Flt.of_mpqf
                              moment) )
                       !annot
              done;
              !annot
            | DAssign (x, e) ->
              (match Typed.refine e with
              | `ArithExpr ae ->
                let annot = P.subst_var_annot (x.txt, poly_of_arith_expr ae) dst_annot in
                annot
              | `BoolExpr be ->
                (match poly_of_bool_expr be with
                | Some p -> P.subst_var_annot (x.txt, p) dst_annot
                | None -> P.forget_var st x.txt dst_annot)
              | `ProbExpr pe -> P.subst_var_annot (x.txt, poly_of_prob_expr pe) dst_annot)
            | DWeaken es ->
              if i > cost_free
              then (
                let hint = Option.value hint ~default:[] in
                let es = List.map es ~f:poly_of_arith_expr in
                let rewrite = rewrite_for (es @ hint) deg in
                let annot = P.relax_annot st dst_annot rewrite in
                annot)
              else dst_annot)
      in
      lo, hi)
  ;;

  let sym_prob_branch0 x (dst_annots1, dst_annots2) =
    let p = Flt_poly.of_monom (Monom.of_var x) Flt.one in
    let q = Flt_poly.sub (Flt_poly.const Flt.one) p in
    Array.map2_exn dst_annots1 dst_annots2 ~f:(fun dst_annot1 dst_annot2 ->
        let p_annot = P.mul_poly p dst_annot1 in
        let q_annot = P.mul_poly q dst_annot2 in
        P.add_annot p_annot q_annot)
  ;;

  let prob_branch0 a b (dst_annots1, dst_annots2) =
    let p = Flt.(of_int a / of_int Int.(a + b)) in
    let q = Flt.(of_int b / of_int Int.(a + b)) in
    Array.map2_exn dst_annots1 dst_annots2 ~f:(fun dst_annot1 dst_annot2 ->
        let p_annot = P.scale_annot p dst_annot1 in
        let q_annot = P.scale_annot q dst_annot2 in
        P.add_annot p_annot q_annot)
  ;;

  let prob_branch
      _
      ?hint:_
      pe
      ((dst_annots1_lo, dst_annots1_hi), (dst_annots2_lo, dst_annots2_hi))
      _
    =
    match Typed.destruct_prob pe with
    | `Prob (p, q) ->
      ( prob_branch0 p q (dst_annots1_lo, dst_annots2_lo)
      , prob_branch0 p q (dst_annots1_hi, dst_annots2_hi) )
    | `Var (x, _) ->
      ( sym_prob_branch0 x.txt (dst_annots1_lo, dst_annots2_lo)
      , sym_prob_branch0 x.txt (dst_annots1_hi, dst_annots2_hi) )
  ;;

  let cond_branch0 st (dst_annots1, dst_annots2) =
    Array.map2_exn dst_annots1 dst_annots2 ~f:(fun dst_annot1 dst_annot2 ->
        P.constrain_annot st dst_annot1 `Eq dst_annot2;
        dst_annot1)
  ;;

  let cond_branch
      st
      ?hint:_
      _
      ((dst_annots1_lo, dst_annots1_hi), (dst_annots2_lo, dst_annots2_hi))
      _
    =
    ( cond_branch0 st (dst_annots1_lo, dst_annots2_lo)
    , cond_branch0 st (dst_annots1_hi, dst_annots2_hi) )
  ;;

  let call0 st returns xs dst_annots cost_free deg =
    let split_dst_annots =
      let acc =
        Array.foldi dst_annots ~init:Poly.zero ~f:(fun i acc dst_annot ->
            let split_dst_annot = P.split_annot_on st dst_annot xs in
            Poly.fold split_dst_annot ~init:acc ~f:(fun ~mono ~coef acc ->
                Poly.add_monom ~addi:List.append mono [ i, coef ] acc))
      in
      Poly.map
        (fun coefs ->
          let ret = Array.create ~len:(V.mom + 1) (P.zero_annot st) in
          List.iter coefs ~f:(fun (i, annot) -> ret.(i) <- annot);
          ret)
        acc
    in
    let call_annots =
      Poly.get_coeff
        ~zer:(Array.create ~len:(V.mom + 1) (P.zero_annot st))
        Monom.one
        split_dst_annots
    in
    let returned_calls =
      Array.map call_annots ~f:(fun call_annot ->
          let returned_call =
            List.fold2_exn returns xs ~init:call_annot ~f:(fun acc return x ->
                P.subst_var_annot
                  (x, Flt_poly.of_monom (Monom.of_var ("RET_" ^ return)) Flt.one)
                  acc)
          in
          let returned_call =
            List.fold returns ~init:returned_call ~f:(fun acc return ->
                P.subst_var_annot
                  ("RET_" ^ return, Flt_poly.of_monom (Monom.of_var return) Flt.one)
                  acc)
          in
          returned_call)
    in
    if deg = 0 || cost_free + 1 > V.mom
    then returned_calls, []
    else (
      let trans_annots =
        Poly.fold split_dst_annots ~init:[] ~f:(fun ~mono ~coef acc ->
            let d = Monom.degree mono in
            if d = 0
            then (
              let returned_trans =
                gen_new_annot0 ~cost_free:(cost_free + 1) st returns (deg - 1)
              in
              Array.iteri returned_trans ~f:(fun i returned_trans ->
                  returned_calls.(i) <- P.sub_annot returned_calls.(i) returned_trans);
              ((cost_free + 1, deg - 1, Monom.one), returned_trans) :: acc)
            else (
              let returned_trans =
                Array.map coef ~f:(fun coef ->
                    let returned_trans =
                      List.fold2_exn returns xs ~init:coef ~f:(fun acc return x ->
                          P.subst_var_annot
                            (x, Flt_poly.of_monom (Monom.of_var ("RET_" ^ return)) Flt.one)
                            acc)
                    in
                    let returned_trans =
                      List.fold returns ~init:returned_trans ~f:(fun acc return ->
                          P.subst_var_annot
                            ( "RET_" ^ return
                            , Flt_poly.of_monom (Monom.of_var return) Flt.one )
                            acc)
                    in
                    returned_trans)
              in
              ((cost_free + 1, deg - d, mono), returned_trans) :: acc))
      in
      returned_calls, trans_annots)
  ;;

  let call st ?hint:_ _ _ _ returns xs _ (dst_annots_lo, dst_annots_hi) (cost_free, deg, _)
    =
    let call_lo = call0 st returns xs dst_annots_lo cost_free deg in
    let call_hi = call0 st returns xs dst_annots_hi cost_free deg in
    ( (fst call_lo, fst call_hi)
    , List.map2_exn
        (snd call_lo)
        (snd call_hi)
        ~f:(fun (lev_lo, annot_lo) (lev_hi, annot_hi) ->
          assert (equal_level lev_lo lev_hi);
          lev_lo, (annot_lo, annot_hi)) )
  ;;

  let return0 st args params called_annots transed_annots =
    let returned_calleds =
      Array.map called_annots ~f:(fun called_annot ->
          let returned_called =
            List.fold2_exn args params ~init:called_annot ~f:(fun acc arg param ->
                P.subst_var_annot
                  (param, Flt_poly.of_monom (Monom.of_var ("ARG_" ^ arg)) Flt.one)
                  acc)
          in
          let returned_called =
            List.fold args ~init:returned_called ~f:(fun acc arg ->
                P.subst_var_annot
                  ("ARG_" ^ arg, Flt_poly.of_monom (Monom.of_var arg) Flt.one)
                  acc)
          in
          returned_called)
    in
    List.fold
      transed_annots
      ~init:returned_calleds
      ~f:(fun acc ((_, _, mon), transed_annots) ->
        let returned_transeds =
          Array.map transed_annots ~f:(fun transed_annot ->
              let returned_transed =
                List.fold2_exn args params ~init:transed_annot ~f:(fun acc arg param ->
                    P.subst_var_annot
                      (param, Flt_poly.of_monom (Monom.of_var ("ARG_" ^ arg)) Flt.one)
                      acc)
              in
              let returned_transed =
                List.fold args ~init:returned_transed ~f:(fun acc arg ->
                    P.subst_var_annot
                      ("ARG_" ^ arg, Flt_poly.of_monom (Monom.of_var arg) Flt.one)
                      acc)
              in
              returned_transed)
        in
        Array.map2_exn acc returned_transeds ~f:(fun acc returned_transed ->
            P.add_annot acc (P.annot_mul_monom st returned_transed mon)))
  ;;

  let return
      st
      ?hint:_
      args
      params
      _
      _
      _
      _
      _
      (called_annots_lo, called_annots_hi)
      transed_annots
      (_, _, _)
    =
    let lo =
      return0
        st
        args
        params
        called_annots_lo
        (List.map transed_annots ~f:(fun (lev, (annot, _)) -> lev, annot))
    in
    let hi =
      return0
        st
        args
        params
        called_annots_hi
        (List.map transed_annots ~f:(fun (lev, (_, annot)) -> lev, annot))
    in
    lo, hi
  ;;

  let solve st ?init ?hint:_ ?(stat = false) ctx vars (goal_los, goal_his) =
    let goal_lo, goal_hi = Array.last goal_los, Array.last goal_his in
    let init = Option.value init ~default:(Typed.mk_bool ~loc:Location.none true) in
    let smt = smt_of_bool_expr init in
    let mk_var x =
      match tyctx_get_exn ctx (Location.mknoloc x) with
      | `TyInt | `TyBool -> Smt.mk_ivar x
      | `TyReal | `TyProb -> Smt.mk_rvar x
    in
    let eval_var m x =
      match tyctx_get_exn ctx (Location.mknoloc x) with
      | `TyInt | `TyBool -> `Int (Smt.eval_ivar m x)
      | `TyReal | `TyProb -> Smt.eval_rvar m x
    in
    let env =
      List.map vars ~f:(fun x ->
          let ty = tyctx_get_exn ctx (Location.mknoloc x) in
          match ty with
          | `TyInt | `TyReal ->
            Smt.mk_and
              (Smt.mk_ge (Smt.mk_abs (mk_var x)) (Smt.mk_const 10))
              (Smt.mk_le (Smt.mk_abs (mk_var x)) (Smt.mk_const 110))
          | `TyBool | `TyProb ->
            Smt.mk_and
              (Smt.mk_ge (mk_var x) (Smt.mk_const 0))
              (Smt.mk_le (mk_var x) (Smt.mk_const 1)))
    in
    let env2 =
      List.cartesian_product vars vars
      |> List.filter_map ~f:(fun (x, y) ->
             if String.compare x y > 0
                &&
                match
                  ( tyctx_get_exn ctx (Location.mknoloc x)
                  , tyctx_get_exn ctx (Location.mknoloc y) )
                with
                | `TyInt, `TyInt | `TyReal, `TyReal -> true
                | _ -> false
             then
               Some
                 (Smt.mk_ge
                    (Smt.mk_abs (Smt.mk_minus (mk_var x) (mk_var y)))
                    (Smt.mk_const 10))
             else None)
    in
    let%bind model = Result.try_with (fun () -> Smt.fit (smt :: List.append env env2)) in
    let binds = List.map vars ~f:(fun x -> x, eval_var model x) in
    let res =
      match V.obj with
      | `Upper -> P.solve_min ~stat st goal_hi binds
      | `Lower -> P.solve_max ~stat st goal_lo binds
    in
    if not res then Error Lp_failure else Ok ()
  ;;

  let gen_solution st (annots_lo, annots_hi) =
    match V.obj with
    | `Upper -> P.obtain_sol st (Array.last annots_hi)
    | `Lower -> P.obtain_sol st (Array.last annots_lo)
  ;;

  let print_annot fmt (annots_lo, annots_hi) =
    Array.iteri
      (match V.obj with
      | `Upper -> annots_hi
      | `Lower -> annots_lo)
      ~f:(fun i annot -> Format.fprintf fmt "\tmom %d = %a@." i P.print_annot annot)
  ;;

  let print_sol fmt sol =
    Format.fprintf
      fmt
      "\tmoment %d %s %a@."
      V.mom
      (match V.obj with
      | `Upper -> "<="
      | `Lower -> ">=")
      P.print_sol
      sol
  ;;
end
