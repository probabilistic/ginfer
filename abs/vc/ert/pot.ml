open Core
open Polynomial
open Ffloat

module type S = sig
  type lp_manager

  module Flt : FLOAT
  module Flt_poly : Polynomial.FLOAT_POLY with type elt = Flt.t

  val create_lp_manager : unit -> lp_manager

  type annotation

  val print_annot : Format.formatter -> annotation -> unit
  val zero_annot : lp_manager -> annotation
  val new_annot : lp_manager -> Polynomial.Monom.t list -> annotation
  val annot_of_poly : lp_manager -> Flt_poly.t -> annotation
  val subst_var_annot : string * Flt_poly.t -> annotation -> annotation
  val subst_var_expo_annot : int -> string * Flt_poly.t -> annotation -> annotation
  val mul_poly : Flt_poly.t -> annotation -> annotation

  val constrain_annot
    :  lp_manager
    -> annotation
    -> [ `Ge | `Le | `Eq ]
    -> annotation
    -> unit

  val const_interval_annot : lp_manager -> Flt.t option -> Flt.t option -> annotation
  val const_annot : lp_manager -> Flt.t -> annotation
  val scale_annot : Flt.t -> annotation -> annotation
  val add_annot : annotation -> annotation -> annotation
  val sub_annot : annotation -> annotation -> annotation
  val weaken_annot : lp_manager -> annotation -> Flt_poly.t list -> annotation
  val relax_annot : lp_manager -> annotation -> Flt_poly.t list -> annotation

  val split_annot_on
    :  lp_manager
    -> annotation
    -> string list
    -> annotation Polynomial.Poly.t

  val forget_var : lp_manager -> string -> annotation -> annotation
  val annot_mul_monom : lp_manager -> annotation -> Polynomial.Monom.t -> annotation
  val annot_has_monom : lp_manager -> annotation -> Polynomial.Monom.t -> bool

  type solution

  val print_sol : Format.formatter -> solution -> unit

  val solve_min
    :  ?stat:bool
    -> lp_manager
    -> annotation
    -> (string * [ `Real of float | `Frac of Mpqf.t | `Int of int ]) list
    -> bool

  val solve_max
    :  ?stat:bool
    -> lp_manager
    -> annotation
    -> (string * [ `Real of float | `Frac of Mpqf.t | `Int of int ]) list
    -> bool

  val obtain_sol : lp_manager -> annotation -> solution
  val add_sol : Flt.t -> solution -> solution -> solution
  val pow_sol : int -> solution -> solution
  val mul_sol : solution -> solution -> solution
  val zero_sol : solution
end

module Make (Flt : FLOAT) (Solver : Lp.SOLVER with type lp_flt := Flt.t) = struct
  module Flt = Flt
  module Flt_poly = Make_float_poly (Flt)

  type lp_manager = Solver.lp_manager

  let create_lp_manager () = Solver.create_lp_manager ()

  type lpvar = Solver.lp_var [@@deriving sexp, compare]

  let new_lpvar st = Solver.new_lpvar st

  module LP_var_map = Map.Make (struct
    type t = lpvar [@@deriving sexp, compare]
  end)

  type linexpr =
    { le_coef : Flt.t LP_var_map.t
    ; le_cst : Flt.t
    }

  let print_linexpr fmt le =
    Format.fprintf fmt "@[<h>";
    let is_zero =
      LP_var_map.fold le.le_coef ~init:true ~f:(fun ~key:lv ~data:kv acc ->
          if not acc then Format.fprintf fmt " + ";
          Format.fprintf fmt "%a * v%d" Flt.pp kv lv;
          false)
    in
    let is_zero =
      if Flt.is_zero le.le_cst
      then is_zero
      else (
        if not is_zero then Format.fprintf fmt " + ";
        Format.fprintf fmt "%a" Flt.pp le.le_cst;
        false)
    in
    if is_zero then Format.fprintf fmt "0";
    Format.fprintf fmt "@]"
  ;;

  let zero_linexpr = { le_coef = LP_var_map.empty; le_cst = Flt.zero }
  let linexpr_iszero le = LP_var_map.is_empty le.le_coef && Flt.is_zero le.le_cst

  let linexpr_minimize le =
    { le with le_coef = LP_var_map.filter le.le_coef ~f:(fun kv -> not Flt.(is_zero kv)) }
  ;;

  let linexpr_add le1 le2 =
    { le_coef =
        LP_var_map.merge
          ~f:
            (fun ~key:_ -> function
              | `Both (kv1, kv2) -> Some Flt.(kv1 + kv2)
              | `Left kv1 -> Some kv1
              | `Right kv2 -> Some kv2)
          le1.le_coef
          le2.le_coef
    ; le_cst = Flt.(le1.le_cst + le2.le_cst)
    }
    |> linexpr_minimize
  ;;

  (** [linexpr_addmul le2 k le1] computs [le2 + k * le1]. *)
  let linexpr_addmul le2 k le1 =
    { le_coef =
        LP_var_map.merge le1.le_coef le2.le_coef ~f:(fun ~key:_ -> function
          | `Both (kv1, kv2) -> Some Flt.((k * kv1) + kv2)
          | `Left kv1 -> Some Flt.(k * kv1)
          | `Right kv2 -> Some kv2)
    ; le_cst = Flt.((k * le1.le_cst) + le2.le_cst)
    }
    |> linexpr_minimize
  ;;

  let linexpr_scale k le =
    { le_coef = LP_var_map.map le.le_coef ~f:Flt.(( * ) k); le_cst = Flt.(k * le.le_cst) }
    |> linexpr_minimize
  ;;

  let add_lprow_array st ?(k = Flt.zero) arr o = Solver.add_lprow_array st ~k arr o

  let add_lprow st ?(k = Flt.zero) le o =
    add_lprow_array
      st
      ~k:Flt.(k - le.le_cst)
      (le.le_coef |> LP_var_map.to_alist |> Array.of_list)
      o
  ;;

  type annotation = linexpr Poly.t

  let swap_order = function
    | `Le -> `Ge
    | `Ge -> `Le
    | `Eq -> `Eq
  ;;

  let constrain_annot st annot1 o annot2 =
    Poly.void_merge annot1 annot2 ~f:(fun ~mono:_ -> function
      | `Both (le1, le2) ->
        let le = linexpr_addmul le1 Flt.minus_one le2 in
        add_lprow st le o
      | `Left le1 -> add_lprow st le1 o
      | `Right le2 -> add_lprow st le2 (swap_order o))
  ;;

  let const_interval_annot st l r =
    let lv = new_lpvar st in
    let le = { le_coef = LP_var_map.singleton lv Flt.one; le_cst = Flt.zero } in
    l |> Option.iter ~f:(fun l -> add_lprow st le `Ge ~k:l);
    r |> Option.iter ~f:(fun r -> add_lprow st le `Le ~k:r);
    Poly.of_monom Monom.one le
  ;;

  let const_annot _ k =
    let le = { le_coef = LP_var_map.empty; le_cst = k } in
    Poly.of_monom Monom.one le
  ;;

  let scale_annot k annot = Poly.map ~is_zero:linexpr_iszero (linexpr_scale k) annot

  (** [addmul annot1 k annot2] computes [annot1 + k * annot2]. *)
  let addmul annot1 k annot2 =
    Poly.add_scale
      ~is_zero:linexpr_iszero
      ~addi:linexpr_add
      ~mult:linexpr_scale
      k
      annot2
      annot1
  ;;

  let add_annot annot1 annot2 = addmul annot1 Flt.one annot2
  let sub_annot annot1 annot2 = addmul annot1 Flt.minus_one annot2

  let annot_of_poly _ p =
    Flt_poly.fold p ~init:Poly.zero ~f:(fun ~mono ~coef acc ->
        let le = { le_coef = LP_var_map.empty; le_cst = coef } in
        Poly.add_monom ~is_zero:linexpr_iszero ~addi:linexpr_add mono le acc)
  ;;

  let add_poly le p annot =
    Poly.fold p ~init:annot ~f:(fun ~mono ~coef acc ->
        Poly.add_monom
          ~is_zero:linexpr_iszero
          ~addi:linexpr_add
          mono
          (linexpr_scale coef le)
          acc)
  ;;

  let mul_monom m annot =
    Poly.fold annot ~init:Poly.zero ~f:(fun ~mono ~coef acc ->
        Poly.add_monom
          ~is_zero:linexpr_iszero
          ~addi:linexpr_add
          (Monom.mul mono m)
          coef
          acc)
  ;;

  let mul_poly p annot =
    Flt_poly.fold p ~init:Poly.zero ~f:(fun ~mono ~coef acc ->
        Poly.add
          ~is_zero:linexpr_iszero
          ~addi:linexpr_add
          acc
          (scale_annot coef @@ mul_monom mono annot))
  ;;

  let subst_var_annot (x, e) annot =
    let subst e =
      Poly.fold annot ~init:Poly.zero ~f:(fun ~mono ~coef acc ->
          add_poly coef (Flt_poly.monom_subst x e mono) acc)
    in
    subst e
  ;;

  let subst_var_expo_annot expo (x, e) annot =
    let subst e =
      Poly.fold annot ~init:Poly.zero ~f:(fun ~mono ~coef acc ->
          add_poly coef (Flt_poly.monom_subst_expo expo x e mono) acc)
    in
    subst e
  ;;

  let new_annot st base =
    List.fold base ~init:Poly.zero ~f:(fun acc monom ->
        Poly.add_monom
          ~is_zero:linexpr_iszero
          ~addi:linexpr_add
          monom
          (let lv = new_lpvar st in
           let le = { le_coef = LP_var_map.singleton lv Flt.one; le_cst = Flt.zero } in
           le)
          acc)
  ;;

  let zero_annot _ = Poly.zero
  let print_annot fmt annot = Poly.print print_linexpr fmt annot

  let weaken_annot st annot rewrite =
    let annot =
      List.fold_left rewrite ~init:annot ~f:(fun acc p ->
          let lv = new_lpvar st in
          let le = { le_coef = LP_var_map.singleton lv Flt.one; le_cst = Flt.zero } in
          add_lprow st le `Ge;
          add_poly le p acc)
    in
    annot
  ;;

  let relax_annot st annot rewrite =
    let annot =
      List.fold_left rewrite ~init:annot ~f:(fun acc p ->
          let lv = new_lpvar st in
          let le = { le_coef = LP_var_map.singleton lv Flt.one; le_cst = Flt.zero } in
          add_lprow st le `Le;
          add_poly le p acc)
    in
    annot
  ;;

  let split_annot_on _ annot vars =
    let acc =
      Poly.fold annot ~init:Poly.zero ~f:(fun ~mono ~coef acc ->
          let mon1, mon2 = Monom.split_on vars mono in
          Poly.add_monom ~addi:add_annot mon2 (Poly.of_monom mon1 coef) acc)
    in
    acc
  ;;

  let forget_var st var annot =
    Poly.fold annot ~init:Poly.zero ~f:(fun ~mono ~coef acc ->
        if Monom.var_exists (String.equal var) mono
        then (
          add_lprow st coef `Eq;
          acc)
        else Poly.add_monom ~is_zero:linexpr_iszero ~addi:linexpr_add mono coef acc)
  ;;

  let annot_mul_monom st annot monom =
    Poly.fold annot ~init:(zero_annot st) ~f:(fun ~mono ~coef acc ->
        Poly.add_monom
          ~is_zero:linexpr_iszero
          ~addi:linexpr_add
          (Monom.mul mono monom)
          coef
          acc)
  ;;

  let annot_has_monom _ annot monom = Poly.monom_exists monom annot

  type solution = Flt_poly.t

  let print_sol fmt sol = Flt_poly.print fmt sol

  let annot_constant st annot =
    Poly.void_merge annot (zero_annot st) ~f:(fun ~mono -> function
      | `Both (le1, le2) ->
        if not (Monom.is_one mono)
        then (
          let le = linexpr_addmul le1 Flt.minus_one le2 in
          add_lprow st le `Eq)
      | `Left le1 -> if not (Monom.is_one mono) then add_lprow st le1 `Eq
      | `Right le2 -> if not (Monom.is_one mono) then add_lprow st le2 `Eq)
  ;;

  let solve_min ?(stat = false) st goal binds =
    let goal' =
      List.fold binds ~init:goal ~f:(fun acc (x, n) ->
          subst_var_annot
            ( x
            , Flt_poly.const
                (match n with
                | `Int n -> Flt.of_int n
                | `Real n -> Flt.of_float n
                | `Frac n -> Flt.of_mpqf n) )
            acc)
    in
    annot_constant st goal';
    let coeff = Poly.get_coeff ~zer:zero_linexpr Monom.one goal' in
    Solver.change_objective_coefficients
      st
      (LP_var_map.to_alist coeff.le_coef |> Array.of_list);
    Solver.set_log_level st 0;
    if stat then Solver.output_stats st;
    Solver.initial_solve st `Minimize;
    Solver.status st = 0
  ;;

  let solve_max ?(stat = false) st goal binds =
    let goal' =
      List.fold binds ~init:goal ~f:(fun acc (x, n) ->
          subst_var_annot
            ( x
            , Flt_poly.const
                (match n with
                | `Int n -> Flt.of_int n
                | `Real n -> Flt.of_float n
                | `Frac n -> Flt.of_mpqf n) )
            acc)
    in
    annot_constant st goal';
    let coeff = Poly.get_coeff ~zer:zero_linexpr Monom.one goal' in
    Solver.change_objective_coefficients
      st
      (LP_var_map.to_alist coeff.le_coef |> Array.of_list);
    Solver.set_log_level st 0;
    if stat then Solver.output_stats st;
    Solver.initial_solve st `Maximize;
    Solver.status st = 0
  ;;

  let obtain_sol st =
    let sol = Solver.primal_column_solution st in
    fun annot ->
      Poly.map
        ~is_zero:Flt.is_zero
        (fun le ->
          Flt.(
            le.le_cst
            + LP_var_map.fold le.le_coef ~init:Flt.zero ~f:(fun ~key:lv ~data:kv acc ->
                  Flt.(acc + (kv * sol.(lv))))))
        annot
  ;;

  let add_sol scal sol1 sol2 = Flt_poly.add_scale scal sol1 sol2
  let pow_sol n sol = Flt_poly.pow n sol
  let mul_sol sol1 sol2 = Flt_poly.mul sol1 sol2
  let zero_sol = Flt_poly.zero
end
