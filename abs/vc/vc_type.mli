open Action_types
open Action_ops

module type VCOND0 = sig
  type builder
  type level [@@deriving sexp, compare, equal, hash]
  type annotation
  type solution
  type hint

  val new_builder : unit -> builder
  val toplevel : level
  val is_trivial_level : level -> bool
  val gen_new_annot : builder -> ?hint:hint -> string list -> level -> annotation
  val gen_query_annot : builder -> annotation
  val identify_annots : builder -> annotation -> annotation -> unit

  val extend
    :  builder
    -> ?hint:hint
    -> string list
    -> Typed.data_action
    -> annotation
    -> level
    -> annotation

  val prob_branch
    :  builder
    -> ?hint:hint * hint
    -> Typed.prob_expr
    -> annotation * annotation
    -> level
    -> annotation

  val cond_branch
    :  builder
    -> ?hint:hint * hint
    -> Typed.bool_expr
    -> annotation * annotation
    -> level
    -> annotation

  val call
    :  builder
    -> ?hint:hint * hint * hint * hint
    -> string list
    -> string list
    -> string list
    -> string list
    -> string list
    -> string list
    -> annotation
    -> level
    -> annotation * (level * annotation) list

  val return
    :  builder
    -> ?hint:hint * hint * hint * hint
    -> string list
    -> string list
    -> string list
    -> string list
    -> string list
    -> string list
    -> annotation
    -> annotation
    -> (level * annotation) list
    -> level
    -> annotation

  val solve
    :  builder
    -> ?init:Typed.bool_expr
    -> ?hint:hint
    -> ?stat:bool
    -> Typed.rhs_ty tycontext
    -> string list
    -> annotation
    -> (unit, exn) Result.t

  val gen_solution : builder -> annotation -> solution
  val print_annot : Format.formatter -> annotation -> unit
  val print_sol : Format.formatter -> solution -> unit
  val print_level : Format.formatter -> level -> unit
end

module type CFG_VCOND0 = sig
  type solution
  type hint
  type context

  val analyze
    :  ?init:Typed.bool_expr
    -> ?hints:(context * Cfg.vertex -> hint)
    -> ?stat:bool
    -> Cfg.program
    -> (context * Cfg.vertex -> solution, exn) Result.t
end
