open Ffloat

module type S = sig
  type lp_manager

  module Flt : FLOAT
  module Flt_poly : Polynomial.FLOAT_POLY with type elt = Flt.t

  val create_lp_manager : unit -> lp_manager

  type annotation

  val print_annot : Format.formatter -> annotation -> unit
  val zero_annot : lp_manager -> annotation
  val new_annot : lp_manager -> Polynomial.Monom.t list -> annotation
  val annot_of_poly : lp_manager -> Flt_poly.t -> annotation
  val subst_var_annot : string * Flt_poly.t -> annotation -> annotation
  val subst_var_expo_annot : int -> string * Flt_poly.t -> annotation -> annotation
  val mul_poly : Flt_poly.t -> annotation -> annotation

  val constrain_annot
    :  lp_manager
    -> annotation
    -> [ `Ge | `Le | `Eq ]
    -> annotation
    -> unit

  val const_interval_annot : lp_manager -> Flt.t option -> Flt.t option -> annotation
  val const_annot : lp_manager -> Flt.t -> annotation
  val scale_annot : Flt.t -> annotation -> annotation
  val add_annot : annotation -> annotation -> annotation
  val sub_annot : annotation -> annotation -> annotation
  val weaken_annot : lp_manager -> annotation -> Flt_poly.t list -> annotation
  val relax_annot : lp_manager -> annotation -> Flt_poly.t list -> annotation

  val split_annot_on
    :  lp_manager
    -> annotation
    -> string list
    -> annotation Polynomial.Poly.t

  val forget_var : lp_manager -> string -> annotation -> annotation
  val annot_mul_monom : lp_manager -> annotation -> Polynomial.Monom.t -> annotation
  val annot_has_monom : lp_manager -> annotation -> Polynomial.Monom.t -> bool

  type solution

  val print_sol : Format.formatter -> solution -> unit

  val solve_min
    :  ?stat:bool
    -> lp_manager
    -> annotation
    -> (string * [ `Real of float | `Frac of Mpqf.t | `Int of int ]) list
    -> bool

  val solve_max
    :  ?stat:bool
    -> lp_manager
    -> annotation
    -> (string * [ `Real of float | `Frac of Mpqf.t | `Int of int ]) list
    -> bool

  val obtain_sol : lp_manager -> annotation -> solution
  val add_sol : Flt.t -> solution -> solution -> solution
  val pow_sol : int -> solution -> solution
  val mul_sol : solution -> solution -> solution
  val zero_sol : solution
end
