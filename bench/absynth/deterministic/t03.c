void f(int x, int n) {
  while (x > 0) {
    x = x - 1;
    if (x > n) {
      x = n;
    }
    tick(1);
  }
}
