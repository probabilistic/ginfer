// 2-d random walk
// d = |x| + |y|

void f(int d, int n) {
  int x, y;

  while (d < n) {
    if (x > 0) {
      if (y > 0) {
        if (prob(1,3)) {
          x = x + 2;
          d = d + 2;
        } else {
          if (prob(1,2)) {
            y = y + 2;
            d = d + 2;
          } else {
            if (prob(1,1)) {
              x = x - 1;
              d = d - 1;
            } else {
              y = y - 1;
              d = d - 1;
            }
          }
        }
      } else {
        if (y < 0) {
          if (prob(1,3)) {
            x = x + 2;
            d = d + 2;
          } else {
            if (prob(1,2)) {
              y = y + 1;
              d = d - 1;
            } else {
              if (prob(1,1)) {
                x = x - 1;
                d = d - 1;
              } else {
                y = y - 2;
                d = d + 2;
              }
            }
          }
        } else {
          if (prob(1,3)) {
            x = x + 2;
            d = d + 2;
          } else {
            if (prob(1,2)) {
              y = y + 1;
              d = d + 1;
            } else {
              if (prob(1,1)) {
                x = x - 1;
                d = d - 1;
              } else {
                y = y - 1;
                d = d + 1;
              }
            }
          }
        }
      }
    } else {
      if (x < 0) {
        if (y > 0) {
          if (prob(1,3)) {
            x = x + 1;
            d = d - 1;
          } else {
            if (prob(1,2)) {
              y = y + 2;
              d = d + 2;
            } else {
              if (prob(1,1)) {
                x = x - 2;
                d = d + 2;
              } else {
                y = y - 1;
                d = d - 1;
              }
            }
          }
        } else {
          if (y < 0) {
            if (prob(1,3)) {
              x = x + 1;
              d = d - 1;
            } else {
              if (prob(1,2)) {
                y = y + 1;
                d = d - 1;
              } else {
                if (prob(1,1)) {
                  x = x - 2;
                  d = d + 2;
                } else {
                  y = y - 2;
                  d = d + 2;
                }
              }
            }
          } else {
            if (prob(1,3)) {
              x = x + 1;
              d = d - 1;
            } else {
              if (prob(1,2)) {
                y = y + 1;
                d = d + 1;
              } else {
                if (prob(1,1)) {
                  x = x - 2;
                  d = d + 2;
                } else {
                  y = y - 1;
                  d = d + 1;
                }
              }
            }
          }
        }
      } else {
        if (y > 0) {
          if (prob(1,3)) {
            x = x + 1;
            d = d + 1;
          } else {
            if (prob(1,2)) {
              y = y + 2;
              d = d + 2;
            } else {
              if (prob(1,1)) {
                x = x - 1;
                d = d + 1;
              } else {
                y = y - 1;
                d = d - 1;
              }
            }
          }
        } else {
          if (y < 0) {
            if (prob(1,3)) {
              x = x + 1;
              d = d + 1;
            } else {
              if (prob(1,2)) {
                y = y + 1;
                d = d - 1;
              } else {
                if (prob(1,1)) {
                  x = x - 1;
                  d = d + 1;
                } else {
                  y = y - 2;
                  d = d + 2;
                }
              }
            }
          } else {
            if (prob(1,3)) {
              x = x + 1;
              d = d + 1;
            } else {
              if (prob(1,2)) {
                y = y + 1;
                d = d + 1;
              } else {
                if (prob(1,1)) {
                  x = x - 1;
                  d = d + 1;
                } else {
                  y = y - 1;
                  d = d + 1;
                }
              }
            }
          }
        }
      }
    }
    tick(1);
  }
}
