// t09 in C4B figure 8

void f(int x) {
  int i, j;

  i = 1;
  j = 0;
  while (j < x) {
    j = j + 1;
    if (i >= 4) {
      i = 1;
      tick(40);
    } else {
      i = i + Unif(1,3);
    }
    tick(1);
  }
}
