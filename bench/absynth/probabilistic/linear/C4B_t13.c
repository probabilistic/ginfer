// t13 in C4B figure 8

void f(int x, int y) {
  while (x > 0) {
    x = x - 1;
    if (prob(1,3)) {
      y = y + 1;
    } else {
      while (y > 0) {
        y = y - 1;
        tick(1);
      }
    }
    tick(1);
  }
}
