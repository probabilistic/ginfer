// t15 in C4B figure 8

void f(int x, int y) {
  int t;

  while (x > y) {
    x = x - y;
    x = x - 1;
    t = y;
    while (t > 0) {
      t = t - Unif(1,2);
      tick(1);
    }
    tick(1);
  }
}
