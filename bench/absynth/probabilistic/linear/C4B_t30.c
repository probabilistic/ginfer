// t30 in C4B figure 8

void f(int x, int y) {
  int t;

  while (x > 0) {
    x = x - Unif(1,3);
    t = x;
    x = y;
    y = t;
    tick(1);
  }
}
