// tick N = 10

void f(int l) {
  while (l >= 8) {
    l = l - Unif(6,8);
    tick(10);
  }

  while (l > 0) {
    l = l - 1;
    tick(1);
  }
}
