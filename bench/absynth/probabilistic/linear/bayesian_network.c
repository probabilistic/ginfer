// Example of Bayesian Network
// Perform n iterations over the network with 5 nodes

void f(int n) {
  int i, d, s, l, g, z;

  while (n > 0) {
    i = Ber(3,10);
    z = z + 1;
    d = Ber(2,5);
    tick(1);

    if (i < 1 && d < 1) {
      g = Ber(7,10);
      tick(1);
    } else {
      if (i < 1 && d > 0) {
        g = Ber(19,20);
        tick(1);
      } else {
        if (i > 0 && d < 1) {
          g = Ber(1,10);
          tick(1);
        } else {
          g = Ber(1,2);
          tick(1);
        }
      }
    }
    if (i < 1) {
      s = Ber(1,20);
      tick(1);
    } else {
      s = Ber(4,5);
      tick(1);
    }
    if (g < 1) {
      l = Ber(1,10);
      tick(1);
    } else {
      l = Ber(3,5);
      tick(1);
    }
    n = n - 1;
  }
}
