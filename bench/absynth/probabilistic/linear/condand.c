// conjunction conditional loop

void f(int n, int m) {
  while (n > 0 && m > 0) {
    if (prob(1,1)) {
      n = n - 1;
    } else {
      m = m - 1;
    }
    tick(1);
  }
}
