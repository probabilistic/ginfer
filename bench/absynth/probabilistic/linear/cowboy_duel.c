// The duel cowboys
// The probability A hits B is p_a = 3/4
// The probability B hits A is p_b = 1/3
// The duel ends when one hits the other
// A shoots first
// Compute the expected number of turns that the duel will end
// Exact answer, it is a geometric distribution with p = (3/4 + 1/4*1/3) = 5/6, E(1_Head) = 6/5 = 1.2

// Alternatively, 1.(3/4 + 1/4.1/3) + 2.(1/4.2/3.3/4 + 1/4.2/3.1/4.1/3) ....
// = 1.5/6 + 2.(5/6^2) + 3.(5/6^3) + ... + n.(5/6^n)
// Arithmetic-geometric sequence a = 1, b = 5/6, d = 1, r = 1/6
// t_n = [1 + (n-1).1].5/6.(1/6)^(n-1)
// limit of Sn = 5/6(6/5 + 6/25) = 1 + 1/5 = 1.2

// flag : 0: one is hit, otherwise 1
void f() {
  int flag;

  flag = 1;
  while (flag > 0) {
    if (prob(3,1)) {
      flag = 0;
    } else {
      if (prob(1,2)) {
        flag = 0;
      } else {
        flag = 1;
      }
    }
    tick(1);
  }
}
