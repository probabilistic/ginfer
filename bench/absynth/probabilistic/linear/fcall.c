// Figure 1 on left

void f(int x, int n) {
  if (x < n) {
    x = x + Unif(0,1);
    tick(1);
    f(x, n);
  }
}
