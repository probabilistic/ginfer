// filling containers

void f(int volToFill) {
  int fast, medium, slow, volMeasured;

  fast = 10;
  medium = 3;
  slow = 1;
  volMeasured = 0;
  volMeasured = volMeasured + Unif(0,1);

  while (volMeasured <= volToFill) {
    if (volToFill < volMeasured + fast) {
      volMeasured = volMeasured + Unif(9,10);
    } else {
      if (volToFill < volMeasured + medium) {
        volMeasured = volMeasured + Unif(2,4);
      } else {
        volMeasured = volMeasured + Unif(0,2);
      }
    }

    volMeasured = volMeasured + Unif(0,1);
    tick(1);
  }
}
