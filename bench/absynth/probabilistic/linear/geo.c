// Truncated geometric distribution

void f() {
  int c;

  while (true) {
    if (prob(1,1)) {
      c = 1;
      tick(1);
      break;
    } else {
      c = 0;
      tick(1);
    }
    tick(2);
  }
  tick(1);
}
