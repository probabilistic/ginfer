void f(int x) {
  while (x >= 2) {
    if (prob(1,2)) {
      x = x - 1;
    } else {
      x = x - 2;
    }
    tick(1);
  }
}
