// figure 3 on right with T = 2, p = 1/2, K1 = 2, K2 = 5

void f(int x, int n) {
  while (x < n) {
    if (prob(1,1)) {
      x = x + Unif(0,2);
    } else {
      x = x + Unif(0,5);
    }
    tick(2);
  }
}
