// interacting nested loops

void f(int y, int n) {
  while (n < 0) {
    if (prob(9,1)) {
      n = n + 1;
    }
    tick(9);
    if (demon) {
      while (y >= 100) {
        if (prob(1,1)) {
          y = y - 100;
        } else {
          y = y - 90;
        }
        tick(5);
      }
      y = y + 1000;
    }
  }
}
