// sequential loops program so that
// the second loop depends on the first
// both are probabilistic loops

void f(int x, int y) {
  while (x - y > 2) {
    y = y + Unif(1,3);
    tick(3);
  }
  while (y > 9) {
    if (prob(2,1)) {
      y = y - 10;
    } else {
      y = y - 0;
    }
    tick(1);
  }
}
