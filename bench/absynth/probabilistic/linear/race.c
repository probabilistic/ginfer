// Race between tortoise and hare

void f(int h, int t) {
  // h = 0;
  // t = 30;
  while (h <= t) {
    t = t + 1;
    if (prob(1,1)) {
      h = h + Unif(0,10);
    } else {
      h = h + 0;
    }
    tick(1);
  }
}
