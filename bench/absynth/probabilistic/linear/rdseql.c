// simulate the randomization of pushing one and always popping many

void f(int x, int y) {
  while (x > 0) {
    x = x - 1;

    if (prob(1,3)) {
      y = y + 1;
    }

    while (y > 0) {
      y = y - 1;
      tick(1);
    }

    tick(2);
  }
}
