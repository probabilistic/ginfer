// random walk with T = 1, K1 = 2, K2 = 1, p = 1/2

void f(int x, int n) {
  while (x < n) {
    if (prob(1,1)) {
      x = x + 2;
    } else {
      x = x - 1;
    }
    tick(1);
  }
}
