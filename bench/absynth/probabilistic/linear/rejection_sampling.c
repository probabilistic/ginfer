// rejection sampling

void f(int n) {
  int x, y, k;

  while (n > 0) {
    k = 1;
    while (k > 0) {
      if (prob(1,1)) {
        x = 0;
      } else {
        x = 1;
      }
      if (prob(1,1)) {
        y = 0;
      } else {
        y = 1;
      }
      if (x == y) {
        k = 1;
      } else {
        k = 0;
      }
      tick(1);
    }
    n = n - 1;
  }
}
