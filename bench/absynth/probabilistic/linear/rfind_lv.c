// Las Vegas algorithm
// An array of n≥2 elements, in which half are ‘a’s and the other half are ‘b’s.
// Find an ‘a’ in the array
// Compute the expected number of iterations

void f() {
  int flag;

  flag = 1;
  while (flag > 0) {
    if (prob(1,1)) {
      flag = 0;
    } else {
      flag = 1;
    }
    tick(1);
  }
}
