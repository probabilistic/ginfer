// model for dead reckoning robot
// 0 : Nord
// 1 : South
// 2 : Est
// 3 : West
// 4 : NE
// 5 : SE
// 6 : NW
// 7 : SW
// 8 : Stay

void f(int n) {
  while (n > 0) {
    if (prob(1,9)) {
      n = n - 1;
    } else {
      if (prob(1,8)) {
        n = n - 2;
      } else {
        if (prob(1,7)) {
          n = n - 3;
        } else {
          if (prob(1,6)) {
            n = n - 4;
          } else {
            if (prob(1,5)) {
              n = n - 5;
            } else {
              if (prob(1,4)) {
                n = n - 6;
              } else {
                if (prob(1,3)) {
                  n = n - 7;
                } else {
                  if (prob(1,2)) {
                    n = n - 8;
                  } else {
                    n = n + 0;
                  }
                }
              }
            }
          }
        }
      }
    }

    tick(1);
  }
}
