// Betting strategy for Roulette
// Amount of betting is random value from 5 to 10
// Compute the expectation of games such that the player
// have at least n units of money after

void f(int n) {
  int money;

  money = 10;
  while (money >= n) {
    // bank lost
    if (prob(36,1)) {
      if (prob(1,2)) {
        // col 1
        if (prob(1,1)) {
          // Red
          // bet = Unif(5,10);
          // money = money - bet + 1.6 * bet = money + 0.6 * bet;
          money = money + Unif(3,6);
        } else {
          // Black
          money = money + Unif(1,2);
        }
      } else {
        if (prob(1,1)) {
          // col 2
          if (prob(1,2)) {
            // Red
            money = money + Unif(3,6);
          } else {
            // Black
            money = money + Unif(1,2);
          }
        } else {
          // col 3
          if (prob(2,1)) {
            // Red
            money = money - Unif(3,6);
          }  else {
            money = money - Unif(5,10);
          }
        }
      }
    } else {
      money = money - Unif(5,10);
    }
    tick(1);
  }
}
