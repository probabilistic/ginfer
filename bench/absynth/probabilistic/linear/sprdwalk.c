// simple random walk

void f(int x, int n) {
  while (x < n) {
    x = x + Unif(0,1);
    tick(1);
  }
}
