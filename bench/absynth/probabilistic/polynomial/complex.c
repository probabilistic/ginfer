// a complex expected polynomial bound program
// it involves sequential and nested loops
// and different types of probabilistic commands

void f(int y, int w, int n, int m) {
  int x;

  x = 0;
  while (x < n) {
    x = x + Unif(0,1);
    y = y + 3 * m;
    y = y + Bin(1,2,3);
  }

  while (y > 0) {
    if (prob(1,1)) {
      while (w > 0) {
        w = w - 1;
        tick(1);
      }
      y = y - 1;
      tick(1);
    } else {
      y = y - 2;
      tick(1);
    }
  }
}
