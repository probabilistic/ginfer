// Multiple races between tortoise and hare
// Each unit of time, tortoise moves 1 step forward, with probability 3/4 hare moves a random number of steps of 1 to 3, # otherwise, stays in the same position
// Compute the expected value of time elapsed

void f(int n, int m) {
  int h, t;

  while (n > 0) {
    h = 0;
    t = m;
    while (h <= t) {
      if (prob(3,1)) {
        h = h + Unif(1,3);
      }
      t = t + 1;
      tick(1);
    }
    n = n - 1;
  }
}
