void start(int x) {
  int y;

  while (x > 0) {
    if (prob(1,2)) {
      x = x + 1;
    } else {
      x = x - 1;
    }
    y = x - 1;
    while (y > 0) {
      if (prob(1,2)) {
        y = y + 1;
      } else {
        y = y - 1;
      }
      tick(1);
    }
  }
}
