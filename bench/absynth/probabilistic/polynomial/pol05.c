void start(int x) {
  int y;

  while (x > 0) {
    if (prob(1,3)) {
      x = x + 1;
    } else {
      x = x - 1;
    }
    y = x - 1;
    while (y > 0) {
      y = y - Unif(0,3);
      tick(1);
    }
  }
}
