// deterministic version takes 0.5.max(0, n-1).max(0, n-2)
// probabilistic version takes (1/p_1)*(1/p_2).0.5.max(0, n-1).max(0, n-2)

void f(int n) {
  int m;

  while (n > 0) {
    n = n - Unif(0,1);
    m = n;

    while (m > 0) {
      if (prob(1,2)) {
        m = m - 1;
      } else {
        m = m - 0;
      }
      tick(1);
    }
  }
}
