// recursive function with polynomial bound

void qsquad(int arg_h, int arg_l) {
  int h, l, m, c;

  h = arg_h;
  l = arg_l;

  if (l < h) {
    m = l;
    c = Unif(0,1);
    while (m < h - 1 && c > 0) {
      tick(1);
      m = m + 1;
    }

    tick(1);
    arg_h = m;
    arg_l = l;
    qsquad(arg_h, arg_l);

    tick(1);
    arg_h = h;
    arg_l = m + 1;
    qsquad(arg_h, arg_l);
  }
}
