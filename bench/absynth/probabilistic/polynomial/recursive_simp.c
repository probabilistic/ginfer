// recursive function with polynomial bound

void qsquad(int arg_h, int arg_l) {
  int m;

  if (arg_l < arg_h) {
    m = arg_l;
    if (prob(1,1)) {
      while (m < arg_h - 1) {
        tick(1);
        m = m + 1;
      }
    }

    tick(1);
    qsquad(m, arg_l);

    tick(1);
    m = m + 1;
    qsquad(arg_h, m);
  }
}
