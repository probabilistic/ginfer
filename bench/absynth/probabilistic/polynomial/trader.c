void trade(int sPrice) {
  int numShares;

  numShares = Unif(0,3);
  while (numShares > 0) {
    numShares = numShares - 1;
    tick(sPrice);
  }
}

void start(int sPrice, int minPrice) {
  while (minPrice < sPrice) {
    if (prob(1,3)) {
      sPrice = sPrice + 1;
    } else {
      sPrice = sPrice - 1;
    }
    trade(sPrice);
  }
}
