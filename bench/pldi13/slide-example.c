void main() {
  int x, y, z;

  x = Unif(-5,4);
  y = Unif(-2,3);
  while (x + y <= 10) {
    z = Unif(-1,1);
    if (z >= 0) {
      x = x + 1;
      y = y + 2;
    }
    tick(1);
  }
}
