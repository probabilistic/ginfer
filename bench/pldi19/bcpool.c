void main(int y) {
  int i;

  while (y >= 1) {
    tick(y);
    i = 1;
    while (i <= y) {
      if (prob(5,9995)) {
        if (prob(99,1)) {
          tick(-5000);
        } else {
          if (demon) {
            tick(-5000);
          } else {
            tick(0);
          }
        }
      } else {
        tick(0);
      }
      i = i + 1;
    }
    if (prob(1,1)) {
      y = y - 1;
    } else {
      if (prob(1,4)) {
        y = y;
      } else {
        y = y + 1;
      }
    }
  }
}
