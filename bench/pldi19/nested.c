void main(int i) {
  int x;

  while (i >= 1) {
    x = i;
    while (x >= 1) {
      if (prob(1,3)) {
        x = x + 1;
      } else {
        x = x - 1;
      }
      if (prob(2,1)) {
        tick(1);
      } else {
        tick(-1);
      }
    }
    if (prob(1,3)) {
      i = i + 1;
    } else {
      i = i - 1;
    }
    if (prob(2,1)) {
      tick(0 - i);
    } else {
      tick(i);
    }
  }
}
