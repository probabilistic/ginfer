void main(int n) {
  int x, y;

  while (n >= 10) {
    if (prob(3,2)) {
      x = Unif(1,10);
      n = n - x;
      n = n + Unif(2,8);
      tick(5 * x);
    } else {
      y = Unif(1,10);
      n = n - y;
      n = n + Unif(2,8);
      tick(5 * y);
    }
    tick(-1/5 * n);
  }
}
