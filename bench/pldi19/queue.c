void main(int n) {
  int l1, l2, i;

  l1 = 0;
  l2 = 0;
  i = 1;
  while (i <= n) {
    if (l1 >= 1) {
      l1 = l1 - 1;
    } else {
      l1 = l1;
    }
    if (l2 >= 1) {
      l2 = l2 - 1;
    } else {
      l2 = l2;
    }
    if (prob(2,98)) {
      if (prob(2,8)) {
        l1 = l1 + 3;
      } else {
        if (prob(1,1)) {
          l2 = l2 + 2;
        } else {
          l1 = l1 + 2;
          l2 = l2 + 1;
        }
      }
      if (l1 >= l2) {
        tick(l1);
      } else {
        tick(l2);
      }
    } else {
      tick(0);
    }
    i = i + 1;
  }
}
