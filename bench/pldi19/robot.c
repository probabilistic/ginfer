void main(int x, int y) {
  while (y <= x) {
    if (prob(1,4)) {
      y = y + Unif(1,3);
    } else {
      if (prob(1,9)) {
        y = y - Unif(1,3);
      } else {
        if (prob(1,9)) {
          x = x + Unif(1,3);
        } else {
          if (prob(1,9)) {
            x = x - Unif(1,3);
          } else {
            if (prob(1,9)) {
              x = x + Unif(1,3);
              y = y + Unif(1,3);
            } else {
              if (prob(1,9)) {
                x = x + Unif(1,3);
                y = y - Unif(1,3);
              } else {
                if (prob(1,9)) {
                  x = x - Unif(1,3);
                  y = y + Unif(1,3);
                } else {
                  if (prob(1,9)) {
                    x = x - Unif(1,3);
                    y = y - Unif(1,3);
                  } else {
                    tick(0);
                  }
                }
              }
            }
          }
        }
      }
    }

    if (x >= y) {
      tick(0.7071 * (x - y));
    } else {
      tick(0.7071 * (x - y));
    }
  }
}
