void main(int x) {
  while (x >= 1) {
    if (prob(1,3)) {
      x = x + 1;
    } else {
      x = x - 1;
    }
    if (prob(2,1)) {
      tick(x);
    } else {
      tick(0 - x);
    }
  }
}
