void main(real a, real b) {
  while (a >= 5.0 && b >= 5.0) {
    tick(a + b);
    if (prob(1,1)) {
      b = 9/10 * b;
      a = 11/10 * a;
    } else {
      b = 11/10 * b;
      a = 9/10 * a;
    }
  }
}
