#!/usr/bin/env python3

import subprocess

TOOL = "ginfer"
LOG = "table-coupon.log"
L = 1
R = 1000


def execute_task(out, path):
    cmd = [TOOL, "run-vi-mon", path, "-m",
           "4", "-d", "0", "-s", "grb", "-n"]
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    return str(ret.stdout, "utf-8")


def look_for_runtime(msg):
    lines = msg.splitlines()
    for line in lines:
        if line.find("total time") != -1:
            text = line.split(":")[-1].strip()
            if text[-2:] == "ns":
                return float(text[:-2]) / 1e6
            elif text[-2:] == "us":
                return float(text[:-2]) / 1e3
            elif text[-2:] == 'ms':
                return float(text[:-2])
            elif text[-1:] == 's':
                return float(text[:-1]) * 1e3
    return None


if __name__ == "__main__":
    tims = []
    with open(LOG, "w") as log:
        first = True
        for i in range(L, R + 1):
            path = "tests/coupon_gen/coupon{}g.c".format(i)
            log.write("Analyzing %s\n" % path)
            print(path)

            out = execute_task(log, path)
            log.write(out)
            tim = look_for_runtime(out)
            tims.append(tim)

            log.write("\n\n\n\n")
            log.write("=" * 80 + "\n")
    print(tims)
