#!/usr/bin/env python3

Y_LABEL = "tail probability $\\mathbb{P}(T \\ge d)$"
COLORS = ["green", "red"]

CONFS = [
    {
        "title": "1-1",
        "domain": (19, 50),
        "xticks": [20, 30, 40, 50],
        "raw": [13, 201, 3829, 90705],
        "vi": (13, (32, 9728)),
    },
    {
        "title": "1-2",
        "domain": (75, 200),
        "xticks": [100, 150, 200],
        "raw": [68, 3124, 171932, 12049876],
        "vi": (44.6667, [362, 955973]),
    },
    {
        "title": "2-1",
        "domain": (50, 600),
        "xticks": [200, 400, 600],
        "raw": [20, 2320],
        "vi": (20, [1920, 289873920]),
    },
    {
        "title": "2-2",
        "domain": (150, 600),
        "xticks": [200, 400, 600],
        "raw": [75, 8375],
        "vi": (75, [5875, 447053126]),
    }
]


def generate_latex(out, title, domain, xticks, raw, vi):
    (xmin, xmax) = domain
    out.write("\\begin{{tikzpicture}}\n".format())
    out.write(
        "\\begin{{axis}}[ylabel={ylabel},domain={xmin}:{xmax},xmin={xmin},xmax={xmax},xtick={{{xticks}}},restrict y to domain=0:1.0,ymin=0,ymax=0.25,ytick={{0,0.1,0.2}},title={{{title}}}]\n"
        .format(ylabel=Y_LABEL, xmin=xmin, xmax=xmax, xticks=",".join(map(str, xticks)), title=title))
    out.write(
        "\\addplot[gray,thick,smooth]{{min({vals})}};\n"
        .format(vals=",".join([f"{v}/(x^{i+1})" for (i, v) in enumerate(raw)]))
    )
    out.write("\\addlegendentry{{by raw moments [26]}}\n".format())
    (mean, central) = vi
    for (i, v) in enumerate(central):
        out.write(
            "\\addplot[{col},thick,smooth,domain={xmin}:{xmax}]{{{val}}};\n"
            .format(col=COLORS[i], xmin=xmin, xmax=xmax, val=f"{v}/({v}+(x-{mean})^2)" if i == 0 else f"{v}/((x-{mean})^{(i+1)*2})")
        )
        out.write(
            "\\addlegendentry{{by {mom}\\textsuperscript{{{suf}}} central moment}}\n"
            .format(mom=(i+1)*2, suf="nd" if i == 0 else "th")
        )
    out.write("\\end{{axis}}\n".format())
    out.write("\\end{{tikzpicture}}\n".format())


if __name__ == "__main__":
    with open("figure-8.tex", "w") as out:
        out.writelines([
            "\\documentclass[tikz]{standalone}\n",
            "\\usepackage{amsfonts}\n"
            "\\usepackage{pgfplots}\n",
            "\\begin{document}\n",
        ])

        for conf in CONFS:
            generate_latex(out, **conf)

        out.writelines([
            "\\end{document}\n",
        ])
