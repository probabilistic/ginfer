#!/usr/bin/env python3

import matplotlib.pyplot as plt
import seaborn as sns
import random


def simulate(func, times=500000):
    return [func() for _ in range(times)]


def rdwalk1():
    x = 10
    tick = 0
    while x >= 1:
        tick += 1
        if random.random() < 3/5:
            tick += 1
            x -= 1
            tick += 1
        else:
            tick += 1
            x += 1
            tick += 1
        tick += 1
    return tick


def rdwalk2():
    x = 10
    tick = 0
    while x >= 1:
        tick += 1
        if random.random() < 11/15:
            tick += 1
            x -= 1
            tick += 1
        else:
            tick += 1
            x += 2
            tick += 1
        tick += 1
    return tick


rdwalk1_sim = simulate(rdwalk1)
rdwalk2_sim = simulate(rdwalk2)

fig, (ax1, ax2) = plt.subplots(2, 1)

sns.kdeplot(data=rdwalk1_sim, clip=(70, 500), ax=ax1, label="rdwalk-1")
sns.kdeplot(data=rdwalk2_sim, clip=(70, 500), ax=ax1, label="rdwalk-2")
ax1.legend()

sns.kdeplot(data=rdwalk1_sim, clip=(500, 1000), ax=ax2, label="rdwalk-1")
sns.kdeplot(data=rdwalk2_sim, clip=(500, 1000), ax=ax2, label="rdwalk-2")
ax2.legend()

plt.savefig("figure-9.pdf")
