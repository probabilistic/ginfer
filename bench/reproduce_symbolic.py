#!/usr/bin/env python3

import subprocess

TOOL = "ginfer"
LOG = "symbolic.log"

BENCHMARKS = [
    {
        "name": "2-1: 1D Random Walk with Integer-valued Steps",
        "path": "tacas19/rdwalk1d.c",
        "tasks": [
            {"mode": "vi-mon", "moment": 2, "degree": 2, "init_cond": "x >= 0"},
        ],
    },
    {
        "name": "2-2: 1D Random Walk with Real-valued Steps",
        "path": "tacas19/rdwalk1dunif.c",
        "tasks": [
            {"mode": "vi-mon", "moment": 2, "degree": 2, "init_cond": "x >= 0."},
        ],
    },
]

MODE_DESC = {
    "bi-mon": "raw-moment analysis (with non-negative costs)",
    "bi": "raw-moment analysis (with general costs)",
    "vi-mon": "central-moment analysis (with non-negative costs)",
    "vi": "central-moment analysis (with general costs)",
    "ti": "termination-time analysis",
}


def execute_task(out, path, mode, moment, degree, init_cond=None):
    cmd = [TOOL, "run-%s" % mode, path, "-m",
           str(moment), "-d", str(degree), "-s", "grb"]
    if init_cond:
        cmd.extend(["-i", init_cond])
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    out.write(str(ret.stdout, "utf-8"))


def write_both(out, s):
    print(s)
    out.write(s + "\n")


if __name__ == "__main__":
    with open(LOG, "w") as log:
        first = True
        for benchmark in BENCHMARKS:
            if first:
                first = False
                write_both(log, "=" * 80)

            write_both(log, "Benchmark %s" % benchmark["name"])
            path = benchmark["path"]

            for task in benchmark["tasks"]:
                log.write("\n" + "-" * 80 + "\n")
                write_both(log, "Run in mode %s on moment %d" %
                           (MODE_DESC[task["mode"]], task["moment"]))
                execute_task(log, path, **task)

            log.write("\n\n\n\n")
            write_both(log, "=" * 80)
