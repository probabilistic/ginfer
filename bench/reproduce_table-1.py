#!/usr/bin/env python3

from tabulate import tabulate
import subprocess

TOOL = "ginfer"
LOG = "table-1.log"

BENCHMARKS = [
    {
        "name": "1-1: Coupon Collector with 2 Coupons",
        "path": "tacas19-concrete/coupon2.c",
        "tasks": [
            {"mode": "bi-mon", "moment": 2, "degree": 2},
            {"mode": "bi-mon", "moment": 3, "degree": 2},
            {"mode": "bi-mon", "moment": 4, "degree": 2},
            {"mode": "vi-mon", "moment": 2, "degree": 2},
            {"mode": "vi-mon", "moment": 4, "degree": 2},
        ],
    },
    {
        "name": "1-2: Coupon Collector with 4 Coupons",
        "path": "tacas19-concrete/coupon4.c",
        "tasks": [
            {"mode": "bi-mon", "moment": 2, "degree": 4},
            {"mode": "bi-mon", "moment": 3, "degree": 4},
            {"mode": "bi-mon", "moment": 4, "degree": 4},
            {"mode": "vi-mon", "moment": 2, "degree": 4},
            {"mode": "vi-mon", "moment": 4, "degree": 4},
        ],
    },
    {
        "name": "2-1: 1D Random Walk with Integer-valued Steps",
        "path": "tacas19-concrete/rdwalk1d.c",
        "tasks": [
            {"mode": "bi-mon", "moment": 2, "degree": 2},
            {"mode": "bi-mon", "moment": 3, "degree": 3},
            {"mode": "bi-mon", "moment": 4, "degree": 4},
            {"mode": "vi-mon", "moment": 2, "degree": 2},
            {"mode": "vi-mon", "moment": 4, "degree": 4},
        ],
    },
    {
        "name": "2-2: 1D Random Walk with Real-valued Steps",
        "path": "tacas19-concrete/rdwalk1dunif.c",
        "tasks": [
            {"mode": "bi-mon", "moment": 2, "degree": 2},
            {"mode": "bi-mon", "moment": 3, "degree": 3},
            {"mode": "bi-mon", "moment": 4, "degree": 4},
            {"mode": "vi-mon", "moment": 2, "degree": 2},
            {"mode": "vi-mon", "moment": 4, "degree": 4},
        ],
    },
]

MODE_DESC = {
    "bi-mon": "raw-moment analysis (with non-negative costs)",
    "bi": "raw-moment analysis (with general costs)",
    "vi-mon": "central-moment analysis (with non-negative costs)",
    "vi": "central-moment analysis (with general costs)",
    "ti": "termination-time analysis",
}


def look_for_runtime(msg):
    lines = msg.splitlines()
    for line in lines:
        if line.find("total time") != -1:
            return line.split(":")[-1].strip()
    return "N/A"


def get_result(log):
    lines = log.splitlines()
    for line in lines:
        if line.find("moment") != -1:
            return line.split("<=")[-1].strip()
    return None


def execute_task(out, path, mode, moment, degree, init_cond=None):
    cmd = [TOOL, "run-%s" % mode, path, "-m",
           str(moment), "-d", str(degree), "-s", "grb"]
    if init_cond:
        cmd.extend(["-i", init_cond])
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    txt = str(ret.stdout, "utf-8")
    out.write(txt)

    return (mode + " -moment " + str(moment), "<= " + get_result(txt), look_for_runtime(txt))


def write_both(out, s):
    print(s)
    out.write(s + "\n")


if __name__ == "__main__":
    tabs = []

    with open(LOG, "w") as log:
        first = True
        for benchmark in BENCHMARKS:
            if first:
                first = False
                write_both(log, "=" * 80)

            write_both(log, "Benchmark %s" % benchmark["name"])
            path = benchmark["path"]

            for task in benchmark["tasks"]:
                log.write("\n" + "-" * 80 + "\n")
                write_both(log, "Run in mode %s on moment %d" %
                           (MODE_DESC[task["mode"]], task["moment"]))
                tab = [benchmark["name"], benchmark["path"]]
                tab.extend(execute_task(log, path, **task))
                tabs.append(tab)

            log.write("\n\n\n\n")
            write_both(log, "=" * 80)

    print(tabulate(tabs, headers=["Name", "Path", "Moment", "Result", "Time"]))
