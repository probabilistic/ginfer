#!/usr/bin/env python3

import subprocess

TOOL = "ginfer"
LOG = "table-2.log"

BENCHMARKS = [
    {
        "name": "rdwalk-1",
        "path": "tacas19-concrete/rdwalk-1.c",
        "tasks": [
            {"mode": "vi-mon", "moment": 2, "degree": 2},
            {"mode": "vi-mon", "moment": 3, "degree": 3},
            {"mode": "vi-mon", "moment": 4, "degree": 4},
        ],
    },
    {
        "name": "rdwalk-2",
        "path": "tacas19-concrete/rdwalk-2.c",
        "tasks": [
            {"mode": "vi-mon", "moment": 2, "degree": 2},
            {"mode": "vi-mon", "moment": 3, "degree": 3},
            {"mode": "vi-mon", "moment": 4, "degree": 4},
        ],
    },
]

MODE_DESC = {
    "bi-mon": "raw-moment analysis (with non-negative costs)",
    "bi": "raw-moment analysis (with general costs)",
    "vi-mon": "central-moment analysis (with non-negative costs)",
    "vi": "central-moment analysis (with general costs)",
    "ti": "termination-time analysis",
}


def get_result(log):
    lines = log.splitlines()
    for line in lines:
        if line.find("central moment") != -1:
            return float(line.split("<=")[-1])
    return None


def execute_task(out, path, mode, moment, degree, init_cond=None):
    cmd = [TOOL, "run-%s" % mode, path, "-m",
           str(moment), "-d", str(degree), "-s", "grb"]
    if init_cond:
        cmd.extend(["-i", init_cond])
    ret = subprocess.run(cmd, stdout=subprocess.PIPE)
    tmp = str(ret.stdout, "utf-8")
    out.write(tmp)
    return get_result(tmp)


def write_both(out, s):
    print(s)
    out.write(s + "\n")


if __name__ == "__main__":
    with open(LOG, "w") as log:
        first = True
        for benchmark in BENCHMARKS:
            if first:
                first = False
                write_both(log, "=" * 80)

            write_both(log, "Benchmark %s" % benchmark["name"])
            path = benchmark["path"]

            moments = {}

            for task in benchmark["tasks"]:
                log.write("\n" + "-" * 80 + "\n")
                write_both(log, "Run in mode %s on moment %d" %
                           (MODE_DESC[task["mode"]], task["moment"]))
                ret = execute_task(log, path, **task)
                moments[task["moment"]] = ret

            print(f"skewness = {moments[3]/(moments[2] ** 1.5)}")
            print(f"kurtosis = {moments[4]/(moments[2] ** 2)}")

            log.write("\n\n\n\n")
            write_both(log, "=" * 80)
