int attack(int secret) {
  int secret_min, time;
  int success;

  secret_min = 0;

  time = 0;
  if (secret_min + 2 <= secret) {
    time = time + Unif(1,3);
    time = time + Unif(1,3);
    time = time + Unif(1,3);
  } else {
    time = time + Unif(2,4);
    time = time + Unif(2,4);
    time = time + Unif(2,4);
  }
  if (time < 8) {
    secret_min = secret_min + 2;
  }

  time = 0;
  if (secret_min + 1 <= secret) {
    time = time + Unif(1,3);
    time = time + Unif(1,3);
    time = time + Unif(1,3);
  } else {
    time = time + Unif(2,4);
    time = time + Unif(2,4);
    time = time + Unif(2,4);
  }
  if (time < 8) {
    secret_min = secret_min + 1;
  }

  if (secret_min == secret) {
    success = 1;
  } else {
    success = 0;
  }

  return success;
}

void start() {
  int secret, success;

  secret = 0;
  success = attack(secret);
  if (success == 0) {
    tick(1);
  }
}
