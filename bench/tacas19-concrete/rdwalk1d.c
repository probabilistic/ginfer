void f(int x) {
  x = 1;
  // tick(1); // l5->l0
  while (x >= 1) {
    tick(1); // l0->l2
    if (prob(3,2)) {
      tick(1); // l2->l4
      x = x - 1;
      tick(1); // l4->l1
    } else {
      tick(1); // l2->l3
      x = x + 1;
      tick(1); // l3->l1
    }
    tick(1); // l1->l0
  }
  // tick(-1); // compensate
}
