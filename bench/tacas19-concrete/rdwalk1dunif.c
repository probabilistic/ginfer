void f(real x) {
  real z;

  x = 2.0;
  // tick(1); // l7->l0
  while (x >= 0.0) {
    tick(1); // l0 -> l2
    if (prob(7,3)) {
      tick(1); // l2->l6
      z = UnifR(0.0,1.0);
      tick(1); // l6->l5
      x = x - z;
      tick(1); // l5->l1
    } else {
      tick(1); // l2->l4
      z = UnifR(0.0,1.0);
      tick(1); // l4->l3
      x = x + z;
      tick(1); // l3->l1
    }
    tick(1); // l1->l0
  }
  // tick(-1); // compensate
}
