void f(real x, real y) {
  real z;

  x = 2.0;
  // tick(1); // l9->l8
  y = 2.0;
  tick(1); // l8->l0
  while (x > 0.0 && y > 0.0) {
    tick(1); // l0->l3
    if (demon) {
      tick(1); // l3->l7
      z = UnifR(-2.0,1.0);
      tick(1); // l7->l6
      x = x + z;
      tick(1); // l6->l2
    } else {
      tick(1); // l3->l5
      z = UnifR(-2.0,1.0);
      tick(1); // l5->l4
      y = y + z;
      tick(1); // l4->l2
    }
    tick(1); // l2->l1
    tick(1); // l1->l0
  }
  // tick(-1); // compensate
}
