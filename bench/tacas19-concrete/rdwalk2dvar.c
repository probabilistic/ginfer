void f(real x, real y) {
  real z;

  x = 3.0;
  // tick(1); // l14->l13
  y = 2.0;
  // tick(1); // l13->l0
  while (x > y) {
    tick(1); // l0->l2
    if (demon) {
      tick(1); // l2->l8
      if (prob(7,3)) {
        tick(1); // l8->l12
        z = UnifR(-2.0,1.0);
        tick(1); // l12->l11
        x = x + z;
        tick(1); // l11->l1
      } else {
        tick(1); // l8->l10
        z = UnifR(-2.0,1.0);
        tick(1); // l10->l9
        y = y + z;
        tick(1); // l9->l1
      }
    } else {
      tick(1); // l2->l3
      if (prob(7,3)) {
        tick(1); // l3->l7
        z = UnifR(-1.0,2.0);
        tick(1); // l7->l6
        y = y + z;
        tick(1); // l6->l1
      } else {
        tick(1); // l3->l5
        z = UnifR(-1.0,2.0);
        tick(1); // l5->l4
        x = x + z;
        tick(1); // l4->l1
      }
    }
    tick(1); // l1->l0
  }
  // tick(-2); // compensate
}
