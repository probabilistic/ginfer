void f() {
  int c0, c1;

  c0 = 0;
  // tick(1); // l6->l5
  c1 = 0;
  tick(1); // l5->l0
  while (c0 + c1 < 2) {
    tick(1); // l0->l2
    if (prob(1,1)) {
      tick(1); // l2->l4
      c0 = 1;
      tick(1); // l4->l1
    } else {
      tick(1); // l2->l3
      c1 = 1;
      tick(1); // l3->l1
    }
    tick(1); // l1->l0
  }
  // tick(-1); // compensate
}
