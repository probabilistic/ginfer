void f() {
  int c0, c1, c2, c3;

  c0 = 0;
  // tick(1); // l12->l11
  c1 = 0;
  tick(1); // l11->l10
  c2 = 0;
  tick(1); // l10->l9
  c3 = 0;
  tick(1); // l9->l0
  while (c0 + c1 + c2 + c3 < 4) {
    tick(1); // l0->l2
    if (prob(1,1)) {
      tick(1); // l2->l6
      if (prob(1,1)) {
        tick(1); // l6->l8
        c0 = 1;
        tick(1); // # l8->l1
      } else {
        tick(1); // l6->l7
        c1 = 1;
        tick(1); // l7->l1
      }
    } else {
      tick(1); // l2->l3
      if (prob(1,1)) {
        tick(1); // l3->l5
        c2 = 1;
        tick(1); // l5->l1
      } else {
        tick(1); // l3->l4
        c3 = 1;
        tick(1); // l4->l1
      }
    }
    tick(1); // l1->l0
  }
  // tick(-1); // compensate
}
