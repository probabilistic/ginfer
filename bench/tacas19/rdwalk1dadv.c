void f(int x) {
  // x = 2;
  // tick(1); // l17->l0
  while (x > 0) {
    tick(1); // l0->l12
    if (prob(4,1)) {
      tick(1); // l12->l16
      tick(1); // l16->l2
    } else {
      tick(1); // l12->l13
      if (prob(1,1)) {
        tick(1); // l13->l15
        x = x + 1;
        tick(1); // l15->l2
      } else {
        tick(1); // l13->l14
        x = x + 2;
        tick(1); // l14->l2
      }
    }
    if (demon) {
      tick(1); // l2->l9
      if (prob(7,1)) {
        tick(1); // l9->l11
        x = x - 1;
        tick(1); // l11->l1
      } else {
        tick(1); // l9->l10
        tick(1); // l10->l1
      }
    } else {
      tick(1); // l2->l4
      if (prob(4,1)) {
        tick(1); // l4->l8
        tick(1); // l8->l3
      } else {
        tick(1); // l4->l5
        if (prob(1,1)) {
          tick(1); // l5->l7
          x = x + 1;
          tick(1); // l7->l3
        } else {
          tick(1); // l5->l6
          x = x + 2;
          tick(1); // l6->l3
        }
      }
      x = x - 1;
      tick(1); // l3->l1
    }
    tick(1); // l1->l0
  }
  // tick(-1); // compensate
}
