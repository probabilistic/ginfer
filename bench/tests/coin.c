int expo(int n) {
  int r;

  if (n == 0) {
    r = 1;
  } else {
    if (prob(1,1)) {
      r = 0;
    } else {
      n = n - 1;
      r = expo(n);
    }
  }
  return r;
}

void main() {
  int n, r;

  n = 4;
  r = expo(n);
  tick(r); // E[r] = 1/2^n
}
