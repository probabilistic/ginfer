void f(int i) {
  if (i < 5) {
    if (i == 0) {
      i = i + 1;
    } else {
      if (i == 1) {
        if (prob(1,4)) {
          i = 1;
        } else {
          i = i + 1;
        }
      } else {
        if (i == 2) {
          if (prob(2,3)) {
            i = 2;
          } else {
            i = i + 1;
          }
        } else {
          if (i == 3) {
            if (prob(3,2)) {
              i = 3;
            } else {
              i = i + 1;
            }
          } else {
            if (prob(4,1)) {
              i = 4;
            } else {
              i = i + 1;
            }
          }
        }
      }
    }
    f(i);
    tick(1);
  }
}

void main() {
  int i;

  i = 0;
  f(i);
}
