// Coupon with N = 20

void f() {
  int i;

  i = 0;
  while (i < 20) {
    if (i == 0) {
      i = i + 1;
    } else {
      if (i == 1) {
        if (prob(1,19)) {
          i = 1;
        } else {
          i = i + 1;
        }
      } else {
        if (i == 2) {
          if (prob(2,18)) {
            i = 2;
          } else {
            i = i + 1;
          }
        } else {
          if (i == 3) {
            if (prob(3,17)) {
              i = 3;
            } else {
              i = i + 1;
            }
          } else {
            if (i == 4) {
              if (prob(4,16)) {
                i = 4;
              } else {
                i = i + 1;
              }
            } else {
              if (i == 5) {
                if (prob(5,15)) {
                  i = 5;
                } else {
                  i = i + 1;
                }
              } else {
                if (i == 6) {
                  if (prob(6,14)) {
                    i = 6;
                  } else {
                    i = i + 1;
                  }
                } else {
                  if (i == 7) {
                    if (prob(7,13)) {
                      i = 7;
                    } else {
                      i = i + 1;
                    }
                  } else {
                    if (i == 8) {
                      if (prob(8,12)) {
                        i = 8;
                      } else {
                        i = i + 1;
                      }
                    } else {
                      if (i == 9) {
                        if (prob(9,11)) {
                          i = 9;
                        } else {
                          i = i + 1;
                        }
                      } else {
                        if (i == 10) {
                          if (prob(10,10)) {
                            i = 10;
                          } else {
                            i = i + 1;
                          }
                        } else {
                          if (i == 11) {
                            if (prob(11,9)) {
                              i = 11;
                            } else {
                              i = i + 1;
                            }
                          } else {
                            if (i == 12) {
                              if (prob(12,8)) {
                                i = 12;
                              } else {
                                i = i + 1;
                              }
                            } else {
                              if (i == 13) {
                                if (prob(13,7)) {
                                  i = 13;
                                } else {
                                  i = i + 1;
                                }
                              } else {
                                if (i == 14) {
                                  if (prob(14,6)) {
                                    i = 14;
                                  } else {
                                    i = i + 1;
                                  }
                                } else {
                                  if (i == 15) {
                                    if (prob(15,5)) {
                                      i = 15;
                                    } else {
                                      i = i + 1;
                                    }
                                  } else {
                                    if (i == 16) {
                                      if (prob(16,4)) {
                                        i = 16;
                                      } else {
                                        i = i + 1;
                                      }
                                    } else {
                                      if (i == 17) {
                                        if (prob(17,3)) {
                                          i = 17;
                                        } else {
                                          i = i + 1;
                                        }
                                      } else {
                                        if (i == 18) {
                                          if (prob(18,2)) {
                                            i = 18;
                                          } else {
                                            i = i + 1;
                                          }
                                        } else {
                                          if (prob(19,1)) {
                                            i = 19;
                                          } else {
                                            i = i + 1;
                                          }
                                        }
                                      }
                                    }
                                  }
                                }
                              }
                            }
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    tick(1);
  }
}
