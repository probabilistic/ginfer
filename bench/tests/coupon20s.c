void f20() {
  return;
}

void f19() {
  tick(1);
  if (prob(19,1)) {
    f19();
  } else {
    f20();
  }
}

void f18() {
  tick(1);
  if (prob(18,2)) {
    f18();
  } else {
    f19();
  }
}

void f17() {
  tick(1);
  if (prob(17,3)) {
    f17();
  } else {
    f18();
  }
}

void f16() {
  tick(1);
  if (prob(16,4)) {
    f16();
  } else {
    f17();
  }
}

void f15() {
  tick(1);
  if (prob(15,5)) {
    f15();
  } else {
    f16();
  }
}

void f14() {
  tick(1);
  if (prob(14,6)) {
    f14();
  } else {
    f15();
  }
}

void f13() {
  tick(1);
  if (prob(13,7)) {
    f13();
  } else {
    f14();
  }
}

void f12() {
  tick(1);
  if (prob(12,8)) {
    f12();
  } else {
    f13();
  }
}

void f11() {
  tick(1);
  if (prob(11,9)) {
    f11();
  } else {
    f12();
  }
}

void f10() {
  tick(1);
  if (prob(10,10)) {
    f10();
  } else {
    f11();
  }
}

void f9() {
  tick(1);
  if (prob(9,11)) {
    f9();
  } else {
    f10();
  }
}

void f8() {
  tick(1);
  if (prob(8,12)) {
    f8();
  } else {
    f9();
  }
}

void f7() {
  tick(1);
  if (prob(7,13)) {
    f7();
  } else {
    f8();
  }
}

void f6() {
  tick(1);
  if (prob(6,14)) {
    f6();
  } else {
    f7();
  }
}

void f5() {
  tick(1);
  if (prob(5,15)) {
    f5();
  } else {
    f6();
  }
}

void f4() {
  tick(1);
  if (prob(4,16)) {
    f4();
  } else {
    f5();
  }
}

void f3() {
  tick(1);
  if (prob(3,17)) {
    f3();
  } else {
    f4();
  }
}

void f2() {
  tick(1);
  if (prob(2,18)) {
    f2();
  } else {
    f3();
  }
}

void f1() {
  tick(1);
  if (prob(1,19)) {
    f1();
  } else {
    f2();
  }
}

void f0() {
  tick(1);
  f1();
}
