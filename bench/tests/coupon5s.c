void f5() {
  return;
}

void f4() {
  tick(1);
  if (prob(4,1)) {
    f4();
  } else {
    f5();
  }
}

void f3() {
  tick(1);
  if (prob(3,2)) {
    f3();
  } else {
    f4();
  }
}

void f2() {
  tick(1);
  if (prob(2,3)) {
    f2();
  } else {
    f3();
  }
}

void f1() {
  tick(1);
  if (prob(1,4)) {
    f1();
  } else {
    f2();
  }
}

void f0() {
  tick(1);
  f1();
}
