#!/usr/bin/env python3

L = 1
R = 1000


def gen(out, n):
    out.writelines([
        f"void f{n}() {{\n",
        "\treturn;\n",
        "}\n",
    ])
    for i in reversed(range(1, n)):
        out.writelines([
            f"void f{i}() {{\n",
            "\ttick(1);\n",
            f"\tif (prob({i},{n-i})) {{\n",
            f"\t\tf{i}();\n",
            "\t} else {\n",
            f"\t\tf{i+1}();\n",
            "\t}\n",
            "}\n",
        ])
    out.writelines([
        "void f0() {\n",
        "\ttick(1);\n",
        "\tf1();\n",
        "}\n",
    ])


for i in range(L, R + 1):
    fn = f"coupon{i}g.c"
    with open(fn, "w") as f:
        gen(f, i)
