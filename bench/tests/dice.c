int dice1() {
  int r;

  if (prob(1,1)) {
    if (prob(1,1)) {
      // true, true
      tick(1);
      r = dice1();
    } else {
      // true, false
      r = 1;
    }
  } else {
    if (prob(1,1)) {
      // false, true
      r = 2;
    } else {
      // false, false
      r = 3;
    }
  }
  return r;
}

int dice2() {
  int r;

  if (prob(1,1)) {
    if (prob(1,1)) {
      // true, true
      tick(1);
      r = dice2();
    } else {
      // true, false
      r = 4;
    }
  } else {
    if (prob(1,1)) {
      // false, true
      r = 5;
    } else {
      // false, false
      r = 6;
    }
  }
  return r;
}

int dice() {
  int r;

  if (prob(1,1)) {
    tick(1);
    r = dice1();
  } else {
    tick(1);
    r = dice2();
  }
  return r;
}

void main() {
  int r;

  r = dice(); // E[tick] = 4/3
  tick(r); // E[r] = 7/2
  if (r == 1) {
    tick(1); // E[delta] = 1/6
  }
}
