void main(int t) {
  int p1, x;

  p1 = 0;
  x = 0;
  while (t > 0) {
    t = t - 1;
    if (p1 == 0) {
      p1 = p1 + 1;
    } else {
      p1 = p1 - 1;
    }
    if (p1 > x) {
      x = x + 1;
      tick(1);
    } else {
      if (p1 < x) {
        x = x - 1;
        tick(-1);
      }
    }
  }
}
