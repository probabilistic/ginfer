int f1(int x) {
  int y;
  y = 0;
  while (x > 0) {
    if (prob(1, 3)) {
      x = x + 1;
    } else {
      x = x - 1;
    }
    tick(1);
    y = y + 1;
  }
  return y;
}

void f2(int x) {
  x = f1(x);
  x = f1(x);
}

void f3(int x) {
  f2(x);
  f2(x);
}

void f4(int x) {
  f3(x);
  f3(x);
}

void f5(int x) {
  f4(x);
  f4(x);
}

void f6(int x) {
  f5(x);
  f5(x);
}

void f7(int x) {
  f6(x);
  f6(x);
}

void f8(int x) {
  f7(x);
  f7(x);
}

void f9(int x) {
  f8(x);
  f8(x);
}

void f10(int x) {
  f9(x);
  f9(x);
}

void f11(int x) {
  f10(x);
  f10(x);
}

void f12(int x) {
  f11(x);
  f11(x);
}

void f13(int x) {
  f12(x);
  f12(x);
}

void f14(int x) {
  f13(x);
  f13(x);
}

void f15(int x) {
  f14(x);
  f14(x);
}

void f16(int x) {
  if (prob(1,3)) { f16(x); } else { f15(x); }
  f15(x);
}

// void f17(int x) {
//   f16(x);
//   f16(x);
// }
