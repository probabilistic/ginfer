int f91(int x) {
  if (x > 100) {
    x = x - 10;
    return x;
  } else {
    x = x + 11;
    tick(1);
    x = f91(x);
    if (prob(3,1)) {
      tick(1);
      x = f91(x);
    }
    return x;
  }
}
