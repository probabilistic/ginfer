void main(int n) {
  while (n > 0) {
    if (prob(3,1)) {
      tick(n - 1);
      n = n - 1;
    } else {
      tick(-2);
      tick(-3 * (n - 1));
      n = n + 1;
    }
  }
}
