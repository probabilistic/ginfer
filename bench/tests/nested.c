void main(int n) {
  int x1, x2, x3, x4, r;
  x1 = n;
  while (x1 > 0) {
    r = Unif(-2, 1);
    x1 = x1 + r;
    tick(1);

    x2 = n;
    while (x2 > 0) {
      r = Unif(-2, 1);
      x2 = x2 + r;
      tick(1);

      x3 = n;
      while (x3 > 0) {
        r = Unif(-2, 1);
        x3 = x3 + r;
        tick(1);

        x4 = n;
        while (x4 > 0) {
          r = Unif(-2, 1);
          x4 = x4 + r;
          tick(1);
        }
      }
    }
  }
}
