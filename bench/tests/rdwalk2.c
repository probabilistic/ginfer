int f0(int x) {
  int z;
  if (x > 0) {
    if (prob(2,1)) {
      x = x - 1;
    } else {
      x = x + 1;
    }
    z = f0(x);
    z = z + 1;
    tick(1);
  } else {
    z = 0;
  }
  return z;
}

int f1(int x) {
  int z;
  if (x > 0) {
    if (prob(12,1)) {
      x = x - 1;
    } else {
      x = x + 1;
    }
    z = f1(x);
    z = z + 1;
    tick(1);
  } else {
    z = 0;
  }
  return z;
}

void main(int x) {
  tick(1);
  x = f0(x);
  tick(1);
  x = f1(x);
  tick(1);
}
