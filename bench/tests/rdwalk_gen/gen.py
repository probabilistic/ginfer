#!/usr/bin/env python3

L = 1
R = 1000


def gen(out, n):
    per_block = 20

    for i in range(0, n):
        init = per_block * i
        out.write(f"""func f{i}(int x) [{init+1},{init+0}] local:{{int z}} return:{{z}}
	{init+1} -- skip --> {{{init+6}}}
	{init+2} -- skip --> {{{init+0}}}
	{init+3} -- skip --> {{{init+4}}}
	{init+4} -- weaken(x, z - x) --> {{{init+0}}}
	{init+5} -- skip --> {{{init+3}}}
	{init+6} -- [x > 0] --> {{{init+18},{init+19}}}
	{init+7} -- skip --> {{{init+5}}}
	{init+8} -- score(1) --> {{{init+7}}}
	{init+9} -- z := z + 1 --> {{{init+8}}}
	{init+10} -- call f{i}(x) to {{z}} --> {{{init+9}}}
	{init+11} -- [prob({i*10+2},1)] --> {{{init+13},{init+15}}}
	{init+12} -- skip --> {{{init+10}}}
	{init+13} -- x := x - 1 --> {{{init+12}}}
	{init+14} -- skip --> {{{init+10}}}
	{init+15} -- x := x + 1 --> {{{init+14}}}
	{init+16} -- skip --> {{{init+5}}}
	{init+17} -- z := 0 --> {{{init+16}}}
	{init+18} -- weaken(x - 1) --> {{{init+11}}}
	{init+19} -- weaken(x, 0 - x) --> {{{init+17}}}
""")
    start = per_block * n
    end = start + n * 2 + 1
    out.writelines([
        f"func main(int x) [{start},{end}] local:{{}} return:{{}}\n",
        f"\t{start} -- score(1) --> {{{start+1}}}\n",
    ])
    for i in range(0, n):
        out.writelines([
            f"\t{start+i*2+1} -- call f{i}(x) to {{x}} --> {{{start+i*2+2}}}\n",
            f"\t{start+i*2+2} -- score(1) --> {{{start+i*2+3}}}\n",
        ])


for i in range(L, R + 1):
    fn = f"rdwalk{i}g.cfg"
    with open(fn, "w") as f:
        gen(f, i)
