void rdwalk(real x, real n) {
  real r;

  if (x < n) {
    r = UnifR(-1.0,2.0);
    x = x + r;
    rdwalk(x, n);
    tick(1);
  }
}
