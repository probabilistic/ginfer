int half(int n) {
  int m;

  m = 0;
  while (n > 1) {
    n = n - 2;
    m = m + 1;
  }
  return m;
}

void generate(int n) {
  int m;

  if (n > 0) {
    tick(1);
    m = half(n);
    tick(1);
    generate(m);
    if (prob(1,1)) {
      tick(1);
      generate(m);
    }
  }
}
