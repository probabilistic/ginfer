int incr(int x) {
  x = x + 1;
  return x;
}

int decr(int x) {
  x = x - 1;
  return x;
}

void main(int x, int n) {
  x = incr(x);
  while (x < n) {
    if (prob(1,1)) {
      x = incr(x);
      x = incr(x);
    } else {
      x = decr(x);
    }
    tick(1);
  }
}
