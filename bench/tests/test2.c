int fac(int n) {
  if (n > 0) {
    n = n - 1;
    tick(1);
    n = fac(n);
    n = n + 1;
  } else {
    n = 0;
  }
  return n;
}

void main(int n) {
  n = fac(n);
  while (n > 0) {
    n = n - 1;
    tick(1);
  }
}
