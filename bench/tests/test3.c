void walk(int x, int n) {
  if (x < n) {
    if (prob(1,1)) {
      x = x + 2;
    } else {
      x = x - 1;
    }
    walk(x, n);
    tick(1);
  }
}
