int f(int n, int m) {
  if (n > 0) {
    n = n - 1;
    if (prob(1,1)) {
      m = m + 1;
    }
    m = f(n, m);
  }
  return m;
}

int g(int m) {
  if (m > 0) {
    if (prob(3,1)) {
      m = m - 1;
    } else {
      m = m + 1;
    }
    m = g(m);
    tick(1);
  }
  return m;
}

void main(int n, int m) {
  m = g(m);
  m = f(n, m);
  m = g(m);
}
