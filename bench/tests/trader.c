void trader(int price, int min) {
  int nShares;
  while (price > min && min >= 0) {
    if (prob(1, 3)) {
      price = price + 1;
    } else {
      price = price - 1;
    }
    nShares = Unif(0, 100000);
    while (nShares > 0) {
      nShares = nShares - 1;
      tick(price);
    }
  }
}
