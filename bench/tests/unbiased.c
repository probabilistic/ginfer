int biased() {
  int r;

  if (prob(9,1)) {
    r = 0;
  } else {
    r = 1;
  }
  return r;
}

int unbiased() {
  int a, b, r;

  a = biased();
  b = biased();
  if (a == b) {
    tick(1);
    r = unbiased();
  } else {
    r = a;
  }
  return r;
}

int unbiased_loop() {
  int a, b;

  a = biased();
  b = biased();
  while (a == b) {
    a = biased();
    b = biased();
    tick(1);
  }
  return a;
}

void main() {
  int r;
  r = unbiased(); // E[tick] = 41/9
  tick(r); // E[r] = 1/2
}
