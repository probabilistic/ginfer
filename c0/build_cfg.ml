open Core

let counter = ref 0
let reset_counter () = counter := 0

let fresh_vertex () =
  Int.incr counter;
  !counter
;;

module VT = Hashtbl.Make_plain (Hcfg.G.V)
module VM = Map.Make_plain (Hcfg.G.V)

let scope_store = VT.create ()

let diff_scope u v =
  let su = VT.find_exn scope_store u in
  let sv = VT.find_exn scope_store v in
  let lu = List.length su in
  let lv = List.length sv in
  List.take su (lu - lv) |> List.map ~f:fst
;;

let new_vertex ~scope g =
  let u = fresh_vertex () in
  VT.add_exn scope_store ~key:u ~data:scope;
  u, Hcfg.G.add_vertex g u
;;

let add_weaken_edge g (u, l, vs) =
  let vs', g =
    List.fold_right vs ~init:([], g) ~f:(fun v (acc, g) ->
        let v', g = new_vertex ~scope:(VT.find_exn scope_store v) g in
        let g = Hcfg.G.add_edge_e g (v', C_seq A_weaken, [ v ]) in
        v' :: acc, g)
  in
  Hcfg.G.add_edge_e g (u, l, vs')
;;

let add_clear_scope_edge g u v =
  let d = diff_scope u v in
  match d with
  | [] -> Hcfg.G.add_edge g u [ v ]
  | _ -> Hcfg.G.add_edge_e g (u, C_seq (A_dead_vars d), [ v ])
;;

let rec g_stmt g u scope exit bopt copt = function
  | Sast.S_assign (x, e) ->
    let v, g = new_vertex ~scope g in
    let g = Hcfg.G.add_edge_e g (u, C_seq (A_assign (x, e)), [ v ]) in
    v, scope, g
  | S_sample (x, mu) ->
    let v, g = new_vertex ~scope g in
    let g = Hcfg.G.add_edge_e g (u, C_seq (A_sample (x, mu)), [ v ]) in
    v, scope, g
  | S_call (x_opt, f, xs) ->
    let v, g = new_vertex ~scope g in
    let g = Hcfg.G.add_edge_e g (u, C_call (x_opt, f, xs), [ v ]) in
    v, scope, g
  | S_array_put (a, e0, e1) ->
    let v, g = new_vertex ~scope g in
    let g = Hcfg.G.add_edge_e g (u, C_seq (A_array_put (a, e0, e1)), [ v ]) in
    v, scope, g
  | S_array_get (x, a, e0) ->
    let v, g = new_vertex ~scope g in
    let g = Hcfg.G.add_edge_e g (u, C_seq (A_array_get (x, a, e0)), [ v ]) in
    v, scope, g
  | S_array_make (a, t, e) ->
    let v, g = new_vertex ~scope g in
    let g = Hcfg.G.add_edge_e g (u, C_seq (A_array_make (a, t, e)), [ v ]) in
    v, scope, g
  | S_decl_var (x, t) ->
    let scope' = (x, t) :: scope in
    let v, g = new_vertex ~scope:scope' g in
    let g = Hcfg.G.add_edge_e g (u, C_seq (A_decl_var (x, t)), [ v ]) in
    v, scope', g
  | S_if (e, s1, s2) ->
    let u1, g = new_vertex ~scope g in
    let u2, g = new_vertex ~scope g in
    let g = add_weaken_edge g (u, C_cond e, [ u1; u2 ]) in
    let v1, _, g = g_stmt g u1 scope exit bopt copt s1 in
    let v2, _, g = g_stmt g u2 scope exit bopt copt s2 in
    let v, g = new_vertex ~scope g in
    let g = add_clear_scope_edge g v1 v in
    let g = add_clear_scope_edge g v2 v in
    v, scope, g
  | S_flip (e, s1, s2) ->
    let u1, g = new_vertex ~scope g in
    let u2, g = new_vertex ~scope g in
    let g = Hcfg.G.add_edge_e g (u, C_prob e, [ u1; u2 ]) in
    let v1, _, g = g_stmt g u1 scope exit bopt copt s1 in
    let v2, _, g = g_stmt g u2 scope exit bopt copt s2 in
    let v, g = new_vertex ~scope g in
    let g = add_clear_scope_edge g v1 v in
    let g = add_clear_scope_edge g v2 v in
    v, scope, g
  | S_loop (loop_unroll, (s1, e1), s2, s3) ->
    if loop_unroll
    then (
      let v_b, g = new_vertex ~scope g in
      let v_c, g = new_vertex ~scope g in
      let v1, scope1, g = g_stmts g u scope exit bopt copt s1 in
      let u2, g = new_vertex ~scope:scope1 g in
      let u_b, g = new_vertex ~scope:scope1 g in
      let g = add_weaken_edge g (v1, C_cond e1, [ u2; u_b ]) in
      let u2_actual, g = new_vertex ~scope g in
      let g = add_clear_scope_edge g u2 u2_actual in
      let g = add_clear_scope_edge g u_b v_b in
      let v2, _, g = g_stmt g u2_actual scope exit (Some v_b) (Some v_c) s2 in
      let g = add_clear_scope_edge g v2 v_c in
      let v3, _, g = g_stmt g v_c scope exit bopt copt s3 in
      let u', g = new_vertex ~scope g in
      let g = add_clear_scope_edge g v3 u' in
      let v_b', g = new_vertex ~scope g in
      let v_c', g = new_vertex ~scope g in
      let g = add_clear_scope_edge g v_b' v_b in
      let v1', scope1, g = g_stmts g u' scope exit bopt copt s1 in
      let u2', g = new_vertex ~scope:scope1 g in
      let u_b', g = new_vertex ~scope:scope1 g in
      let g = add_weaken_edge g (v1', C_cond e1, [ u2'; u_b' ]) in
      let u2_actual', g = new_vertex ~scope g in
      let g = add_clear_scope_edge g u2' u2_actual' in
      let g = add_clear_scope_edge g u_b' v_b' in
      let v2', _, g = g_stmt g u2_actual' scope exit (Some v_b') (Some v_c') s2 in
      let g = add_clear_scope_edge g v2' v_c' in
      let v3', _, g = g_stmt g v_c' scope exit bopt copt s3 in
      let g = add_clear_scope_edge g v3' u' in
      v_b, scope, g)
    else (
      let u', g = new_vertex ~scope g in
      let g = add_clear_scope_edge g u u' in
      let v_b, g = new_vertex ~scope g in
      let v_c, g = new_vertex ~scope g in
      let v1, scope1, g = g_stmts g u' scope exit bopt copt s1 in
      let u2, g = new_vertex ~scope:scope1 g in
      let u_b, g = new_vertex ~scope:scope1 g in
      let g = add_weaken_edge g (v1, C_cond e1, [ u2; u_b ]) in
      let u2_actual, g = new_vertex ~scope g in
      let g = add_clear_scope_edge g u2 u2_actual in
      let g = add_clear_scope_edge g u_b v_b in
      let v2, _, g = g_stmt g u2_actual scope exit (Some v_b) (Some v_c) s2 in
      let g = add_clear_scope_edge g v2 v_c in
      let v3, _, g = g_stmt g v_c scope exit bopt copt s3 in
      let g = add_clear_scope_edge g v3 u' in
      v_b, scope, g)
  | S_return e_opt ->
    (match e_opt with
    | None ->
      let v, g = new_vertex ~scope g in
      let vw, g = new_vertex ~scope g in
      let g = Hcfg.G.add_edge_e g (u, C_seq A_weaken, [ vw ]) in
      let g = add_clear_scope_edge g vw exit in
      v, scope, g
    | Some e ->
      let v_res, g = new_vertex ~scope g in
      let v, g = new_vertex ~scope g in
      let vw, g = new_vertex ~scope g in
      let g = Hcfg.G.add_edge_e g (u, C_seq (A_assign ("\\result", e)), [ v_res ]) in
      let g = Hcfg.G.add_edge_e g (v_res, C_seq A_weaken, [ vw ]) in
      let g = add_clear_scope_edge g vw exit in
      v, scope, g)
  | S_block s0s ->
    let v1, _, g = g_stmts g u scope exit bopt copt s0s in
    let v, g = new_vertex ~scope g in
    let g = add_clear_scope_edge g v1 v in
    v, scope, g
  | S_break ->
    let v, g = new_vertex ~scope g in
    let v_b = Option.value_exn bopt in
    let g = add_clear_scope_edge g u v_b in
    v, scope, g
  | S_continue ->
    let v, g = new_vertex ~scope g in
    let v_c = Option.value_exn copt in
    let g = add_clear_scope_edge g u v_c in
    v, scope, g
  | S_tick e ->
    let v, g = new_vertex ~scope g in
    let g = Hcfg.G.add_edge_e g (u, C_seq (A_tick e), [ v ]) in
    v, scope, g
  | S_meta_assume e ->
    let v, g = new_vertex ~scope g in
    let g = add_weaken_edge g (u, C_seq (A_assume e), [ v ]) in
    v, scope, g
  | S_meta_perform s0 -> g_stmt g u scope exit bopt copt s0
  | S_meta_split e ->
    let v, g = new_vertex ~scope g in
    let g = Hcfg.G.add_edge_e g (u, C_seq (A_split e), [ v ]) in
    v, scope, g

and g_stmts g u scope exit bopt copt = function
  | [] -> u, scope, g
  | s0h :: s0t ->
    let vh, scopeh, g = g_stmt g u scope exit bopt copt s0h in
    let vt, scopet, g = g_stmts g vh scopeh exit bopt copt s0t in
    vt, scopet, g
;;

let g_proc { Sast.name; ret; params; logicals; requires; instance; body } =
  VT.clear scope_store;
  let ret' = Option.map ret ~f:(fun tret -> "\\result", tret) in
  let scope = List.rev (params @ logicals) in
  let scope' = Option.to_list ret' @ scope in
  let g = Hcfg.G.empty in
  let entry, g = new_vertex ~scope g in
  let exit, g = new_vertex ~scope:scope' g in
  let fake_entry, g = new_vertex ~scope:scope' g in
  let fake_exit, _, g = g_stmts g fake_entry scope' exit None None body in
  let g =
    Hcfg.G.add_edge_e
      g
      ( entry
      , C_seq
          (match ret' with
          | None -> A_skip
          | Some (x, t) -> A_decl_var (x, t))
      , [ fake_entry ] )
  in
  let g = add_clear_scope_edge g fake_exit exit in
  let reachable = VT.create () in
  let module Tr = Hyper_graph.Traverse.Make (Hcfg.G) in
  Tr.Dfs.forward g ~on:entry ~init:() ~f:(fun () u ->
      VT.add_exn reachable ~key:u ~data:());
  assert (VT.mem reachable exit);
  let g =
    let g' =
      Hcfg.G.fold_vertex g ~init:Hcfg.G.empty ~f:(fun u acc ->
          if VT.mem reachable u then Hcfg.G.add_vertex acc u else acc)
    in
    Hcfg.G.fold_edges_e g ~init:g' ~f:(fun e acc ->
        let u = Hcfg.G.E.src e in
        let vs = Hcfg.G.E.dsts e in
        if List.for_all (u :: vs) ~f:(VT.mem reachable)
        then Hcfg.G.add_edge_e acc e
        else acc)
  in
  let scope_map = VM.of_alist_exn (VT.to_alist scope_store) in
  { Hcfg.name
  ; ret = ret'
  ; params
  ; logicals
  ; requires
  ; instance
  ; body = g
  ; entry
  ; exit
  ; scope = (fun u -> VM.find_exn scope_map u)
  }
;;

let g_prog { Sast.filename; procs } =
  reset_counter ();
  { Hcfg.filename; procs = List.map procs ~f:g_proc }
;;
