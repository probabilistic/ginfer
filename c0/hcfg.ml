open Core

type frac = Mpqf.t

let compare_frac = Mpqf.cmp

type type_expression = Sast.type_expression =
  | T_bool
  | T_int
  | T_real
  | T_array of type_expression
  | T_prob
[@@deriving sexp, compare]

type ident = string [@@deriving sexp, compare]

type un_op = Sast.un_op =
  | Uop_not
  | Uop_negate
[@@deriving sexp, compare]

type bin_op = Sast.bin_op =
  | Bop_and
  | Bop_or
  | Bop_add
  | Bop_sub
  | Bop_mul
  | Bop_div
  | Bop_lt
  | Bop_le
  | Bop_ge
  | Bop_gt
  | Bop_eq
  | Bop_ne
[@@deriving sexp, compare]

type control =
  | C_seq of action
  | C_cond of expression
  | C_prob of expression
  | C_call of ident option * ident * ident list

and action =
  | A_assign of ident * expression
  | A_sample of ident * distribution
  | A_array_put of ident * expression * expression
  | A_array_get of ident * ident * expression
  | A_array_make of ident * type_expression * expression
  | A_decl_var of ident * type_expression
  | A_dead_vars of ident list
  | A_skip
  | A_tick of expression
  | A_assume of expression
  | A_split of expression
  | A_weaken

and expression = Sast.expression =
  { exp_desc : expression_desc
  ; exp_type : type_expression
  }

and expression_desc = Sast.expression_desc =
  | E_literal of literal
  | E_var of ident
  | E_binary of bin_op * expression * expression
  | E_unary of un_op * expression
  | E_demon

and literal = Sast.literal =
  | L_true
  | L_false
  | L_int of int
  | L_float of float
  | L_frac of (frac[@sexp.opaque])
  | L_prob of int * int

and distribution = Sast.distribution =
  { dist_desc : distribution_desc
  ; dist_type : type_expression
  }

and distribution_desc = Sast.distribution_desc =
  | D_discrete_uniform of expression * expression
  | D_uniform of expression * expression
  | D_bernoulli of expression
  | D_binomial of expression * expression
  | D_hyper of expression * expression * expression
[@@deriving sexp, compare]

module G =
  Hyper_graph.Make
    (Int)
    (struct
      type t = control [@@deriving sexp]

      let default = C_seq A_skip
    end)

type program =
  { filename : string
  ; procs : procedure list
  }

and procedure =
  { name : ident
  ; ret : (ident * type_expression) option
  ; params : (ident * type_expression) list
  ; logicals : (ident * type_expression) list
  ; requires : expression
  ; instance : literal String.Map.t
  ; body : G.t
  ; entry : G.vertex
  ; exit : G.vertex
  ; scope : G.vertex -> (ident * type_expression) list
  }
[@@deriving sexp]

let rec fv_exp e =
  match e.exp_desc with
  | E_literal _ | E_demon -> String.Set.empty
  | E_var x -> String.Set.singleton x
  | E_binary (_, e1, e2) -> String.Set.union (fv_exp e1) (fv_exp e2)
  | E_unary (_, e0) -> fv_exp e0
;;

let fv_dist mu =
  match mu.dist_desc with
  | D_discrete_uniform (e1, e2) -> String.Set.union (fv_exp e1) (fv_exp e2)
  | D_uniform (e1, e2) -> String.Set.union (fv_exp e1) (fv_exp e2)
  | D_bernoulli e -> fv_exp e
  | D_binomial (e1, e2) -> String.Set.union (fv_exp e1) (fv_exp e2)
  | D_hyper (e1, e2, e3) -> String.Set.union_list [ fv_exp e1; fv_exp e2; fv_exp e3 ]
;;
