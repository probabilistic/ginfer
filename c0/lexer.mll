{
open Core
open Parser

let reserved_words =
  [ ("alloc_array", ALLOC_ARRAY)
  ; ("bool", BOOL)
  ; ("break", BREAK)
  ; ("continue", CONTINUE)
  ; ("demon", DEMON)
  ; ("else", ELSE)
  ; ("false", FALSE)
  ; ("flip", FLIP)
  ; ("for", FOR)
  ; ("if", IF)
  ; ("int", INT)
  ; ("prob", PROB)
  ; ("real", REAL)
  ; ("return", RETURN)
  ; ("sample", SAMPLE)
  ; ("tick", TICK)
  ; ("true", TRUE)
  ; ("void", VOID)
  ; ("while", WHILE)

  ;( "&&", AMPAMP)
  ; ("*", AST)
  ; ("*=", ASTEQ)
  ; ("!", BANG)
  ; ("!=", BANGEQ)
  ; ("||", BARBAR)
  ; (":", COLON)
  ; (",", COMMA)
  ; ("=", EQ)
  ; ("==", EQEQ)
  ; (">=", GE)
  ; (">", GT)
  ; ("{", LCURLY)
  ; ("<=", LE)
  ; ("(", LPAREN)
  ; ("[", LSQUARE)
  ; ("<", LT)
  ; ("-", MINUS)
  ; ("-=", MINUSEQ)
  ; ("--", MINUSMINUS)
  ; ("+", PLUS)
  ; ("+=", PLUSEQ)
  ; ("++", PLUSPLUS)
  ; ("?", QUESTION)
  ; ("}", RCURLY)
  ; (")", RPAREN)
  ; ("]", RSQUARE)
  ; (";", SEMI)
  ; ("/", SLASH)
  ; ("/=", SLASHEQ)

  ; ("//@assume", ASSUME)
  ; ("//@auxiliary", AUXILIARY)
  ; ("//@instance", INSTANCE)
  ; ("//@perform", PERFORM)
  ; ("//@requires", REQUIRES)
  ; ("//@split", SPLIT)
  ; ("//@unroll", UNROLL) ]

let symbol_table = Hashtbl.of_alist_exn (module String) reserved_words

let update_loc lexbuf file line absolute chars =
  let pos = lexbuf.Lexing.lex_curr_p in
  let new_file = match file with None -> pos.pos_fname | Some s -> s in
  lexbuf.lex_curr_p <-
    { pos with
      pos_fname= new_file
    ; pos_lnum= (if absolute then line else pos.pos_lnum + line)
    ; pos_bol= pos.pos_cnum - chars }

exception Lex_error of string * Location.t

let error lexbuf msg = raise (Lex_error (msg, Location.curr lexbuf))

let () =
  Location.register_error_of_exn (function
      | Lex_error (msg, loc) -> Some (Location.errorf ~loc "%s" msg)
      | _ -> None)
}

let newline = ('\013'* '\010')
let blank = [' ' '\009' '\012']
let digit = ['0'-'9']
let lower = ['a'-'z']
let upper = ['A'-'Z']

let decimal_literal = digit+
let hex_literal = "0x" ['0'-'9' 'A'-'F' 'a'-'f']+
let int_literal = decimal_literal | hex_literal

let float_literal = digit+ '.' digit+ (['E' 'e'] '-'? digit+)?

rule token_exn = parse
  | newline
    { update_loc lexbuf None 1 false 0; token_exn lexbuf }
  | blank+
    { token_exn lexbuf }
  | (lower | upper | '_') (lower | upper | digit | '_')* as name
    { match Hashtbl.find symbol_table name with
      | Some kwd ->
          kwd
      | None ->
          ID name }
  | int_literal as lit
    { INTV (Int.of_string lit) }
  | float_literal as lit
    { FLOATV (Float.of_string lit) }
  | (int_literal '/' int_literal) as lit
    { FRACV (Mpqf.of_string lit) }
  | "//@assume" | "//@auxiliary" | "//@instance" | "//@perform" | "//@requires" | "//@split" | "//@unroll"
    { Hashtbl.find_exn symbol_table (Lexing.lexeme lexbuf) }
  | "//"
    { comment_exn lexbuf }
  | "&&" | "*=" | "!=" | "||" | "==" | ">=" | "<=" | "-=" | "--" | "+=" | "++" | "/="
    { Hashtbl.find_exn symbol_table (Lexing.lexeme lexbuf) }
  | ['*' '!' ':' ',' '=' '>' '{' '(' '[' '<' '-' '+' '?' '}' ')' ']' ';' '/']
    { Hashtbl.find_exn symbol_table (Lexing.lexeme lexbuf) }
  | eof
    { EOF }
  | _ as illegal_char
    { error lexbuf ("illegal character (" ^ Char.escaped illegal_char ^ ")") }

and comment_exn = parse
  | newline
    { update_loc lexbuf None 1 false 0; token_exn lexbuf }
  | _
    { comment_exn lexbuf }
