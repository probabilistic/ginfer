%{
open Past

let make_loc (start_pos, end_pos) =
  { Location.loc_start = start_pos
  ; Location.loc_end = end_pos
  ; Location.loc_ghost = false
  }

let mk_proc ~loc proc_desc = { proc_desc; proc_loc = make_loc loc }
let mk_stmt ~loc stmt_desc = { stmt_desc; stmt_loc = make_loc loc }
let mk_simp_stmt ~loc simp_stmt_desc = { simp_stmt_desc; simp_stmt_loc = make_loc loc }
let mk_lv ~loc lv_desc = { lv_desc; lv_loc = make_loc loc }
let mk_ty ~loc ty_desc = { ty_desc; ty_loc = make_loc loc }
let mk_exp ~loc exp_desc = { exp_desc; exp_loc = make_loc loc }
let mk_dist ~loc dist_desc = { dist_desc; dist_loc = make_loc loc }
%}

%token ALLOC_ARRAY      "alloc_array"
%token BOOL             "bool"
%token BREAK            "break"
%token CONTINUE         "continue"
%token DEMON            "demon"
%token ELSE             "else"
%token FALSE            "false"
%token FLIP             "flip"
%token FOR              "for"
%token IF               "if"
%token INT              "int"
%token PROB             "prob"
%token REAL             "real"
%token RETURN           "return"
%token SAMPLE           "sample"
%token TICK             "tick"
%token TRUE             "true"
%token VOID             "void"
%token WHILE            "while"

%token <float>  FLOATV  "1.0"   (* just an example *)
%token <Mpqf.t> FRACV   "1/2"   (* just an example *)
%token <string> ID      "ident" (* just an example *)
%token <int>    INTV     "42"   (* just an example *)

%token AMPAMP           "&&"
%token AST              "*"
%token ASTEQ            "*="
%token BANG             "!"
%token BANGEQ           "!="
%token BARBAR           "||"
%token COLON            ":"
%token COMMA            ","
%token EQ               "="
%token EQEQ             "=="
%token GE               ">="
%token GT               ">"
%token LCURLY           "{"
%token LE               "<="
%token LPAREN           "("
%token LSQUARE          "["
%token LT               "<"
%token MINUS            "-"
%token MINUSEQ          "-="
%token MINUSMINUS       "--"
%token PLUS             "+"
%token PLUSEQ           "+="
%token PLUSPLUS         "++"
%token QUESTION         "?"
%token RCURLY           "}"
%token RPAREN           ")"
%token RSQUARE          "]"
%token SEMI             ";"
%token SLASH            "/"
%token SLASHEQ          "/="

%token ASSUME           "//@assume"
%token AUXILIARY        "//@auxiliary"
%token INSTANCE         "//@instance"
%token PERFORM          "//@perform"
%token REQUIRES         "//@requires"
%token SPLIT            "//@split"
%token UNROLL           "//@unroll"

%token EOF              ""

%nonassoc prec_if
%nonassoc ELSE
%right QUESTION COLON
%left BARBAR
%left AMPAMP
%left EQEQ BANGEQ
%left LT LE GE GT
%left PLUS MINUS
%left AST SLASH
%right BANG prec_unary_minus
%left LSQUARE

%start program_exn
%type <filename:string -> Past.program> program_exn

%%

%inline mk_proc(symb): symb { mk_proc ~loc:$sloc $1 }
%inline mk_stmt(symb): symb { mk_stmt ~loc:$sloc $1 }
%inline mk_simp_stmt(symb): symb { mk_simp_stmt ~loc:$sloc $1 }
%inline mk_lv(symb): symb { mk_lv ~loc:$sloc $1 }
%inline mk_ty(symb): symb { mk_ty ~loc:$sloc $1 }
%inline mk_exp(symb): symb { mk_exp ~loc:$sloc $1 }
%inline mk_dist(symb): symb { mk_dist ~loc:$sloc $1 }

program_exn:
  | nonempty_list(procedure) EOF
    { fun ~filename -> { filename; procs = $1 } }

procedure:
  | mk_proc(procedure_desc)
    { $1 }

procedure_desc:
  | ty ident LPAREN separated_list(COMMA, vtbind) RPAREN list(auxiliary) list(require) list(instance) LCURLY list(statement) RCURLY
    { { name = $2; ret = $1; params = $4; logicals = $6; requires = $7; instance = List.concat $8; body = $10 } }

auxiliary:
  | AUXILIARY vtbind SEMI
    { $2 }

require:
  | REQUIRES expression SEMI
    { $2 }

instance:
  | INSTANCE separated_list(COMMA, vebind) SEMI
    { $2 }

%inline vebind:
  | ident EQ expression
    { ($1, $3) }

statement:
  | mk_stmt(statement_desc)
    { $1 }

statement_desc:
  | simple_statement SEMI
    { S_simple $1 }
  | IF LPAREN expression RPAREN statement %prec prec_if
    { S_if ($3, $5, None) }
  | IF LPAREN expression RPAREN statement ELSE statement
    { S_if ($3, $5, Some $7) }
  | FLIP LPAREN expression RPAREN statement %prec prec_if
    { S_flip ($3, $5, None) }
  | FLIP LPAREN expression RPAREN statement ELSE statement
    { S_flip ($3, $5, Some $7) }
  | WHILE LPAREN expression RPAREN option(unroll_flag) statement
    { S_while (Option.is_some $5, $3, $6) }
  | FOR LPAREN option(simple_statement) SEMI expression SEMI option(simple_statement) RPAREN option(unroll_flag) statement
    { S_for (Option.is_some $9, $3, $5, $7, $10) }
  | RETURN option(expression) SEMI
    { S_return $2 }
  | LCURLY list(statement) RCURLY
    { S_block $2 }
  | BREAK SEMI
    { S_break }
  | CONTINUE SEMI
    { S_continue }
  | ASSUME expression SEMI
    { S_meta_assume $2 }
  | PERFORM simple_statement SEMI
    { S_meta_perform $2 }
  | SPLIT expression SEMI
    { S_meta_split $2 }

%inline unroll_flag:
  | UNROLL SEMI
    { () }

simple_statement:
  | mk_simp_stmt(simple_statement_desc)
    { $1 }

simple_statement_desc:
  | lvalue assign_op expression
    { S_assign ($2, $1, $3) }
  | lvalue PLUSPLUS
    { S_incr $1 }
  | lvalue MINUSMINUS
    { S_decr $1 }
  | expression
    { S_eval_exp $1 }
  | vtbind option(preceded(EQ, expression))
    { S_decl_var ($1, $2) }

lvalue:
  | mk_lv(lvalue_desc)
    { $1 }

lvalue_desc:
  | ident
    { L_var $1 }
  | lvalue LSQUARE expression RSQUARE
    { L_array_get ($1, $3) }

ty:
  | mk_ty(ty_desc)
    { $1 }

ty_desc:
  | BOOL
    { T_bool }
  | INT
    { T_int }
  | REAL
    { T_real }
  | VOID
    { T_void }
  | ty LSQUARE RSQUARE
    { T_array $1 }
  | PROB
    { T_prob }

expression:
  | LPAREN expression RPAREN
    { $2 }
  | mk_exp(expression_desc)
    { $1 }

expression_desc:
  | TRUE
    { E_true }
  | FALSE
    { E_false }
  | INTV
    { E_int $1 }
  | FLOATV
    { E_float $1 }
  | FRACV
    { E_frac $1 }
  | ident
    { E_var $1 }
  | expression bin_op expression
    { E_binary ($2, $1, $3) }
  | BANG expression
    { match $2.exp_desc with
      | E_true -> E_false
      | E_false -> E_true
      | E_prob (a, b) -> E_prob (b, a)
      | _ -> E_unary (Uop_not, $2) }
  | MINUS expression %prec prec_unary_minus
    { match $2.exp_desc with
      | E_int n -> E_int (- n)
      | E_float d -> E_float (-. d)
      | E_frac f -> E_frac (Mpqf.neg f)
      | _ -> E_unary (Uop_negate, $2) }
  | expression QUESTION expression COLON expression
    { E_cond ($1, $3, $5) }
  | ident LPAREN separated_list(COMMA, expression) RPAREN
    { E_call ($1, $3) }
  | expression LSQUARE expression RSQUARE
    { E_array_get ($1, $3) }
  | ALLOC_ARRAY LPAREN ty COMMA expression RPAREN
    { E_alloc_array ($3, $5) }
  | PROB LPAREN INTV COMMA INTV RPAREN
    { E_prob ($3, $5) }
  | SAMPLE LPAREN distribution RPAREN
    { E_sample $3 }
  | TICK LPAREN expression RPAREN
    { E_tick $3 }
  | DEMON LPAREN RPAREN
    { E_demon }

distribution:
  | mk_dist(distribution_desc)
    { $1 }

distribution_desc:
  | ident LPAREN separated_list(COMMA, expression) RPAREN
    { lookup_distribution_desc_exn $1 $3 }

%inline ident:
  | ID
    { Location.mkloc $1 (make_loc $sloc) }

%inline vtbind:
  | ty ident
    { ($2, $1) }

%inline bin_op:
  | AMPAMP
    { Bop_and }
  | BARBAR
    { Bop_or }
  | PLUS
    { Bop_add }
  | MINUS
    { Bop_sub }
  | AST
    { Bop_mul }
  | SLASH
    { Bop_div }
  | LT
    { Bop_lt }
  | LE
    { Bop_le }
  | GE
    { Bop_ge }
  | GT
    { Bop_gt }
  | EQEQ
    { Bop_eq }
  | BANGEQ
    { Bop_ne }

%inline assign_op:
  | EQ
    { Aop_mov }
  | PLUSEQ
    { Aop_add }
  | MINUSEQ
    { Aop_sub }
  | ASTEQ
    { Aop_mul }
  | SLASHEQ
    { Aop_div }
