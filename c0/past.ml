type 'a loc = 'a Location.loc =
  { txt : 'a
  ; loc : Location.t
  }

type ident = string loc

type un_op =
  | Uop_not
  | Uop_negate

type bin_op =
  | Bop_and
  | Bop_or
  | Bop_add
  | Bop_sub
  | Bop_mul
  | Bop_div
  | Bop_lt
  | Bop_le
  | Bop_ge
  | Bop_gt
  | Bop_eq
  | Bop_ne

type assign_op =
  | Aop_mov
  | Aop_add
  | Aop_sub
  | Aop_mul
  | Aop_div

type program =
  { filename : string
  ; procs : procedure list
  }

and procedure =
  { proc_desc : procedure_desc
  ; proc_loc : Location.t
  }

and procedure_desc =
  { name : ident
  ; ret : ty
  ; params : (ident * ty) list
  ; logicals : (ident * ty) list
  ; requires : expression list
  ; instance : (ident * expression) list
  ; body : statement list
  }

and statement =
  { stmt_desc : statement_desc
  ; stmt_loc : Location.t
  }

and statement_desc =
  | S_simple of simple_statement
  | S_if of expression * statement * statement option
  | S_flip of expression * statement * statement option
  | S_while of bool * expression * statement
  | S_for of
      bool * simple_statement option * expression * simple_statement option * statement
  | S_return of expression option
  | S_block of statement list
  | S_break
  | S_continue
  | S_meta_assume of expression
  | S_meta_perform of simple_statement
  | S_meta_split of expression

and simple_statement =
  { simp_stmt_desc : simple_statement_desc
  ; simp_stmt_loc : Location.t
  }

and simple_statement_desc =
  | S_assign of assign_op * lvalue * expression
  | S_incr of lvalue
  | S_decr of lvalue
  | S_eval_exp of expression
  | S_decl_var of (ident * ty) * expression option

and lvalue =
  { lv_desc : lvalue_desc
  ; lv_loc : Location.t
  }

and lvalue_desc =
  | L_var of ident
  | L_array_get of lvalue * expression

and ty =
  { ty_desc : ty_desc
  ; ty_loc : Location.t
  }

and ty_desc =
  | T_bool
  | T_int
  | T_real
  | T_void
  | T_array of ty
  | T_prob

and expression =
  { exp_desc : expression_desc
  ; exp_loc : Location.t
  }

and expression_desc =
  | E_true
  | E_false
  | E_int of int
  | E_float of float
  | E_frac of Mpqf.t
  | E_var of ident
  | E_binary of bin_op * expression * expression
  | E_unary of un_op * expression
  | E_cond of expression * expression * expression
  | E_call of ident * expression list
  | E_array_get of expression * expression
  | E_alloc_array of ty * expression
  | E_prob of int * int
  | E_sample of distribution
  | E_tick of expression
  | E_demon

and distribution =
  { dist_desc : distribution_desc
  ; dist_loc : Location.t
  }

and distribution_desc =
  | D_discrete_uniform of expression * expression
  | D_uniform of expression * expression
  | D_bernoulli of expression
  | D_binomial of expression * expression
  | D_hyper of expression * expression * expression

exception Unknown_distribution of string * Location.t

let lookup_distribution_desc_exn u es =
  match u.txt, es with
  | "DiscreteUniform", [ e1; e2 ] -> D_discrete_uniform (e1, e2)
  | "Uniform", [ e1; e2 ] -> D_uniform (e1, e2)
  | "Bernoulli", [ e ] -> D_bernoulli e
  | "Binomial", [ e1; e2 ] -> D_binomial (e1, e2)
  | "HyperGeometric", [ e1; e2; e3 ] -> D_hyper (e1, e2, e3)
  | _ -> raise (Unknown_distribution (u.txt, u.loc))
;;

let () =
  Location.register_error_of_exn (function
      | Unknown_distribution (name, loc) ->
        Some (Location.errorf ~loc "unknown distribution: %s" name)
      | _ -> None)
;;
