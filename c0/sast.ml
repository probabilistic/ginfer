open Core

type type_expression =
  | T_bool
  | T_int
  | T_real
  | T_array of type_expression
  | T_prob

type ident = string

type un_op = Tast.un_op =
  | Uop_not
  | Uop_negate

type bin_op = Tast.bin_op =
  | Bop_and
  | Bop_or
  | Bop_add
  | Bop_sub
  | Bop_mul
  | Bop_div
  | Bop_lt
  | Bop_le
  | Bop_ge
  | Bop_gt
  | Bop_eq
  | Bop_ne

type program =
  { filename : string
  ; procs : procedure list
  }

and procedure =
  { name : ident
  ; ret : type_expression option
  ; params : (ident * type_expression) list
  ; logicals : (ident * type_expression) list
  ; requires : expression
  ; instance : literal String.Map.t
  ; body : statement list
  }

and statement =
  | S_assign of ident * expression
  | S_sample of ident * distribution
  | S_call of ident option * ident * ident list
  | S_array_put of ident * expression * expression
  | S_array_get of ident * ident * expression
  | S_array_make of ident * type_expression * expression
  | S_decl_var of ident * type_expression
  | S_if of expression * statement * statement
  | S_flip of expression * statement * statement
  | S_loop of bool * (statement list * expression) * statement * statement
  | S_return of expression option
  | S_block of statement list
  | S_break
  | S_continue
  | S_tick of expression
  | S_meta_assume of expression
  | S_meta_perform of statement
  | S_meta_split of expression

and expression =
  { exp_desc : expression_desc
  ; exp_type : type_expression
  }

and expression_desc =
  | E_literal of literal
  | E_var of ident
  | E_binary of bin_op * expression * expression
  | E_unary of un_op * expression
  | E_demon

and literal =
  | L_true
  | L_false
  | L_int of int
  | L_float of float
  | L_frac of Mpqf.t
  | L_prob of int * int

and distribution =
  { dist_desc : distribution_desc
  ; dist_type : type_expression
  }

and distribution_desc =
  | D_discrete_uniform of expression * expression
  | D_uniform of expression * expression
  | D_bernoulli of expression
  | D_binomial of expression * expression
  | D_hyper of expression * expression * expression
