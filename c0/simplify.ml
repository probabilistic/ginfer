open Core

let counter = ref 0
let reset_counter () = counter := 0

let fresh_ident x =
  Int.incr counter;
  Format.sprintf "%s.%d" x !counter
;;

let sym_of_type = function
  | Sast.T_bool -> "bool"
  | T_int -> "int"
  | T_real -> "real"
  | T_array _ -> "array"
  | T_prob -> "prob"
;;

let fresh_temp t =
  Int.incr counter;
  Format.sprintf "%%%s.%d" (sym_of_type t) !counter
;;

let rec coerce_assignable_type = function
  | Tast.T_bool -> Sast.T_bool
  | T_int -> T_int
  | T_real -> T_real
  | T_void -> assert false
  | T_array t0 -> T_array (coerce_assignable_type t0)
  | T_prob -> T_prob
;;

let coerce_type = function
  | Tast.T_bool -> Some Sast.T_bool
  | T_int -> Some T_int
  | T_real -> Some T_real
  | T_void -> None
  | T_array t0 -> Some (T_array (coerce_assignable_type t0))
  | T_prob -> Some T_prob
;;

let rec g_exp ctx e =
  let c exp_desc =
    Some { Sast.exp_desc; exp_type = coerce_assignable_type e.Tast.exp_type }
  in
  match e.exp_desc with
  | E_true -> c (E_literal L_true), []
  | E_false -> c (E_literal L_false), []
  | E_int n -> c (E_literal (L_int n)), []
  | E_float d -> c (E_literal (L_float d)), []
  | E_frac f -> c (E_literal (L_frac f)), []
  | E_var x -> c (E_var (Map.find_exn ctx x)), []
  | E_binary (bop, e1, e2) ->
    let e1'_opt, ss1 = g_exp ctx e1 in
    let e2'_opt, ss2 = g_exp ctx e2 in
    let e1' = Option.value_exn e1'_opt in
    let e2' = Option.value_exn e2'_opt in
    c (E_binary (bop, e1', e2')), ss1 @ ss2
  | E_unary (uop, e0) ->
    let e0'_opt, ss0 = g_exp ctx e0 in
    let e0' = Option.value_exn e0'_opt in
    c (E_unary (uop, e0')), ss0
  | E_cond (e0, e1, e2) ->
    let e0'_opt, ss0 = g_exp ctx e0 in
    let e0' = Option.value_exn e0'_opt in
    let e1'_opt, ss1 = g_exp ctx e1 in
    let e2'_opt, ss2 = g_exp ctx e2 in
    (match e1'_opt, e2'_opt with
    | None, None -> None, ss0 @ [ Sast.S_if (e0', S_block ss1, S_block ss2) ]
    | Some e1', Some e2' ->
      let t = coerce_assignable_type e.exp_type in
      let temp = fresh_temp t in
      ( c (E_var temp)
      , ss0
        @ [ S_decl_var (temp, t)
          ; S_if
              ( e0'
              , S_block (ss1 @ [ S_assign (temp, e1') ])
              , S_block (ss2 @ [ S_assign (temp, e2') ]) )
          ] )
    | _ -> assert false)
  | E_call (f, e0s) ->
    let rec loop rev_acc rev_ss = function
      | [] ->
        (match coerce_type e.Tast.exp_type with
        | None ->
          ( None
          , [ Sast.S_block
                (List.concat (List.rev rev_ss)
                @ [ Sast.S_call (None, f, List.rev rev_acc) ])
            ] )
        | Some t ->
          let temp = fresh_temp t in
          ( c (E_var temp)
          , [ S_decl_var (temp, t)
            ; S_block
                (List.concat (List.rev rev_ss)
                @ [ S_call (Some temp, f, List.rev rev_acc) ])
            ] ))
      | e0h :: e0t ->
        let xh, ss = g_exp_named ~force_fresh:true ctx e0h in
        loop (xh :: rev_acc) (ss :: rev_ss) e0t
    in
    loop [] [] e0s
  | E_array_get (e0, e1) ->
    let x0, ss0 = g_exp_named ctx e0 in
    let e1'_opt, ss1 = g_exp ctx e1 in
    let e1' = Option.value_exn e1'_opt in
    let t = coerce_assignable_type e.exp_type in
    let temp = fresh_temp t in
    ( c (E_var temp)
    , [ S_decl_var (temp, t); S_block (ss0 @ ss1 @ [ S_array_get (temp, x0, e1') ]) ] )
  | E_alloc_array (t, e0) ->
    let t' = coerce_assignable_type t in
    let t_of_arr = coerce_assignable_type e.exp_type in
    let e0'_opt, ss0 = g_exp ctx e0 in
    let e0' = Option.value_exn e0'_opt in
    let temp = fresh_temp t_of_arr in
    ( c (E_var temp)
    , [ S_decl_var (temp, t_of_arr); S_block (ss0 @ [ S_array_make (temp, t', e0') ]) ] )
  | E_prob (a, b) -> c (E_literal (L_prob (a, b))), []
  | E_sample mu ->
    let mu', ss = g_dist ctx mu in
    let temp = fresh_temp mu'.Sast.dist_type in
    ( c (E_var temp)
    , [ S_decl_var (temp, mu'.dist_type); S_block (ss @ [ Sast.S_sample (temp, mu') ]) ] )
  | E_tick e0 ->
    let e0'_opt, ss0 = g_exp ctx e0 in
    let e0' = Option.value_exn e0'_opt in
    None, [ S_block (ss0 @ [ S_tick e0' ]) ]
  | E_demon -> c E_demon, []

and g_exp_named ?(force_fresh = false) ctx e =
  let e'_opt, ss = g_exp ctx e in
  let e' = Option.value_exn e'_opt in
  match e'.exp_desc with
  | E_var x when not force_fresh -> x, ss
  | _ ->
    let t = coerce_assignable_type e.Tast.exp_type in
    let temp = fresh_temp t in
    temp, [ S_decl_var (temp, t); S_block (ss @ [ S_assign (temp, e') ]) ]

and g_dist ctx mu =
  let c dist_desc =
    { Sast.dist_desc; dist_type = coerce_assignable_type mu.Tast.dist_type }
  in
  match mu.Tast.dist_desc with
  | D_discrete_uniform (e1, e2) ->
    let e1'_opt, ss1 = g_exp ctx e1 in
    let e2'_opt, ss2 = g_exp ctx e2 in
    let e1' = Option.value_exn e1'_opt in
    let e2' = Option.value_exn e2'_opt in
    c (D_discrete_uniform (e1', e2')), ss1 @ ss2
  | D_uniform (e1, e2) ->
    let e1'_opt, ss1 = g_exp ctx e1 in
    let e2'_opt, ss2 = g_exp ctx e2 in
    let e1' = Option.value_exn e1'_opt in
    let e2' = Option.value_exn e2'_opt in
    c (D_uniform (e1', e2')), ss1 @ ss2
  | D_bernoulli e ->
    let e'_opt, ss = g_exp ctx e in
    let e' = Option.value_exn e'_opt in
    c (D_bernoulli e'), ss
  | D_binomial (e1, e2) ->
    let e1'_opt, ss1 = g_exp ctx e1 in
    let e2'_opt, ss2 = g_exp ctx e2 in
    let e1' = Option.value_exn e1'_opt in
    let e2' = Option.value_exn e2'_opt in
    c (D_binomial (e1', e2')), ss1 @ ss2
  | D_hyper (e1, e2, e3) ->
    let e1'_opt, ss1 = g_exp ctx e1 in
    let e2'_opt, ss2 = g_exp ctx e2 in
    let e3'_opt, ss3 = g_exp ctx e3 in
    let e1' = Option.value_exn e1'_opt in
    let e2' = Option.value_exn e2'_opt in
    let e3' = Option.value_exn e3'_opt in
    c (D_hyper (e1', e2', e3')), ss1 @ ss2 @ ss3
;;

let rec g_lv ctx lv k =
  match lv.Tast.lv_desc with
  | L_var x -> k (`Var (Map.find_exn ctx x), [])
  | L_array_get (lv0, e) ->
    g_lv ctx lv0 (fun (lv0', ss0) ->
        let e'_opt, ss1 = g_exp ctx e in
        let e' = Option.value_exn e'_opt in
        match lv0' with
        | `Var x0 -> k (`Array_get (x0, e'), ss0 @ ss1)
        | `Array_get (x0, e0) ->
          let t = coerce_assignable_type lv0.lv_type in
          let temp = fresh_temp t in
          k
            ( `Array_get (temp, e')
            , [ Sast.S_decl_var (temp, t)
              ; S_block (ss0 @ [ S_array_get (temp, x0, e0) ])
              ]
              @ ss1 ))
;;

let rec g_stmt ctx = function
  | Tast.S_assign (lv, e) ->
    g_lv ctx lv (fun (lv', ss0) ->
        let e'_opt, ss1 = g_exp ctx e in
        let e' = Option.value_exn e'_opt in
        match e'.exp_type with
        | T_bool
          when match e'.exp_desc with
               | E_literal _ -> false
               | _ -> true ->
          ( [ Sast.S_block
                (ss0
                @ ss1
                @ [ S_if
                      ( e'
                      , (match lv' with
                        | `Var x0 ->
                          S_assign (x0, { exp_desc = E_literal L_true; exp_type = T_bool })
                        | `Array_get (x0, e0) ->
                          S_array_put
                            (x0, e0, { exp_desc = E_literal L_true; exp_type = T_bool }))
                      , match lv' with
                        | `Var x0 ->
                          S_assign
                            (x0, { exp_desc = E_literal L_false; exp_type = T_bool })
                        | `Array_get (x0, e0) ->
                          S_array_put
                            (x0, e0, { exp_desc = E_literal L_false; exp_type = T_bool })
                      )
                  ])
            ]
          , ctx )
        | _ ->
          ( [ S_block
                (ss0
                @ ss1
                @ [ (match lv' with
                    | `Var x0 -> S_assign (x0, e')
                    | `Array_get (x0, e0) -> S_array_put (x0, e0, e'))
                  ])
            ]
          , ctx ))
  | S_eval_exp e ->
    let e'_opt, ss = g_exp ctx e in
    (match coerce_type e.exp_type with
    | None ->
      assert (Option.is_none e'_opt);
      [ S_block ss ], ctx
    | Some t ->
      let temp = fresh_temp t in
      let e' = Option.value_exn e'_opt in
      [ S_block (ss @ [ S_decl_var (temp, t); S_assign (temp, e') ]) ], ctx)
  | S_decl_var ((x, t), e_opt) ->
    let t' = coerce_assignable_type t in
    let x' = fresh_ident x in
    let ctx' = Map.add_exn ctx ~key:x ~data:x' in
    (match e_opt with
    | None -> [ S_decl_var (x', t') ], ctx'
    | Some e ->
      let e'_opt, ss = g_exp ctx e in
      let e' = Option.value_exn e'_opt in
      [ S_decl_var (x', t'); S_block (ss @ [ S_assign (x', e') ]) ], ctx')
  | S_if (e, s1, s2_opt) ->
    let e'_opt, ss = g_exp ctx e in
    let e' = Option.value_exn e'_opt in
    let s1', _ = g_stmt ctx s1 in
    let s2' =
      match s2_opt with
      | None -> []
      | Some s2 -> g_stmt ctx s2 |> fst
    in
    [ S_block (ss @ [ S_if (e', S_block s1', S_block s2') ]) ], ctx
  | S_flip (e, s1, s2_opt) ->
    let e'_opt, ss = g_exp ctx e in
    let e' = Option.value_exn e'_opt in
    let s1', _ = g_stmt ctx s1 in
    let s2' =
      match s2_opt with
      | None -> []
      | Some s2 -> g_stmt ctx s2 |> fst
    in
    [ S_block (ss @ [ S_flip (e', S_block s1', S_block s2') ]) ], ctx
  | S_while (uf, e, s0) ->
    let e'_opt, ss = g_exp ctx e in
    let e' = Option.value_exn e'_opt in
    let s0', _ = g_stmt ctx s0 in
    [ S_loop (uf, (ss, e'), S_block s0', S_block []) ], ctx
  | S_for (uf, s1_opt, e, s2_opt, s3) ->
    let s1', ctx' =
      match s1_opt with
      | None -> [], ctx
      | Some s1 -> g_stmt ctx s1
    in
    let e'_opt, ss = g_exp ctx' e in
    let e' = Option.value_exn e'_opt in
    let s2' =
      match s2_opt with
      | None -> []
      | Some s2 -> g_stmt ctx' s2 |> fst
    in
    let s3', _ = g_stmt ctx' s3 in
    [ S_block (s1' @ [ S_loop (uf, (ss, e'), S_block s3', S_block s2') ]) ], ctx
  | S_return e_opt ->
    (match e_opt with
    | None -> [ S_return None ], ctx
    | Some e ->
      let e'_opt, ss = g_exp ctx e in
      (match e'_opt with
      | None -> [ S_block (ss @ [ S_return None ]) ], ctx
      | Some e' -> [ S_block (ss @ [ S_return (Some e') ]) ], ctx))
  | S_block s0s ->
    let rev_ss, _ =
      List.fold s0s ~init:([], ctx) ~f:(fun (acc, ctx) s0 ->
          let ss, ctx' = g_stmt ctx s0 in
          ss :: acc, ctx')
    in
    [ S_block (List.concat (List.rev rev_ss)) ], ctx
  | S_break -> [ S_break ], ctx
  | S_continue -> [ S_continue ], ctx
  | S_meta_assume e ->
    let e'_opt, ss = g_exp ctx e in
    assert (List.is_empty ss);
    [ S_meta_assume (Option.value_exn e'_opt) ], ctx
  | S_meta_perform s0 ->
    let s0', ctx' = g_stmt ctx s0 in
    assert (List.length s0' = 1);
    [ S_meta_perform (List.hd_exn s0') ], ctx'
  | S_meta_split e ->
    let e'_opt, ss = g_exp ctx e in
    assert (List.is_empty ss);
    [ S_meta_split (Option.value_exn e'_opt) ], ctx
;;

let g_proc { Tast.name; ret; params; logicals; requires; instance; body } =
  let ret' = coerce_type ret in
  let params' = List.map params ~f:(fun (x, t) -> x, coerce_assignable_type t) in
  let logicals' = List.map logicals ~f:(fun (x, t) -> x, coerce_assignable_type t) in
  let requires' =
    let ctx =
      List.fold (params' @ logicals') ~init:String.Map.empty ~f:(fun acc (x, _) ->
          Map.add_exn acc ~key:x ~data:x)
    in
    List.fold_right
      requires
      ~init:{ Sast.exp_desc = E_literal L_true; exp_type = T_bool }
      ~f:(fun require acc ->
        let require'_opt, ss = g_exp ctx require in
        assert (List.is_empty ss);
        { Sast.exp_desc = E_binary (Bop_and, Option.value_exn require'_opt, acc)
        ; exp_type = T_bool
        })
  in
  let instance =
    String.Map.map instance ~f:(fun e ->
        let e'_opt, ss = g_exp String.Map.empty e in
        assert (List.is_empty ss);
        match Option.value_exn e'_opt with
        | { exp_desc = E_literal lit; _ } -> lit
        | _ -> assert false)
  in
  let fork_params' = List.map params' ~f:(fun (x, _) -> fresh_ident x) in
  let ctx =
    List.fold2_exn
      (params' @ logicals')
      (fork_params' @ List.map logicals' ~f:fst)
      ~init:String.Map.empty
      ~f:(fun acc (x, _) x' -> Map.add_exn acc ~key:x ~data:x')
  in
  let rev_ss, _ =
    List.fold body ~init:([], ctx) ~f:(fun (acc, ctx) s ->
        let ss, ctx' = g_stmt ctx s in
        ss :: acc, ctx')
  in
  let body' =
    List.map2_exn params' fork_params' ~f:(fun (_, t) x' -> Sast.S_decl_var (x', t))
    @ List.map2_exn params' fork_params' ~f:(fun (x, t) x' ->
          Sast.S_assign (x', { exp_desc = E_var x; exp_type = t }))
    @ List.concat (List.rev rev_ss)
  in
  { Sast.name
  ; ret = ret'
  ; params = params'
  ; logicals = logicals'
  ; requires = requires'
  ; instance
  ; body = body'
  }
;;

let g_prog { Tast.filename; procs } =
  reset_counter ();
  { Sast.filename; procs = List.map procs ~f:g_proc }
;;
