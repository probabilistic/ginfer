open Core

let rec g_stmt = function
  | Sast.S_if (e, s1, s2) -> Sast.S_if (e, g_stmt s1, g_stmt s2)
  | S_flip (e, s1, s2) -> S_flip (e, g_stmt s1, g_stmt s2)
  | S_loop (uf, (s1, e1), s2, s3) -> S_loop (uf, (g_stmts s1, e1), g_stmt s2, g_stmt s3)
  | S_block s0s ->
    let s0's = g_stmts s0s in
    (match s0's with
    | [] -> S_block []
    | [ s0'h ] ->
      (match s0'h with
      | S_decl_var _ -> S_block [ s0'h ]
      | _ -> s0'h)
    | s0's -> S_block s0's)
  | s -> s

and g_stmts ss =
  List.filter_map ss ~f:(fun s ->
      match g_stmt s with
      | S_block [] -> None
      | s' -> Some s')
;;

let g_proc ({ Sast.body; _ } as proc) =
  match g_stmt (S_block body) with
  | S_block s0's -> { proc with body = s0's }
  | s' -> { proc with body = [ s' ] }
;;

let g_prog ({ Sast.procs; _ } as prog) = { prog with procs = List.map procs ~f:g_proc }
