open Core

type type_expression =
  | T_bool
  | T_int
  | T_real
  | T_void
  | T_array of type_expression
  | T_prob
[@@deriving equal]

type ident = string

type un_op = Past.un_op =
  | Uop_not
  | Uop_negate

type bin_op = Past.bin_op =
  | Bop_and
  | Bop_or
  | Bop_add
  | Bop_sub
  | Bop_mul
  | Bop_div
  | Bop_lt
  | Bop_le
  | Bop_ge
  | Bop_gt
  | Bop_eq
  | Bop_ne

type program =
  { filename : string
  ; procs : procedure list
  }

and procedure =
  { name : ident
  ; ret : type_expression
  ; params : (ident * type_expression) list
  ; logicals : (ident * type_expression) list
  ; requires : expression list
  ; instance : expression String.Map.t
  ; body : statement list
  }

and statement =
  | S_assign of lvalue * expression
  | S_eval_exp of expression
  | S_decl_var of (ident * type_expression) * expression option
  | S_if of expression * statement * statement option
  | S_flip of expression * statement * statement option
  | S_while of bool * expression * statement
  | S_for of bool * statement option * expression * statement option * statement
  | S_return of expression option
  | S_block of statement list
  | S_break
  | S_continue
  | S_meta_assume of expression
  | S_meta_perform of statement
  | S_meta_split of expression

and lvalue =
  { lv_desc : lvalue_desc
  ; lv_type : type_expression
  }

and lvalue_desc =
  | L_var of ident
  | L_array_get of lvalue * expression

and expression =
  { exp_desc : expression_desc
  ; exp_type : type_expression
  }

and expression_desc =
  | E_true
  | E_false
  | E_int of int
  | E_float of float
  | E_frac of Mpqf.t
  | E_var of ident
  | E_binary of bin_op * expression * expression
  | E_unary of un_op * expression
  | E_cond of expression * expression * expression
  | E_call of ident * expression list
  | E_array_get of expression * expression
  | E_alloc_array of type_expression * expression
  | E_prob of int * int
  | E_sample of distribution
  | E_tick of expression
  | E_demon

and distribution =
  { dist_desc : distribution_desc
  ; dist_type : type_expression
  }

and distribution_desc =
  | D_discrete_uniform of expression * expression
  | D_uniform of expression * expression
  | D_bernoulli of expression
  | D_binomial of expression * expression
  | D_hyper of expression * expression * expression
