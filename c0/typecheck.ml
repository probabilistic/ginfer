open Core
open Result.Let_syntax

exception Type_check_error of string * Location.t

let find_fsig fsig f =
  match Map.find fsig f.Past.txt with
  | Some (tparams, tret) -> Ok (tparams, tret)
  | None -> Error (Type_check_error ("undefined function " ^ f.txt, f.loc))
;;

let find_tenv ?(meta = false) tenv undefs x =
  match Map.find tenv x.Past.txt with
  | Some (`O, t) ->
    if Set.mem undefs x.txt
    then Error (Type_check_error ("uninitialized variable " ^ x.txt, x.loc))
    else Ok t
  | Some (`A, t) ->
    if meta
    then Ok t
    else Error (Type_check_error ("cannot refer to logical variable " ^ x.txt, x.loc))
  | None -> Error (Type_check_error ("undefined variable " ^ x.txt, x.loc))
;;

let find_tenv_aux tenv undefs x =
  match Map.find tenv x.Past.txt with
  | Some (`A, t) ->
    if Set.mem undefs x.txt
    then Error (Type_check_error ("uninitialized variable " ^ x.txt, x.loc))
    else Ok t
  | Some (`O, _) ->
    Error (Type_check_error ("cannot refer to ordinary variable " ^ x.txt, x.loc))
  | None -> Error (Type_check_error ("undefined variable " ^ x.txt, x.loc))
;;

let is_movable ~f ~t =
  match f, t with
  | Tast.T_bool, Tast.T_bool
  | T_int, T_int
  | T_int, T_real
  | T_real, T_real
  | T_prob, T_prob -> true
  | T_array t10, T_array t20 when Tast.equal_type_expression t10 t20 -> true
  | _ -> false
;;

let rec check_assignable_ty t =
  match t.Past.ty_desc with
  | T_bool -> Ok Tast.T_bool
  | T_int -> Ok T_int
  | T_real -> Ok T_real
  | T_void -> Error (Type_check_error ("void is not assignable", t.ty_loc))
  | T_array t0 ->
    let%bind t0' = check_assignable_ty t0 in
    Ok (Tast.T_array t0')
  | T_prob -> Ok T_prob
;;

let check_ty t =
  match t.Past.ty_desc with
  | T_bool -> Ok Tast.T_bool
  | T_int -> Ok T_int
  | T_real -> Ok T_real
  | T_void -> Ok T_void
  | T_array t0 ->
    let%bind t0' = check_assignable_ty t0 in
    Ok (Tast.T_array t0')
  | T_prob -> Ok T_prob
;;

let extend_fsig fsig f (tparams, tret) =
  if Map.mem fsig f.Past.txt
  then Error (Type_check_error ("duplicate function " ^ f.txt, f.loc))
  else Ok (Map.add_exn fsig ~key:f.txt ~data:(tparams, tret))
;;

let extend_tenv tenv x t =
  if Map.mem tenv x.Past.txt
  then Error (Type_check_error ("duplicate variable " ^ x.txt, x.loc))
  else Ok (Map.add_exn tenv ~key:x.txt ~data:t)
;;

let check_proc_call ~loc (tparams, tret) targs =
  if List.length tparams <> List.length targs
  then Error (Type_check_error ("mismatched arity", loc))
  else (
    let%bind () =
      List.fold_result (List.zip_exn tparams targs) ~init:() ~f:(fun () (tparam, targ) ->
          if is_movable ~f:targ ~t:tparam
          then Ok ()
          else Error (Type_check_error ("mismatched signature", loc)))
    in
    Ok tret)
;;

let rec g_exp ?(meta = false) fsig tenv undefs =
  let ret exp_desc exp_type = Ok { Tast.exp_desc; exp_type } in
  let rec aux e =
    match e.Past.exp_desc with
    | E_true -> ret E_true T_bool
    | E_false -> ret E_false T_bool
    | E_int n -> ret (E_int n) T_int
    | E_float d -> ret (E_float d) T_real
    | E_frac f -> ret (E_frac f) T_real
    | E_var x ->
      let%bind t = find_tenv ~meta tenv undefs x in
      ret (E_var x.txt) t
    | E_binary (bop, e1, e2) ->
      let%bind e1' = aux e1 in
      let%bind e2' = aux e2 in
      let t1 = e1'.exp_type in
      let t2 = e2'.exp_type in
      let%bind t =
        match bop with
        | Bop_and | Bop_or ->
          (match t1, t2 with
          | T_bool, T_bool -> Ok Tast.T_bool
          | _ -> Error (Type_check_error ("expect boolean operands", e.exp_loc)))
        | Bop_add | Bop_sub | Bop_div ->
          (match t1, t2 with
          | T_int, T_int -> Ok T_int
          | T_int, T_real | T_real, T_int | T_real, T_real -> Ok T_real
          | _ -> Error (Type_check_error ("expect numeric operands", e.exp_loc)))
        | Bop_mul ->
          (match t1, t2 with
          | T_int, T_int -> Ok T_int
          | T_int, T_real | T_real, T_int | T_real, T_real -> Ok T_real
          | T_prob, T_prob -> Ok T_prob
          | _ ->
            Error
              (Type_check_error ("expect numeric or probabilistic operands", e.exp_loc)))
        | Bop_lt | Bop_le | Bop_ge | Bop_gt ->
          (match t1, t2 with
          | T_int, T_int | T_int, T_real | T_real, T_int | T_real, T_real -> Ok T_bool
          | _ -> Error (Type_check_error ("expect numeric operands", e.exp_loc)))
        | Bop_eq | Bop_ne ->
          (match t1, t2 with
          | T_bool, T_bool
          | T_int, T_int
          | T_int, T_real
          | T_real, T_int
          | T_real, T_real
          | T_prob, T_prob -> Ok T_bool
          | T_array t10, T_array t20 when Tast.equal_type_expression t10 t20 -> Ok T_bool
          | _ -> Error (Type_check_error ("expect operands of scalar types", e.exp_loc)))
      in
      ret (E_binary (bop, e1', e2')) t
    | E_unary (uop, e0) ->
      let%bind e0' = aux e0 in
      let t0 = e0'.exp_type in
      let%bind t =
        match uop, t0 with
        | Uop_not, T_bool -> Ok Tast.T_bool
        | Uop_not, T_prob -> Ok T_prob
        | Uop_negate, T_int -> Ok T_int
        | Uop_negate, T_real -> Ok T_real
        | _ ->
          Error (Type_check_error ("expect an operand of a compatible type", e.exp_loc))
      in
      ret (E_unary (uop, e0')) t
    | E_cond (e0, e1, e2) ->
      let%bind e0' = aux e0 in
      let%bind e1' = aux e1 in
      let%bind e2' = aux e2 in
      let%bind t =
        match e0'.exp_type with
        | T_bool ->
          (match e1'.exp_type, e2'.exp_type with
          | T_bool, T_bool -> Ok Tast.T_bool
          | T_int, T_int -> Ok T_int
          | T_int, T_real | T_real, T_int | T_real, T_real -> Ok T_real
          | T_void, T_void -> Ok T_void
          | T_array t10, T_array t20 when Tast.equal_type_expression t10 t20 ->
            Ok (T_array t10)
          | T_prob, T_prob -> Ok T_prob
          | _ -> Error (Type_check_error ("expect compatible branch types", e.exp_loc)))
        | _ -> Error (Type_check_error ("expect a boolean predicate", e0.exp_loc))
      in
      ret (E_cond (e0', e1', e2')) t
    | E_call (f, e0s) ->
      let%bind tparams, tret = find_fsig fsig f in
      let%bind e0's = aux_list e0s in
      let t0s = List.map e0's ~f:(fun e0' -> e0'.Tast.exp_type) in
      let%bind t = check_proc_call ~loc:e.exp_loc (tparams, tret) t0s in
      ret (E_call (f.txt, e0's)) t
    | E_array_get (e0, e1) ->
      let%bind e0' = aux e0 in
      let%bind e1' = aux e1 in
      let%bind t =
        match e0'.exp_type with
        | T_array t00 ->
          (match e1'.exp_type with
          | T_int -> Ok t00
          | _ -> Error (Type_check_error ("expect an integer index", e1.exp_loc)))
        | _ -> Error (Type_check_error ("expect an array", e0.exp_loc))
      in
      ret (E_array_get (e0', e1')) t
    | E_alloc_array (t, e0) ->
      let%bind t' = check_assignable_ty t in
      let%bind e0' = aux e0 in
      let%bind t =
        match e0'.exp_type with
        | T_int -> Ok (Tast.T_array t')
        | _ -> Error (Type_check_error ("expect an integer length", e0.exp_loc))
      in
      ret (E_alloc_array (t', e0')) t
    | E_prob (a, b) ->
      if a > 0 && b > 0
      then ret (E_prob (a, b)) T_prob
      else Error (Type_check_error ("invalid probability", e.exp_loc))
    | E_sample mu ->
      let%bind mu' = g_dist ~meta fsig tenv undefs mu in
      ret (E_sample mu') mu'.dist_type
    | E_tick e0 ->
      let%bind e0' = aux e0 in
      if is_movable ~f:e0'.exp_type ~t:T_real
      then ret (E_tick e0') T_void
      else Error (Type_check_error ("expect a numeric expression", e0.exp_loc))
    | E_demon -> ret E_demon T_bool
  and aux_list = function
    | [] -> Ok []
    | eh :: et ->
      let%bind eh' = aux eh in
      let%bind et' = aux_list et in
      Ok (eh' :: et')
  in
  aux

and g_dist ?(meta = false) fsig tenv undefs mu =
  let ret dist_desc dist_type = Ok { Tast.dist_desc; dist_type } in
  let err =
    Error (Type_check_error ("incompatible distribution arguments", mu.Past.dist_loc))
  in
  match mu.dist_desc with
  | D_discrete_uniform (e1, e2) ->
    let%bind e1' = g_exp ~meta fsig tenv undefs e1 in
    let%bind e2' = g_exp ~meta fsig tenv undefs e2 in
    (match e1'.exp_type, e2'.exp_type with
    | T_int, T_int -> ret (D_discrete_uniform (e1', e2')) T_int
    | _ -> err)
  | D_uniform (e1, e2) ->
    let%bind e1' = g_exp ~meta fsig tenv undefs e1 in
    let%bind e2' = g_exp ~meta fsig tenv undefs e2 in
    (match e1'.exp_type, e2'.exp_type with
    | T_int, T_int | T_int, T_real | T_real, T_int | T_real, T_real ->
      ret (D_uniform (e1', e2')) T_real
    | _ -> err)
  | D_bernoulli e ->
    let%bind e' = g_exp ~meta fsig tenv undefs e in
    (match e'.exp_type with
    | T_prob -> ret (D_bernoulli e') T_bool
    | _ -> err)
  | D_binomial (e1, e2) ->
    let%bind e1' = g_exp ~meta fsig tenv undefs e1 in
    let%bind e2' = g_exp ~meta fsig tenv undefs e2 in
    (match e1'.exp_type, e2'.exp_type with
    | T_int, T_prob -> ret (D_binomial (e1', e2')) T_int
    | _ -> err)
  | D_hyper (e1, e2, e3) ->
    let%bind e1' = g_exp ~meta fsig tenv undefs e1 in
    let%bind e2' = g_exp ~meta fsig tenv undefs e2 in
    let%bind e3' = g_exp ~meta fsig tenv undefs e3 in
    (match e1'.exp_type, e2'.exp_type, e3'.exp_type with
    | T_int, T_int, T_int -> ret (D_hyper (e1', e2', e3')) T_int
    | _ -> err)
;;

let rec lv_to_exp lv =
  match lv.Past.lv_desc with
  | L_var x -> { Past.exp_desc = E_var x; exp_loc = lv.lv_loc }
  | L_array_get (lv0, e) ->
    { exp_desc = E_array_get (lv_to_exp lv0, e); exp_loc = lv.lv_loc }
;;

let rec g_lv ?(meta = false) fsig tenv undefs ?(outer = false) lv =
  let ret lv_desc lv_type = Ok { Tast.lv_desc; lv_type } in
  match lv.Past.lv_desc with
  | L_var x ->
    let%bind t = find_tenv ~meta tenv (if outer then String.Set.empty else undefs) x in
    ret (L_var x.txt) t
  | L_array_get (lv0, e) ->
    let%bind lv0' = g_lv ~meta fsig tenv undefs lv0 in
    let%bind e' = g_exp ~meta fsig tenv undefs e in
    let%bind t =
      match lv0'.lv_type with
      | T_array t00 ->
        (match e'.exp_type with
        | T_int -> Ok t00
        | _ -> Error (Type_check_error ("expect an integer index", e.exp_loc)))
      | _ -> Error (Type_check_error ("expect an array type", lv0.lv_loc))
    in
    ret (L_array_get (lv0', e')) t
;;

let rec is_lv_in_meta tenv lv =
  match lv.Tast.lv_desc with
  | L_var x -> Result.is_ok (find_tenv_aux tenv String.Set.empty (Location.mknoloc x))
  | L_array_get (lv0, _) -> is_lv_in_meta tenv lv0
;;

let rec is_pure_for_meta e =
  match e.Tast.exp_desc with
  | E_true | E_false | E_int _ | E_float _ | E_frac _ | E_var _ | E_prob _ -> true
  | E_binary (_, e1, e2) -> is_pure_for_meta e1 && is_pure_for_meta e2
  | E_unary (_, e0) -> is_pure_for_meta e0
  | E_cond _
  | E_call _
  | E_array_get _
  | E_alloc_array _
  | E_sample _
  | E_tick _
  | E_demon -> false
;;

let rec g_stmt fsig tret =
  let rec aux ?(in_loop = false) tenv undefs s =
    match s.Past.stmt_desc with
    | S_simple s0 -> g_simp_stmt fsig tenv undefs s0
    | S_if (e, s1, s2_opt) ->
      let%bind e' = g_exp fsig tenv undefs e in
      (match e'.exp_type with
      | T_bool ->
        let%bind s1', _, undefs1, returned1 = aux ~in_loop tenv undefs s1 in
        let%bind s2'_opt, undefs2, returned2 =
          match s2_opt with
          | None -> Ok (None, undefs, false)
          | Some s2 ->
            let%bind s2', _, undefs2, returned2 = aux ~in_loop tenv undefs s2 in
            Ok (Some s2', undefs2, returned2)
        in
        Ok
          ( Tast.S_if (e', s1', s2'_opt)
          , tenv
          , Set.filter undefs ~f:(fun x -> Set.mem undefs1 x || Set.mem undefs2 x)
          , returned1 && returned2 )
      | _ -> Error (Type_check_error ("expect a boolean predicate", e.exp_loc)))
    | S_flip (e, s1, s2_opt) ->
      let%bind e' = g_exp fsig tenv undefs e in
      (match e'.exp_type with
      | T_prob ->
        let%bind s1', _, undefs1, returned1 = aux ~in_loop tenv undefs s1 in
        let%bind s2'_opt, undefs2, returned2 =
          match s2_opt with
          | None -> Ok (None, undefs, false)
          | Some s2 ->
            let%bind s2', _, undefs2, returned2 = aux ~in_loop tenv undefs s2 in
            Ok (Some s2', undefs2, returned2)
        in
        Ok
          ( Tast.S_flip (e', s1', s2'_opt)
          , tenv
          , Set.filter undefs ~f:(fun x -> Set.mem undefs1 x || Set.mem undefs2 x)
          , returned1 && returned2 )
      | _ -> Error (Type_check_error ("expect probability type", e.exp_loc)))
    | S_while (uf, e, s0) ->
      let%bind e' = g_exp fsig tenv undefs e in
      (match e'.exp_type with
      | T_bool ->
        let%bind s0', _, _, _ = aux ~in_loop:true tenv undefs s0 in
        Ok (Tast.S_while (uf, e', s0'), tenv, undefs, false)
      | _ -> Error (Type_check_error ("expect a boolean predicate", e.exp_loc)))
    | S_for (uf, s1_opt, e, s2_opt, s3) ->
      let%bind s1'_opt, tenv', undefs' =
        match s1_opt with
        | None -> Ok (None, tenv, undefs)
        | Some s1 ->
          let%bind s1', tenv', undefs', _ = g_simp_stmt fsig tenv undefs s1 in
          Ok (Some s1', tenv', undefs')
      in
      let%bind e' = g_exp fsig tenv' undefs' e in
      (match e'.exp_type with
      | T_bool ->
        let%bind s2'_opt =
          match s2_opt with
          | None -> Ok None
          | Some ({ simp_stmt_desc = S_decl_var _; _ } as s2) ->
            Error
              (Type_check_error
                 ("the step statement cannot be a declaration", s2.simp_stmt_loc))
          | Some s2 ->
            let%bind s2', _, _, _ = g_simp_stmt fsig tenv' undefs' s2 in
            Ok (Some s2')
        in
        let%bind s3', _, _, _ = aux ~in_loop:true tenv' undefs' s3 in
        Ok
          ( Tast.S_for (uf, s1'_opt, e', s2'_opt, s3')
          , tenv
          , Set.filter undefs ~f:(fun x -> Set.mem undefs' x)
          , false )
      | _ -> Error (Type_check_error ("expect a boolean predicate", e.exp_loc)))
    | S_return e_opt ->
      let%bind e'_opt =
        match e_opt with
        | None ->
          (match tret with
          | Tast.T_void -> Ok None
          | _ -> Error (Type_check_error ("expect void return", s.stmt_loc)))
        | Some e ->
          let%bind e' = g_exp fsig tenv undefs e in
          if is_movable ~f:e'.exp_type ~t:tret
          then Ok (Some e')
          else Error (Type_check_error ("incompatible return value", e.exp_loc))
      in
      Ok (Tast.S_return e'_opt, tenv, undefs, true)
    | S_block s0s ->
      let%bind s0's, _, undefs', returned =
        g_stmt_list fsig tret ~in_loop tenv undefs s0s
      in
      Ok
        ( Tast.S_block s0's
        , tenv
        , Set.filter undefs ~f:(fun x -> Set.mem undefs' x)
        , returned )
    | S_break ->
      if in_loop
      then Ok (Tast.S_break, tenv, undefs, false)
      else Error (Type_check_error ("break can only be used in loops", s.stmt_loc))
    | S_continue ->
      if in_loop
      then Ok (Tast.S_continue, tenv, undefs, false)
      else Error (Type_check_error ("continue can only be used in loops", s.stmt_loc))
    | S_meta_assume e ->
      let%bind e' = g_exp ~meta:true fsig tenv undefs e in
      (match e'.exp_type with
      | T_bool ->
        if is_pure_for_meta e'
        then Ok (Tast.S_meta_assume e', tenv, undefs, false)
        else
          Error (Type_check_error ("meta assumes can only be pure formulas", e.exp_loc))
      | _ -> Error (Type_check_error ("expect a Boolean predicate", e.exp_loc)))
    | S_meta_perform s0 ->
      let%bind s0', tenv', undefs', returned' =
        g_simp_stmt ~meta:true fsig tenv undefs s0
      in
      (match s0' with
      | S_assign (lv, e) when is_lv_in_meta tenv lv && is_pure_for_meta e ->
        Ok (s0', tenv', undefs', returned')
      | _ ->
        Error
          (Type_check_error
             ( "meta operations can only be pure assignments to logical variables"
             , s0.simp_stmt_loc )))
    | S_meta_split e ->
      let%bind e' = g_exp ~meta:true fsig tenv undefs e in
      (match e'.exp_type with
      | T_bool ->
        if is_pure_for_meta e'
        then Ok (Tast.S_meta_split e', tenv, undefs, false)
        else Error (Type_check_error ("meta splits can only be pure formulas", e.exp_loc))
      | _ -> Error (Type_check_error ("expect a Boolean predicate", e.exp_loc)))
  in
  aux

and g_stmt_list fsig tret ?(in_loop = false) tenv undefs = function
  | [] -> Ok ([], tenv, undefs, false)
  | sh :: st ->
    let%bind sh', tenv', undefs', returned' = g_stmt fsig tret ~in_loop tenv undefs sh in
    let%bind st', tenv'', undefs'', returned'' =
      g_stmt_list fsig tret ~in_loop tenv' undefs' st
    in
    Ok (sh' :: st', tenv'', undefs'', returned' || returned'')

and g_simp_stmt ?(meta = false) fsig tenv undefs s =
  let h ?(aop = Past.Aop_mov) lv rhs =
    g_simp_stmt
      ~meta
      fsig
      tenv
      undefs
      { s with
        Past.simp_stmt_desc =
          S_assign (aop, lv, { exp_desc = rhs; exp_loc = s.Past.simp_stmt_loc })
      }
  in
  match s.Past.simp_stmt_desc with
  | S_assign (aop, lv, e) ->
    (match aop with
    | Aop_add -> h lv (E_binary (Bop_add, lv_to_exp lv, e))
    | Aop_sub -> h lv (E_binary (Bop_sub, lv_to_exp lv, e))
    | Aop_mul -> h lv (E_binary (Bop_mul, lv_to_exp lv, e))
    | Aop_div -> h lv (E_binary (Bop_div, lv_to_exp lv, e))
    | Aop_mov ->
      let%bind lv' = g_lv ~meta fsig tenv undefs ~outer:true lv in
      let%bind e' = g_exp ~meta fsig tenv undefs e in
      if is_movable ~f:e'.exp_type ~t:lv'.lv_type
      then (
        let undefs' =
          match lv.lv_desc with
          | L_var x -> Set.remove undefs x.txt
          | _ -> undefs
        in
        Ok (Tast.S_assign (lv', e'), tenv, undefs', false))
      else Error (Type_check_error ("cannot assign", s.simp_stmt_loc)))
  | S_incr lv -> h ~aop:Aop_add lv (E_int 1)
  | S_decr lv -> h ~aop:Aop_sub lv (E_int 1)
  | S_eval_exp e ->
    let%bind e' = g_exp ~meta fsig tenv undefs e in
    Ok (Tast.S_eval_exp e', tenv, undefs, false)
  | S_decl_var ((x, t), e_opt) ->
    let%bind t' = check_assignable_ty t in
    let%bind tenv' = extend_tenv tenv x (`O, t') in
    (match e_opt with
    | None -> Ok (Tast.S_decl_var ((x.txt, t'), None), tenv', Set.add undefs x.txt, false)
    | Some e ->
      let%bind e' = g_exp ~meta fsig tenv undefs e in
      if is_movable ~f:e'.exp_type ~t:t'
      then Ok (Tast.S_decl_var ((x.txt, t'), Some e'), tenv', undefs, false)
      else Error (Type_check_error ("cannot assign", s.simp_stmt_loc)))
;;

let get_sig_of_proc ret params logicals =
  let%bind ret' = check_ty ret in
  let rec loop tenv rev_acc tag = function
    | [] -> Ok (List.rev rev_acc, tenv)
    | (x, t) :: rest ->
      let%bind t' = check_assignable_ty t in
      let%bind tenv' = extend_tenv tenv x (tag, t') in
      loop tenv' ((x.txt, t') :: rev_acc) tag rest
  in
  let%bind params', tenv = loop String.Map.empty [] `O params in
  let%bind logicals', tenv = loop tenv [] `A logicals in
  Ok (ret', params', logicals', tenv)
;;

let g_proc fsig proc =
  match proc.Past.proc_desc with
  | { name; ret; params; logicals; requires; instance; body } ->
    let%bind ret', params', logicals', tenv = get_sig_of_proc ret params logicals in
    let%bind rev_require's =
      List.fold_result requires ~init:[] ~f:(fun acc require ->
          let%bind require' =
            g_exp ~meta:true String.Map.empty tenv String.Set.empty require
          in
          match require'.exp_type with
          | T_bool -> Ok (require' :: acc)
          | _ ->
            Error (Type_check_error ("expect a Boolean pre-condition", require.exp_loc)))
    in
    let require's = List.rev rev_require's in
    let%bind instance' =
      List.fold_result instance ~init:String.Map.empty ~f:(fun acc (x, e) ->
          let%bind t = find_tenv ~meta:true tenv String.Set.empty x in
          if String.Map.mem acc x.txt
          then Error (Type_check_error ("duplicate instance for " ^ x.txt, x.loc))
          else (
            let%bind e' =
              g_exp ~meta:true String.Map.empty String.Map.empty String.Set.empty e
            in
            if is_movable ~f:e'.exp_type ~t
            then Ok (String.Map.add_exn acc ~key:x.txt ~data:e')
            else Error (Type_check_error ("mismatched type", e.exp_loc))))
    in
    let%bind body', _, _, returned = g_stmt_list fsig ret' tenv String.Set.empty body in
    if (not returned)
       &&
       match ret' with
       | T_void -> false
       | _ -> true
    then Error (Type_check_error ("missing return statement", proc.proc_loc))
    else
      Ok
        { Tast.name = name.txt
        ; ret = ret'
        ; params = params'
        ; logicals = logicals'
        ; requires = require's
        ; instance = instance'
        ; body = body'
        }
;;

let g_prog { Past.filename; procs } =
  let rec loop fsig = function
    | [] -> Ok fsig
    | ph :: pt ->
      (match ph.Past.proc_desc with
      | { name; ret; params; logicals; _ } ->
        let%bind ret', params', _, _ = get_sig_of_proc ret params logicals in
        let%bind fsig' = extend_fsig fsig name (List.map params' ~f:snd, ret') in
        loop fsig' pt)
  in
  let%bind fsig = loop String.Map.empty procs in
  let%bind rev_proc's =
    List.fold_result procs ~init:[] ~f:(fun acc proc ->
        let%bind proc' = g_proc fsig proc in
        Ok (proc' :: acc))
  in
  Ok { Tast.filename; procs = List.rev rev_proc's }
;;

let () =
  Location.register_error_of_exn (function
      | Type_check_error (msg, loc) -> Some (Location.errorf ~loc "%s" msg)
      | _ -> None)
;;
