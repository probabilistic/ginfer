open Core

let hash_fold_array hf h a = hash_fold_list hf h (Array.to_list a)

type t = bool array [@@deriving sexp, compare, hash, equal]

let pp fmt t =
  Format.pp_print_string fmt "[ ";
  Format.pp_print_list
    ~pp_sep:(fun fmt () -> Format.pp_print_string fmt "; ")
    Format.pp_print_bool
    fmt
    (Array.to_list t);
  Format.pp_print_string fmt " ]"
;;

let fold_size n ~init ~f =
  let rec iter i cur acc =
    if i = n
    then f acc (Array.of_list (List.rev cur))
    else (
      let acc = iter (i + 1) (false :: cur) acc in
      let acc = iter (i + 1) (true :: cur) acc in
      acc)
  in
  iter 0 [] init
;;

module Set = Set.Make (struct
  type nonrec t = t [@@deriving sexp, compare]
end)

module Map0 = Map.Make (struct
  type nonrec t = t [@@deriving sexp, compare]
end)

module Map = struct
  include Map0

  let pp pp_data fmt t =
    Format.pp_print_list
      ~pp_sep:(fun fmt () -> Format.pp_print_string fmt "; ")
      (fun fmt (k, v) -> Format.fprintf fmt "%a: %a" pp k pp_data v)
      fmt
      (to_alist t)
  ;;

  let create n ~f =
    fold_size n ~init:empty ~f:(fun acc bv -> add_exn acc ~key:bv ~data:(f bv))
  ;;
end
