open Core

type t = bool array [@@deriving sexp, compare, hash, equal]

val pp : Format.formatter -> t -> unit
val fold_size : int -> init:'a -> f:('a -> t -> 'a) -> 'a

module Set : Set.S with type Elt.t = t
module Map0 : Map.S with type Key.t = t

module Map : sig
  val pp : (Format.formatter -> 'a -> unit) -> Format.formatter -> 'a Map0.t -> unit
  val create : int -> f:(t -> 'a) -> 'a Map0.t

  include module type of Map0
end
