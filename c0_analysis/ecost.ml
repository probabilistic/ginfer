open Core
open Result.Let_syntax
open C0
module A = Amortized

exception Lp_failure

let () =
  Caml.Printexc.register_printer (function
      | Lp_failure -> Some "analysis failure: LP failed"
      | _ -> None)
;;

let has_potential = function
  | Hcfg.T_bool | T_int | T_prob | T_real -> true
  | _ -> false
;;

let translate_lit = function
  | Hcfg.L_true -> `Int 1
  | L_false -> `Int 0
  | L_int n -> `Int n
  | L_float d -> `Real d
  | L_frac f -> `Frac f
  | L_prob (a, b) -> `Frac (Mpqf.of_frac a (a + b))
;;

let cache ~f =
  let memo = Hashtbl.Poly.create () in
  fun x ->
    match Hashtbl.find memo x with
    | Some v -> v
    | None ->
      let v = f x in
      Hashtbl.add_exn memo ~key:x ~data:v;
      v
;;

let cache_recursive ~f =
  let memo = Hashtbl.Poly.create () in
  let rec g x =
    match Hashtbl.find memo x with
    | Some v -> v
    | None ->
      let v = f g x in
      Hashtbl.add_exn memo ~key:x ~data:v;
      v
  in
  g
;;

let base_monoms =
  cache ~f:(fun (vars, deg) ->
      let rec iter n last acc =
        if n = 0
        then acc
        else (
          let step =
            List.fold last ~init:[] ~f:(fun acc (monom, k) ->
                List.foldi vars ~init:acc ~f:(fun i acc x ->
                    if i >= k then (A.Polynomial.Monom.mul_var x monom, i) :: acc else acc))
          in
          iter (n - 1) step (List.append (List.unzip step |> fst) acc))
      in
      iter deg [ A.Polynomial.Monom.one, 0 ] [ A.Polynomial.Monom.one ] |> List.rev)
;;

module Make_potential_utility (P : A.S) = struct
  let infeasible_for focus =
    List.exists focus ~f:(fun p ->
        match P.Flt_poly.is_const p with
        | Some f -> P.Flt.(f < zero && not (is_zero f))
        | None -> false)
  ;;

  let rec poly_of_expr e =
    let open Option.Let_syntax in
    match e.Hcfg.exp_desc with
    | E_literal (L_int n) -> return @@ P.Flt_poly.const (P.Flt.of_int n)
    | E_literal (L_float d) -> return @@ P.Flt_poly.const (P.Flt.of_float d)
    | E_literal (L_frac f) -> return @@ P.Flt_poly.const (P.Flt.of_mpqf f)
    | E_literal L_true -> return @@ P.Flt_poly.const P.Flt.one
    | E_literal L_false -> return @@ P.Flt_poly.const P.Flt.zero
    | E_literal (L_prob (a, b)) ->
      return @@ P.Flt_poly.const (P.Flt.of_mpqf (Mpqf.of_frac a (a + b)))
    | E_var x -> return @@ P.Flt_poly.of_monom (A.Polynomial.Monom.of_var x) P.Flt.one
    | E_binary (bop, e1, e2) ->
      let%bind e1' = poly_of_expr e1 in
      let%bind e2' = poly_of_expr e2 in
      (match bop with
      | Bop_and -> return @@ P.Flt_poly.mul e1' e2'
      | Bop_or ->
        let one = P.Flt_poly.const P.Flt.one in
        return
        @@ P.Flt_poly.sub
             one
             (P.Flt_poly.mul (P.Flt_poly.sub one e1') (P.Flt_poly.sub one e2'))
      | Bop_add -> return @@ P.Flt_poly.add e1' e2'
      | Bop_sub -> return @@ P.Flt_poly.sub e1' e2'
      | Bop_mul -> return @@ P.Flt_poly.mul e1' e2'
      | Bop_div ->
        (match P.Flt_poly.is_const e2' with
        | Some elt
          when match e.exp_type with
               | T_real -> true
               | _ -> false ->
          return @@ P.Flt_poly.mul e1' (P.Flt_poly.const P.Flt.(one / elt))
        | _ -> None)
      | _ -> None)
    | E_unary (uop, e0) ->
      let%bind e0' = poly_of_expr e0 in
      (match uop with
      | Uop_not -> return @@ P.Flt_poly.sub (P.Flt_poly.const P.Flt.one) e0'
      | Uop_negate -> return @@ P.Flt_poly.sub (P.Flt_poly.const P.Flt.zero) e0')
    | E_demon -> None
  ;;

  let moments_of_dist mu =
    match mu.Hcfg.dist_desc with
    | D_discrete_uniform (e1, e2) ->
      let e1' = poly_of_expr e1 |> Option.value_exn in
      let e2' = poly_of_expr e2 |> Option.value_exn in
      let n = P.Flt_poly.add (P.Flt_poly.sub e2' e1') (P.Flt_poly.const P.Flt.one) in
      cache_recursive ~f:(fun f -> function
        | 0 -> P.Flt_poly.const P.Flt.one
        | 1 ->
          P.Flt_poly.scale (P.Flt.of_mpqf (Mpqf.of_frac 1 2)) (P.Flt_poly.add e1' e2')
        | 2 ->
          let m1 = f 1 in
          P.Flt_poly.(
            add
              (pow 2 m1)
              (scale
                 (P.Flt.of_mpqf (Mpqf.of_frac 1 12))
                 (sub (pow 2 n) (const P.Flt.one))))
        | 3 ->
          let m1 = f 1 in
          let m2 = f 2 in
          P.Flt_poly.(
            sub (scale (P.Flt.of_int 3) (mul m2 m1)) (scale (P.Flt.of_int 2) (pow 3 m1)))
        | 4 ->
          let m1 = f 1 in
          let m2 = f 2 in
          let m3 = f 3 in
          P.Flt_poly.(
            add
              (add
                 (sub
                    (scale (P.Flt.of_int 4) (mul m3 m1))
                    (scale (P.Flt.of_int 6) (mul m2 (pow 2 m1))))
                 (scale (P.Flt.of_int 3) (pow 4 m1)))
              (scale
                 (P.Flt.of_mpqf (Mpqf.of_frac 1 240))
                 (mul
                    (sub (pow 2 n) (const P.Flt.one))
                    (sub (scale (P.Flt.of_int 3) (pow 2 n)) (const (P.Flt.of_int 7))))))
        | _ -> failwith "[interpret_act] TODO: unimplemented moments")
    | D_uniform (e1, e2) ->
      let e1' = poly_of_expr e1 |> Option.value_exn in
      let e2' = poly_of_expr e2 |> Option.value_exn in
      fun k ->
        let acc = ref (P.Flt_poly.const P.Flt.zero) in
        for i = 0 to k do
          acc
            := P.Flt_poly.add
                 !acc
                 (P.Flt_poly.mul (P.Flt_poly.pow i e1') (P.Flt_poly.pow (k - i) e2'))
        done;
        acc := P.Flt_poly.scale P.Flt.(one / of_int Int.(k + 1)) !acc;
        !acc
    | D_bernoulli e ->
      let e' = poly_of_expr e |> Option.value_exn in
      fun _ -> e'
    | D_binomial (e1, e2) ->
      let e1' = poly_of_expr e1 |> Option.value_exn in
      let e2' = poly_of_expr e2 |> Option.value_exn in
      (match P.Flt_poly.is_const e1', P.Flt_poly.is_const e2' with
      | Some n, Some p ->
        let n = P.Flt.to_int n in
        let p = P.Flt.to_mpqf p in
        let pa, pb = Mpqf.get_num p, Mpqf.get_den p in
        let mu = Dist.int_bin (Mpz.get_int pa) (Mpz.get_int pb) n in
        fun k ->
          P.Flt_poly.const
            (Either.value_map (mu.dist_mom k) ~first:P.Flt.of_float ~second:P.Flt.of_mpqf)
      | _ -> failwith "[interpret_act] TODO: symbolic distributions")
    | D_hyper (e1, e2, e3) ->
      let e1' = poly_of_expr e1 |> Option.value_exn in
      let e2' = poly_of_expr e2 |> Option.value_exn in
      let e3' = poly_of_expr e3 |> Option.value_exn in
      (match
         P.Flt_poly.is_const e1', P.Flt_poly.is_const e2', P.Flt_poly.is_const e3'
       with
      | Some n, Some r, Some m ->
        let n = P.Flt.to_int n in
        let r = P.Flt.to_int r in
        let m = P.Flt.to_int m in
        let mu = Dist.int_hyper n r m in
        fun k ->
          P.Flt_poly.const
            (Either.value_map (mu.dist_mom k) ~first:P.Flt.of_float ~second:P.Flt.of_mpqf)
      | _ -> failwith "[interpret_act] TODO: symbolic distributions")
  ;;
end

module Make_scope_utility
    (P : A.S) (S : sig
      val scope : Hcfg.G.vertex -> (Hcfg.ident * Hcfg.type_expression) list
    end) =
struct
  include Make_potential_utility (P)

  let scope = cache ~f:S.scope

  let abs_scope =
    cache ~f:(fun u ->
        List.filter_map (scope u) ~f:(fun (x, t) ->
            if has_potential t then Some x else None))
  ;;

  let abs_scope_map = cache ~f:(fun u -> String.Set.of_list (abs_scope u))

  type manager = P.lp_manager

  let alloc_manager () = P.create_lp_manager ()
end

module Make_intra_domain
    (P : A.S) (S : sig
      val scope : Hcfg.G.vertex -> (Hcfg.ident * Hcfg.type_expression) list
      val hint : Hcfg.G.vertex -> P.Flt_poly.t list
      val degree : int
      val moment : int
      val direction : [ `Upper | `Lower ]
      val instance : Hcfg.literal String.Map.t
    end) =
struct
  include Make_scope_utility (P) (S)

  type annotation = P.annotation array
  type solution = P.solution

  let query_annotation _ manager =
    Array.init (S.moment + 1) ~f:(fun i ->
        P.annot_of_poly
          manager
          (P.Flt_poly.const (if i = 0 then P.Flt.one else P.Flt.zero)))
  ;;

  let identify_annotations _ manager annots1 annots2 =
    Array.iter2_exn annots1 annots2 ~f:(fun annot1 annot2 ->
        P.constrain_annot manager annot1 `Eq annot2)
  ;;

  let optimize _ manager ~goal =
    let binds = Map.map S.instance ~f:translate_lit |> Map.to_alist in
    let res =
      match S.direction with
      | `Upper -> P.solve_min ~stat:true manager (Array.last goal) binds
      | `Lower -> P.solve_max ~stat:true manager (Array.last goal) binds
    in
    if not res then Error Lp_failure else Ok ()
  ;;

  let obtain_solution _ manager annots = P.obtain_sol manager (Array.last annots)

  let rewrite_for =
    cache ~f:(fun (u, deg) ->
        let focus = S.hint u in
        let rec iter n last acc =
          if n = 0
          then acc
          else (
            let step =
              List.fold last ~init:[] ~f:(fun acc (p, k) ->
                  List.foldi focus ~init:acc ~f:(fun i acc f ->
                      if i >= k then (P.Flt_poly.mul p f, i) :: acc else acc))
            in
            iter (n - 1) step (List.append (List.unzip step |> fst) acc))
        in
        let result =
          iter deg [ P.Flt_poly.const P.Flt.one, 0 ] [ P.Flt_poly.const P.Flt.one ]
          |> List.rev
        in
        result)
  ;;

  let new_annotation u manager =
    Array.init (S.moment + 1) ~f:(fun i ->
        if i = 0
        then P.const_annot manager P.Flt.one
        else P.new_annot manager (base_monoms (abs_scope u, S.degree)))
  ;;

  let interpret_act u act manager dst_annots =
    if infeasible_for (S.hint u)
    then new_annotation u manager
    else (
      let abs_m = abs_scope_map u in
      match act with
      | Hcfg.A_assign (x, e) when Set.mem abs_m x ->
        let e' = poly_of_expr e |> Option.value_exn in
        Array.map dst_annots ~f:(fun dst_annot -> P.subst_var_annot (x, e') dst_annot)
      | A_sample (x, mu) when Set.mem abs_m x ->
        let moments = moments_of_dist mu in
        Array.map dst_annots ~f:(fun dst_annot ->
            let annot = ref dst_annot in
            for k = S.degree downto 1 do
              let moment = moments k in
              annot := P.subst_var_expo_annot k (x, moment) !annot
            done;
            !annot)
      | A_array_get (x, _, _) when Set.mem abs_m x ->
        Array.map dst_annots ~f:(fun dst_annot -> P.forget_var manager x dst_annot)
      | A_tick e ->
        let tick_expr = poly_of_expr e |> Option.value_exn in
        Array.mapi dst_annots ~f:(fun i _ ->
            let tick_poly =
              P.Flt_poly.pow
                i
                (P.Flt_poly.add_monom
                   (A.Polynomial.Monom.of_var "TICK")
                   P.Flt.one
                   tick_expr)
            in
            P.Flt_poly.fold
              tick_poly
              ~init:(P.zero_annot manager)
              ~f:(fun ~mono ~coef acc ->
                let this, that = A.Polynomial.Monom.split_on [ "TICK" ] mono in
                P.add_annot
                  acc
                  (P.scale_annot
                     coef
                     (P.annot_mul_monom
                        manager
                        dst_annots.(A.Polynomial.Monom.degree this)
                        that))))
      | A_weaken ->
        let rewrite = rewrite_for (u, S.degree) in
        Array.mapi dst_annots ~f:(fun i dst_annot ->
            let dst_annot =
              if i > 0
              then (
                match S.direction with
                | `Upper -> P.weaken_annot manager dst_annot rewrite
                | `Lower -> P.relax_annot manager dst_annot rewrite)
              else dst_annot
            in
            dst_annot)
      | _ -> dst_annots)
  ;;

  let interpret e manager dst_annotss =
    let u = Hcfg.G.E.src e in
    let l = Hcfg.G.E.label e in
    match l with
    | C_seq act -> interpret_act u act manager (List.hd_exn dst_annotss)
    | C_cond _ ->
      let dst_annots1 = List.nth_exn dst_annotss 0 in
      let dst_annots2 = List.nth_exn dst_annotss 1 in
      Array.map
        (Array.zip_exn dst_annots1 dst_annots2)
        ~f:(fun (dst_annot1, dst_annot2) ->
          P.constrain_annot manager dst_annot1 `Eq dst_annot2;
          dst_annot1)
    | C_prob e ->
      let p = poly_of_expr e |> Option.value_exn in
      let q = P.Flt_poly.sub (P.Flt_poly.const P.Flt.one) p in
      let dst_annots1 = List.nth_exn dst_annotss 0 in
      let dst_annots2 = List.nth_exn dst_annotss 1 in
      Array.map2_exn dst_annots1 dst_annots2 ~f:(fun dst_annot1 dst_annot2 ->
          let p_annot = P.mul_poly p dst_annot1 in
          let q_annot = P.mul_poly q dst_annot2 in
          P.add_annot p_annot q_annot)
    | C_call (Some x, _, _) when Set.mem (abs_scope_map u) x ->
      Array.map (List.hd_exn dst_annotss) ~f:(P.forget_var manager x)
    | _ -> List.hd_exn dst_annotss
  ;;
end

module Make_inter_domain
    (Ctx : Hyper_graph.Context.S with type vertex := Hcfg.G.vertex)
    (P : A.S) (S : sig
      val scope : Hcfg.G.vertex -> (Hcfg.ident * Hcfg.type_expression) list
      val live : Hcfg.G.vertex -> String.Set.t
      val fsig : string -> Hcfg.G.vertex * Hcfg.G.vertex
      val hint : Ctx.t * Hcfg.G.vertex -> P.Flt_poly.t list
      val degree : int
      val moment : int
      val direction : [ `Upper | `Lower ]
      val instance : Hcfg.literal String.Map.t
    end) =
struct
  include Make_scope_utility (P) (S)

  type annotation = P.annotation array
  type solution = P.solution

  let query_annotation _ manager =
    Array.init (S.moment + 1) ~f:(fun i ->
        P.annot_of_poly
          manager
          (P.Flt_poly.const (if i = 0 then P.Flt.one else P.Flt.zero)))
  ;;

  let identify_annotations _ manager annots1 annots2 =
    Array.iter2_exn annots1 annots2 ~f:(fun annot1 annot2 ->
        P.constrain_annot manager annot1 `Eq annot2)
  ;;

  let optimize _ manager ~goal =
    let binds = Map.map S.instance ~f:translate_lit |> Map.to_alist in
    let res =
      match S.direction with
      | `Upper -> P.solve_min ~stat:true manager (Array.last goal) binds
      | `Lower -> P.solve_max ~stat:true manager (Array.last goal) binds
    in
    if not res then Error Lp_failure else Ok ()
  ;;

  let obtain_solution _ manager annots = P.obtain_sol manager (Array.last annots)

  type level = int * int * A.Polynomial.Monom.t

  let top_level = 0, S.degree, A.Polynomial.Monom.one
  let is_trivial_level (cost_free, deg, _) = cost_free = S.moment && deg = 0

  let rewrite_for =
    cache ~f:(fun (cu, deg) ->
        let focus = S.hint cu in
        let rec iter n last acc =
          if n = 0
          then acc
          else (
            let step =
              List.fold last ~init:[] ~f:(fun acc (p, k) ->
                  List.foldi focus ~init:acc ~f:(fun i acc f ->
                      if i >= k then (P.Flt_poly.mul p f, i) :: acc else acc))
            in
            iter (n - 1) step (List.append (List.unzip step |> fst) acc))
        in
        let result =
          iter deg [ P.Flt_poly.const P.Flt.one, 0 ] [ P.Flt_poly.const P.Flt.one ]
          |> List.rev
        in
        result)
  ;;

  let new_annotation0 manager cost_free deg scope =
    Array.init (S.moment + 1) ~f:(fun i ->
        if i < cost_free
        then P.zero_annot manager
        else if i = 0
        then P.const_annot manager P.Flt.one
        else P.new_annot manager (base_monoms (scope, deg)))
  ;;

  let new_annotation u manager (cost_free, deg, _) =
    let live_vars = S.live u in
    new_annotation0
      manager
      cost_free
      deg
      (List.filter (abs_scope u) ~f:(fun x -> Set.mem live_vars x))
  ;;

  let is_call edge =
    match Hcfg.G.E.label edge with
    | C_call (_, f, _) -> Some (S.fsig f)
    | _ -> None
  ;;

  let interpret_act c u act manager ((cost_free, deg, _) as level) dst_annots =
    if infeasible_for (S.hint (c, u))
    then new_annotation u manager level
    else (
      let abs_m = abs_scope_map u in
      match act with
      | Hcfg.A_assign (x, e) when Set.mem abs_m x ->
        let e' = poly_of_expr e |> Option.value_exn in
        Array.map dst_annots ~f:(fun dst_annot -> P.subst_var_annot (x, e') dst_annot)
      | A_sample (x, mu) when Set.mem abs_m x ->
        let moments = moments_of_dist mu in
        Array.map dst_annots ~f:(fun dst_annot ->
            let annot = ref dst_annot in
            for k = deg downto 1 do
              let moment = moments k in
              annot := P.subst_var_expo_annot k (x, moment) !annot
            done;
            !annot)
      | A_array_get (x, _, _) when Set.mem abs_m x ->
        Array.map dst_annots ~f:(fun dst_annot -> P.forget_var manager x dst_annot)
      | A_tick e ->
        let tick_expr = poly_of_expr e |> Option.value_exn in
        Array.mapi dst_annots ~f:(fun i _ ->
            let tick_poly =
              P.Flt_poly.pow
                i
                (P.Flt_poly.add_monom
                   (A.Polynomial.Monom.of_var "TICK")
                   P.Flt.one
                   tick_expr)
            in
            P.Flt_poly.fold
              tick_poly
              ~init:(P.zero_annot manager)
              ~f:(fun ~mono ~coef acc ->
                let this, that = A.Polynomial.Monom.split_on [ "TICK" ] mono in
                P.add_annot
                  acc
                  (P.scale_annot
                     coef
                     (P.annot_mul_monom
                        manager
                        dst_annots.(A.Polynomial.Monom.degree this)
                        that))))
      | A_weaken ->
        let rewrite = rewrite_for ((c, u), deg) in
        Array.mapi dst_annots ~f:(fun i dst_annot ->
            let dst_annot =
              if i > cost_free
              then (
                match S.direction with
                | `Upper -> P.weaken_annot manager dst_annot rewrite
                | `Lower -> P.relax_annot manager dst_annot rewrite)
              else dst_annot
            in
            dst_annot)
      | _ -> dst_annots)
  ;;

  let interpret c e manager level dst_annotss =
    let u = Hcfg.G.E.src e in
    let l = Hcfg.G.E.label e in
    match l with
    | C_seq act -> interpret_act c u act manager level (List.hd_exn dst_annotss)
    | C_cond _ ->
      let dst_annots1 = List.nth_exn dst_annotss 0 in
      let dst_annots2 = List.nth_exn dst_annotss 1 in
      Array.map
        (Array.zip_exn dst_annots1 dst_annots2)
        ~f:(fun (dst_annot1, dst_annot2) ->
          P.constrain_annot manager dst_annot1 `Eq dst_annot2;
          dst_annot1)
    | C_prob e ->
      let p = poly_of_expr e |> Option.value_exn in
      let q = P.Flt_poly.sub (P.Flt_poly.const P.Flt.one) p in
      let dst_annots1 = List.nth_exn dst_annotss 0 in
      let dst_annots2 = List.nth_exn dst_annotss 1 in
      Array.map2_exn dst_annots1 dst_annots2 ~f:(fun dst_annot1 dst_annot2 ->
          let p_annot = P.mul_poly p dst_annot1 in
          let q_annot = P.mul_poly q dst_annot2 in
          P.add_annot p_annot q_annot)
    | C_call (Some x, _, _) when Set.mem (abs_scope_map u) x ->
      Array.map (List.hd_exn dst_annotss) ~f:(P.forget_var manager x)
    | _ -> List.hd_exn dst_annotss
  ;;

  let call0 manager returns xs dst_annots cost_free deg =
    let split_dst_annots =
      let acc =
        Array.foldi dst_annots ~init:A.Polynomial.Poly.zero ~f:(fun i acc dst_annot ->
            let split_dst_annot = P.split_annot_on manager dst_annot xs in
            A.Polynomial.Poly.fold split_dst_annot ~init:acc ~f:(fun ~mono ~coef acc ->
                A.Polynomial.Poly.add_monom ~addi:List.append mono [ i, coef ] acc))
      in
      A.Polynomial.Poly.map
        (fun coefs ->
          let ret = Array.create ~len:(S.moment + 1) (P.zero_annot manager) in
          List.iter coefs ~f:(fun (i, annot) -> ret.(i) <- annot);
          ret)
        acc
    in
    let call_annots =
      A.Polynomial.Poly.get_coeff
        ~zer:(Array.create ~len:(S.moment + 1) (P.zero_annot manager))
        A.Polynomial.Monom.one
        split_dst_annots
    in
    let returned_calls =
      Array.map call_annots ~f:(fun call_annot ->
          let returned_call =
            List.fold2_exn returns xs ~init:call_annot ~f:(fun acc return x ->
                P.subst_var_annot
                  (x, P.Flt_poly.of_monom (A.Polynomial.Monom.of_var return) P.Flt.one)
                  acc)
          in
          returned_call)
    in
    if deg = 0 || cost_free + 1 > S.moment
    then returned_calls, []
    else (
      let trans_annotss =
        A.Polynomial.Poly.fold split_dst_annots ~init:[] ~f:(fun ~mono ~coef acc ->
            let d = A.Polynomial.Monom.degree mono in
            if d = 0
            then (
              let returned_trans =
                new_annotation0 manager (cost_free + 1) (deg - 1) returns
              in
              Array.iteri returned_trans ~f:(fun i returned_trans ->
                  returned_calls.(i) <- P.sub_annot returned_calls.(i) returned_trans);
              ((cost_free + 1, deg - 1, A.Polynomial.Monom.one), returned_trans) :: acc)
            else (
              let returned_trans =
                Array.map coef ~f:(fun coef ->
                    let returned_trans =
                      List.fold2_exn returns xs ~init:coef ~f:(fun acc return x ->
                          P.subst_var_annot
                            ( x
                            , P.Flt_poly.of_monom
                                (A.Polynomial.Monom.of_var return)
                                P.Flt.one )
                            acc)
                    in
                    returned_trans)
              in
              ((cost_free + 1, deg - d, mono), returned_trans) :: acc))
      in
      returned_calls, trans_annotss)
  ;;

  let call ~caller:e ~callee:(_, callee_exit) manager dst_annots (cost_free, deg, _) =
    let returns = abs_scope callee_exit in
    let u = Hcfg.G.E.src e in
    let xs =
      match Hcfg.G.E.label e with
      | C_call (Some x, _, _) when Set.mem (abs_scope_map u) x -> [ x ]
      | C_call _ -> []
      | _ -> assert false
    in
    call0 manager (List.take returns (List.length xs)) xs dst_annots cost_free deg
  ;;

  let return0 manager args params called_annots transed_annotss =
    let returned_calleds =
      Array.map called_annots ~f:(fun called_annot ->
          let returned_called =
            List.fold2_exn args params ~init:called_annot ~f:(fun acc arg param ->
                P.subst_var_annot
                  (param, P.Flt_poly.of_monom (A.Polynomial.Monom.of_var arg) P.Flt.one)
                  acc)
          in
          returned_called)
    in
    List.fold
      transed_annotss
      ~init:returned_calleds
      ~f:(fun acc ((_, _, mon), transed_annots) ->
        let returned_transeds =
          Array.map transed_annots ~f:(fun transed_annot ->
              let returned_transed =
                List.fold2_exn args params ~init:transed_annot ~f:(fun acc arg param ->
                    P.subst_var_annot
                      ( param
                      , P.Flt_poly.of_monom (A.Polynomial.Monom.of_var arg) P.Flt.one )
                      acc)
              in
              returned_transed)
        in
        Array.map2_exn acc returned_transeds ~f:(fun acc returned_transed ->
            P.add_annot acc (P.annot_mul_monom manager returned_transed mon)))
  ;;

  let return ~caller:e ~callee:(fun_entry, _) manager called_annots transed_annotss =
    let params = abs_scope fun_entry in
    let u = Hcfg.G.E.src e in
    let args =
      match Hcfg.G.E.label e with
      | C_call (_, _, args) -> List.filter (List.rev args) ~f:(Set.mem (abs_scope_map u))
      | _ -> assert false
    in
    return0 manager args params called_annots transed_annotss
  ;;
end

module To_polys (Abs : Numerical.DOMAIN) (Flt : Ffloat.FLOAT) = struct
  module Flt_poly = A.Polynomial.Make_float_poly (Flt)

  let coeff_to_float = function
    | Apron.Coeff.Scalar s ->
      (match s with
      | Float f -> Flt.of_float f
      | Mpqf f -> Flt.of_mpqf f
      | Mpfrf f -> Flt.of_mpfrf f)
    | _ -> assert false
  ;;

  let f ?not_live abs =
    let abs' =
      match not_live with
      | None -> abs
      | Some not_live -> Abs.forget_list abs not_live
    in
    let lcs = Abs.to_lincons_list abs' in
    List.fold lcs ~init:[] ~f:(fun acc lc ->
        let env = Apron.Lincons1.get_env lc in
        let ivars, rvars = Apron.Environment.vars env in
        let p =
          Array.fold
            (Array.append ivars rvars)
            ~init:(Flt_poly.const (Apron.Lincons1.get_cst lc |> coeff_to_float))
            ~f:(fun acc x ->
              let v = Apron.Var.to_string x in
              Flt_poly.add_monom
                (A.Polynomial.Monom.of_var v)
                (Apron.Lincons1.get_coeff lc x |> coeff_to_float)
                acc)
        in
        match Apron.Lincons1.get_typ lc with
        | EQ -> p :: Flt_poly.sub (Flt_poly.const Flt.zero) p :: acc
        | SUPEQ | SUP -> p :: acc
        | _ -> assert false)
  ;;
end

module Make_intra (P : A.S) = struct
  let g_proc
      ~degree
      ~moment
      ~direction
      ({ Hcfg.entry; exit; body; scope; instance; _ } as proc)
    =
    let module A = Numerical.Apron_wrapper.Make_elina_poly () in
    let module L = Lia.Make_intra (A) in
    let module Gen = To_polys (A) (P.Flt) in
    let abs = L.g_proc proc in
    let module S = struct
      let scope = scope
      let hint = cache ~f:(fun u -> abs u |> Gen.f)
      let degree = degree
      let moment = moment
      let direction = direction
      let instance = instance
    end
    in
    let module D = Make_intra_domain (P) (S) in
    let module F = Hyper_graph.Intra_vcgen.Make (Hcfg.G) (D) in
    let%bind sol = F.analyze body ~entry ~exit () in
    Ok (fun fmt () -> P.print_sol fmt sol)
  ;;

  let g_prog ~degree ~moment ~direction { Hcfg.procs; _ } =
    let%bind ress =
      List.fold_result procs ~init:[] ~f:(fun acc proc ->
          let%bind res = g_proc ~degree ~moment ~direction proc in
          Ok (res :: acc))
    in
    Ok (List.rev ress)
  ;;
end

module Make_inter
    (Ctx : Hyper_graph.Context.S with type vertex := Hcfg.G.vertex)
    (P : A.S) =
struct
  let g_prog ~degree ~moment ~direction ({ Hcfg.procs; _ } as prog) =
    let module A_oct = Numerical.Apron_wrapper.Make_oct () in
    let module A_poly = Numerical.Apron_wrapper.Make_elina_poly () in
    let module L = Lia.Make_inter (Ctx) (A_oct) (A_poly) in
    let module Gen = To_polys (A_poly) (P.Flt) in
    let abs =
      Timer.wrap_duration "abstract-interpretation" (fun () ->
          let abs = L.g_prog prog in
          abs)
    in
    let scope_table = Hashtbl.create (module Hcfg.G.V) in
    List.iter procs ~f:(fun { Hcfg.body; scope; _ } ->
        Hcfg.G.iter_vertex body ~f:(fun u ->
            (* Format.eprintf "%d: %a@." u A_poly.pp (abs (Ctx.empty, u)); *)
            Hashtbl.add_exn scope_table ~key:u ~data:(scope u)));
    let live_table = Hashtbl.create (module Hcfg.G.V) in
    List.iter procs ~f:(fun ({ Hcfg.body; _ } as proc) ->
        let live = Live.g_needed_proc proc in
        Hcfg.G.iter_vertex body ~f:(fun u ->
            (* Format.eprintf "%d: %a@." u Sexp.pp_hum (String.Set.sexp_of_t @@ live u); *)
            Hashtbl.add_exn live_table ~key:u ~data:(live u)));
    let fsig_table = String.Table.create () in
    List.iter procs ~f:(fun { Hcfg.name; entry; exit; _ } ->
        Hashtbl.add_exn fsig_table ~key:name ~data:(entry, exit));
    let g =
      List.fold procs ~init:Hcfg.G.empty ~f:(fun acc { Hcfg.body; _ } ->
          Hcfg.G.union acc body)
    in
    let entry = (List.last_exn procs).Hcfg.entry in
    let exit = (List.last_exn procs).Hcfg.exit in
    let instance = (List.last_exn procs).Hcfg.instance in
    let module S = struct
      let scope u = Hashtbl.find_exn scope_table u
      let live u = Hashtbl.find_exn live_table u
      let fsig f = Hashtbl.find_exn fsig_table f

      let hint =
        cache ~f:(fun (c, u) ->
            let live_vars = live u in
            let not_live_vars =
              scope u
              |> List.filter_map ~f:(fun (x, t) ->
                     if not (has_potential t)
                     then None
                     else if Set.mem live_vars x
                     then None
                     else Some x)
            in
            abs (c, u) |> Gen.f ~not_live:not_live_vars)
      ;;

      let degree = degree
      let moment = moment
      let direction = direction
      let instance = instance
    end
    in
    let module D = Make_inter_domain (Ctx) (P) (S) in
    let module F = Hyper_graph.Inter_vcgen.Make (Hcfg.G) (Ctx) (D) in
    let%bind sol = F.analyze g ~entry ~exit () in
    Ok (fun fmt () -> P.print_sol fmt sol)
  ;;
end
