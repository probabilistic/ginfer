open C0

module Make_intra (P : Amortized.S) : sig
  val g_proc
    :  degree:int
    -> moment:int
    -> direction:[ `Lower | `Upper ]
    -> Hcfg.procedure
    -> (Format.formatter -> unit -> unit, exn) Result.t

  val g_prog
    :  degree:int
    -> moment:int
    -> direction:[ `Lower | `Upper ]
    -> Hcfg.program
    -> ((Format.formatter -> unit -> unit) list, exn) result
end

module Make_inter
    (Ctx : Hyper_graph.Context.S with type vertex := Hcfg.G.vertex)
    (P : Amortized.S) : sig
  val g_prog
    :  degree:int
    -> moment:int
    -> direction:[ `Lower | `Upper ]
    -> Hcfg.program
    -> (Format.formatter -> unit -> unit, exn) Result.t
end
