open Core
open C0
module N = Numerical
module Syntax = N.Syntax

let classify_type = function
  | Hcfg.T_bool -> Some `T_bool
  | T_int -> Some `T_int
  | T_real -> Some `T_real
  | T_prob -> Some `T_prob
  | _ -> None
;;

let rec translate_arith_exp e =
  match e.Hcfg.exp_desc with
  | E_literal (L_int n) -> Syntax.mk_int n
  | E_literal (L_float d) -> Syntax.mk_float d
  | E_literal (L_frac f) -> Syntax.mk_frac f
  | E_var x ->
    (match e.exp_type with
    | T_int -> Syntax.mk_int_var x
    | T_real -> Syntax.mk_real_var x
    | _ -> assert false)
  | E_binary (bop, e1, e2) ->
    let e1' = translate_arith_exp e1 in
    let e2' = translate_arith_exp e2 in
    (match bop with
    | Bop_add -> Syntax.mk_add e1' e2'
    | Bop_sub -> Syntax.mk_sub e1' e2'
    | Bop_mul -> Syntax.mk_mul e1' e2'
    | Bop_div -> Syntax.mk_div e1' e2'
    | _ -> assert false)
  | E_unary (uop, e0) ->
    let e0' = translate_arith_exp e0 in
    (match uop with
    | Uop_negate -> Syntax.mk_negate e0'
    | _ -> assert false)
  | _ -> assert false
;;

let rec translate_prob_exp e =
  match e.Hcfg.exp_desc with
  | E_var x -> Syntax.mk_prob_var x
  | E_binary (Bop_mul, e1, e2) ->
    Syntax.mk_mul_prob (translate_prob_exp e1) (translate_prob_exp e2)
  | E_unary (Uop_not, e0) -> Syntax.mk_inv_prob (translate_prob_exp e0)
  | E_literal (L_prob (a, b)) -> Syntax.mk_prob (a, b)
  | _ -> assert false
;;

let rec translate_bool_exp e =
  match e.Hcfg.exp_desc with
  | E_literal L_true -> Syntax.mk_bool true
  | E_literal L_false -> Syntax.mk_bool false
  | E_var x -> Syntax.mk_bool_var x
  | E_binary (bop, e1, e2) ->
    (match bop with
    | Bop_and -> Syntax.mk_and (translate_bool_exp e1) (translate_bool_exp e2)
    | Bop_or -> Syntax.mk_or (translate_bool_exp e1) (translate_bool_exp e2)
    | Bop_lt -> Syntax.mk_lt (translate_arith_exp e1) (translate_arith_exp e2)
    | Bop_le -> Syntax.mk_le (translate_arith_exp e1) (translate_arith_exp e2)
    | Bop_ge -> Syntax.mk_ge (translate_arith_exp e1) (translate_arith_exp e2)
    | Bop_gt -> Syntax.mk_gt (translate_arith_exp e1) (translate_arith_exp e2)
    | Bop_eq ->
      (match e1.exp_type with
      | T_bool -> Syntax.mk_bool_eq (translate_bool_exp e1) (translate_bool_exp e2)
      | T_prob -> Syntax.mk_prob_eq (translate_prob_exp e1) (translate_prob_exp e2)
      | T_int | T_real ->
        Syntax.mk_arith_eq (translate_arith_exp e1) (translate_arith_exp e2)
      | _ -> Syntax.mk_nondet)
    | Bop_ne ->
      (match e1.exp_type with
      | T_bool -> Syntax.mk_bool_ne (translate_bool_exp e1) (translate_bool_exp e2)
      | T_prob -> Syntax.mk_prob_ne (translate_prob_exp e1) (translate_prob_exp e2)
      | T_int | T_real ->
        Syntax.mk_arith_ne (translate_arith_exp e1) (translate_arith_exp e2)
      | _ -> Syntax.mk_nondet)
    | _ -> assert false)
  | E_unary (uop, e0) ->
    let e0' = translate_bool_exp e0 in
    (match uop with
    | Uop_not -> Syntax.mk_not e0'
    | _ -> assert false)
  | E_demon -> Syntax.mk_nondet
  | _ -> assert false
;;

let support_guard_of_dist x mu =
  match mu.Hcfg.dist_desc with
  | D_discrete_uniform (e1, e2) ->
    let x =
      match Syntax.refine x with
      | `Arith x -> x
      | _ -> assert false
    in
    let e1' = translate_arith_exp e1 in
    let e2' = translate_arith_exp e2 in
    Syntax.(mk_and (mk_le e1' x) (mk_le x e2'))
  | D_uniform (e1, e2) ->
    let x =
      match Syntax.refine x with
      | `Arith x -> x
      | _ -> assert false
    in
    let e1' = translate_arith_exp e1 in
    let e2' = translate_arith_exp e2 in
    Syntax.(mk_and (mk_lt e1' x) (mk_lt x e2'))
  | D_bernoulli _ -> Syntax.mk_bool true
  | D_binomial (e1, _) ->
    let x =
      match Syntax.refine x with
      | `Arith x -> x
      | _ -> assert false
    in
    let e1' = translate_arith_exp e1 in
    Syntax.(mk_and (mk_le (mk_int 0) x) (mk_le x e1'))
  | D_hyper (e1, e2, e3) ->
    let x =
      match Syntax.refine x with
      | `Arith x -> x
      | _ -> assert false
    in
    let e1' = translate_arith_exp e1 in
    let e2' = translate_arith_exp e2 in
    let e3' = translate_arith_exp e3 in
    let n1, n2, t = e2', Syntax.mk_sub e1' e2', e3' in
    Syntax.(
      mk_and
        (mk_and (mk_le (mk_int 0) x) (mk_le (mk_sub t n2) x))
        (mk_and (mk_le x t) (mk_le x n1)))
;;

let cache ~f =
  let memo = Hashtbl.Poly.create () in
  fun x ->
    match Hashtbl.find memo x with
    | Some v -> v
    | None ->
      let v = f x in
      Hashtbl.add_exn memo ~key:x ~data:v;
      v
;;

module Make_scope_utility_and_act_interp (S : sig
  val scope : Hcfg.G.vertex -> (Hcfg.ident * Hcfg.type_expression) list
end)
(D : N.DOMAIN) =
struct
  let scope = cache ~f:S.scope
  let scope_map = cache ~f:(fun u -> String.Map.of_alist_exn (scope u))

  let abs_scope =
    cache ~f:(fun u ->
        List.filter_map (scope u) ~f:(fun (x, t) ->
            classify_type t |> Option.map ~f:(fun t' -> x, t')))
  ;;

  let abs_scope_map = cache ~f:(fun u -> String.Map.of_alist_exn (abs_scope u))

  let interp_act_dom u =
    let abs_m = abs_scope_map u in
    function
    | Hcfg.A_assign (x, e) when Map.mem abs_m x ->
      (match e.exp_type with
      | T_bool -> fun p -> D.assign_bool p x (translate_bool_exp e)
      | T_prob -> fun p -> D.assign_prob p x (translate_prob_exp e)
      | _ -> fun p -> D.assign_arith p x (translate_arith_exp e))
    | A_decl_var (x, t) ->
      (match classify_type t with
      | Some t' ->
        fun p -> D.change_scope p (Map.add_exn abs_m ~key:x ~data:t' |> Map.to_alist)
      | None -> fun p -> p)
    | A_dead_vars xs ->
      let xs = String.Set.of_list xs in
      fun p ->
        D.change_scope
          p
          (Map.filter_keys abs_m ~f:(fun x -> not (Set.mem xs x)) |> Map.to_alist)
    | A_array_get (x, _, _) when Map.mem abs_m x -> fun p -> D.forget_list p [ x ]
    | A_sample (x, mu) when Map.mem abs_m x ->
      let on =
        match Map.find_exn (scope_map u) x with
        | T_bool -> (Syntax.mk_bool_var x :> Syntax.ty Syntax.expression)
        | T_int -> (Syntax.mk_int_var x :> Syntax.ty Syntax.expression)
        | T_real -> (Syntax.mk_real_var x :> Syntax.ty Syntax.expression)
        | T_prob -> (Syntax.mk_prob_var x :> Syntax.ty Syntax.expression)
        | _ -> assert false
      in
      let support_guard = support_guard_of_dist on mu in
      fun p -> D.meet_formula_list (D.forget_list p [ x ]) [ support_guard ]
    | A_assume e ->
      let be' = translate_bool_exp e in
      fun p -> D.meet_formula_list p [ be' ]
    | _ -> fun p -> p
  ;;
end

module Make_intra_domain (S : sig
  val scope : Hcfg.G.vertex -> (Hcfg.ident * Hcfg.type_expression) list
end)
(D : N.DOMAIN) =
struct
  type t = D.t

  let mode = `Fwd
  let equal abs1 abs2 = D.is_eq abs1 abs2
  let pp fmt abs = D.pp fmt abs

  open Make_scope_utility_and_act_interp (S) (D)

  let bot u = D.bottom (abs_scope u)
  let join _ abs1 abs2 = D.join abs1 abs2
  let widen _ abs1 abs2 = D.widening abs1 abs2
  let alpha u e = D.of_formula (abs_scope u) @@ translate_bool_exp e

  let interpret edge =
    let u = Hcfg.G.E.src edge in
    let l = Hcfg.G.E.label edge in
    match l with
    | C_seq act -> [ interp_act_dom u act ]
    | C_cond e ->
      [ (fun abs -> interp_act_dom u (A_assume e) abs)
      ; (fun abs ->
          interp_act_dom
            u
            (A_assume { exp_desc = E_unary (Uop_not, e); exp_type = T_bool })
            abs)
      ]
    | C_prob _ -> [ (fun abs -> abs); (fun abs -> abs) ]
    | C_call (x_opt, _, _) ->
      (match x_opt with
      | Some x when Map.mem (abs_scope_map u) x ->
        [ (fun abs -> D.forget_list abs [ x ]) ]
      | _ -> [ (fun abs -> abs) ])
  ;;
end

module Make_inter_domain
    (Ctx : Hyper_graph.Context.S with type vertex := Hcfg.G.vertex) (S : sig
      val scope : Hcfg.G.vertex -> (Hcfg.ident * Hcfg.type_expression) list
      val fsig : string -> Hcfg.G.vertex * Hcfg.G.vertex
      val hint : Ctx.t * Hcfg.G.vertex -> Apron.Lincons1.t list option
    end)
    (D : N.DOMAIN) =
struct
  type t = D.t

  let equal abs1 abs2 = D.is_eq abs1 abs2
  let pp fmt abs = D.pp fmt abs

  open Make_scope_utility_and_act_interp (S) (D)

  let bot _ u = D.bottom (abs_scope u)
  let join _ _ abs1 abs2 = D.join abs1 abs2

  let widen ctx u abs1 abs2 =
    match S.hint (ctx, u) with
    | None -> D.widening abs1 abs2
    | Some hint -> D.widening_threshold_list abs1 abs2 hint
  ;;

  let alpha _ u e = D.of_formula (abs_scope u) @@ translate_bool_exp e

  let interpret _ edge =
    let u = Hcfg.G.E.src edge in
    let l = Hcfg.G.E.label edge in
    match l with
    | C_seq act -> [ interp_act_dom u act ]
    | C_cond e ->
      [ (fun abs -> interp_act_dom u (A_assume e) abs)
      ; (fun abs ->
          interp_act_dom
            u
            (A_assume { exp_desc = E_unary (Uop_not, e); exp_type = T_bool })
            abs)
      ]
    | C_prob _ -> [ (fun abs -> abs); (fun abs -> abs) ]
    | C_call (x_opt, _, _) ->
      (match x_opt with
      | Some x when Map.mem (abs_scope_map u) x ->
        [ (fun abs -> D.forget_list abs [ x ]) ]
      | _ -> [ (fun abs -> abs) ])
  ;;

  let is_call edge =
    match Hcfg.G.E.label edge with
    | C_call (_, f, _) -> Some (S.fsig f)
    | _ -> None
  ;;

  let call ~caller:(_, edge) ~callee:(_, entry, _) =
    let u = Hcfg.G.E.src edge in
    let abs_m = abs_scope_map u in
    match Hcfg.G.E.label edge with
    | C_call (_, _, args) ->
      let abs_scope_call_site =
        List.filter_map args ~f:(fun arg ->
            if Map.mem abs_m arg then Some (arg, Map.find_exn abs_m arg) else None)
      in
      let args' = List.map abs_scope_call_site ~f:fst in
      let abs_params_entry = List.map (List.rev (abs_scope entry)) ~f:fst in
      fun abs ->
        let abs' = D.change_scope abs abs_scope_call_site in
        D.rename_list_with abs' args' abs_params_entry;
        abs'
    | _ -> assert false
  ;;

  let return ~caller:(_, edge) ~callee:(_, _, exit) =
    let v = List.hd_exn @@ Hcfg.G.E.dsts edge in
    let abs_m = abs_scope_map v in
    match Hcfg.G.E.label edge with
    | C_call (ret_opt, _, args) ->
      let abs_params_ret_exit = List.rev (abs_scope exit) in
      let abs_params_ret_exit_names = List.map abs_params_ret_exit ~f:fst in
      let abs_scope_return_site = abs_scope v in
      let args' =
        List.filter_map args ~f:(fun arg -> if Map.mem abs_m arg then Some arg else None)
      in
      (match ret_opt with
      | Some ret when Map.mem abs_m ret ->
        fun abs ->
          let abs' = D.rename_list abs abs_params_ret_exit_names (args' @ [ ret ]) in
          D.change_scope_with abs' abs_scope_return_site;
          abs'
      | _ ->
        fun abs ->
          let abs' = D.rename_list abs abs_params_ret_exit_names args' in
          D.change_scope_with abs' abs_scope_return_site;
          abs')
    | _ -> assert false
  ;;

  let merge _ _ abs1 abs2 = D.meet abs1 abs2
end

module Make_intra (A : N.DOMAIN) = struct
  let g_proc { Hcfg.entry; body; scope; requires; _ } =
    let module S = struct
      let scope = scope
    end
    in
    let module D = Make_intra_domain (S) (A) in
    let module F = Hyper_graph.Intra_fixpoint.Make (Hcfg.G) (D) in
    F.analyze
      body
      ~entry
      ~init:(fun u -> if Hcfg.G.V.equal u entry then D.alpha u requires else D.bot u)
      ~narrow:true
      ()
  ;;

  let g_prog { Hcfg.procs; _ } = List.map procs ~f:g_proc
end

module Make_inter
    (Ctx : Hyper_graph.Context.S with type vertex := Hcfg.G.vertex)
    (A0 : N.DOMAIN)
    (A1 : N.DOMAIN) =
struct
  let g_prog { Hcfg.procs; _ } =
    let scope_table = Hashtbl.create (module Hcfg.G.V) in
    List.iter procs ~f:(fun { Hcfg.body; scope; _ } ->
        Hcfg.G.iter_vertex body ~f:(fun u ->
            Hashtbl.add_exn scope_table ~key:u ~data:(scope u)));
    let fsig_table = String.Table.create () in
    List.iter procs ~f:(fun { Hcfg.name; entry; exit; _ } ->
        Hashtbl.add_exn fsig_table ~key:name ~data:(entry, exit));
    let g =
      List.fold procs ~init:Hcfg.G.empty ~f:(fun acc { Hcfg.body; _ } ->
          Hcfg.G.union acc body)
    in
    let entry = (List.last_exn procs).Hcfg.entry in
    let requires = (List.last_exn procs).Hcfg.requires in
    let module S = struct
      let scope u = Hashtbl.find_exn scope_table u
      let fsig f = Hashtbl.find_exn fsig_table f
    end
    in
    let module D0 =
      Make_inter_domain
        (Ctx)
        (struct
          include S

          let hint _ = None
        end)
        (A0)
    in
    let module F0 = Hyper_graph.Inter_fixpoint.Make (Hcfg.G) (Ctx) (D0) in
    let sol0 =
      F0.analyze
        g
        ~entry
        ~init:(fun (c, u) ->
          if Ctx.equal c Ctx.empty && Hcfg.G.equal_vertex u entry
          then D0.alpha c u requires
          else D0.bot c u)
        ~narrow:true
        ~widen_threshold:0
        ()
    in
    let module D1 =
      Make_inter_domain
        (Ctx)
        (struct
          include S

          let hint =
            cache ~f:(fun (c, u) ->
                let sol = sol0 u in
                List.Assoc.find sol ~equal:Ctx.equal c |> Option.map ~f:A0.to_lincons_list)
          ;;
        end)
        (A1)
    in
    let module F1 = Hyper_graph.Inter_fixpoint.Make (Hcfg.G) (Ctx) (D1) in
    let sol1 =
      F1.analyze
        g
        ~entry
        ~init:(fun (c, u) ->
          if Ctx.equal c Ctx.empty && Hcfg.G.equal_vertex u entry
          then D1.alpha c u requires
          else D1.bot c u)
        ~narrow:true
        ~widen_threshold:0
        ()
    in
    cache ~f:(fun (c, u) ->
        let assoc = sol1 u in
        List.Assoc.find assoc ~equal:Ctx.equal c |> Option.value ~default:(D1.bot c u))
  ;;
end
