open C0

module Make_intra (A : Numerical.DOMAIN) : sig
  val g_proc : Hcfg.procedure -> Hcfg.G.vertex -> A.t
  val g_prog : Hcfg.program -> (Hcfg.G.vertex -> A.t) list
end

module Make_inter
    (Ctx : Hyper_graph.Context.S with type vertex := Hcfg.G.vertex)
    (A0 : Numerical.DOMAIN)
    (A1 : Numerical.DOMAIN) : sig
  val g_prog : Hcfg.program -> Ctx.t * Hcfg.G.vertex -> A1.t
end
