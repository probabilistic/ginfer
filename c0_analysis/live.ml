open Core
open C0

module Domain = struct
  type t = String.Set.t [@@deriving sexp, equal]

  let mode = `Bwd
  let join _ s1 s2 = Set.union s1 s2
  let widen _ _ s2 = s2
  let pp fmt s = Sexp.pp_hum fmt (sexp_of_t s)

  let interp_act = function
    | Hcfg.A_assign (x, e) -> fun s -> Set.union (Set.remove s x) (Hcfg.fv_exp e)
    | A_sample (x, mu) -> fun s -> Set.union (Set.remove s x) (Hcfg.fv_dist mu)
    | A_array_put (x, e1, e2) ->
      fun s -> String.Set.union_list [ Set.add s x; Hcfg.fv_exp e1; Hcfg.fv_exp e2 ]
    | A_array_get (x, a, e2) ->
      fun s -> Set.union (Set.remove s x) (Set.add (Hcfg.fv_exp e2) a)
    | A_array_make (a, _, e) -> fun s -> Set.union (Set.remove s a) (Hcfg.fv_exp e)
    | A_tick e -> fun s -> Set.union s (Hcfg.fv_exp e)
    | A_assume e -> fun s -> Set.union s (Hcfg.fv_exp e)
    | A_split e -> fun s -> Set.union s (Hcfg.fv_exp e)
    | A_skip | A_weaken | A_decl_var _ | A_dead_vars _ -> fun s -> s
  ;;

  let interpret e =
    match Hcfg.G.E.label e with
    | C_seq act -> [ interp_act act ]
    | C_cond e ->
      let xs = Hcfg.fv_exp e in
      [ (fun s -> Set.union s xs); (fun s -> Set.union s xs) ]
    | C_prob e ->
      let xs = Hcfg.fv_exp e in
      [ (fun s -> Set.union s xs); (fun s -> Set.union s xs) ]
    | C_call (x_opt, _, args) ->
      let xs = String.Set.of_list (Option.to_list x_opt) in
      [ (fun s -> Set.union (Set.diff s xs) (String.Set.of_list args)) ]
  ;;
end

let g_proc { Hcfg.exit; ret; body; _ } =
  let rets = String.Set.of_list (Option.to_list ret |> List.map ~f:fst) in
  let module F = Hyper_graph.Intra_fixpoint.Make (Hcfg.G) (Domain) in
  F.analyze
    body
    ~entry:exit
    ~init:(fun u -> if Hcfg.G.equal_vertex u exit then rets else String.Set.empty)
    ()
;;

let g_prog { Hcfg.procs; _ } = List.map procs ~f:g_proc

module Make_needed_domain (S : sig
  val live : Hcfg.G.vertex -> String.Set.t
end) =
struct
  type t = String.Set.t [@@deriving sexp, equal]

  let mode = `Fwd
  let join _ s1 s2 = Set.union s1 s2
  let widen _ _ s2 = s2
  let pp fmt s = Sexp.pp_hum fmt (sexp_of_t s)

  let interp_act v = function
    | Hcfg.A_assign _ | A_dead_vars _ | A_decl_var _ | A_tick _ | A_skip ->
      fun s -> Set.union s (S.live v)
    | _ -> fun _ -> S.live v
  ;;

  let interpret e =
    let vs = Hcfg.G.E.dsts e in
    match Hcfg.G.E.label e with
    | C_seq act -> [ interp_act (List.hd_exn vs) act ]
    | C_cond _ | C_prob _ ->
      let v0 = List.nth_exn vs 0 in
      let v1 = List.nth_exn vs 1 in
      [ (fun s -> Set.union s (S.live v0)); (fun s -> Set.union s (S.live v1)) ]
    | C_call _ -> [ (fun s -> Set.union s (S.live (List.hd_exn vs))) ]
  ;;
end

let g_needed_proc ({ Hcfg.entry; body; _ } as proc) =
  let module S = struct
    let live = g_proc proc
  end
  in
  let module D = Make_needed_domain (S) in
  let module F = Hyper_graph.Intra_fixpoint.Make (Hcfg.G) (D) in
  F.analyze body ~entry ~init:(fun u -> S.live u) ()
;;

let g_needed_prog { Hcfg.procs; _ } = List.map procs ~f:g_needed_proc
