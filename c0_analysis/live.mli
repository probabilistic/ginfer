open Core
open C0

val g_proc : Hcfg.procedure -> Hcfg.G.vertex -> String.Set.t
val g_prog : Hcfg.program -> (Hcfg.G.vertex -> String.Set.t) list
val g_needed_proc : Hcfg.procedure -> Hcfg.G.vertex -> String.Set.t
val g_needed_prog : Hcfg.program -> (Hcfg.G.vertex -> String.Set.t) list
