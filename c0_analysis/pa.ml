open Core
open C0

module Domain = struct
  type t = Hcfg.expression list [@@deriving sexp, compare]

  let mode = `Fwd
  let equal t1 t2 = compare t1 t2 = 0

  let join _ ps1 ps2 =
    if List.length ps1 <= List.length ps2
    then (
      assert (
        List.is_prefix ps2 ~prefix:ps1 ~equal:(fun e1 e2 ->
            Hcfg.compare_expression e1 e2 = 0));
      ps2)
    else (
      assert (
        List.is_prefix ps1 ~prefix:ps2 ~equal:(fun e1 e2 ->
            Hcfg.compare_expression e1 e2 = 0));
      ps1)
  ;;

  let widen _ ps1 ps2 =
    assert (
      List.is_prefix ps2 ~prefix:ps1 ~equal:(fun e1 e2 ->
          Hcfg.compare_expression e1 e2 = 0));
    ps2
  ;;

  let pp fmt ps = Sexp.pp_hum fmt (sexp_of_t ps)

  let interpret_act = function
    | Hcfg.A_split e -> fun ps -> ps @ [ e ]
    | A_dead_vars xs ->
      let xs = String.Set.of_list xs in
      fun ps ->
        List.filter ps ~f:(fun p -> String.Set.(is_empty @@ inter xs (Hcfg.fv_exp p)))
    | _ -> fun ps -> ps
  ;;

  let interpret e =
    match Hcfg.G.E.label e with
    | C_seq act -> [ interpret_act act ]
    | C_cond _ | C_prob _ -> [ (fun ps -> ps); (fun ps -> ps) ]
    | C_call _ -> [ (fun ps -> ps) ]
  ;;

  let empty = []
  let to_array = List.to_array
end

let g_proc { Hcfg.entry; body; _ } =
  let module F = Hyper_graph.Intra_fixpoint.Make (Hcfg.G) (Domain) in
  let sol = F.analyze body ~entry ~init:(fun _ -> Domain.empty) () in
  fun u -> Domain.to_array (sol u)
;;

let g_prog { Hcfg.procs; _ } = List.map procs ~f:g_proc
