open C0

val g_proc : Hcfg.procedure -> Hcfg.G.vertex -> Sast.expression array
val g_prog : Hcfg.program -> (Hcfg.G.vertex -> Sast.expression array) list
