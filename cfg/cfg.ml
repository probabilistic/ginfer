open Core
open Action_types
open Action_ops

type ctrl =
  | CCond of Typed.bool_expr
  | CProb of Typed.prob_expr
  | CSeq of Typed.data_action
  | CCall of string loc * string loc list * string loc list

let ctrl_arity = function
  | CCond _ -> 2
  | CProb _ -> 2
  | CSeq _ -> 1
  | CCall _ -> 1
;;

type vertex = int [@@deriving sexp, compare, equal, hash]

let create_vertex n = n
let vertex_to_string = Int.to_string
let print_vertex fmt v = Format.pp_print_int fmt v

type edge = vertex * ctrl * vertex list * Location.t

let create_edge src act dsts loc = src, act, dsts, loc
let edge_src (src, _, _, _) = src
let edge_dsts (_, _, dsts, _) = dsts
let edge_action (_, act, _, _) = act
let edge_loc (_, _, _, loc) = loc

module Vertex_map = Map.Make (struct
  type t = vertex [@@deriving sexp, compare]
end)

module Vertex_set = Set.Make (struct
  type t = vertex [@@deriving sexp, compare]
end)

type hgraph =
  { hg_vertices : vertex list
  ; hg_edges : edge list
  ; hg_forward : edge list Vertex_map.t
  ; hg_backward : edge list Vertex_map.t
  }

let of_edges hg_edges =
  let hg_vertices =
    List.fold hg_edges ~init:[] ~f:(fun acc (src, _, dsts, _) ->
        List.append dsts (src :: acc))
    |> List.dedup_and_sort ~compare:compare_vertex
  in
  let hg_forward =
    hg_edges
    |> List.map ~f:(fun ((src, _, _, _) as e) -> src, e)
    |> Vertex_map.of_alist_multi
  in
  let hg_backward =
    hg_edges
    |> List.fold ~init:[] ~f:(fun acc ((_, _, dsts, _) as e) ->
           List.fold dsts ~init:acc ~f:(fun acc dst -> (dst, e) :: acc))
    |> Vertex_map.of_alist_multi
  in
  { hg_vertices; hg_edges; hg_forward; hg_backward }
;;

let nb_vertex cfg = List.length cfg.hg_vertices
let nb_edge cfg = List.length cfg.hg_edges
let mem_vertex cfg v = List.mem cfg.hg_vertices v ~equal:equal_vertex
let succ_e cfg v = Vertex_map.find cfg.hg_forward v |> Option.value ~default:[]
let pred_e cfg v = Vertex_map.find cfg.hg_backward v |> Option.value ~default:[]
let fold_vertex f init cfg = List.fold cfg.hg_vertices ~init ~f
let iter_vertex f cfg = List.iter cfg.hg_vertices ~f
let fold_edges_e f init cfg = List.fold cfg.hg_edges ~init ~f
let iter_edges_e f cfg = List.iter cfg.hg_edges ~f
let fold_succ_e f init cfg v = List.fold (succ_e cfg v) ~init ~f
let iter_succ_e f cfg v = List.iter (succ_e cfg v) ~f
let fold_pred_e f init cfg v = List.fold (pred_e cfg v) ~init ~f
let iter_pred_e f cfg v = List.iter (pred_e cfg v) ~f

let print_ctrl fmt = function
  | CProb p -> Format.fprintf fmt "[%a]" print_prob_expr p
  | CCond phi -> Format.fprintf fmt "[%a]" print_bool_expr phi
  | CSeq act -> Format.fprintf fmt "%a" print_data_action act
  | CCall (name, args, xs) ->
    Format.fprintf
      fmt
      "call %s(%s) to {%s}"
      name.txt
      (String.concat ~sep:", " (List.map args ~f:(fun arg -> arg.txt)))
      (String.concat ~sep:", " (List.map xs ~f:(fun x -> x.txt)))
;;

let print_hgraph fmt hg =
  iter_vertex
    (fun v ->
      iter_succ_e
        (fun (src, ctrl, dsts, _) ->
          Format.fprintf
            fmt
            "\t%d -- %a --> {%s}@."
            src
            print_ctrl
            ctrl
            (String.concat ~sep:"," (List.map dsts ~f:(fun dst -> Int.to_string dst))))
        hg
        v)
    hg
;;

module Dfs = struct
  let forward f init cfg v =
    let rec iter vis acc v =
      let vis' = Vertex_set.add vis v in
      let acc' = f acc v in
      fold_succ_e
        (fun init (_, _, dsts, _) ->
          List.fold dsts ~init ~f:(fun (acc, vis) u ->
              if Vertex_set.mem vis u then acc, vis else iter vis acc u))
        (acc', vis')
        cfg
        v
    in
    fst (iter Vertex_set.empty init v)
  ;;

  let backward f init cfg v =
    let rec iter vis acc v =
      let vis' = Vertex_set.add vis v in
      let acc' = f acc v in
      fold_pred_e
        (fun (acc, vis) (src, _, _, _) ->
          if Vertex_set.mem vis src then acc, vis else iter vis acc src)
        (acc', vis')
        cfg
        v
    in
    fst (iter Vertex_set.empty init v)
  ;;
end

type func =
  { fun_name : string loc
  ; fun_params : string loc list
  ; fun_locals : string loc list
  ; fun_returns : string loc list
  ; fun_scope : Typed.rhs_ty tycontext
  ; fun_init : vertex
  ; fun_exit : vertex
  ; fun_cfg : hgraph
  }

type program =
  { prg_funcs : func list
  ; prg_loc : Location.t
  }

let main_func prog = List.last_exn prog.prg_funcs

let find_func_ funcs name =
  List.find_exn funcs ~f:(fun func -> String.equal name.txt func.fun_name.txt)
;;

let find_func prog name = find_func_ prog.prg_funcs name

let is_prog_recursive prog =
  let vis = Hashtbl.create (module String) ~size:16 in
  let exception Recur in
  let rec dfs func =
    Hashtbl.set vis ~key:func.fun_name.txt ~data:true;
    iter_edges_e
      (fun e ->
        match edge_action e with
        | CCall (name, _, _) ->
          if not (Hashtbl.mem vis name.txt)
          then dfs (find_func prog name)
          else raise Recur
        | _ -> ())
      func.fun_cfg;
    Hashtbl.remove vis func.fun_name.txt
  in
  try
    dfs (main_func prog);
    false
  with
  | Recur -> true
;;

let print_func
    fmt
    { fun_name
    ; fun_params
    ; fun_scope
    ; fun_init
    ; fun_exit
    ; fun_locals
    ; fun_returns
    ; fun_cfg
    ; _
    }
  =
  Format.fprintf
    fmt
    "func %s(%s) [%a,%a] local:{%s} return:{%s}@.%a"
    fun_name.txt
    (String.concat
       ~sep:", "
       (List.map fun_params ~f:(fun param ->
            (tyctx_get_exn fun_scope param |> string_of_type) ^ " " ^ param.txt)))
    print_vertex
    fun_init
    print_vertex
    fun_exit
    (String.concat
       ~sep:", "
       (List.map fun_locals ~f:(fun param ->
            (tyctx_get_exn fun_scope param |> string_of_type) ^ " " ^ param.txt)))
    (String.concat
       ~sep:", "
       (List.map fun_returns ~f:(fun param ->
            (tyctx_get_exn fun_scope param |> string_of_type) ^ " " ^ param.txt)))
    print_hgraph
    fun_cfg
;;
