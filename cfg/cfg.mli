open Action_types
open Action_ops

type ctrl =
  | CCond of Typed.bool_expr
  | CProb of Typed.prob_expr
  | CSeq of Typed.data_action
  | CCall of string loc * string loc list * string loc list

val ctrl_arity : ctrl -> int

type vertex [@@deriving sexp, compare, equal, hash]

val vertex_to_string : vertex -> string
val create_vertex : int -> vertex
val print_vertex : Format.formatter -> vertex -> unit

type edge

val create_edge : vertex -> ctrl -> vertex list -> Location.t -> edge
val edge_src : edge -> vertex
val edge_dsts : edge -> vertex list
val edge_action : edge -> ctrl
val edge_loc : edge -> Location.t

type hgraph

val of_edges : edge list -> hgraph
val print_hgraph : Format.formatter -> hgraph -> unit
val nb_vertex : hgraph -> int
val nb_edge : hgraph -> int
val mem_vertex : hgraph -> vertex -> bool
val succ_e : hgraph -> vertex -> edge list
val pred_e : hgraph -> vertex -> edge list
val fold_vertex : ('a -> vertex -> 'a) -> 'a -> hgraph -> 'a
val iter_vertex : (vertex -> unit) -> hgraph -> unit
val fold_edges_e : ('a -> edge -> 'a) -> 'a -> hgraph -> 'a
val iter_edges_e : (edge -> unit) -> hgraph -> unit
val fold_succ_e : ('a -> edge -> 'a) -> 'a -> hgraph -> vertex -> 'a
val iter_succ_e : (edge -> unit) -> hgraph -> vertex -> unit
val fold_pred_e : ('a -> edge -> 'a) -> 'a -> hgraph -> vertex -> 'a
val iter_pred_e : (edge -> unit) -> hgraph -> vertex -> unit

module Dfs : sig
  val forward : ('a -> vertex -> 'a) -> 'a -> hgraph -> vertex -> 'a
  val backward : ('a -> vertex -> 'a) -> 'a -> hgraph -> vertex -> 'a
end

type func =
  { fun_name : string loc
  ; fun_params : string loc list
  ; fun_locals : string loc list
  ; fun_returns : string loc list
  ; fun_scope : Typed.rhs_ty tycontext
  ; fun_init : vertex
  ; fun_exit : vertex
  ; fun_cfg : hgraph
  }

type program =
  { prg_funcs : func list
  ; prg_loc : Location.t
  }

val main_func : program -> func
val find_func : program -> string loc -> func
val is_prog_recursive : program -> bool
val print_func : Format.formatter -> func -> unit
