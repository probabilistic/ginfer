open Core
open Result.Let_syntax
open Action_types
open Action_ops

let equal_ty (ty1 : Typed.rhs_ty) (ty2 : Typed.rhs_ty) = Typed.equal_rhs_ty ty1 ty2

module Vertex = struct
  module T = struct
    type t = Cfg.vertex [@@deriving sexp, compare]
  end

  include T
  include Comparator.Make (T)
end

let is_mutually_exclusive comparator ls =
  List.fold
    ls
    ~init:(true, Set.empty comparator)
    ~f:(fun (acc, s) l ->
      if not acc
      then acc, s
      else (
        let acc = not (List.exists l ~f:(Set.mem s)) in
        acc, Set.union s (Set.of_list comparator l)))
  |> fst
;;

let check_edge sigma ctx edge =
  let dsts = Cfg.edge_dsts edge in
  let ctrl = Cfg.edge_action edge in
  let loc = Cfg.edge_loc edge in
  if List.length dsts <> Cfg.ctrl_arity ctrl
  then Error (Static_error ("control action arity mismatch", loc))
  else (
    match ctrl with
    | CCall (name, args, xs) ->
      if not (String.Map.mem sigma name.txt)
      then Error (Static_error ("callee function not found", loc))
      else (
        let%bind () =
          List.fold_result (List.append args xs) ~init:() ~f:(fun () x ->
              tyctx_get ctx x |> Result.ignore_m)
        in
        if not
             (List.equal
                equal_ty
                (String.Map.find_exn sigma name.txt |> fst)
                (List.map args ~f:(tyctx_get_exn ctx)))
        then Error (Static_error ("calling signature mismatch", loc))
        else if not
                  (List.equal
                     equal_ty
                     (String.Map.find_exn sigma name.txt |> snd)
                     (List.map xs ~f:(tyctx_get_exn ctx)))
        then Error (Static_error ("returning signature mismatch", loc))
        else Ok ())
    | CProb _ -> Ok ()
    | CSeq _ -> Ok ()
    | CCond _ -> Ok ())
;;

let check_func sigma func =
  let loc = func.Cfg.fun_name.loc in
  if not (Cfg.mem_vertex func.Cfg.fun_cfg func.Cfg.fun_init)
  then Error (Static_error ("init vertex not in cfg", loc))
  else if not (Cfg.mem_vertex func.fun_cfg func.fun_exit)
  then Error (Static_error ("exit vertex not in cfg", loc))
  else if List.length (Cfg.pred_e func.Cfg.fun_cfg func.fun_init) <> 0
  then Error (Static_error ("init having incoming edges", loc))
  else if List.length (Cfg.succ_e func.Cfg.fun_cfg func.fun_exit) <> 0
  then Error (Static_error ("exit leading to other edges", loc))
  else
    Result.try_with (fun () ->
        Cfg.iter_edges_e
          (fun e -> check_edge sigma func.fun_scope e |> Result.ok_exn)
          func.fun_cfg)
;;

let check_prog prog =
  if not
       (is_mutually_exclusive
          (module String)
          (List.map prog.Cfg.prg_funcs ~f:(fun func -> [ func.fun_name.txt ])))
  then Error (Static_error ("functions must have different names", prog.prg_loc))
  else if not
            (is_mutually_exclusive
               (module Vertex)
               (List.map prog.Cfg.prg_funcs ~f:(fun func ->
                    Cfg.fold_vertex (fun acc v -> v :: acc) [] func.fun_cfg)))
  then Error (Static_error ("function vertices not mutually exclusive", prog.prg_loc))
  else (
    let%bind sigma =
      Result.try_with (fun () ->
          String.Map.of_alist_exn
            (List.map prog.prg_funcs ~f:(fun func ->
                 ( func.fun_name.txt
                 , ( List.map func.fun_params ~f:(tyctx_get_exn func.fun_scope)
                   , List.map func.fun_returns ~f:(tyctx_get_exn func.fun_scope) ) ))))
    in
    List.fold_result prog.prg_funcs ~init:() ~f:(fun () func -> check_func sigma func))
;;
