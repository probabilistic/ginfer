{
open Core
open Lexing
open Appl_parser

exception Lexer_error of string * Location.t

(* The table of keywords *)

let reserved_words = [
  ("assume", ASSUME);
  ("bool", BOOL);
  ("break", BREAK);
  ("call", CALL);
  ("continue", CONTINUE);
  ("demon", DEMON);
  ("do", DO);
  ("else", ELSE);
  ("false", FALSE);
  ("func", FUNC);
  ("if", IF);
  ("int", INT);
  ("local", LOCAL);
  ("prob", PROB);
  ("real", REAL);
  ("return", RETURN);
  ("score", SCORE);
  ("skip", SKIP);
  ("tick", TICK);
  ("to", TO);
  ("true", TRUE);
  ("void", VOID);
  ("weaken", WEAKEN);
  ("while", WHILE);

  ("+", ADD);
  ("!", BANG);
  ("!=", BANGEQ);
  (":", COLON);
  (":=", COLONEQ);
  (",", COMMA);
  ("&&", DAMP);
  ("||", DBAR);
  ("--", DSUB);
  ("-->", DSUBGT);
  ("=", EQ);
  ("==", EQEQ);
  (">=", GE);
  (">", GT);
  ("{", LCURLY);
  ("<=", LE);
  ("(", LPAREN);
  ("[", LSQUARE);
  ("<", LT);
  ("}", RCURLY);
  (")", RPAREN);
  ("]", RSQUARE);
  (";", SEMI);
  ("/", SLASH);
  ("*", STAR);
  ("-", SUB);
  ("~", TILDE);
]

let symbol_table = Hashtbl.of_alist_exn (module String) reserved_words

(* Update the current location with file name and line number. *)

let update_loc lexbuf file line absolute chars =
  let pos = lexbuf.lex_curr_p in
  let new_file = match file with
                 | None -> pos.pos_fname
                 | Some s -> s
  in
  lexbuf.lex_curr_p <- { pos with
    pos_fname = new_file;
    pos_lnum = if absolute then line else pos.pos_lnum + line;
    pos_bol = pos.pos_cnum - chars;
  }

(* Error report *)

let error lexbuf msg = raise (Lexer_error (msg, Location.curr lexbuf))

let () =
  Location.register_error_of_exn
    (function
      | Lexer_error (msg, loc) -> Some (Location.errorf ~loc "%s" msg)
      | _ -> None
    )
}

let newline = ('\013'* '\010')
let blank = [' ' '\009' '\012']
let lowercase = ['a'-'z' '_']
let uppercase = ['A'-'Z']
let identchar = ['A'-'Z' 'a'-'z' '_' '\'' '0'-'9']

let decimal_literal = ['0'-'9'] ['0'-'9' '_']*
let hex_digit = ['0'-'9' 'A'-'F' 'a'-'f']
let hex_literal = '0' ['x' 'X'] ['0'-'9' 'A'-'F' 'a'-'f']['0'-'9' 'A'-'F' 'a'-'f' '_']*
let oct_literal = '0' ['o' 'O'] ['0'-'7'] ['0'-'7' '_']*
let bin_literal = '0' ['b' 'B'] ['0'-'1'] ['0'-'1' '_']*
let int_literal = decimal_literal | hex_literal | oct_literal | bin_literal
let float_literal =
  ['0'-'9'] ['0'-'9' '_']*
  ('.' ['0'-'9' '_']* )?
  (['e' 'E'] ['+' '-']? ['0'-'9'] ['0'-'9' '_']* )?
let hex_float_literal =
  '0' ['x' 'X']
  ['0'-'9' 'A'-'F' 'a'-'f'] ['0'-'9' 'A'-'F' 'a'-'f' '_']*
  ('.' ['0'-'9' 'A'-'F' 'a'-'f' '_']* )?
  (['p' 'P'] ['+' '-']? ['0'-'9'] ['0'-'9' '_']* )?

rule token_exn = parse
  | newline
    { update_loc lexbuf None 1 false 0; token_exn lexbuf }
  | blank+
    { token_exn lexbuf }
  | lowercase identchar* as name
    { match Hashtbl.find symbol_table name with
      | Some kwd -> kwd
      | None -> LID name }
  | uppercase identchar* as name
    { UID name }
  | int_literal as lit
    { INTV (Int.of_string lit) }
  | (float_literal | hex_float_literal) as lit
    { FLOATV (Float.of_string lit) }
  | (float_literal | hex_float_literal | int_literal) identchar+ as invalid
    { error lexbuf ("invalid literal " ^ invalid) }
  | "//"
    { comment_exn lexbuf }
  | "-->"
    { Hashtbl.find_exn symbol_table (lexeme lexbuf) }
  | "!=" | ":=" | "&&" | "||" | "--" | "==" | ">=" | "<="
    { Hashtbl.find_exn symbol_table (lexeme lexbuf) }
  | ['+' '!' ':' ',' '=' '>' '{' '(' '[' '<' '}' ')' ']' ';' '/' '*' '-' '~']
    { Hashtbl.find_exn symbol_table (lexeme lexbuf) }
  | eof
    { EOF }
  | _ as illegal_char
    { error lexbuf ("illegal character (" ^ Char.escaped illegal_char ^ ")") }

and comment_exn = parse
  | newline
    { update_loc lexbuf None 1 false 0; token_exn lexbuf }
  | _
    { comment_exn lexbuf }
