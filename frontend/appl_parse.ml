open Core
open Result.Let_syntax
open Action_types

let token lexbuf = Result.try_with (fun () -> Appl_lexer.token_exn lexbuf)

let wrap parsing_fun lexbuf =
  let res = parsing_fun lexbuf in
  Parsing.clear_parser ();
  res
;;

let rec loop lexbuf checkpoint =
  let module I = Appl_parser.MenhirInterpreter in
  match checkpoint with
  | I.InputNeeded _ ->
    let%bind token = token lexbuf in
    let triple = token, lexbuf.Lexing.lex_start_p, lexbuf.Lexing.lex_curr_p in
    let checkpoint = I.offer checkpoint triple in
    loop lexbuf checkpoint
  | I.Shifting _ | I.AboutToReduce _ -> loop lexbuf (I.resume checkpoint)
  | I.Accepted v -> Ok v
  | I.Rejected ->
    let loc = Location.curr lexbuf in
    Error (Static_error ("Syntax error", loc))
  | I.HandlingError env ->
    let loc = Location.curr lexbuf in
    let state =
      match I.stack env with
      | (lazy Nil) -> 0
      | (lazy (Cons (I.Element (state, _, _, _), _))) -> I.number state
    in
    let msg = "Syntax error (state " ^ Int.to_string state ^ ")" in
    Error (Static_error (msg, loc))
;;

let wrap_menhir entry_exn lexbuf =
  let initial = entry_exn lexbuf.Lexing.lex_curr_p in
  wrap (fun lexbuf -> loop lexbuf initial) lexbuf
;;

let toplevel_cfg = wrap_menhir Appl_parser.Incremental.toplevel_cfg_exn
let toplevel_imp = wrap_menhir Appl_parser.Incremental.toplevel_imp_exn
let toplevel_bool_expr = wrap_menhir Appl_parser.Incremental.toplevel_bool_expr_exn
