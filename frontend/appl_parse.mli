open Action_types
open Action_ops

val toplevel_cfg : Lexing.lexbuf -> (string -> Cfg.program, exn) Result.t
val toplevel_imp : Lexing.lexbuf -> (string -> Imp.program, exn) Result.t

val toplevel_bool_expr
  :  Lexing.lexbuf
  -> (Typed.rhs_ty tycontext -> Typed.bool_expr, exn) result
