%{
open Action_types
open Action_ops
open Action_checkers

module List = Core.List
module Result = Core.Result

let mkloc = Location.mkloc

let make_loc (start_pos, end_pos) = {
  Location.loc_start = start_pos;
  Location.loc_end = end_pos;
  Location.loc_ghost = false;
}

let mkexpr ~loc mk = mk ~loc:(make_loc loc)

let mk_bool_expr ctx e =
  match Typed.refine @@ Result.ok_exn @@ tycheck_expr ctx e with
  | `BoolExpr be -> be
  | _ -> raise (Static_error ("non-Boolean expression", Untyped.expr_loc e))

let mk_arith_expr ctx e =
  match Typed.refine @@ Result.ok_exn @@ tycheck_expr ctx e with
  | `ArithExpr ae -> ae
  | _ -> raise (Static_error ("non-numeric expression", Untyped.expr_loc e))

let mk_bool_or_prob_expr ctx e =
  match Typed.refine @@ Result.ok_exn @@ tycheck_expr ctx e with
  | `ProbExpr pe -> `Prob pe
  | `BoolExpr be -> `Bool be
  | _ -> raise (Static_error ("non-probability and non-Boolean condition", Untyped.expr_loc e))

let mkrexpr ~loc mk_rexp_desc = fun ctx -> {
  Imp.rexp_desc = mk_rexp_desc ctx;
  rexp_loc = make_loc loc;
}

let mkstmt ~loc mk_stm_desc = fun ctx -> {
  Imp.stm_desc = mk_stm_desc ctx;
  stm_loc = make_loc loc;
}

let build_scope_exn params locals =
  List.fold_result (List.append params locals) ~init:tyctx_empty ~f:(fun ctx (x, ty) -> tyctx_add x ty ctx)
  |> Result.ok_exn

let create_random_expr_exn ~loc name params =
  match create_sample_dist_exn ~loc name params with
  | `Int imp_dist -> Imp.REIntDist imp_dist
  | `Real real_dist -> Imp.RERealDist real_dist
%}

%token ASSUME       "assume"
%token BOOL         "bool"
%token BREAK        "break"
%token CALL         "call"
%token CONTINUE     "continue"
%token DEMON        "demon"
%token DO           "do"
%token ELSE         "else"
%token FALSE        "false"
%token FUNC         "func"
%token IF           "if"
%token INT          "int"
%token LOCAL        "local"
%token PROB         "prob"
%token REAL         "real"
%token RETURN       "return"
%token SCORE        "score"
%token SKIP         "skip"
%token TICK         "tick"
%token TO           "to"
%token TRUE         "true"
%token VOID         "void"
%token WEAKEN       "weaken"
%token WHILE        "while"

%token ADD          "+"
%token BANG         "!"
%token BANGEQ       "!="
%token COLON        ":"
%token COLONEQ      ":="
%token COMMA        ","
%token DAMP         "&&"
%token DBAR         "||"
%token DSUB         "--"
%token DSUBGT       "-->"
%token EQ           "="
%token EQEQ         "=="
%token GE           ">="
%token GT           ">"
%token LCURLY       "{"
%token LE           "<="
%token LPAREN       "("
%token LSQUARE      "["
%token LT           "<"
%token RCURLY       "}"
%token RPAREN       ")"
%token RSQUARE      "]"
%token SEMI         ";"
%token SLASH        "/"
%token STAR         "*"
%token SUB          "-"
%token TILDE        "~"

%token <string> LID
%token <string> UID
%token <int> INTV
%token <float> FLOATV

%token EOF

%start toplevel_cfg_exn toplevel_imp_exn toplevel_bool_expr_exn
%type <string -> Cfg.program> toplevel_cfg_exn
%type <string -> Imp.program> toplevel_imp_exn
%type <Typed.rhs_ty tycontext -> Typed.bool_expr> toplevel_bool_expr_exn

%%

%inline mkloc(symb): symb { mkloc $1 (make_loc $sloc) }
%inline mkexpr(symb): symb { mkexpr ~loc:$sloc $1 }
%inline mkrexpr(symb): symb { mkrexpr ~loc:$sloc $1 }
%inline mkstmt(symb): symb { mkstmt ~loc:$sloc $1 }

// Toplevels

toplevel_cfg_exn:
  | funcs = nonempty_list(cfg_func); EOF
    { fun filename -> { Cfg.prg_funcs = funcs; prg_loc = Location.in_file filename } }

toplevel_imp_exn:
  | funcs = nonempty_list(imp_func); EOF
    { fun filename -> { Imp.prg_funcs = funcs; prg_loc = Location.in_file filename } }

toplevel_bool_expr_exn:
  | e = expr; EOF
    { fun ctx ->
      match Typed.refine (Result.ok_exn @@ tycheck_expr ctx e) with
      | `BoolExpr be -> be
      | _ -> raise (Static_error ("non-Boolean predicate", make_loc $sloc)) }

// Cfg

cfg_func:
  | FUNC; name = mkloc(LID);
    LPAREN; params = separated_list(COMMA, cfg_var_decl); RPAREN;
    LSQUARE; init = cfg_vertex; COMMA; exit = cfg_vertex; RSQUARE;
    LOCAL; COLON; LCURLY; locals = separated_list(COMMA, cfg_var_decl); RCURLY;
    RETURN; COLON; LCURLY returns = separated_list(COMMA, mkloc(LID)); RCURLY;
    edges = list(cfg_edge)
    { let scope = build_scope_exn params locals in
      { Cfg.fun_name = name;
        fun_params = List.unzip params |> fst;
        fun_locals = List.unzip locals |> fst;
        fun_returns = returns;
        fun_scope = scope;
        fun_init = init;
        fun_exit = exit;
        fun_cfg = Cfg.of_edges (List.map edges ~f:(fun mk_edge -> mk_edge scope));
      } }

cfg_edge:
  | src = cfg_vertex; DSUB; mk_ctrl = cfg_control; DSUBGT; LCURLY; dsts = separated_list(COMMA, cfg_vertex); RCURLY
    { fun ctx -> Cfg.create_edge src (mk_ctrl ctx) dsts (make_loc $sloc) }

cfg_control:
  | mk_act = cfg_typed_action
    { fun ctx -> CSeq (mk_act ctx) }
  | LSQUARE; e = expr; RSQUARE
    { fun ctx ->
      match Typed.refine (Result.ok_exn @@ tycheck_expr ctx e) with
      | `BoolExpr be -> CCond be
      | `ProbExpr pe -> CProb pe
      | _ -> raise (Static_error ("condition should be either Boolean or probability", make_loc $sloc)) }
  | CALL; name = mkloc(LID); LPAREN; args = separated_list(COMMA, mkloc(LID)); RPAREN; TO; LCURLY; xs = separated_list(COMMA, mkloc(LID)); RCURLY
    { fun _ -> CCall (name, args, xs) }

cfg_typed_action:
  | act = cfg_action
    { fun ctx -> Result.ok_exn @@ tycheck_data_action ctx act }

cfg_action:
  | SKIP
    { DSkip }
  | WEAKEN; LPAREN; es = separated_list(COMMA, expr); RPAREN
    { DWeaken es }
  | x = mkloc(LID); COLONEQ; e = expr
    { DAssign (x, e) }
  | SCORE; LPAREN; weight = expr; RPAREN
    { DScore (weight, ref None) }
  | x = mkloc(LID); TILDE; name = LID; LPAREN; theta = separated_list(COMMA, expr); RPAREN
    { DSample (x, create_sample_dist_exn ~loc:(make_loc $sloc) name theta) }

cfg_vertex:
  | n = INTV
    { Cfg.create_vertex n }

cfg_var_decl:
  | ty = ty; x = mkloc(LID)
    { (x, ty) }

// Imp

imp_func:
  | return_ty = imp_void_ty; name = mkloc(LID);
    LPAREN; params = separated_list(COMMA, imp_var_decl); RPAREN;
    LCURLY; locals = list(terminated(imp_vars_decl, SEMI)); mk_body = imp_body; RCURLY
    { let scope = build_scope_exn params (List.concat locals) in
      { Imp.fun_name = name;
        fun_return_ty = return_ty;
        fun_params = List.unzip params |> fst;
        fun_locals = List.unzip (List.concat locals) |> fst;
        fun_scope = scope;
        fun_body = mk_body scope;
      } }

imp_body:
  | mkstmt(
      mk_stmts = list(imp_stmt)
      { fun ctx -> Imp.SBlock (List.map mk_stmts ~f:(fun mk_stmt -> mk_stmt ctx)) }
    )
    { $1 }

imp_stmt:
  | mkstmt(
      SKIP; SEMI
      { fun _ -> Imp.SSkip }
    | WEAKEN; LPAREN; es = separated_list(COMMA, expr); RPAREN; SEMI
      { fun ctx -> Imp.SWeaken (List.map es ~f:(fun e -> mk_arith_expr ctx e)) }
    | x = mkloc(LID); EQ; e = imp_rexpr; SEMI
      { fun ctx -> Imp.SAssign (x, e ctx) }
    | TICK; LPAREN; e = expr; RPAREN; SEMI
      { fun ctx -> Imp.STick (mk_arith_expr ctx e) }
    | ASSUME; LPAREN; e = expr; RPAREN; SEMI
      { fun ctx -> Imp.SAssume (mk_bool_expr ctx e) }
    | LCURLY; mk_stmts = list(imp_stmt); RCURLY
      { fun ctx -> Imp.SBlock (List.map mk_stmts ~f:(fun mk_stmt -> mk_stmt ctx)) }
    | IF; LPAREN; e = expr; RPAREN; LCURLY; mk_body1 = imp_body; RCURLY
      { fun ctx ->
        match mk_bool_or_prob_expr ctx e with
        | `Bool be -> Imp.SCond (be, mk_body1 ctx, None)
        | `Prob pe -> Imp.SProb (pe, mk_body1 ctx, None) }
    | IF; LPAREN; e = expr; RPAREN; LCURLY; mk_body1 = imp_body; RCURLY; ELSE; LCURLY; mk_body2 = imp_body; RCURLY
      { fun ctx ->
        match mk_bool_or_prob_expr ctx e with
        | `Bool be -> Imp.SCond (be, mk_body1 ctx, Some (mk_body2 ctx))
        | `Prob pe -> Imp.SProb (pe, mk_body1 ctx, Some (mk_body2 ctx)) }
    | name = mkloc(LID); LPAREN; args = separated_list(COMMA, mkloc(LID)); RPAREN; SEMI
      { fun _ -> Imp.SCall (name, args, None) }
    | x = mkloc(LID); EQ; name = mkloc(LID); LPAREN; args = separated_list(COMMA, mkloc(LID)); RPAREN; SEMI
      { fun _ -> Imp.SCall (name, args, Some x) }
    | BREAK; SEMI
      { fun _ -> Imp.SBreak }
    | CONTINUE; SEMI
      { fun _ -> Imp.SContinue }
    | RETURN; SEMI
      { fun _ -> Imp.SReturn None }
    | RETURN; x = mkloc(LID); SEMI
      { fun _ -> Imp.SReturn (Some x) }
    | WHILE; LPAREN; e = expr; RPAREN; LCURLY; mk_body = imp_body; RCURLY
      { fun ctx -> Imp.SWhile (mk_bool_expr ctx e, mk_body ctx) }
    | DO; LCURLY; mk_body = imp_body; RCURLY; WHILE; LPAREN; e = expr; RPAREN; SEMI
      { fun ctx -> Imp.SRepeat (mk_body ctx, mk_bool_expr ctx e) }
    )
    { $1 }

imp_void_ty:
  | ty = ty
    { Some ty }
  | VOID
    { None }

imp_rexpr:
  | e = imp_rexpr1
    { e }
  | mkrexpr(
      e1 = imp_rexpr1; DAMP; e2 = imp_rexpr
      { fun ctx -> Imp.REAnd (e1 ctx, e2 ctx) }
    | e1 = imp_rexpr1; DBAR; e2 = imp_rexpr
      { fun ctx -> Imp.REOr (e1 ctx, e2 ctx) }
    )
    { $1 }

imp_rexpr1:
  | e = imp_rexpr2
    { e }
  | mkrexpr(
      e1 = imp_rexpr1; LE; e2 = imp_rexpr2
      { fun ctx -> Imp.RERelOp (LE, e1 ctx, e2 ctx) }
    | e1 = imp_rexpr1; GE; e2 = imp_rexpr2
      { fun ctx -> Imp.RERelOp (GE, e1 ctx, e2 ctx) }
    | e1 = imp_rexpr1; LT; e2 = imp_rexpr2
      { fun ctx -> Imp.RERelOp (LT, e1 ctx, e2 ctx) }
    | e1 = imp_rexpr1; GT; e2 = imp_rexpr2
      { fun ctx -> Imp.RERelOp (GT, e1 ctx, e2 ctx) }
    | e1 = imp_rexpr1; EQEQ; e2 = imp_rexpr2
      { fun ctx -> Imp.RERelOp (EQ, e1 ctx, e2 ctx) }
    | e1 = imp_rexpr1; BANGEQ; e2 = imp_rexpr2
      { fun ctx -> Imp.RERelOp (NE, e1 ctx, e2 ctx) }
    )
    { $1 }

imp_rexpr2:
  | e = imp_rexpr3
    { e }
  | mkrexpr(
      e1 = imp_rexpr2; ADD; e2 = imp_rexpr3
      { fun ctx -> Imp.REBinOp (Add, e1 ctx, e2 ctx) }
    | e1 = imp_rexpr2; SUB; e2 = imp_rexpr3
      { fun ctx -> Imp.REBinOp (Sub, e1 ctx, e2 ctx) }
    )
    { $1 }

imp_rexpr3:
  | e = imp_rexpr4
    { e }
  | mkrexpr(
      e1 = imp_rexpr3; STAR; e2 = imp_rexpr4
      { fun ctx -> Imp.REBinOp (Mul, e1 ctx, e2 ctx) }
    )
    { $1 }

imp_rexpr4:
  | e = imp_rexpr5
    { e }
  | mkrexpr(
      SUB; e0 = imp_rexpr5
      { fun ctx -> Imp.RENegate (e0 ctx) }
    | BANG; e0 = imp_rexpr5
      { fun ctx -> Imp.RENot (e0 ctx) }
    )
    { $1 }

imp_rexpr5:
  | LPAREN; e = imp_rexpr; RPAREN
    { e }
  | mkrexpr(
      x = mkloc(LID)
      { fun ctx ->
        match tyctx_get_exn ctx x with
        | `TyReal -> Imp.REVar (x, `TyReal)
        | `TyInt -> Imp.REVar (x, `TyInt)
        | _ -> raise (Static_error ("a numeric variable is expected", x.loc)) }
    | n = INTV
      { fun _ -> Imp.REInt n }
    | f = FLOATV
      { fun _ -> Imp.REReal f }
    | n = INTV; SLASH; d = INTV
      { fun _ -> Imp.REFrac (Mpqf.of_frac n d) }
    | TRUE
      { fun _ -> Imp.REBool true }
    | FALSE
      { fun _ -> Imp.REBool false }
    | DEMON
      { fun _ -> Imp.RENondet }
    | PROB; LPAREN; a = INTV; COMMA; b = INTV; RPAREN
      { fun _ -> Imp.REProb (a, b) }
    | name = UID; LPAREN; theta = separated_list(COMMA, expr); RPAREN
      { fun _ -> create_random_expr_exn ~loc:(make_loc $sloc) name theta }
    )
    { $1 }

imp_var_decl:
  | ty = ty; x = mkloc(LID)
    { (x, ty) }

imp_vars_decl:
  | ty = ty; xs = separated_nonempty_list(COMMA, mkloc(LID))
    { List.map xs ~f:(fun x -> (x, ty)) }

// Common

ty:
  | INT
    { `TyInt }
  | REAL
    { `TyReal }
  | PROB
    { `TyProb }
  | BOOL
    { `TyBool }

expr:
  | e = expr1
    { e }
  | mkexpr(
      e1 = expr1; DAMP; e2 = expr
      { Untyped.mk_and (e1, e2) }
    | e1 = expr1; DBAR; e2 = expr
      { Untyped.mk_or (e1, e2) }
    )
    { $1 }

expr1:
  | e = expr2
    { e }
  | mkexpr(
      e1 = expr1; LE; e2 = expr2
      { Untyped.mk_rel_op (LE, e1, e2) }
    | e1 = expr1; GE; e2 = expr2
      { Untyped.mk_rel_op (GE, e1, e2) }
    | e1 = expr1; LT; e2 = expr2
      { Untyped.mk_rel_op (LT, e1, e2) }
    | e1 = expr1; GT; e2 = expr2
      { Untyped.mk_rel_op (GT, e1, e2) }
    | e1 = expr1; EQEQ; e2 = expr2
      { Untyped.mk_rel_op (EQ, e1, e2) }
    | e1 = expr1; BANGEQ; e2 = expr2
      { Untyped.mk_rel_op (NE, e1, e2) }
    )
    { $1 }

expr2:
  | e = expr3
    { e }
  | mkexpr(
      e1 = expr2; ADD; e2 = expr3
      { Untyped.mk_bin_op (Add, e1, e2) }
    | e1 = expr2; SUB; e2 = expr3
      { Untyped.mk_bin_op (Sub, e1, e2) }
    )
    { $1 }

expr3:
  | e = expr4
    { e }
  | mkexpr(
      e1 = expr3; STAR; e2 = expr4
      { Untyped.mk_bin_op (Mul, e1, e2) }
    )
    { $1 }

expr4:
  | e = expr5
    { e }
  | mkexpr(
      SUB; e0 = expr5
      { Untyped.mk_negate e0 }
    | BANG; e0 = expr5
      { Untyped.mk_not e0 }
    )
    { $1 }

expr5:
  | LPAREN; e = expr; RPAREN
    { e }
  | mkexpr(
      x = mkloc(LID)
      { Untyped.mk_var x }
    | n = INTV
      { Untyped.mk_int n }
    | f = FLOATV
      { Untyped.mk_real f }
    | n = INTV; SLASH; d = INTV
      { Untyped.mk_frac (Mpqf.of_frac n d) }
    | TRUE
      { Untyped.mk_bool true }
    | FALSE
      { Untyped.mk_bool false }
    | DEMON
      { Untyped.mk_nondet }
    | PROB; LPAREN; a = INTV; COMMA; b = INTV; RPAREN
      { Untyped.mk_prob (a, b) }
    )
    { $1 }
