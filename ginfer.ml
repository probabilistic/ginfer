open Core
open Result.Let_syntax
open Ffloat
module Build_info = Build_info.V1

exception Invalid_cmd_options of string

let () =
  Caml.Printexc.register_printer (function
    | Invalid_cmd_options msg -> Some ("invalid command line options: " ^ msg)
    | _ -> None)
;;

let version_to_string v =
  Option.value_map ~f:Build_info.Version.to_string v ~default:"[unknown]"
;;

let version = version_to_string (Build_info.version ())

let build_info =
  let libs =
    List.map (Build_info.Statically_linked_libraries.to_list ()) ~f:(fun lib ->
      ( Build_info.Statically_linked_library.name lib
      , version_to_string (Build_info.Statically_linked_library.version lib) ))
    |> List.sort ~compare:[%compare: string * string]
  in
  let max_length =
    List.fold_left libs ~init:0 ~f:(fun n (name, _) -> max n (String.length name))
  in
  String.concat
    ~sep:"\n"
    ((Printf.sprintf "%-*s %s" (max_length + 2) "ocaml:" Sys.ocaml_version
     :: "statically linked libraries:"
     :: List.map libs ~f:(fun (name, v) -> Printf.sprintf "- %-*s %s" max_length name v))
    @ [ "version:" ])
;;

let analyzed = ref false

let report_error exn =
  try
    Format.eprintf "@.%a" Location.report_exception exn;
    exit 1
  with
  | e ->
    Format.eprintf "%a@." Exn.pp e;
    exit 2
;;

let report_result result = Result.iter_error result ~f:report_error

let parse_file ~loop_unroll filename =
  Timer.wrap_duration "parsing" (fun () ->
    match Sys_unix.file_exists filename with
    | `No | `Unknown -> Error (Invalid_cmd_options "file not found")
    | `Yes ->
      In_channel.with_file filename ~f:(fun pi ->
        let lexbuf = Lexing.from_channel pi in
        Location.init lexbuf filename;
        Location.input_name := filename;
        Location.input_lexbuf := Some lexbuf;
        if String.is_suffix filename ~suffix:".c"
        then (
          let%bind gen = Appl_parse.toplevel_imp lexbuf in
          gen filename |> Imp_reader.transform ~loop_unroll)
        else (
          let%bind gen = Appl_parse.toplevel_cfg lexbuf in
          Ok (gen filename))))
;;

let parse_logic str_opt =
  match str_opt with
  | None -> Ok None
  | Some str ->
    let lexbuf = Lexing.from_string str in
    Location.init lexbuf "";
    Location.input_name := "";
    Location.input_lexbuf := Some lexbuf;
    let%bind cond = Appl_parse.toplevel_bool_expr lexbuf in
    Ok (Some cond)
;;

let get_context n =
  if n >= 0
  then
    (module Kcfa.Make (struct
      let k = n
    end) : Ctxt_type.CFG_CONTEXT)
  else (module Acyclic_cfa.Make () : Ctxt_type.CFG_CONTEXT)
;;

let potentials = Hashtbl.create (module String) ~size:2

let () =
  Lp.iter_lp_solvers_std ~f:(fun ~name ~solver ->
    Hashtbl.set
      potentials
      ~key:name
      ~data:(module Pot.Make (Std_float) ((val solver)) : Pot.S));
  Lp.iter_lp_solvers_mpq ~f:(fun ~name ~solver ->
    Hashtbl.set
      potentials
      ~key:name
      ~data:(module Pot.Make (Mpq_float) ((val solver)) : Pot.S))
;;

let solver_list = Hashtbl.keys potentials

let get_solver name =
  try Hashtbl.find_exn potentials name with
  | _ -> report_error (Invalid_cmd_options ("unsupported solver " ^ name))
;;

module Make_ai (Context_for_ai : Ctxt_type.CFG_CONTEXT) (Flt : FLOAT) = struct
  let run
    ?init_cond
    ~loop_unroll
    ~is_backward
    ?(debug = false)
    ?(no_ai = false)
    ?(calc_ncc = false)
    filename
    =
    let%bind prog = parse_file ~loop_unroll filename in
    let%bind () = Cfg_typechecker.check_prog prog in
    let%bind init_cond = parse_logic init_cond in
    let main_func = Cfg.main_func prog in
    let%bind init_cond =
      match init_cond with
      | None -> Ok None
      | Some init_cond -> Result.try_with (fun () -> Some (init_cond main_func.fun_scope))
    in
    let module Octagon0 = Polyhedron.Make0 (Polyhedron.Oct0) in
    let module Cfg_analysis_oct0 = Cfg_analysis.Make0 (Octagon0) (Context_for_ai) in
    let module Ppl0 = Polyhedron.Make0 (Polyhedron.Ppl0) in
    let module Cfg_Analysis_ppl0 = Cfg_analysis.Make0 (Ppl0) (Context_for_ai) in
    let dir = if is_backward then `Backward else `Forward in
    let sol_ppl =
      Timer.wrap_duration "abstract-interpretation" (fun () ->
        if no_ai
        then fun _ -> [ Context_for_ai.empty, Ppl0.top [] ]
        else (
          let sol_oct = Cfg_analysis_oct0.analyze ?init:init_cond dir prog in
          Cfg_Analysis_ppl0.analyze
            ?init:init_cond
            ~widen_hints:(fun (ctx, v) ->
              List.Assoc.find_exn (sol_oct v) ~equal:Context_for_ai.equal ctx
              |> Octagon0.to_hint)
            dir
            prog))
    in
    if debug
    then (
      Format.printf "@.";
      List.iter prog.prg_funcs ~f:(fun func ->
        Format.printf
          "%s [entry: %a; exit: %a]@."
          func.Cfg.fun_name.txt
          Cfg.print_vertex
          func.fun_init
          Cfg.print_vertex
          func.fun_exit;
        Cfg.iter_vertex
          (fun v ->
            Format.printf "\t%s.%a@." func.fun_name.txt Cfg.print_vertex v;
            let abss = sol_ppl v in
            List.iter abss ~f:(fun (ctx, abs) ->
              Format.printf "\t\t%a -> %a@." Context_for_ai.print ctx Ppl0.print abs))
          func.fun_cfg));
    let ncc = ref 0 in
    if calc_ncc
    then
      Timer.wrap_duration "degree-check-computation" (fun () ->
        List.iter prog.prg_funcs ~f:(fun func ->
          Cfg.iter_vertex
            (fun v ->
              let ppl_abss = sol_ppl v in
              if not (List.is_empty ppl_abss)
              then (
                let ppl_abs =
                  List.fold
                    (List.tl_exn ppl_abss)
                    ~init:(List.hd_exn ppl_abss |> snd)
                    ~f:(fun acc (_, abs) -> Ppl0.join acc abs)
                in
                Cfg.iter_succ_e
                  (fun e ->
                    let ctrl = Cfg.edge_action e in
                    match ctrl with
                    | Cfg.CSeq (Action_types.Typed.DScore (tick, sgn)) ->
                      if Option.is_none !sgn then ncc := max !ncc (Action_ops.degree tick);
                      let smt = Ppl0.to_smt ppl_abs in
                      let tick_e = Action_ops.smt_of_arith_expr tick in
                      if Smt.check smt (Smt.mk_ge tick_e (Smt.mk_const 0))
                      then sgn := Some true
                      else if Smt.check smt (Smt.mk_le tick_e (Smt.mk_const 0))
                      then sgn := Some false
                    | _ -> ())
                  func.fun_cfg
                  v))
            func.fun_cfg));
    let module Gen_poly = Ppl0.Gen_poly (Flt) in
    Ok (prog, init_cond, sol_ppl, Gen_poly.to_polys, !ncc)
  ;;
end

module Make (Context_for_vc : Ctxt_type.CFG_CONTEXT) (Pot : Pot.S) = struct
  module Flt = Pot.Flt
  module Ai = Make_ai (Context_for_vc) (Flt)

  let process_file_and_do_ai_for_vc
    ?init_cond
    ~loop_unroll
    ~no_ai
    ?(debug = false)
    filename
    =
    let%bind prog, init_cond, sol_ppl, to_polys, ncc =
      Ai.run
        ?init_cond
        ~loop_unroll
        ~is_backward:false
        ~debug
        ~no_ai
        ~calc_ncc:true
        filename
    in
    if debug then Format.printf "@.";
    Ok (prog, init_cond, sol_ppl, to_polys, ncc)
  ;;

  let run_bi ?init_cond ~loop_unroll ~no_ai filename mom deg is_lower =
    let%bind prog, init_cond, sol_ppl, ppl_to_polys, ncc =
      process_file_and_do_ai_for_vc ?init_cond ~loop_unroll ~no_ai filename
    in
    let module V = struct
      let deg = deg
      let mom = mom
      let obj = if is_lower then `Lower else `Upper
    end
    in
    let module Bound_infer0 = Bound_infer.Make0 (Pot) (V) in
    let module Cfg_vc_gen0 = Cfg_vc_gen.Make0 (Bound_infer0) (Context_for_vc) in
    let%bind sol =
      Cfg_vc_gen0.analyze
        ~stat:true
        ?init:init_cond
        ~hints:(fun (ctx, v) ->
          let ppl_result =
            List.Assoc.find_exn (sol_ppl v) ~equal:Context_for_vc.equal ctx
          in
          ppl_to_polys ppl_result)
        prog
    in
    let main_func = Cfg.main_func prog in
    let v_sol = sol (Context_for_vc.empty, main_func.fun_init) in
    Format.printf "@.";
    if is_lower
    then
      Format.printf
        "note: degree for termination check: %d@."
        (max
           ((ncc + 1) * mom)
           (deg + Bool.to_int (Cfg.is_prog_recursive prog) (* + (ncc + 1) * (mom - 1) *)));
    Format.printf
      "%s.%a:@.%a"
      main_func.fun_name.txt
      Cfg.print_vertex
      main_func.fun_init
      Bound_infer0.print_sol
      v_sol;
    Ok ()
  ;;

  let run_bi2 ?init_cond ~loop_unroll ~no_ai filename mom deg is_lower =
    let%bind prog, init_cond, sol_ppl, ppl_to_polys, ncc =
      process_file_and_do_ai_for_vc ?init_cond ~loop_unroll ~no_ai filename
    in
    let module V = struct
      let deg = deg
      let mom = mom
      let obj = if is_lower then `Lower else `Upper
    end
    in
    let module Bound_infer0 = Bound_infer2.Make0 (Pot) (V) in
    let module Cfg_vc_gen0 = Cfg_vc_gen.Make0 (Bound_infer0) (Context_for_vc) in
    let%bind sol =
      Cfg_vc_gen0.analyze
        ~stat:true
        ?init:init_cond
        ~hints:(fun (ctx, v) ->
          let ppl_result =
            List.Assoc.find_exn (sol_ppl v) ~equal:Context_for_vc.equal ctx
          in
          ppl_to_polys ppl_result)
        prog
    in
    let main_func = Cfg.main_func prog in
    let v_sol = sol (Context_for_vc.empty, main_func.fun_init) in
    Format.printf
      "@.note: degree for termination check: %d@."
      (max
         ((ncc + 1) * mom)
         (deg + Bool.to_int (Cfg.is_prog_recursive prog) (* + (ncc + 1) * (mom - 1) *)));
    Format.printf
      "%s.%a:@.%a"
      main_func.fun_name.txt
      Cfg.print_vertex
      main_func.fun_init
      Bound_infer0.print_sol
      v_sol;
    Ok ()
  ;;

  let run_vi ?init_cond ~loop_unroll ~no_ai filename mom deg is_lower =
    let%bind prog, init_cond, sol_ppl, ppl_to_polys, ncc =
      process_file_and_do_ai_for_vc ?init_cond ~loop_unroll ~no_ai filename
    in
    let module Bound_infer_fun = Bound_infer.Make0 (Pot) in
    let goal_poly =
      Pot.(
        Flt_poly.pow
          mom
          (Flt_poly.add_monom
             (Polynomial.Monom.of_var "TICK")
             Flt.one
             (Flt_poly.const Flt.minus_one)))
    in
    let%bind sols =
      Result.try_with (fun () ->
        Array.init mom ~f:(fun m ->
          let rec_lb = ref None in
          let rec_ub = ref None in
          let v_sol_ub () =
            match !rec_ub with
            | Some sol -> sol
            | None ->
              let module V = struct
                let deg = deg
                let mom = m + 1
                let obj = `Upper
              end
              in
              let module Bound_infer0 = Bound_infer_fun (V) in
              let module Cfg_vc_gen0 = Cfg_vc_gen.Make0 (Bound_infer0) (Context_for_vc) in
              let sol =
                Cfg_vc_gen0.analyze
                  ?init:init_cond
                  ~hints:(fun (ctx, v) ->
                    let ppl_result =
                      List.Assoc.find_exn (sol_ppl v) ~equal:Context_for_vc.equal ctx
                    in
                    ppl_to_polys ppl_result)
                  prog
                |> Result.ok_exn
              in
              let main_func = Cfg.main_func prog in
              let v_sol_ub = sol (Context_for_vc.empty, main_func.fun_init) in
              rec_ub := Some v_sol_ub;
              v_sol_ub
          in
          let v_sol_lb () =
            match !rec_lb with
            | Some sol -> sol
            | None ->
              let module V = struct
                let deg = deg
                let mom = m + 1
                let obj = `Lower
              end
              in
              let module Bound_infer0 = Bound_infer_fun (V) in
              let module Cfg_vc_gen0 = Cfg_vc_gen.Make0 (Bound_infer0) (Context_for_vc) in
              let sol =
                Cfg_vc_gen0.analyze
                  ?init:init_cond
                  ~hints:(fun (ctx, v) ->
                    let ppl_result =
                      List.Assoc.find_exn (sol_ppl v) ~equal:Context_for_vc.equal ctx
                    in
                    ppl_to_polys ppl_result)
                  prog
                |> Result.ok_exn
              in
              let main_func = Cfg.main_func prog in
              let v_sol_lb = sol (Context_for_vc.empty, main_func.fun_init) in
              rec_lb := Some v_sol_lb;
              v_sol_lb
          in
          v_sol_lb, v_sol_ub))
    in
    let%bind v_sol =
      Result.try_with (fun () ->
        if is_lower
        then
          Pot.(
            Flt_poly.fold goal_poly ~init:Pot.zero_sol ~f:(fun ~mono ~coef acc_lb ->
              let d = Polynomial.Monom.degree mono in
              if d = 0
              then acc_lb
              else if d = 1
              then
                if Flt.(coef >= zero)
                then
                  Pot.add_sol
                    Flt.(coef - one)
                    (Pot.pow_sol mom ((sols.(0) |> fst) ()))
                    acc_lb
                else
                  Pot.add_sol
                    Flt.(coef + one)
                    (Pot.pow_sol mom ((sols.(0) |> snd) ()))
                    acc_lb
              else if d = mom
              then
                if Flt.(coef >= zero)
                then Pot.add_sol coef ((sols.(d - 1) |> fst) ()) acc_lb
                else Pot.add_sol coef ((sols.(d - 1) |> snd) ()) acc_lb
              else if Flt.(coef >= zero)
              then
                Pot.add_sol
                  coef
                  (Pot.mul_sol
                     ((sols.(d - 1) |> fst) ())
                     (Pot.pow_sol (mom - d) ((sols.(0) |> fst) ())))
                  acc_lb
              else
                Pot.add_sol
                  coef
                  (Pot.mul_sol
                     ((sols.(d - 1) |> snd) ())
                     (Pot.pow_sol (mom - d) ((sols.(0) |> snd) ())))
                  acc_lb))
        else
          Pot.(
            Flt_poly.fold goal_poly ~init:Pot.zero_sol ~f:(fun ~mono ~coef acc_ub ->
              let d = Polynomial.Monom.degree mono in
              if d = 0
              then acc_ub
              else if d = 1
              then
                if Flt.(coef >= zero)
                then
                  Pot.add_sol
                    Flt.(coef - one)
                    (Pot.pow_sol mom ((sols.(0) |> snd) ()))
                    acc_ub
                else
                  Pot.add_sol
                    Flt.(coef + one)
                    (Pot.pow_sol mom ((sols.(0) |> fst) ()))
                    acc_ub
              else if d = mom
              then
                if Flt.(coef >= zero)
                then Pot.add_sol coef ((sols.(d - 1) |> snd) ()) acc_ub
                else Pot.add_sol coef ((sols.(d - 1) |> fst) ()) acc_ub
              else if Flt.(coef >= zero)
              then
                Pot.add_sol
                  coef
                  (Pot.mul_sol
                     ((sols.(d - 1) |> snd) ())
                     (Pot.pow_sol (mom - d) ((sols.(0) |> snd) ())))
                  acc_ub
              else
                Pot.add_sol
                  coef
                  (Pot.mul_sol
                     ((sols.(d - 1) |> fst) ())
                     (Pot.pow_sol (mom - d) ((sols.(0) |> fst) ())))
                  acc_ub)))
    in
    let main_func = Cfg.main_func prog in
    Format.printf "@.";
    if mom > 1 || is_lower
    then
      Format.printf
        "note: degree for termination check: %d@."
        (max
           ((ncc + 1) * mom)
           (deg + Bool.to_int (Cfg.is_prog_recursive prog) (* + (ncc + 1) * (mom - 1) *)));
    Format.printf
      "%s.%a:@.\tcentral moment %d %s %a@."
      main_func.fun_name.txt
      Cfg.print_vertex
      main_func.fun_init
      mom
      (if is_lower then ">=" else "<=")
      Pot.print_sol
      v_sol;
    Ok ()
  ;;

  let run_vi2 ?init_cond ~loop_unroll ~no_ai filename mom deg =
    let%bind prog, init_cond, sol_ppl, ppl_to_polys, ncc =
      process_file_and_do_ai_for_vc ?init_cond ~loop_unroll ~no_ai filename
    in
    let module Bound_infer_fun = Bound_infer2.Make0 (Pot) in
    let%bind sols =
      Result.try_with (fun () ->
        Array.init mom ~f:(fun m ->
          let rec_lb = ref None in
          let rec_ub = ref None in
          let v_sol_ub =
            match !rec_ub with
            | Some sol -> sol
            | None ->
              let module V = struct
                let deg = deg
                let mom = m + 1
                let obj = `Upper
              end
              in
              let module Bound_infer0 = Bound_infer_fun (V) in
              let module Cfg_vc_gen0 = Cfg_vc_gen.Make0 (Bound_infer0) (Context_for_vc) in
              let sol =
                Cfg_vc_gen0.analyze
                  ?init:init_cond
                  ~hints:(fun (ctx, v) ->
                    let ppl_result =
                      List.Assoc.find_exn (sol_ppl v) ~equal:Context_for_vc.equal ctx
                    in
                    ppl_to_polys ppl_result)
                  prog
                |> Result.ok_exn
              in
              let main_func = Cfg.main_func prog in
              let v_sol_ub = sol (Context_for_vc.empty, main_func.fun_init) in
              rec_ub := Some v_sol_ub;
              v_sol_ub
          in
          let v_sol_lb =
            match !rec_lb with
            | Some sol -> sol
            | None ->
              let module V = struct
                let deg = deg
                let mom = m + 1
                let obj = `Lower
              end
              in
              let module Bound_infer0 = Bound_infer_fun (V) in
              let module Cfg_vc_gen0 = Cfg_vc_gen.Make0 (Bound_infer0) (Context_for_vc) in
              let sol =
                Cfg_vc_gen0.analyze
                  ?init:init_cond
                  ~hints:(fun (ctx, v) ->
                    let ppl_result =
                      List.Assoc.find_exn (sol_ppl v) ~equal:Context_for_vc.equal ctx
                    in
                    ppl_to_polys ppl_result)
                  prog
                |> Result.ok_exn
              in
              let main_func = Cfg.main_func prog in
              let v_sol_lb = sol (Context_for_vc.empty, main_func.fun_init) in
              rec_lb := Some v_sol_lb;
              v_sol_lb
          in
          v_sol_lb, v_sol_ub))
    in
    let main_func = Cfg.main_func prog in
    Format.printf
      "@.note: degree for termination check: %d@."
      (max
         ((ncc + 1) * mom)
         (deg + Bool.to_int (Cfg.is_prog_recursive prog) (* + (ncc + 1) * (mom - 1) *)));
    Format.printf "%s.%a:@." main_func.fun_name.txt Cfg.print_vertex main_func.fun_init;
    Array.iteri sols ~f:(fun i (lb, rb) ->
      Format.printf
        "\tmoment %d >= %a@.\tmoment %d <= %a@."
        (i + 1)
        Pot.print_sol
        lb
        (i + 1)
        Pot.print_sol
        rb);
    Ok ()
  ;;

  let run_ti ?init_cond ~loop_unroll ~no_ai filename mom deg =
    let%bind prog, init_cond, sol_ppl, ppl_to_polys, _ =
      process_file_and_do_ai_for_vc ?init_cond ~loop_unroll ~no_ai filename
    in
    let module V = struct
      let deg = deg
      let mom = mom
    end
    in
    let module Termination0 = Termination.Make0 (Pot) (V) in
    let module Cfg_vc_gen0 = Cfg_vc_gen.Make0 (Termination0) (Context_for_vc) in
    let%bind sol =
      Cfg_vc_gen0.analyze
        ~stat:true
        ?init:init_cond
        ~hints:(fun (ctx, v) ->
          let ppl_result =
            List.Assoc.find_exn (sol_ppl v) ~equal:Context_for_vc.equal ctx
          in
          ppl_to_polys ppl_result)
        prog
    in
    let main_func = Cfg.main_func prog in
    let v_sol = sol (Context_for_vc.empty, main_func.fun_init) in
    Format.printf
      "@.%s.%a:@.%a"
      main_func.fun_name.txt
      Cfg.print_vertex
      main_func.fun_init
      Termination0.print_sol
      v_sol;
    Ok ()
  ;;
end

let filename_param = Command.Param.(anon ("filename" %: Filename_unix.arg_type))

let init_cond_spec =
  Command.Spec.(flag "-init" (optional string) ~doc:" specify the initial condition")
;;

let moment_spec =
  Command.Spec.(flag "-mom" (required int) ~doc:" specify the inferred moment")
;;

let degree_spec =
  Command.Spec.(flag "-deg" (required int) ~doc:" specify the maximal degree")
;;

let lower_bound_spec = Command.Spec.(flag "-lower" no_arg ~doc:" infer lower bounds")

let unroll_spec =
  Command.Spec.(flag "-unroll" no_arg ~doc:" unroll loops by one iteration")
;;

let solver_spec =
  Command.Spec.(
    flag
      "-solver"
      (required string)
      ~doc:(" specify the LP solver (" ^ String.concat ~sep:", " solver_list ^ ")"))
;;

let backward_spec = Command.Spec.(flag "-backward" no_arg ~doc:" run in backward mode")

let kcfa_spec =
  Command.Spec.(
    flag "-kcfa" (optional_with_default 0 int) ~doc:" specify the context sensitivity")
;;

let no_ai_spec =
  Command.Spec.(flag "-no-ai" no_arg ~doc:" disable abstract interpretation")
;;

let driver_run_ai =
  Command.basic
    ~summary:"Run abstract interpretation"
    Command.Let_syntax.(
      let%map_open filename = filename_param
      and kcfa = kcfa_spec
      and init_cond = init_cond_spec
      and loop_unroll = unroll_spec
      and is_backward = backward_spec in
      fun () ->
        analyzed := true;
        let ctxt = get_context kcfa in
        let module Ctxt = (val ctxt : Ctxt_type.CFG_CONTEXT) in
        let module M = Make_ai (Ctxt) (Std_float) in
        M.run ?init_cond ~loop_unroll ~is_backward ~debug:true filename |> report_result)
;;

let driver_run_bi =
  Command.basic
    ~summary:"Run bound inference (with non-negative costs)"
    Command.Let_syntax.(
      let%map_open filename = filename_param
      and kcfa = kcfa_spec
      and mom = moment_spec
      and deg = degree_spec
      and is_lower = lower_bound_spec
      and init_cond = init_cond_spec
      and loop_unroll = unroll_spec
      and solver = solver_spec
      and no_ai = no_ai_spec in
      fun () ->
        analyzed := true;
        let ctxt = get_context kcfa in
        let module Ctxt = (val ctxt : Ctxt_type.CFG_CONTEXT) in
        let pot = get_solver solver in
        let module Pot = (val pot : Pot.S) in
        let module M = Make (Ctxt) (Pot) in
        M.run_bi ?init_cond ~loop_unroll ~no_ai filename mom deg is_lower |> report_result)
;;

let driver_run_bi2 =
  Command.basic
    ~summary:"Run bound inference (with general costs)"
    Command.Let_syntax.(
      let%map_open filename = filename_param
      and kcfa = kcfa_spec
      and mom = moment_spec
      and deg = degree_spec
      and is_lower = lower_bound_spec
      and init_cond = init_cond_spec
      and loop_unroll = unroll_spec
      and solver = solver_spec
      and no_ai = no_ai_spec in
      fun () ->
        analyzed := true;
        let ctxt = get_context kcfa in
        let module Ctxt = (val ctxt : Ctxt_type.CFG_CONTEXT) in
        let pot = get_solver solver in
        let module Pot = (val pot : Pot.S) in
        let module M = Make (Ctxt) (Pot) in
        M.run_bi2 ?init_cond ~loop_unroll ~no_ai filename mom deg is_lower
        |> report_result)
;;

let driver_run_ti =
  Command.basic
    ~summary:"Run termination analysis"
    Command.Let_syntax.(
      let%map_open filename = filename_param
      and kcfa = kcfa_spec
      and mom = moment_spec
      and deg = degree_spec
      and init_cond = init_cond_spec
      and loop_unroll = unroll_spec
      and solver = solver_spec
      and no_ai = no_ai_spec in
      fun () ->
        analyzed := true;
        let ctxt = get_context kcfa in
        let module Ctxt = (val ctxt : Ctxt_type.CFG_CONTEXT) in
        let pot = get_solver solver in
        let module Pot = (val pot : Pot.S) in
        let module M = Make (Ctxt) (Pot) in
        M.run_ti ?init_cond ~loop_unroll ~no_ai filename mom deg |> report_result)
;;

let driver_run_vi =
  Command.basic
    ~summary:"Run bound inference on central moments (with non-negative costs)"
    Command.Let_syntax.(
      let%map_open filename = filename_param
      and kcfa = kcfa_spec
      and mom = moment_spec
      and deg = degree_spec
      and is_lower = lower_bound_spec
      and init_cond = init_cond_spec
      and loop_unroll = unroll_spec
      and solver = solver_spec
      and no_ai = no_ai_spec in
      fun () ->
        analyzed := true;
        let ctxt = get_context kcfa in
        let module Ctxt = (val ctxt : Ctxt_type.CFG_CONTEXT) in
        let pot = get_solver solver in
        let module Pot = (val pot : Pot.S) in
        let module M = Make (Ctxt) (Pot) in
        M.run_vi ?init_cond ~loop_unroll ~no_ai filename mom deg is_lower |> report_result)
;;

let driver_run_vi2 =
  Command.basic
    ~summary:"Run bound inference on central moments (with general costs)"
    Command.Let_syntax.(
      let%map_open filename = filename_param
      and kcfa = kcfa_spec
      and mom = moment_spec
      and deg = degree_spec
      and init_cond = init_cond_spec
      and loop_unroll = unroll_spec
      and solver = solver_spec
      and no_ai = no_ai_spec in
      fun () ->
        analyzed := true;
        let ctxt = get_context kcfa in
        let module Ctxt = (val ctxt : Ctxt_type.CFG_CONTEXT) in
        let pot = get_solver solver in
        let module Pot = (val pot : Pot.S) in
        let module M = Make (Ctxt) (Pot) in
        M.run_vi2 ?init_cond ~loop_unroll ~no_ai filename mom deg |> report_result)
;;

module type C0_CONTEXT = Hyper_graph.Context.S with type vertex := C0.Hcfg.G.vertex

let get_c0_context n =
  if n < 0
  then (module Hyper_graph.Context.Make_acyclic_cfa (C0.Hcfg.G.V) : C0_CONTEXT)
  else
    (module Hyper_graph.Context.Make_kcfa
              (C0.Hcfg.G.V)
              (struct
                let k = n
              end) : C0_CONTEXT)
;;

let c0_potentials = Hashtbl.create (module String) ~size:2

let () =
  Lp.iter_lp_solvers_std ~f:(fun ~name ~solver ->
    Hashtbl.set
      c0_potentials
      ~key:name
      ~data:(module Amortized.Potential.Make (Std_float) ((val solver)) : Amortized.S));
  Lp.iter_lp_solvers_mpq ~f:(fun ~name ~solver ->
    Hashtbl.set
      c0_potentials
      ~key:name
      ~data:(module Amortized.Potential.Make (Mpq_float) ((val solver)) : Amortized.S))
;;

let get_c0_solver name =
  try Hashtbl.find_exn c0_potentials name with
  | _ -> report_error (Invalid_cmd_options ("unsupported solver " ^ name))
;;

let test_c0_mono filename ~kcfa ~deg ~mom ~is_lower ~solver =
  match Sys_unix.file_exists filename with
  | `No | `Unknown -> Error (Invalid_cmd_options "file not found")
  | `Yes ->
    In_channel.with_file filename ~f:(fun ch ->
      let%bind prog =
        Timer.wrap_duration "parsing" (fun () ->
          let lexbuf = Lexing.from_channel ch in
          Location.init lexbuf filename;
          Location.input_name := filename;
          Location.input_lexbuf := Some lexbuf;
          let%bind parsing_fun = C0.Parse.program lexbuf in
          let prog = parsing_fun ~filename in
          let%bind prog = C0.Typecheck.g_prog prog in
          let prog = C0.Simplify.g_prog prog in
          let prog = C0.Squash_blocks.g_prog prog in
          let prog = C0.Build_cfg.g_prog prog in
          Ok prog)
      in
      (* Format.eprintf "%a@." Sexp.pp_hum (C0.Hcfg.sexp_of_program prog); *)
      let ctx = get_c0_context kcfa in
      let module Ctx = (val ctx : C0_CONTEXT) in
      let solver = get_c0_solver solver in
      let module P = (val solver : Amortized.S) in
      let module Analysis = C0_analysis.Ecost.Make_inter (Ctx) (P) in
      let%bind sol =
        Analysis.g_prog
          ~degree:deg
          ~moment:mom
          ~direction:(if is_lower then `Lower else `Upper)
          prog
      in
      Format.printf "@.solution: %a@." sol ();
      Ok ())
;;

let driver_c0_mono_cost =
  Command.basic
    ~summary:"Test C0 with monotone costs"
    Command.Let_syntax.(
      let%map_open filename = filename_param
      and kcfa = kcfa_spec
      and mom = moment_spec
      and deg = degree_spec
      and is_lower = lower_bound_spec
      and solver = solver_spec in
      fun () ->
        analyzed := true;
        test_c0_mono filename ~kcfa ~mom ~deg ~is_lower ~solver |> report_result)
;;

let driver =
  Command.group
    ~summary:"General Bound Inference for Probabilistic Programs"
    [ "run-ai", driver_run_ai
    ; "run-bi-mon", driver_run_bi
    ; "run-bi", driver_run_bi2
    ; "run-vi-mon", driver_run_vi
    ; "run-vi", driver_run_vi2
    ; "run-ti", driver_run_ti
    ; "test-mono", driver_c0_mono_cost
    ]
;;

let () =
  let t1 = Time_now.nanoseconds_since_unix_epoch () |> Time_ns.of_int63_ns_since_epoch in
  at_exit (fun () ->
    if !analyzed
    then (
      let t2 =
        Time_now.nanoseconds_since_unix_epoch () |> Time_ns.of_int63_ns_since_epoch
      in
      Format.printf "@.total time: %a@." Time_ns.Span.pp Time_ns.(diff t2 t1)));
  Random.self_init ();
  Command_unix.run ~version ~build_info driver
;;
