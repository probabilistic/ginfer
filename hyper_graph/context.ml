open Core

module type S = sig
  type vertex
  type t [@@deriving sexp, compare, hash, equal]

  val empty : t
  val extend : vertex -> t -> t
  val pp : Format.formatter -> t -> unit
end

module Make_kcfa
    (V : Sig.VERTEX) (K : sig
      val k : int
    end) =
struct
  type t = V.t list [@@deriving sexp, compare, hash, equal]

  let empty = []
  let extend u ctx = List.take (u :: ctx) K.k
  let pp fmt ctx = Format.fprintf fmt "%a" Sexp.pp (List.sexp_of_t V.sexp_of_t ctx)
end

module Make_acyclic_cfa (V : Sig.VERTEX) = struct
  type t = V.t list [@@deriving sexp, compare, hash, equal]

  let empty = []
  let extend u ctx = u :: List.take_while ctx ~f:(fun v -> not (V.equal u v))
  let pp fmt ctx = Format.fprintf fmt "%a" Sexp.pp (List.sexp_of_t V.sexp_of_t ctx)
end
