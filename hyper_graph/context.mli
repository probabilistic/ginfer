module type S = sig
  type vertex
  type t [@@deriving sexp, compare, hash, equal]

  val empty : t
  val extend : vertex -> t -> t
  val pp : Format.formatter -> t -> unit
end

module Make_kcfa
    (V : Sig.VERTEX) (K : sig
      val k : int
    end) : S with type vertex := V.t

module Make_acyclic_cfa (V : Sig.VERTEX) : S with type vertex := V.t
