module type G = Sig.G

include Impl
module Traverse = Traverse
module Context = Context
module Intra_fixpoint = Intra_fixpoint
module Inter_fixpoint = Inter_fixpoint
module Intra_vcgen = Intra_vcgen
module Inter_vcgen = Inter_vcgen
