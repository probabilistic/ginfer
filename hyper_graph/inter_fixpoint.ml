open Core

module type DOMAIN = sig
  type vertex
  type edge
  type context
  type t [@@deriving equal]

  val interpret : context -> edge -> (t -> t) list
  val is_call : edge -> (vertex * vertex) option
  val join : context -> vertex -> t -> t -> t
  val widen : context -> vertex -> t -> t -> t
  val call : caller:context * edge -> callee:context * vertex * vertex -> t -> t
  val return : caller:context * edge -> callee:context * vertex * vertex -> t -> t
  val merge : context -> vertex -> t -> t -> t
  val pp : Format.formatter -> t -> unit
end

module Make
    (G : Sig.G)
    (C : Context.S with type vertex := G.vertex)
    (D : DOMAIN
           with type vertex := G.vertex
            and type edge := G.edge
            and type context := C.t) =
struct
  module Config = struct
    type t = C.t * G.vertex [@@deriving sexp, compare, hash, equal]
  end

  module Directed_graph = Graph.Imperative.Digraph.Concrete (Config)
  module Directed_graph_wto = Graph.WeakTopological.Make (Directed_graph)
  module VT = Hashtbl.Make_plain (G.V)
  module CT = Hashtbl.Make_plain (Config)

  let build_dg g ~entry =
    let dg = Directed_graph.create ~size:(G.nb_vertex g) () in
    let rec expand wl =
      match Queue.dequeue wl with
      | None -> ()
      | Some (ctx, u) ->
        if not (Directed_graph.mem_vertex dg (ctx, u))
        then (
          Directed_graph.add_vertex dg (ctx, u);
          G.iter_succ_edges_e g ~on:u ~f:(fun e ->
              let vs = G.E.dsts e in
              List.iter vs ~f:(fun v -> Queue.enqueue wl (ctx, v));
              match D.is_call e with
              | None -> ()
              | Some (callee_entry, _) ->
                let ctx' = C.extend u ctx in
                Queue.enqueue wl (ctx', callee_entry)));
        expand wl
    in
    let () =
      let wl = Queue.create () in
      Queue.enqueue wl (C.empty, entry);
      expand wl
    in
    let add_edge u v = Directed_graph.add_edge dg u v in
    Directed_graph.iter_vertex
      (fun (ctx, u) ->
        G.iter_succ_edges_e g ~on:u ~f:(fun e ->
            let vs = G.E.dsts e in
            List.iter vs ~f:(fun v -> add_edge (ctx, u) (ctx, v));
            match D.is_call e with
            | None -> ()
            | Some (callee_entry, callee_exit) ->
              assert (List.length vs = 1);
              let ctx' = C.extend u ctx in
              add_edge (ctx, u) (ctx', callee_entry);
              add_edge (ctx', callee_exit) (ctx, List.hd_exn vs)))
      dg;
    dg
  ;;

  let build_trans g dg =
    let trans = CT.create ~size:(Directed_graph.nb_edges dg) () in
    let add_direct cu cv f =
      CT.update trans cv ~f:(function
          | None -> [ `Direct (cu, f) ]
          | Some old -> `Direct (cu, f) :: old)
    in
    let add cu cv f = add_direct cu cv f in
    Directed_graph.iter_vertex
      (fun (ctx, u) ->
        G.iter_succ_edges_e g ~on:u ~f:(fun e ->
            match D.is_call e with
            | None ->
              let vs = G.E.dsts e in
              List.iter2_exn vs (D.interpret ctx e) ~f:(fun v f ->
                  add (ctx, u) (ctx, v) f)
            | Some (callee_entry, callee_exit) ->
              let v = List.hd_exn (G.E.dsts e) in
              let f = List.hd_exn (D.interpret ctx e) in
              let ctx' = C.extend u ctx in
              add_direct (ctx, u) (ctx', callee_entry) (fun abs ->
                  D.call ~caller:(ctx, e) ~callee:(ctx', callee_entry, callee_exit) abs);
              let merge_tran =
                `Merge
                  ( (ctx, u)
                  , f
                  , (ctx', callee_exit)
                  , fun abs ->
                      D.return
                        ~caller:(ctx, e)
                        ~callee:(ctx', callee_entry, callee_exit)
                        abs )
              in
              CT.update trans (ctx, v) ~f:(function
                  | None -> [ merge_tran ]
                  | Some old -> merge_tran :: old)))
      dg;
    trans
  ;;

  let analyze g ~entry ~init ?(widen_threshold = 1) ?(narrow = false) () =
    let dg = build_dg g ~entry in
    let trans = build_trans g dg in
    let sol = CT.create ~size:(Directed_graph.nb_vertex dg) () in
    Directed_graph.iter_vertex (fun cu -> CT.add_exn sol ~key:cu ~data:(init cu)) dg;
    let analyze_vertex ~widen (ctx, u) =
      let preds = CT.find trans (ctx, u) |> Option.value ~default:[] in
      let new_abs =
        List.fold_right
          preds
          ~f:(fun tran acc ->
            match tran with
            | `Direct (cv, tran_cv_to_cu) ->
              let abs_cv = CT.find_exn sol cv in
              D.join ctx u acc (tran_cv_to_cu abs_cv)
            | `Merge (cv1, tran1, cv2, tran2) ->
              let abs_cv1 = CT.find_exn sol cv1 in
              let abs_cv2 = CT.find_exn sol cv2 in
              D.join ctx u acc (D.merge ctx u (tran1 abs_cv1) (tran2 abs_cv2)))
          ~init:(init (ctx, u))
      in
      if widen
      then (
        let old_abs = CT.find_exn sol (ctx, u) in
        CT.set
          sol
          ~key:(ctx, u)
          ~data:(D.widen ctx u old_abs (D.join ctx u old_abs new_abs)))
      else CT.set sol ~key:(ctx, u) ~data:new_abs
    in
    let wto = Directed_graph_wto.recursive_scc dg (C.empty, entry) in
    let rec f () elem =
      match elem with
      | Graph.WeakTopological.Vertex cu -> analyze_vertex ~widen:false cu
      | Graph.WeakTopological.Component (cu, rest) ->
        let rec iter n =
          Graph.WeakTopological.fold_left f () rest;
          let old_abs = CT.find_exn sol cu in
          analyze_vertex ~widen:(n >= widen_threshold) cu;
          let new_abs = CT.find_exn sol cu in
          if D.equal old_abs new_abs then () else iter (n + 1)
        in
        analyze_vertex ~widen:false cu;
        iter 0
    in
    Graph.WeakTopological.fold_left f () wto;
    if narrow
    then (
      let rec f () elem =
        match elem with
        | Graph.WeakTopological.Vertex cu -> analyze_vertex ~widen:false cu
        | Graph.WeakTopological.Component (cu, rest) ->
          analyze_vertex ~widen:false cu;
          Graph.WeakTopological.fold_left f () rest
      in
      Graph.WeakTopological.fold_left f () wto);
    let cache =
      CT.to_alist sol
      |> List.map ~f:(fun ((ctx, u), abs) -> u, (ctx, abs))
      |> VT.of_alist_multi
    in
    fun u -> VT.find cache u |> Option.value ~default:[]
  ;;
end
