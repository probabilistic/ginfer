module type DOMAIN = sig
  type vertex
  type edge
  type context
  type t [@@deriving equal]

  val interpret : context -> edge -> (t -> t) list
  val is_call : edge -> (vertex * vertex) option
  val join : context -> vertex -> t -> t -> t
  val widen : context -> vertex -> t -> t -> t
  val call : caller:context * edge -> callee:context * vertex * vertex -> t -> t
  val return : caller:context * edge -> callee:context * vertex * vertex -> t -> t
  val merge : context -> vertex -> t -> t -> t
  val pp : Format.formatter -> t -> unit
end

module Make
    (G : Sig.G)
    (C : Context.S with type vertex := G.vertex)
    (D : DOMAIN
           with type vertex := G.vertex
            and type edge := G.edge
            and type context := C.t) : sig
  val analyze
    :  G.t
    -> entry:G.vertex
    -> init:(C.t * G.vertex -> D.t)
    -> ?widen_threshold:int
    -> ?narrow:bool
    -> unit
    -> G.vertex
    -> (C.t * D.t) list
end
