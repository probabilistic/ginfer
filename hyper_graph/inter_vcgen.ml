open Core
open Result.Let_syntax

module type DOMAIN = sig
  type vertex
  type edge
  type manager
  type annotation
  type solution
  type level
  type context

  val alloc_manager : unit -> manager
  val new_annotation : vertex -> manager -> level -> annotation
  val query_annotation : vertex -> manager -> annotation
  val interpret : context -> edge -> manager -> level -> annotation list -> annotation
  val identify_annotations : vertex -> manager -> annotation -> annotation -> unit
  val optimize : vertex -> manager -> goal:annotation -> (unit, exn) Result.t
  val obtain_solution : vertex -> manager -> annotation -> solution
  val is_trivial_level : level -> bool
  val top_level : level
  val is_call : edge -> (vertex * vertex) option

  val call
    :  caller:edge
    -> callee:vertex * vertex
    -> manager
    -> annotation
    -> level
    -> annotation * (level * annotation) list

  val return
    :  caller:edge
    -> callee:vertex * vertex
    -> manager
    -> annotation
    -> (level * annotation) list
    -> annotation
end

module Make
    (G : Sig.G)
    (C : Context.S with type vertex := G.vertex)
    (D : DOMAIN
           with type vertex := G.vertex
            and type edge := G.edge
            and type context := C.t) =
struct
  module Config = struct
    type t = C.t * G.vertex [@@deriving sexp, compare, hash, equal]
  end

  module Prefix = struct
    type t = C.t * (G.vertex * G.vertex) [@@deriving sexp, compare]
  end

  module VT = Hashtbl.Make_plain (G.V)
  module CT = Hashtbl.Make_plain (Config)
  module PM = Map.Make_plain (Prefix)

  let build_annot manager g ~entry ~exit =
    let rec do_fun is_global pm ctx ~fun_entry ~fun_exit level to_annot =
      if D.is_trivial_level level
      then to_annot
      else (
        match PM.find pm (ctx, (fun_entry, fun_exit)) with
        | Some (mk_from_annot, rec_to_annot) ->
          D.identify_annotations fun_exit manager rec_to_annot to_annot;
          mk_from_annot ()
        | None ->
          let rec_annot = ref None in
          let mk_rec_annot () =
            match !rec_annot with
            | Some annot -> annot
            | None ->
              let annot = D.new_annotation fun_entry manager level in
              rec_annot := Some annot;
              annot
          in
          let pm =
            PM.add_exn pm ~key:(ctx, (fun_entry, fun_exit)) ~data:(mk_rec_annot, to_annot)
          in
          let pannots = VT.create () in
          let rec dfs u =
            let pannot = VT.find_or_add pannots u ~default:(fun () -> `Todo) in
            match pannot with
            | `Done annot -> annot
            | `Doing ->
              let annot = D.new_annotation u manager level in
              VT.set pannots ~key:u ~data:(`Done annot);
              annot
            | `Todo ->
              VT.set pannots ~key:u ~data:`Doing;
              let annot =
                G.fold_succ_edges_e g ~on:u ~init:None ~f:(fun e acc ->
                    let dsts = G.E.dsts e in
                    let dst_annots = List.map dsts ~f:dfs in
                    let src_annot =
                      match D.is_call e with
                      | None -> D.interpret ctx e manager level dst_annots
                      | Some (callee_entry, callee_exit) ->
                        let ctx' = C.extend u ctx in
                        let dst_annot = List.hd_exn dst_annots in
                        let call_annot, trans_annots =
                          D.call
                            ~caller:e
                            ~callee:(callee_entry, callee_exit)
                            manager
                            dst_annot
                            level
                        in
                        let called_annot =
                          do_fun
                            is_global
                            pm
                            ctx'
                            ~fun_entry:callee_entry
                            ~fun_exit:callee_exit
                            level
                            call_annot
                        in
                        let transed_annots =
                          List.map trans_annots ~f:(fun (trans_level, trans_annot) ->
                              ( trans_level
                              , do_fun
                                  false
                                  PM.empty
                                  ctx'
                                  ~fun_entry:callee_entry
                                  ~fun_exit:callee_exit
                                  trans_level
                                  trans_annot ))
                        in
                        let src_annot =
                          D.return
                            ~caller:e
                            ~callee:(callee_entry, callee_exit)
                            manager
                            called_annot
                            transed_annots
                        in
                        src_annot
                    in
                    match acc with
                    | None -> Some src_annot
                    | Some old_src_annot ->
                      D.identify_annotations u manager old_src_annot src_annot;
                      Some old_src_annot)
              in
              let annot =
                match annot with
                | None ->
                  assert (G.equal_vertex u fun_exit);
                  to_annot
                | Some annot -> annot
              in
              let annot =
                match VT.find_exn pannots u with
                | `Done old_annot ->
                  D.identify_annotations u manager old_annot annot;
                  old_annot
                | `Doing -> annot
                | `Todo -> assert false
              in
              VT.set pannots ~key:u ~data:(`Done annot);
              annot
          in
          let from_annot = dfs fun_entry in
          (match !rec_annot with
          | Some annot -> D.identify_annotations fun_entry manager from_annot annot
          | None -> ());
          from_annot)
    in
    let to_annot = D.query_annotation exit manager in
    let from_annot =
      do_fun true PM.empty C.empty ~fun_entry:entry ~fun_exit:exit D.top_level to_annot
    in
    from_annot
  ;;

  let analyze g ~entry ~exit () =
    let manager = D.alloc_manager () in
    let from_annot =
      Timer.wrap_duration "vc-generation" (fun () -> build_annot manager g ~entry ~exit)
    in
    let%bind () =
      Timer.wrap_duration "vc-solving" (fun () ->
          D.optimize entry manager ~goal:from_annot)
    in
    Ok (D.obtain_solution entry manager from_annot)
  ;;
end
