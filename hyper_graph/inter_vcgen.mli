module type DOMAIN = sig
  type vertex
  type edge
  type manager
  type annotation
  type solution
  type level
  type context

  val alloc_manager : unit -> manager
  val new_annotation : vertex -> manager -> level -> annotation
  val query_annotation : vertex -> manager -> annotation
  val interpret : context -> edge -> manager -> level -> annotation list -> annotation
  val identify_annotations : vertex -> manager -> annotation -> annotation -> unit
  val optimize : vertex -> manager -> goal:annotation -> (unit, exn) result
  val obtain_solution : vertex -> manager -> annotation -> solution
  val is_trivial_level : level -> bool
  val top_level : level
  val is_call : edge -> (vertex * vertex) option

  val call
    :  caller:edge
    -> callee:vertex * vertex
    -> manager
    -> annotation
    -> level
    -> annotation * (level * annotation) list

  val return
    :  caller:edge
    -> callee:vertex * vertex
    -> manager
    -> annotation
    -> (level * annotation) list
    -> annotation
end

module Make
    (G : Sig.G)
    (C : Context.S with type vertex := G.vertex)
    (D : DOMAIN
           with type vertex := G.vertex
            and type edge := G.edge
            and type context := C.t) : sig
  val analyze
    :  G.t
    -> entry:G.vertex
    -> exit:G.vertex
    -> unit
    -> (D.solution, exn) Result.t
end
