open Core

module type DOMAIN = sig
  type vertex
  type edge
  type t [@@deriving equal]

  val mode : [ `Fwd | `Bwd ]
  val interpret : edge -> (t -> t) list
  val join : vertex -> t -> t -> t
  val widen : vertex -> t -> t -> t
  val pp : Format.formatter -> t -> unit
end

module Make (G : Sig.G) (D : DOMAIN with type vertex := G.vertex and type edge := G.edge) =
struct
  module Directed_graph = Graph.Imperative.Digraph.Concrete (G.V)
  module Directed_graph_wto = Graph.WeakTopological.Make (Directed_graph)
  module VT = Hashtbl.Make_plain (G.V)

  let build_dg g =
    let dg = Directed_graph.create ~size:(G.nb_vertex g) () in
    let add =
      match D.mode with
      | `Fwd -> fun u v -> Directed_graph.add_edge dg u v
      | `Bwd -> fun u v -> Directed_graph.add_edge dg v u
    in
    G.iter_edges g ~f:(fun u vs -> List.iter vs ~f:(fun v -> add u v));
    dg
  ;;

  let build_trans g dg =
    let trans = VT.create ~size:(Directed_graph.nb_vertex dg) () in
    let add =
      match D.mode with
      | `Fwd ->
        fun u v f ->
          VT.update trans v ~f:(function
              | None -> [ u, f ]
              | Some old -> (u, f) :: old)
      | `Bwd ->
        fun u v f ->
          VT.update trans u ~f:(function
              | None -> [ v, f ]
              | Some old -> (v, f) :: old)
    in
    G.iter_edges_e g ~f:(fun e ->
        let u = G.E.src e in
        let vs = G.E.dsts e in
        let fs = D.interpret e in
        List.iter2_exn vs fs ~f:(fun v f -> add u v f));
    trans
  ;;

  let analyze g ~entry ~init ?(widen_threshold = 1) ?(narrow = false) () =
    let dg = build_dg g in
    let trans = build_trans g dg in
    let sol = VT.create ~size:(G.nb_vertex g) () in
    G.iter_vertex g ~f:(fun u -> VT.add_exn sol ~key:u ~data:(init u));
    let analyze_vertex ~widen u =
      let preds = VT.find trans u |> Option.value ~default:[] in
      let new_abs =
        List.fold_right
          preds
          ~f:(fun (v, tran_v_to_u) acc ->
            let abs_v = VT.find_exn sol v in
            D.join u acc (tran_v_to_u abs_v))
          ~init:(init u)
      in
      if widen
      then (
        let old_abs = VT.find_exn sol u in
        VT.set sol ~key:u ~data:(D.widen u old_abs (D.join u old_abs new_abs)))
      else VT.set sol ~key:u ~data:new_abs
    in
    let wto = Directed_graph_wto.recursive_scc dg entry in
    let rec f () elem =
      match elem with
      | Graph.WeakTopological.Vertex u -> analyze_vertex ~widen:false u
      | Graph.WeakTopological.Component (u, cs) ->
        let rec iter n =
          Graph.WeakTopological.fold_left f () cs;
          let old_abs = VT.find_exn sol u in
          analyze_vertex ~widen:(n >= widen_threshold) u;
          let new_abs = VT.find_exn sol u in
          if D.equal old_abs new_abs then () else iter (n + 1)
        in
        analyze_vertex ~widen:false u;
        iter 0
    in
    Graph.WeakTopological.fold_left f () wto;
    if narrow
    then (
      let rec f () elem =
        match elem with
        | Graph.WeakTopological.Vertex u -> analyze_vertex ~widen:false u
        | Graph.WeakTopological.Component (u, cs) ->
          analyze_vertex ~widen:false u;
          Graph.WeakTopological.fold_left f () cs
      in
      Graph.WeakTopological.fold_left f () wto);
    fun u -> VT.find_exn sol u
  ;;
end
