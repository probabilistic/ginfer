module type DOMAIN = sig
  type vertex
  type edge
  type t [@@deriving equal]

  val mode : [ `Fwd | `Bwd ]
  val interpret : edge -> (t -> t) list
  val join : vertex -> t -> t -> t
  val widen : vertex -> t -> t -> t
  val pp : Format.formatter -> t -> unit
end

module Make (G : Sig.G) (D : DOMAIN with type vertex := G.vertex and type edge := G.edge) : sig
  val analyze
    :  G.t
    -> entry:G.vertex
    -> init:(G.vertex -> D.t)
    -> ?widen_threshold:int
    -> ?narrow:bool
    -> unit
    -> G.vertex
    -> D.t
end
