open Core
open Result.Let_syntax

module type DOMAIN = sig
  type vertex
  type edge
  type manager
  type annotation
  type solution

  val alloc_manager : unit -> manager
  val new_annotation : vertex -> manager -> annotation
  val query_annotation : vertex -> manager -> annotation
  val interpret : edge -> manager -> annotation list -> annotation
  val identify_annotations : vertex -> manager -> annotation -> annotation -> unit
  val optimize : vertex -> manager -> goal:annotation -> (unit, exn) Result.t
  val obtain_solution : vertex -> manager -> annotation -> solution
end

module Make (G : Sig.G) (D : DOMAIN with type vertex := G.vertex and type edge := G.edge) =
struct
  module VT = Hashtbl.Make_plain (G.V)

  let build_annot manager g ~entry ~exit =
    let to_annot = D.query_annotation exit manager in
    let pannots = VT.create ~size:(G.nb_vertex g) () in
    let rec dfs u =
      let pannot = VT.find_or_add pannots u ~default:(fun () -> `Todo) in
      match pannot with
      | `Done annot -> annot
      | `Doing ->
        let annot = D.new_annotation u manager in
        VT.set pannots ~key:u ~data:(`Done annot);
        annot
      | `Todo ->
        VT.set pannots ~key:u ~data:`Doing;
        let annot =
          G.fold_succ_edges_e g ~on:u ~init:None ~f:(fun e acc ->
              let dsts = G.E.dsts e in
              let dst_annots = List.map dsts ~f:dfs in
              let src_annot = D.interpret e manager dst_annots in
              match acc with
              | None -> Some src_annot
              | Some old_src_annot ->
                D.identify_annotations u manager old_src_annot src_annot;
                Some old_src_annot)
        in
        let annot =
          match annot with
          | None ->
            assert (G.equal_vertex u exit);
            to_annot
          | Some annot -> annot
        in
        let annot =
          match VT.find_exn pannots u with
          | `Done old_annot ->
            D.identify_annotations u manager old_annot annot;
            old_annot
          | `Doing -> annot
          | `Todo -> assert false
        in
        VT.set pannots ~key:u ~data:(`Done annot);
        annot
    in
    let from_annot = dfs entry in
    from_annot
  ;;

  let analyze g ~entry ~exit () =
    let manager = D.alloc_manager () in
    let from_annot = build_annot manager g ~entry ~exit in
    let%bind () = D.optimize entry manager ~goal:from_annot in
    Ok (D.obtain_solution entry manager from_annot)
  ;;
end
