module type DOMAIN = sig
  type vertex
  type edge
  type manager
  type annotation
  type solution

  val alloc_manager : unit -> manager
  val new_annotation : vertex -> manager -> annotation
  val query_annotation : vertex -> manager -> annotation
  val interpret : edge -> manager -> annotation list -> annotation
  val identify_annotations : vertex -> manager -> annotation -> annotation -> unit
  val optimize : vertex -> manager -> goal:annotation -> (unit, exn) result
  val obtain_solution : vertex -> manager -> annotation -> solution
end

module Make : functor
  (G : Sig.G)
  (D : DOMAIN with type vertex := G.vertex and type edge := G.edge)
  -> sig
  val analyze : G.t -> entry:G.vertex -> exit:G.vertex -> unit -> (D.solution, exn) result
end
