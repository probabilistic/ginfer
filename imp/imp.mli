open Action_types
open Action_ops

type random_expr =
  { rexp_desc : random_expr_desc
  ; rexp_loc : Location.t
  }

and random_expr_desc =
  | REVar of string loc * Typed.rhs_ty
  | REBool of bool
  | REInt of int
  | REReal of float
  | REFrac of Mpqf.t
  | REProb of int * int
  | REBinOp of bin_op * random_expr * random_expr
  | RERelOp of rel_op * random_expr * random_expr
  | REAnd of random_expr * random_expr
  | REOr of random_expr * random_expr
  | RENot of random_expr
  | RENegate of random_expr
  | RENondet
  | REIntDist of Dist.int_dist
  | RERealDist of Dist.real_dist

type stmt =
  { stm_desc : stmt_desc
  ; stm_loc : Location.t
  }

and stmt_desc =
  | SSkip
  | SWeaken of Typed.arith_expr list
  | SAssign of string loc * random_expr
  | STick of Typed.arith_expr
  | SAssume of Typed.bool_expr
  | SCond of Typed.bool_expr * stmt * stmt option
  | SProb of Typed.prob_expr * stmt * stmt option
  | SCall of string loc * string loc list * string loc option
  | SReturn of string loc option
  | SBreak
  | SContinue
  | SBlock of stmt list
  | SWhile of Typed.bool_expr * stmt
  | SRepeat of stmt * Typed.bool_expr

type func =
  { fun_name : string loc
  ; fun_return_ty : Typed.rhs_ty option
  ; fun_params : string loc list
  ; fun_locals : string loc list
  ; fun_scope : Typed.rhs_ty tycontext
  ; fun_body : stmt
  }

type program =
  { prg_funcs : func list
  ; prg_loc : Location.t
  }
