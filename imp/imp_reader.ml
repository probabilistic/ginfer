open Core
open Result.Let_syntax
open Action_types
open Action_ops

let transform_random_expr gen x e =
  let imp_binds = ref [] in
  let real_binds = ref [] in
  let rec inner e =
    let loc = e.Imp.rexp_loc in
    let trans_e =
      match e.rexp_desc with
      | Imp.REVar (x, ty) -> Ok (Typed.mk_var ~loc x ty)
      | Imp.REBool b -> Ok (Typed.mk_bool ~loc b :> Typed.rhs_expr)
      | Imp.REInt n -> Ok (Typed.mk_int ~loc n :> Typed.rhs_expr)
      | Imp.REReal r -> Ok (Typed.mk_real ~loc r :> Typed.rhs_expr)
      | Imp.REFrac f -> Ok (Typed.mk_frac ~loc f :> Typed.rhs_expr)
      | Imp.REProb (a, b) -> Ok (Typed.mk_prob ~loc (a, b) :> Typed.rhs_expr)
      | Imp.REBinOp (bop, e1, e2) ->
        let%bind e1' = inner e1 in
        let%bind e2' = inner e2 in
        (match Typed.refine e1', Typed.refine e2' with
        | `ArithExpr ae1', `ArithExpr ae2' ->
          Ok (Typed.mk_bin_op ~loc (bop, ae1', ae2') :> Typed.rhs_expr)
        | _ -> Error (Static_error ("incorrect operand types", loc)))
      | Imp.RERelOp (rop, e1, e2) ->
        let%bind e1' = inner e1 in
        let%bind e2' = inner e2 in
        (match Typed.refine e1', Typed.refine e2' with
        | `ArithExpr ae1', `ArithExpr ae2' ->
          Ok (Typed.mk_rel_op ~loc (rop, ae1', ae2') :> Typed.rhs_expr)
        | _ -> Error (Static_error ("incorrect operand types", loc)))
      | Imp.REAnd (e1, e2) ->
        let%bind e1' = inner e1 in
        let%bind e2' = inner e2 in
        (match Typed.refine e1', Typed.refine e2' with
        | `BoolExpr be1', `BoolExpr be2' ->
          Ok (Typed.mk_and ~loc (be1', be2') :> Typed.rhs_expr)
        | _ -> Error (Static_error ("non-Boolean predicate", loc)))
      | Imp.REOr (e1, e2) ->
        let%bind e1' = inner e1 in
        let%bind e2' = inner e2 in
        (match Typed.refine e1', Typed.refine e2' with
        | `BoolExpr be1', `BoolExpr be2' ->
          Ok (Typed.mk_or ~loc (be1', be2') :> Typed.rhs_expr)
        | _ -> Error (Static_error ("non-Boolean predicate", loc)))
      | Imp.RENot e0 ->
        let%bind e0' = inner e0 in
        (match Typed.refine e0' with
        | `BoolExpr be0' -> Ok (Typed.mk_not ~loc be0' :> Typed.rhs_expr)
        | _ -> Error (Static_error ("non-Boolean operand", loc)))
      | Imp.RENegate e0 ->
        let%bind e0' = inner e0 in
        (match Typed.refine e0' with
        | `ArithExpr ae0' -> Ok (Typed.mk_negate ~loc ae0' :> Typed.rhs_expr)
        | _ -> Error (Static_error ("non-numeric operand", loc)))
      | Imp.RENondet -> Ok (Typed.mk_nondet ~loc :> Typed.rhs_expr)
      | Imp.REIntDist d ->
        let x = gen ~loc `TyInt in
        imp_binds := (x, d) :: !imp_binds;
        Ok (Typed.mk_var ~loc x `TyInt)
      | Imp.RERealDist d ->
        let x = gen ~loc `TyReal in
        real_binds := (x, d) :: !real_binds;
        Ok (Typed.mk_var ~loc x `TyReal)
    in
    trans_e
  in
  let%bind res =
    match e.Imp.rexp_desc with
    | Imp.REIntDist d -> Ok (Typed.DSample (x, `Int d))
    | Imp.RERealDist d -> Ok (Typed.DSample (x, `Real d))
    | _ ->
      let%bind e' = inner e in
      Ok (Typed.DAssign (x, e'))
  in
  Ok (res, !imp_binds, !real_binds)
;;

let build_func_exn ~loop_unroll vcnt imp_func =
  let fun_name = imp_func.Imp.fun_name in
  let fun_params = imp_func.Imp.fun_params in
  let fun_scope = imp_func.Imp.fun_scope in
  let num_ints = ref 0 in
  let num_reals = ref 0 in
  let cnt = ref vcnt in
  let new_vertex () =
    let v = Cfg.create_vertex !cnt in
    incr cnt;
    v
  in
  let edges = ref [] in
  let returns =
    Option.value_map imp_func.fun_return_ty ~default:[] ~f:(fun ty ->
        [ Location.mknoloc "R_0", ty ])
  in
  let add_edge ~loc u act vs = edges := Cfg.create_edge u act vs loc :: !edges in
  let add_weaken_edge ~loc u act vs =
    let vs' =
      List.map vs ~f:(fun v ->
          let v' = new_vertex () in
          add_edge ~loc:Location.none v' (Cfg.CSeq (DWeaken [])) [ v ];
          v')
    in
    add_edge ~loc u act vs'
  in
  let gen_cnt_ints = ref 0 in
  let gen_cnt_reals = ref 0 in
  let gen ~loc ty =
    let prefix = string_of_type (ty :> Typed.rhs_ty) in
    let cnt =
      match ty with
      | `TyInt -> gen_cnt_ints
      | `TyReal -> gen_cnt_reals
    in
    let res = Location.mkloc ("G_" ^ prefix ^ "_" ^ Int.to_string !cnt) loc in
    incr cnt;
    res
  in
  let fun_exit = new_vertex () in
  let rec build_block insts v_exit opt_loop cont_loop =
    let beg = new_vertex () in
    add_edge ~loc:Location.none beg (Cfg.CSeq DSkip) [ v_exit ];
    List.fold_right insts ~init:beg ~f:(fun inst beg ->
        build_inst inst beg opt_loop cont_loop)
  and build_inst_opt inst_opt v_exit opt_loop cont_loop =
    match inst_opt with
    | None ->
      let beg = new_vertex () in
      add_edge ~loc:Location.none beg (Cfg.CSeq DSkip) [ v_exit ];
      beg
    | Some inst -> build_inst inst v_exit opt_loop cont_loop
  and build_inst inst v_exit opt_loop cont_loop =
    let loc = inst.Imp.stm_loc in
    match inst.Imp.stm_desc with
    | Imp.SBlock ss -> build_block ss v_exit opt_loop cont_loop
    | Imp.SBreak ->
      let beg = new_vertex () in
      add_edge
        ~loc
        beg
        (Cfg.CSeq DSkip)
        [ Option.value_exn
            ~error:(Error.of_exn (Static_error ("not inside a loop", loc)))
            opt_loop
        ];
      beg
    | Imp.SContinue ->
      let beg = new_vertex () in
      add_edge
        ~loc
        beg
        (Cfg.CSeq DSkip)
        [ Option.value_exn
            ~error:(Error.of_exn (Static_error ("not inside a loop", loc)))
            cont_loop
        ];
      beg
    | Imp.SSkip ->
      let beg = new_vertex () in
      add_edge ~loc beg (Cfg.CSeq DSkip) [ v_exit ];
      beg
    | Imp.SWeaken es ->
      let beg = new_vertex () in
      add_edge ~loc beg (Cfg.CSeq (DWeaken es)) [ v_exit ];
      beg
    | Imp.SAssume phi ->
      let beg = new_vertex () in
      add_weaken_edge ~loc beg (Cfg.CSeq (DAssume phi)) [ v_exit ];
      beg
    | Imp.STick e ->
      let beg = new_vertex () in
      let sgn =
        match Typed.destruct_arith e with
        | `Int n -> Some (n >= 0)
        | `Real r -> Some Float.(r >= 0.)
        | `Frac f -> Some (Mpqf.(cmp f (of_int 0)) >= 0)
        | _ -> None
      in
      add_edge ~loc beg (Cfg.CSeq (DScore (e, ref sgn))) [ v_exit ];
      beg
    | Imp.SProb (pe, sl, sr) ->
      let beg = new_vertex () in
      let beg_bl = build_inst sl v_exit opt_loop cont_loop in
      let beg_br = build_inst_opt sr v_exit opt_loop cont_loop in
      add_edge
        ~loc:(Typed.expr_loc (pe :> Typed.rhs_expr))
        beg
        (Cfg.CProb pe)
        [ beg_bl; beg_br ];
      beg
    | Imp.SCond (phi, si, se) ->
      let beg = new_vertex () in
      let beg_bi = build_inst si v_exit opt_loop cont_loop in
      let beg_be = build_inst_opt se v_exit opt_loop cont_loop in
      add_weaken_edge
        ~loc:(Typed.expr_loc (phi :> Typed.rhs_expr))
        beg
        (Cfg.CCond phi)
        [ beg_bi; beg_be ];
      beg
    | Imp.SWhile (phi, b) ->
      if not loop_unroll
      then (
        let jmp = new_vertex () in
        let begb = build_inst b jmp (Some v_exit) (Some jmp) in
        add_weaken_edge
          ~loc:(Typed.expr_loc (phi :> Typed.rhs_expr))
          jmp
          (Cfg.CCond phi)
          [ begb; v_exit ];
        jmp)
      else (
        let jmp = new_vertex () in
        let begb = build_inst b jmp (Some v_exit) (Some jmp) in
        add_weaken_edge
          ~loc:(Typed.expr_loc (phi :> Typed.rhs_expr))
          jmp
          (Cfg.CCond phi)
          [ begb; v_exit ];
        let beg_bi = build_inst b jmp (Some v_exit) (Some jmp) in
        let jmp' = new_vertex () in
        add_weaken_edge
          ~loc:(Typed.expr_loc (phi :> Typed.rhs_expr))
          jmp'
          (Cfg.CCond phi)
          [ beg_bi; v_exit ];
        jmp')
    | Imp.SRepeat (b, phi) ->
      if not loop_unroll
      then (
        let jmp = new_vertex () in
        let begb = build_inst b jmp (Some v_exit) (Some jmp) in
        add_weaken_edge
          ~loc:(Typed.expr_loc (phi :> Typed.rhs_expr))
          jmp
          (Cfg.CCond phi)
          [ begb; v_exit ];
        begb)
      else (
        let jmp = new_vertex () in
        let begb = build_inst b jmp (Some v_exit) (Some jmp) in
        add_weaken_edge
          ~loc:(Typed.expr_loc (phi :> Typed.rhs_expr))
          jmp
          (Cfg.CCond phi)
          [ begb; v_exit ];
        let jmp' = new_vertex () in
        let beg_bi = build_inst b jmp' (Some v_exit) (Some jmp') in
        add_weaken_edge
          ~loc:(Typed.expr_loc (phi :> Typed.rhs_expr))
          jmp'
          (Cfg.CCond phi)
          [ begb; v_exit ];
        beg_bi)
    | Imp.SAssign (x, re) ->
      gen_cnt_ints := 0;
      gen_cnt_reals := 0;
      let act', imp_binds, real_binds = Result.ok_exn @@ transform_random_expr gen x re in
      num_ints := max !num_ints !gen_cnt_ints;
      num_reals := max !num_reals !gen_cnt_reals;
      let beg = new_vertex () in
      add_edge ~loc beg (Cfg.CSeq act') [ v_exit ];
      let imp =
        List.fold imp_binds ~init:beg ~f:(fun acc (x, d) ->
            let beg = new_vertex () in
            add_edge ~loc:Location.none beg (Cfg.CSeq (DSample (x, `Int d))) [ acc ];
            beg)
      in
      List.fold real_binds ~init:imp ~f:(fun acc (x, d) ->
          let beg = new_vertex () in
          add_edge ~loc:Location.none beg (Cfg.CSeq (DSample (x, `Real d))) [ acc ];
          beg)
    | Imp.SReturn x_opt ->
      let xs = Option.to_list x_opt in
      let ys = List.mapi xs ~f:(fun i _ -> "R_" ^ Int.to_string i |> Location.mknoloc) in
      let b_exit = new_vertex () in
      add_weaken_edge ~loc b_exit (Cfg.CSeq DSkip) [ fun_exit ];
      List.fold_right (List.zip_exn xs ys) ~init:b_exit ~f:(fun (x, y) v_exit ->
          let beg = new_vertex () in
          add_edge
            ~loc:Location.none
            beg
            (Cfg.CSeq
               (DAssign (y, Typed.mk_var ~loc:Location.none x (tyctx_get_exn fun_scope x))))
            [ v_exit ];
          beg)
    | Imp.SCall (name, args, x_opt) ->
      let beg = new_vertex () in
      add_edge ~loc beg (Cfg.CCall (name, args, Option.to_list x_opt)) [ v_exit ];
      beg
  in
  let imp_body = imp_func.fun_body in
  let fun_init =
    let fake_init = new_vertex () in
    let true_init = build_inst imp_body fun_exit None None in
    add_edge ~loc:Location.none fake_init (Cfg.CSeq DSkip) [ true_init ];
    fake_init
  in
  let locals =
    List.append
      (List.init !num_ints ~f:(fun i ->
           Location.mknoloc ("G_" ^ string_of_type `TyInt ^ "_" ^ Int.to_string i), `TyInt))
      (List.init !num_reals ~f:(fun i ->
           ( Location.mknoloc ("G_" ^ string_of_type `TyReal ^ "_" ^ Int.to_string i)
           , `TyReal )))
  in
  let fun_locals = List.append imp_func.fun_locals (List.map locals ~f:fst) in
  let fun_returns = List.map returns ~f:fst in
  let fun_scope =
    List.fold (List.append locals returns) ~init:fun_scope ~f:(fun acc (x, ty) ->
        tyctx_add_exn x (ty :> Typed.rhs_ty) acc)
  in
  let fun_cfg = Cfg.of_edges !edges in
  let func =
    { Cfg.fun_name
    ; fun_params
    ; fun_locals
    ; fun_returns
    ; fun_scope
    ; fun_init
    ; fun_exit
    ; fun_cfg
    }
  in
  (* Format.printf "%a@." Cfg.print_func func; *)
  func, !cnt
;;

let transform ~loop_unroll imp_prog =
  Result.try_with (fun () ->
      let funcs, _ =
        List.fold_right
          imp_prog.Imp.prg_funcs
          ~init:([], 0)
          ~f:(fun imp_func (acc, vcnt) ->
            let func, vcnt' = build_func_exn ~loop_unroll vcnt imp_func in
            func :: acc, vcnt')
      in
      { Cfg.prg_funcs = funcs; prg_loc = imp_prog.Imp.prg_loc })
;;
