open Core
open Apron
open Syntax

module Lincons2 = struct
  let array_of_list env lcs =
    let lcarr = Lincons1.array_make env (List.length lcs) in
    List.iteri lcs ~f:(fun i lc -> Lincons1.array_set lcarr i lc);
    lcarr
  ;;

  let list_of_array lcarr =
    List.init (Lincons1.array_length lcarr) ~f:(fun i -> Lincons1.array_get lcarr i)
  ;;
end

module Texpr2 = struct
  let rec of_arith_term env ae =
    let t =
      match ty_of_arith_term ae with
      | `T_int -> Texpr1.Int
      | `T_real -> Texpr1.Real
    in
    match destruct_arith_term ae with
    | `Var x -> Texpr1.var env (Var.of_string x)
    | `Int n -> Texpr1.cst env (Coeff.s_of_int n)
    | `Float d -> Texpr1.cst env (Coeff.s_of_float d)
    | `Frac f -> Texpr1.cst env (Coeff.s_of_mpqf f)
    | `Negate ae0 ->
      let te0 = of_arith_term env ae0 in
      Texpr1.unop Texpr1.Neg te0 t Texpr1.Zero
    | `Add (ae1, ae2) ->
      let te1 = of_arith_term env ae1 in
      let te2 = of_arith_term env ae2 in
      Texpr1.binop Texpr1.Add te1 te2 t Texpr1.Zero
    | `Sub (ae1, ae2) ->
      let te1 = of_arith_term env ae1 in
      let te2 = of_arith_term env ae2 in
      Texpr1.binop Texpr1.Sub te1 te2 t Texpr1.Zero
    | `Mul (ae1, ae2) ->
      let te1 = of_arith_term env ae1 in
      let te2 = of_arith_term env ae2 in
      Texpr1.binop Texpr1.Mul te1 te2 t Texpr1.Zero
    | `Div (ae1, ae2) ->
      let te1 = of_arith_term env ae1 in
      let te2 = of_arith_term env ae2 in
      Texpr1.binop Texpr1.Div te1 te2 t Texpr1.Zero
  ;;

  let rec of_prob_term env pe =
    match destruct_prob_term pe with
    | `Var x -> Texpr1.var env (Var.of_string x)
    | `Prob (a, b) -> Texpr1.cst env (Coeff.s_of_frac a (a + b))
    | `Inv pe0 ->
      let te0 = of_prob_term env pe0 in
      Texpr1.binop
        Texpr1.Sub
        (Texpr1.cst env (Coeff.s_of_int 1))
        te0
        Texpr1.Real
        Texpr1.Zero
    | `Mul (pe1, pe2) ->
      let te1 = of_prob_term env pe1 in
      let te2 = of_prob_term env pe2 in
      Texpr1.binop Texpr1.Mul te1 te2 Texpr1.Real Texpr1.Zero
  ;;
end

module Tcons2 = struct
  let array_of_list env tcs =
    let tcarr = Tcons1.array_make env (List.length tcs) in
    List.iteri tcs ~f:(fun i tc -> Tcons1.array_set tcarr i tc);
    tcarr
  ;;

  let list_of_array tcarr =
    List.init (Tcons1.array_length tcarr) ~f:(fun i -> Tcons1.array_get tcarr i)
  ;;
end

module Abstract2 = struct
  let to_lincons_list man abs =
    Abstract1.to_lincons_array man abs |> Lincons2.list_of_array
  ;;

  let to_tcons_list man abs = Abstract1.to_tcons_array man abs |> Tcons2.list_of_array

  let of_lincons_list man env lcs =
    let lcarr = Lincons2.array_of_list env lcs in
    Abstract1.of_lincons_array man env lcarr
  ;;

  let of_lincons man env lc = of_lincons_list man env [ lc ]

  let of_tcons_list man env tcs =
    let tcarr = Tcons2.array_of_list env tcs in
    Abstract1.of_tcons_array man env tcarr
  ;;

  let of_tcons man env tc = of_tcons_list man env [ tc ]

  let meet_list man abss =
    (* XXX: [Abstract1.meet_array] does not work for Elina. *)
    List.fold (List.tl_exn abss) ~init:(List.hd_exn abss) ~f:(fun acc abs ->
        Abstract1.meet man acc abs)
  ;;

  let meet_lincons_list man abs lcs =
    let env = Abstract1.env abs in
    Abstract1.meet_lincons_array man abs (Lincons2.array_of_list env lcs)
  ;;

  let meet_tcons_list man abs tcs =
    let env = Abstract1.env abs in
    Abstract1.meet_tcons_array man abs (Tcons2.array_of_list env tcs)
  ;;

  let join_list man abss = Abstract1.join_array man (Array.of_list abss)

  let meet_lincons_list_with man abs lcs =
    let env = Abstract1.env abs in
    Abstract1.meet_lincons_array_with man abs (Lincons2.array_of_list env lcs)
  ;;

  let meet_tcons_list_with man abs tcs =
    let env = Abstract1.env abs in
    Abstract1.meet_tcons_array_with man abs (Tcons2.array_of_list env tcs)
  ;;

  let forget_list ~project_on_zero man abs xs =
    Abstract1.forget_array man abs (Array.of_list_map xs ~f:Var.of_string) project_on_zero
  ;;

  let forget ~project_on_zero man abs x = forget_list ~project_on_zero man abs [ x ]

  let forget_list_with ~project_on_zero man abs xs =
    Abstract1.forget_array_with
      man
      abs
      (Array.of_list_map xs ~f:Var.of_string)
      project_on_zero
  ;;

  let forget_with ~project_on_zero man abs x =
    forget_list_with ~project_on_zero man abs [ x ]
  ;;

  let rename_list man abs xs ys =
    Abstract1.rename_array
      man
      abs
      (Array.of_list_map xs ~f:Var.of_string)
      (Array.of_list_map ys ~f:Var.of_string)
  ;;

  let rename_list_with man abs xs ys =
    Abstract1.rename_array_with
      man
      abs
      (Array.of_list_map xs ~f:Var.of_string)
      (Array.of_list_map ys ~f:Var.of_string)
  ;;

  let widening_threshold_list man abs1 abs2 lcs =
    let env = Abstract1.env abs1 in
    Abstract1.widening_threshold man abs1 abs2 (Lincons2.array_of_list env lcs)
  ;;

  let rec of_formula man env =
    let rec aux be =
      match destruct_formula be with
      | `Var x ->
        let te =
          Texpr1.binop
            Texpr1.Sub
            (Texpr1.var env (Var.of_string x))
            (Texpr1.cst env (Coeff.s_of_int 1))
            Texpr1.Int
            Texpr1.Zero
        in
        let tc = Tcons1.make te Tcons1.EQ in
        of_tcons man env tc
      | `Bool true -> Abstract1.top man env
      | `Bool false -> Abstract1.bottom man env
      | `Not be0 -> of_formula_complement man env be0
      | `Lt (ae1, ae2) ->
        let te_diff = Texpr2.of_arith_term env (mk_sub ae2 ae1) in
        let tc = Tcons1.make te_diff Tcons1.SUP in
        of_tcons man env tc
      | `Le (ae1, ae2) ->
        let te_diff = Texpr2.of_arith_term env (mk_sub ae2 ae1) in
        let tc = Tcons1.make te_diff Tcons1.SUPEQ in
        of_tcons man env tc
      | `Ge (ae1, ae2) ->
        let te_diff = Texpr2.of_arith_term env (mk_sub ae1 ae2) in
        let tc = Tcons1.make te_diff Tcons1.SUPEQ in
        of_tcons man env tc
      | `Gt (ae1, ae2) ->
        let te_diff = Texpr2.of_arith_term env (mk_sub ae1 ae2) in
        let tc = Tcons1.make te_diff Tcons1.SUP in
        of_tcons man env tc
      | `Eq (e1, e2) ->
        (match refine e1, refine e2 with
        | `Arith ae1, `Arith ae2 ->
          let te_diff = Texpr2.of_arith_term env (mk_sub ae1 ae2) in
          let tc = Tcons1.make te_diff Tcons1.EQ in
          of_tcons man env tc
        | `Prob pe1, `Prob pe2 ->
          let te_diff =
            Texpr1.binop
              Texpr1.Sub
              (Texpr2.of_prob_term env pe1)
              (Texpr2.of_prob_term env pe2)
              Texpr1.Real
              Texpr1.Zero
          in
          let tc = Tcons1.make te_diff Tcons1.EQ in
          of_tcons man env tc
        | `Bool be1, `Bool be2 ->
          let abs1 = aux be1 in
          let abs2 = aux be2 in
          let cabs1 = of_formula_complement man env be1 in
          let cabs2 = of_formula_complement man env be2 in
          Abstract1.join
            man
            (Abstract1.meet man abs1 abs2)
            (Abstract1.meet man cabs1 cabs2)
        | _ -> assert false)
      | `Ne (e1, e2) ->
        (match refine e1, refine e2 with
        | `Arith ae1, `Arith ae2 ->
          let te_diff = Texpr2.of_arith_term env (mk_sub ae1 ae2) in
          let tc = Tcons1.make te_diff Tcons1.DISEQ in
          of_tcons man env tc
        | `Prob pe1, `Prob pe2 ->
          let te_diff =
            Texpr1.binop
              Texpr1.Sub
              (Texpr2.of_prob_term env pe1)
              (Texpr2.of_prob_term env pe2)
              Texpr1.Real
              Texpr1.Zero
          in
          let tc = Tcons1.make te_diff Tcons1.DISEQ in
          of_tcons man env tc
        | `Bool be1, `Bool be2 ->
          let abs1 = aux be1 in
          let abs2 = aux be2 in
          let cabs1 = of_formula_complement man env be1 in
          let cabs2 = of_formula_complement man env be2 in
          Abstract1.join
            man
            (Abstract1.meet man abs1 cabs2)
            (Abstract1.meet man cabs1 abs2)
        | _ -> assert false)
      | `And (be1, be2) ->
        let abs1 = aux be1 in
        let abs2 = aux be2 in
        Abstract1.meet man abs1 abs2
      | `Or (be1, be2) ->
        let abs1 = aux be1 in
        let abs2 = aux be2 in
        Abstract1.join man abs1 abs2
      | `Nondet -> Abstract1.top man env
    in
    aux

  and of_formula_complement man env =
    let rec aux be =
      match destruct_formula be with
      | `Var x ->
        let te = Texpr1.var env (Var.of_string x) in
        let tc = Tcons1.make te Tcons1.EQ in
        of_tcons man env tc
      | `Bool true -> Abstract1.bottom man env
      | `Bool false -> Abstract1.top man env
      | `Not be0 -> of_formula man env be0
      | `Lt (ae1, ae2) ->
        let te_diff = Texpr2.of_arith_term env (mk_sub ae1 ae2) in
        let tc = Tcons1.make te_diff Tcons1.SUPEQ in
        of_tcons man env tc
      | `Le (ae1, ae2) ->
        let te_diff = Texpr2.of_arith_term env (mk_sub ae1 ae2) in
        let tc = Tcons1.make te_diff Tcons1.SUP in
        of_tcons man env tc
      | `Ge (ae1, ae2) ->
        let te_diff = Texpr2.of_arith_term env (mk_sub ae2 ae1) in
        let tc = Tcons1.make te_diff Tcons1.SUP in
        of_tcons man env tc
      | `Gt (ae1, ae2) ->
        let te_diff = Texpr2.of_arith_term env (mk_sub ae2 ae1) in
        let tc = Tcons1.make te_diff Tcons1.SUPEQ in
        of_tcons man env tc
      | `Eq (e1, e2) ->
        (match refine e1, refine e2 with
        | `Arith ae1, `Arith ae2 ->
          let te_diff = Texpr2.of_arith_term env (mk_sub ae1 ae2) in
          let tc = Tcons1.make te_diff Tcons1.DISEQ in
          of_tcons man env tc
        | `Prob pe1, `Prob pe2 ->
          let te_diff =
            Texpr1.binop
              Texpr1.Sub
              (Texpr2.of_prob_term env pe1)
              (Texpr2.of_prob_term env pe2)
              Texpr1.Real
              Texpr1.Zero
          in
          let tc = Tcons1.make te_diff Tcons1.DISEQ in
          of_tcons man env tc
        | `Bool be1, `Bool be2 ->
          let abs1 = of_formula man env be1 in
          let abs2 = of_formula man env be2 in
          let cabs1 = aux be1 in
          let cabs2 = aux be2 in
          Abstract1.join
            man
            (Abstract1.meet man abs1 cabs2)
            (Abstract1.meet man cabs1 abs2)
        | _ -> assert false)
      | `Ne (e1, e2) ->
        (match refine e1, refine e2 with
        | `Arith ae1, `Arith ae2 ->
          let te_diff = Texpr2.of_arith_term env (mk_sub ae1 ae2) in
          let tc = Tcons1.make te_diff Tcons1.EQ in
          of_tcons man env tc
        | `Prob pe1, `Prob pe2 ->
          let te_diff =
            Texpr1.binop
              Texpr1.Sub
              (Texpr2.of_prob_term env pe1)
              (Texpr2.of_prob_term env pe2)
              Texpr1.Real
              Texpr1.Zero
          in
          let tc = Tcons1.make te_diff Tcons1.EQ in
          of_tcons man env tc
        | `Bool be1, `Bool be2 ->
          let abs1 = of_formula man env be1 in
          let abs2 = of_formula man env be2 in
          let cabs1 = aux be1 in
          let cabs2 = aux be2 in
          Abstract1.join
            man
            (Abstract1.meet man abs1 abs2)
            (Abstract1.meet man cabs1 cabs2)
        | _ -> assert false)
      | `And (be1, be2) ->
        let abs1 = aux be1 in
        let abs2 = aux be2 in
        Abstract1.join man abs1 abs2
      | `Or (be1, be2) ->
        let abs1 = aux be1 in
        let abs2 = aux be2 in
        Abstract1.meet man abs1 abs2
      | `Nondet -> Abstract1.top man env
    in
    aux
  ;;
end
