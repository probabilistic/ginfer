open Apron
open Syntax

module Lincons2 : sig
  val array_of_list : Environment.t -> Lincons1.t list -> Lincons1.earray
  val list_of_array : Lincons1.earray -> Lincons1.t list
end

module Texpr2 : sig
  val of_arith_term : Environment.t -> arith_term -> Texpr1.t
  val of_prob_term : Environment.t -> prob_term -> Texpr1.t
end

module Tcons2 : sig
  val array_of_list : Environment.t -> Tcons1.t list -> Tcons1.earray
  val list_of_array : Tcons1.earray -> Tcons1.t list
end

module Abstract2 : sig
  val to_lincons_list : 'a Manager.t -> 'a Abstract1.t -> Lincons1.t list
  val to_tcons_list : 'a Manager.t -> 'a Abstract1.t -> Tcons1.t list
  val of_lincons_list : 'a Manager.t -> Environment.t -> Lincons1.t list -> 'a Abstract1.t
  val of_lincons : 'a Manager.t -> Environment.t -> Lincons1.t -> 'a Abstract1.t
  val of_tcons_list : 'a Manager.t -> Environment.t -> Tcons1.t list -> 'a Abstract1.t
  val of_tcons : 'a Manager.t -> Environment.t -> Tcons1.t -> 'a Abstract1.t
  val meet_list : 'a Manager.t -> 'a Abstract1.t list -> 'a Abstract1.t

  val meet_lincons_list
    :  'a Manager.t
    -> 'a Abstract1.t
    -> Lincons1.t list
    -> 'a Abstract1.t

  val meet_tcons_list : 'a Manager.t -> 'a Abstract1.t -> Tcons1.t list -> 'a Abstract1.t
  val join_list : 'a Manager.t -> 'a Abstract1.t list -> 'a Abstract1.t
  val meet_lincons_list_with : 'a Manager.t -> 'a Abstract1.t -> Lincons1.t list -> unit
  val meet_tcons_list_with : 'a Manager.t -> 'a Abstract1.t -> Tcons1.t list -> unit

  val forget_list
    :  project_on_zero:bool
    -> 'a Manager.t
    -> 'a Abstract1.t
    -> string list
    -> 'a Abstract1.t

  val forget
    :  project_on_zero:bool
    -> 'a Manager.t
    -> 'a Abstract1.t
    -> string
    -> 'a Abstract1.t

  val forget_list_with
    :  project_on_zero:bool
    -> 'a Manager.t
    -> 'a Abstract1.t
    -> string list
    -> unit

  val forget_with
    :  project_on_zero:bool
    -> 'a Manager.t
    -> 'a Abstract1.t
    -> string
    -> unit

  val rename_list
    :  'a Manager.t
    -> 'a Abstract1.t
    -> string list
    -> string list
    -> 'a Abstract1.t

  val rename_list_with
    :  'a Manager.t
    -> 'a Abstract1.t
    -> string list
    -> string list
    -> unit

  val widening_threshold_list
    :  'a Manager.t
    -> 'a Abstract1.t
    -> 'a Abstract1.t
    -> Lincons1.t list
    -> 'a Abstract1.t

  val of_formula : 'a Manager.t -> Environment.t -> formula -> 'a Abstract1.t
  val of_formula_complement : 'a Manager.t -> Environment.t -> formula -> 'a Abstract1.t
end
