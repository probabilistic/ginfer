open Core
open Apron
open Apron_helper
open Syntax

module Make_abs (C : sig
  type abs

  val manager : abs Manager.t
end) =
struct
  type t = { abs : C.abs Abstract1.t (* ; ctx : (string * ty) list *) }

  let copy { abs; _ } = { abs = Abstract1.copy C.manager abs }
  let size { abs; _ } = Abstract1.size C.manager abs
  let minimize { abs; _ } = Abstract1.minimize C.manager abs
  let canonicalize { abs; _ } = Abstract1.canonicalize C.manager abs
  let hash { abs; _ } = Abstract1.hash C.manager abs
  let approximate { abs; _ } alg = Abstract1.approximate C.manager abs alg
  let dump { abs; _ } = Abstract1.fdump C.manager abs
  let pp fmt { abs; _ } = Abstract1.print fmt abs

  let env_of_scope ctx =
    let ivars, rvars =
      List.fold_right ctx ~init:([], []) ~f:(fun (x, t) (ivars, rvars) ->
          match t with
          | `T_bool | `T_int -> x :: ivars, rvars
          | `T_real | `T_prob -> ivars, x :: rvars)
    in
    Environment.make
      (Array.of_list_map ivars ~f:Var.of_string)
      (Array.of_list_map rvars ~f:Var.of_string)
  ;;

  let bottom ctx = { abs = Abstract1.bottom C.manager (env_of_scope ctx) }

  let set_bvar_range abs x =
    let env = Abstract1.env abs in
    let var = Var.of_string x in
    let tc_lb = Tcons1.make (Texpr1.var env var) Tcons1.SUPEQ in
    let tc_ub =
      Tcons1.make
        (Texpr1.binop
           Texpr1.Sub
           (Texpr1.cst env (Coeff.s_of_int 1))
           (Texpr1.var env var)
           Texpr1.Int
           Texpr1.Zero)
        Tcons1.SUPEQ
    in
    Abstract2.meet_tcons_list_with C.manager abs [ tc_lb; tc_ub ]
  ;;

  let set_pvar_range abs x =
    let env = Abstract1.env abs in
    let var = Var.of_string x in
    let tc_lb = Tcons1.make (Texpr1.var env var) Tcons1.SUPEQ in
    let tc_ub =
      Tcons1.make
        (Texpr1.binop
           Texpr1.Sub
           (Texpr1.cst env (Coeff.s_of_int 1))
           (Texpr1.var env var)
           Texpr1.Real
           Texpr1.Zero)
        Tcons1.SUPEQ
    in
    Abstract2.meet_tcons_list_with C.manager abs [ tc_lb; tc_ub ]
  ;;

  let set_var_ranges abs ctx =
    List.iter ctx ~f:(fun (x, t) ->
        match t with
        | `T_bool -> set_bvar_range abs x
        | `T_prob -> set_pvar_range abs x
        | _ -> ())
  ;;

  let top ctx =
    let abs = Abstract1.top C.manager (env_of_scope ctx) in
    set_var_ranges abs ctx;
    { abs }
  ;;

  let is_leq { abs = abs1; _ } { abs = abs2; _ } = Abstract1.is_leq C.manager abs1 abs2
  let is_eq { abs = abs1; _ } { abs = abs2; _ } = Abstract1.is_eq C.manager abs1 abs2

  let sat_formula { abs; _ } be =
    let env = Abstract1.env abs in
    let abs_of_be = Abstract2.of_formula C.manager env be in
    Abstract1.is_leq C.manager abs abs_of_be
  ;;

  let is_variable_unconstrained { abs; _ } x =
    Abstract1.is_variable_unconstrained C.manager abs (Var.of_string x)
  ;;

  let bound_varaible { abs; _ } x =
    Abstract1.bound_variable C.manager abs (Var.of_string x)
  ;;

  let bound_arith_term { abs; _ } ae =
    let env = Abstract1.env abs in
    let te = Texpr2.of_arith_term env ae in
    Abstract1.bound_texpr C.manager abs te
  ;;

  let bound_prob_term { abs; _ } pe =
    let env = Abstract1.env abs in
    let te = Texpr2.of_prob_term env pe in
    Abstract1.bound_texpr C.manager abs te
  ;;

  let to_lincons_list { abs; _ } =
    Abstract1.to_lincons_array C.manager abs |> Lincons2.list_of_array
  ;;

  let meet { abs = abs1; _ } { abs = abs2; _ } =
    { abs = Abstract1.meet C.manager abs1 abs2 }
  ;;

  let meet_list ts =
    { abs = Abstract2.meet_list C.manager (List.map ts ~f:(fun { abs; _ } -> abs)) }
  ;;

  let meet_formula_list { abs; _ } bes =
    let env = Abstract1.env abs in
    { abs =
        Abstract2.meet_list
          C.manager
          (abs :: List.map bes ~f:(Abstract2.of_formula C.manager env))
    }
  ;;

  let join { abs = abs1; _ } { abs = abs2; _ } =
    { abs = Abstract1.join C.manager abs1 abs2 }
  ;;

  let join_list ts =
    { abs = Abstract2.join_list C.manager (List.map ts ~f:(fun { abs; _ } -> abs)) }
  ;;

  let meet_with { abs = abs1; _ } { abs = abs2; _ } =
    Abstract1.meet_with C.manager abs1 abs2
  ;;

  let meet_formula_list_with { abs; _ } bes =
    let env = Abstract1.env abs in
    List.iter bes ~f:(fun be ->
        Abstract1.meet_with C.manager abs (Abstract2.of_formula C.manager env be))
  ;;

  let join_with { abs = abs1; _ } { abs = abs2; _ } =
    Abstract1.join_with C.manager abs1 abs2
  ;;

  let assign_bool { abs; _ } x be =
    let env = Abstract1.env abs in
    let abs_of_be = Abstract2.of_formula C.manager env be in
    let abs_of_be_complement = Abstract2.of_formula_complement C.manager env be in
    { abs =
        Abstract1.join
          C.manager
          (Abstract1.assign_texpr
             C.manager
             (Abstract1.meet C.manager abs abs_of_be)
             (Var.of_string x)
             (Texpr2.of_arith_term env (mk_int 1))
             None)
          (Abstract1.assign_texpr
             C.manager
             (Abstract1.meet C.manager abs abs_of_be_complement)
             (Var.of_string x)
             (Texpr2.of_arith_term env (mk_int 0))
             None)
    }
  ;;

  let substitute_bool { abs; _ } x be =
    let env = Abstract1.env abs in
    let abs_of_be = Abstract2.of_formula C.manager env be in
    let abs_of_be_complement = Abstract2.of_formula_complement C.manager env be in
    { abs =
        Abstract1.join
          C.manager
          (Abstract1.meet
             C.manager
             (Abstract1.substitute_texpr
                C.manager
                abs
                (Var.of_string x)
                (Texpr2.of_arith_term env (mk_int 1))
                None)
             abs_of_be)
          (Abstract1.meet
             C.manager
             (Abstract1.substitute_texpr
                C.manager
                abs
                (Var.of_string x)
                (Texpr2.of_arith_term env (mk_int 0))
                None)
             abs_of_be_complement)
    }
  ;;

  let assign_arith { abs; _ } x ae =
    let env = Abstract1.env abs in
    { abs =
        Abstract1.assign_texpr
          C.manager
          abs
          (Var.of_string x)
          (Texpr2.of_arith_term env ae)
          None
    }
  ;;

  let substitute_arith { abs; _ } x ae =
    let env = Abstract1.env abs in
    { abs =
        Abstract1.substitute_texpr
          C.manager
          abs
          (Var.of_string x)
          (Texpr2.of_arith_term env ae)
          None
    }
  ;;

  let assign_prob { abs; _ } x pe =
    let env = Abstract1.env abs in
    { abs =
        Abstract1.assign_texpr
          C.manager
          abs
          (Var.of_string x)
          (Texpr2.of_prob_term env pe)
          None
    }
  ;;

  let substitute_prob { abs; _ } x pe =
    let env = Abstract1.env abs in
    { abs =
        Abstract1.substitute_texpr
          C.manager
          abs
          (Var.of_string x)
          (Texpr2.of_prob_term env pe)
          None
    }
  ;;

  let assign_bool_with { abs; _ } x be =
    let env = Abstract1.env abs in
    let abs_of_be = Abstract2.of_formula C.manager env be in
    let abs_of_be_complement = Abstract2.of_formula_complement C.manager env be in
    let abs' =
      Abstract1.assign_texpr
        C.manager
        (Abstract1.meet C.manager abs abs_of_be_complement)
        (Var.of_string x)
        (Texpr2.of_arith_term env (mk_int 0))
        None
    in
    Abstract1.meet_with C.manager abs abs_of_be;
    Abstract1.assign_texpr_with
      C.manager
      abs
      (Var.of_string x)
      (Texpr2.of_arith_term env (mk_int 1))
      None;
    Abstract1.join_with C.manager abs abs'
  ;;

  let substitute_bool_with { abs; _ } x be =
    let env = Abstract1.env abs in
    let abs_of_be = Abstract2.of_formula C.manager env be in
    let abs_of_be_complement = Abstract2.of_formula_complement C.manager env be in
    let abs' =
      Abstract1.meet
        C.manager
        (Abstract1.substitute_texpr
           C.manager
           abs
           (Var.of_string x)
           (Texpr2.of_arith_term env (mk_int 0))
           None)
        abs_of_be_complement
    in
    Abstract1.substitute_texpr_with
      C.manager
      abs
      (Var.of_string x)
      (Texpr2.of_arith_term env (mk_int 1))
      None;
    Abstract1.meet_with C.manager abs abs_of_be;
    Abstract1.join_with C.manager abs abs'
  ;;

  let assign_arith_with { abs; _ } x ae =
    let env = Abstract1.env abs in
    Abstract1.assign_texpr_with
      C.manager
      abs
      (Var.of_string x)
      (Texpr2.of_arith_term env ae)
      None
  ;;

  let substitute_arith_with { abs; _ } x ae =
    let env = Abstract1.env abs in
    Abstract1.substitute_texpr_with
      C.manager
      abs
      (Var.of_string x)
      (Texpr2.of_arith_term env ae)
      None
  ;;

  let assign_prob_with { abs; _ } x pe =
    let env = Abstract1.env abs in
    Abstract1.assign_texpr_with
      C.manager
      abs
      (Var.of_string x)
      (Texpr2.of_prob_term env pe)
      None
  ;;

  let substitute_prob_with { abs; _ } x pe =
    let env = Abstract1.env abs in
    Abstract1.substitute_texpr_with
      C.manager
      abs
      (Var.of_string x)
      (Texpr2.of_prob_term env pe)
      None
  ;;

  let forget_list { abs; _ } xs =
    { abs = Abstract2.forget_list C.manager abs xs ~project_on_zero:false }
  ;;

  let forget_list_with { abs; _ } xs =
    Abstract2.forget_list_with C.manager abs xs ~project_on_zero:false
  ;;

  let change_scope { abs; _ } ctx =
    { abs = Abstract1.change_environment C.manager abs (env_of_scope ctx) false }
  ;;

  let rename_list { abs; _ } xs ys = { abs = Abstract2.rename_list C.manager abs xs ys }

  let change_scope_with { abs; _ } ctx =
    Abstract1.change_environment_with C.manager abs (env_of_scope ctx) false
  ;;

  let rename_list_with { abs; _ } xs ys = Abstract2.rename_list_with C.manager abs xs ys

  let widening { abs = abs1; _ } { abs = abs2; _ } =
    { abs = Abstract1.widening C.manager abs1 abs2 }
  ;;

  let widening_threshold_list { abs = abs1; _ } { abs = abs2; _ } lcs =
    { abs = Abstract2.widening_threshold_list C.manager abs1 abs2 lcs }
  ;;

  let closure { abs; _ } = { abs = Abstract1.closure C.manager abs }
  let closure_with { abs; _ } = Abstract1.closure_with C.manager abs

  let of_formula ctx be =
    let abs = Abstract2.of_formula C.manager (env_of_scope ctx) be in
    set_var_ranges abs ctx;
    { abs }
  ;;
end

module Make_box () = Make_abs (struct
  type abs = Box.t

  let manager = Box.manager_alloc ()
end)

module Make_oct () = Make_abs (struct
  type abs = Oct.t

  let manager = Oct.manager_alloc ()
end)

module Make_polka () = Make_abs (struct
  type abs = Polka.loose Polka.t

  let manager = Polka.manager_alloc_loose ()
end)

module Make_ppl () = Make_abs (struct
  type abs = Ppl.loose Ppl.t

  let manager = Ppl.manager_alloc_loose ()
end)

module Make_elina_oct () = Make_abs (struct
  type abs = Elina_oct.t

  let manager = Elina_oct.manager_alloc ()
end)

module Make_elina_poly () = Make_abs (struct
  type abs = Elina_poly.loose Elina_poly.t

  let manager = Elina_poly.manager_alloc_loose ()
end)
