open Apron

module Make_abs (C : sig
  type abs

  val manager : abs Manager.t
end) : Sig.DOMAIN

module Make_box () : Sig.DOMAIN
module Make_oct () : Sig.DOMAIN
module Make_polka () : Sig.DOMAIN
module Make_ppl () : Sig.DOMAIN
module Make_elina_oct () : Sig.DOMAIN
module Make_elina_poly () : Sig.DOMAIN
