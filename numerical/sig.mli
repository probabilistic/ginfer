open Apron
open Syntax

module type DOMAIN = sig
  type t

  val copy : t -> t
  val size : t -> int
  val minimize : t -> unit
  val canonicalize : t -> unit
  val hash : t -> int
  val approximate : t -> int -> unit
  val dump : t -> unit
  val pp : Format.formatter -> t -> unit
  val bottom : (string * ty) list -> t
  val top : (string * ty) list -> t
  val is_leq : t -> t -> bool
  val is_eq : t -> t -> bool
  val sat_formula : t -> formula -> bool
  val is_variable_unconstrained : t -> string -> bool
  val bound_varaible : t -> string -> Interval.t
  val bound_arith_term : t -> arith_term -> Interval.t
  val bound_prob_term : t -> prob_term -> Interval.t
  val to_lincons_list : t -> Lincons1.t list
  val meet : t -> t -> t
  val meet_list : t list -> t
  val meet_formula_list : t -> formula list -> t
  val join : t -> t -> t
  val join_list : t list -> t
  val meet_with : t -> t -> unit
  val meet_formula_list_with : t -> formula list -> unit
  val join_with : t -> t -> unit
  val assign_bool : t -> string -> formula -> t
  val substitute_bool : t -> string -> formula -> t
  val assign_arith : t -> string -> arith_term -> t
  val substitute_arith : t -> string -> arith_term -> t
  val assign_prob : t -> string -> prob_term -> t
  val substitute_prob : t -> string -> prob_term -> t
  val assign_bool_with : t -> string -> formula -> unit
  val substitute_bool_with : t -> string -> formula -> unit
  val assign_arith_with : t -> string -> arith_term -> unit
  val substitute_arith_with : t -> string -> arith_term -> unit
  val assign_prob_with : t -> string -> prob_term -> unit
  val substitute_prob_with : t -> string -> prob_term -> unit
  val forget_list : t -> string list -> t
  val forget_list_with : t -> string list -> unit
  val change_scope : t -> (string * ty) list -> t
  val rename_list : t -> string list -> string list -> t
  val change_scope_with : t -> (string * ty) list -> unit
  val rename_list_with : t -> string list -> string list -> unit
  val widening : t -> t -> t
  val widening_threshold_list : t -> t -> Lincons1.t list -> t
  val closure : t -> t
  val closure_with : t -> unit
  val of_formula : (string * ty) list -> formula -> t
end
