type un_op =
  | Not
  | Negate

type bin_op =
  | Add
  | Sub
  | Mul
  | Div
  | Lt
  | Le
  | Ge
  | Gt
  | Eq
  | Ne
  | And
  | Or

type ty =
  [ `T_bool
  | `T_int
  | `T_real
  | `T_prob
  ]
[@@deriving equal]

type ty_bool = [ `T_bool ]

type ty_arith =
  [ `T_int
  | `T_real
  ]

let join = function
  | `T_int, `T_int -> `T_int
  | _ -> `T_real
;;

type ty_prob = [ `T_prob ]

type sexpression =
  { exp_desc : sexpression_desc
  ; exp_type : ty
  }

and sexpression_desc =
  | E_var of string
  | E_bool of bool
  | E_int of int
  | E_float of float
  | E_frac of Mpqf.t
  | E_prob of int * int
  | E_unary of un_op * sexpression
  | E_binary of bin_op * sexpression * sexpression
  | E_nondet

type 'ty expression = sexpression
type formula = ty_bool expression
type arith_term = ty_arith expression
type prob_term = ty_prob expression

let mk_exp exp_desc exp_type = { exp_desc; exp_type }
let mk_bool_var x = mk_exp (E_var x) `T_bool
let mk_int_var x = mk_exp (E_var x) `T_int
let mk_real_var x = mk_exp (E_var x) `T_real
let mk_prob_var x = mk_exp (E_var x) `T_prob
let mk_bool b = mk_exp (E_bool b) `T_bool
let mk_int n = mk_exp (E_int n) `T_int
let mk_float d = mk_exp (E_float d) `T_real
let mk_frac f = mk_exp (E_frac f) `T_real
let mk_prob (a, b) = mk_exp (E_prob (a, b)) `T_prob
let mk_not be = mk_exp (E_unary (Not, be)) `T_bool
let mk_inv_prob pe = mk_exp (E_unary (Not, pe)) `T_prob
let mk_negate ae = mk_exp (E_unary (Negate, ae)) ae.exp_type
let mk_add ae1 ae2 = mk_exp (E_binary (Add, ae1, ae2)) (join (ae1.exp_type, ae2.exp_type))
let mk_sub ae1 ae2 = mk_exp (E_binary (Sub, ae1, ae2)) (join (ae1.exp_type, ae2.exp_type))
let mk_mul ae1 ae2 = mk_exp (E_binary (Mul, ae1, ae2)) (join (ae1.exp_type, ae2.exp_type))
let mk_mul_prob pe1 pe2 = mk_exp (E_binary (Mul, pe1, pe2)) `T_prob
let mk_div ae1 ae2 = mk_exp (E_binary (Div, ae1, ae2)) (join (ae1.exp_type, ae2.exp_type))
let mk_lt ae1 ae2 = mk_exp (E_binary (Lt, ae1, ae2)) `T_bool
let mk_le ae1 ae2 = mk_exp (E_binary (Le, ae1, ae2)) `T_bool
let mk_ge ae1 ae2 = mk_exp (E_binary (Ge, ae1, ae2)) `T_bool
let mk_gt ae1 ae2 = mk_exp (E_binary (Gt, ae1, ae2)) `T_bool
let mk_bool_eq be1 be2 = mk_exp (E_binary (Eq, be1, be2)) `T_bool
let mk_arith_eq ae1 ae2 = mk_exp (E_binary (Eq, ae1, ae2)) `T_bool
let mk_prob_eq pe1 pe2 = mk_exp (E_binary (Eq, pe1, pe2)) `T_bool
let mk_bool_ne be1 be2 = mk_exp (E_binary (Ne, be1, be2)) `T_bool
let mk_arith_ne ae1 ae2 = mk_exp (E_binary (Ne, ae1, ae2)) `T_bool
let mk_prob_ne pe1 pe2 = mk_exp (E_binary (Ne, pe1, pe2)) `T_bool
let mk_and be1 be2 = mk_exp (E_binary (And, be1, be2)) `T_bool
let mk_or be1 be2 = mk_exp (E_binary (Or, be1, be2)) `T_bool
let mk_nondet = mk_exp E_nondet `T_bool

let refine e =
  match e.exp_type with
  | `T_bool -> `Bool e
  | `T_int | `T_real -> `Arith e
  | `T_prob -> `Prob e
;;

let destruct_formula be =
  match be.exp_desc with
  | E_var x -> `Var x
  | E_bool b -> `Bool b
  | E_unary (Not, be0) -> `Not be0
  | E_binary (Lt, ae1, ae2) -> `Lt (ae1, ae2)
  | E_binary (Le, ae1, ae2) -> `Le (ae1, ae2)
  | E_binary (Ge, ae1, ae2) -> `Ge (ae1, ae2)
  | E_binary (Gt, ae1, ae2) -> `Gt (ae1, ae2)
  | E_binary (Eq, e1, e2) -> `Eq (e1, e2)
  | E_binary (Ne, e1, e2) -> `Ne (e1, e2)
  | E_binary (And, be1, be2) -> `And (be1, be2)
  | E_binary (Or, be1, be2) -> `Or (be1, be2)
  | E_nondet -> `Nondet
  | _ -> assert false
;;

let destruct_arith_term ae =
  match ae.exp_desc with
  | E_var x -> `Var x
  | E_int n -> `Int n
  | E_float d -> `Float d
  | E_frac f -> `Frac f
  | E_unary (Negate, ae0) -> `Negate ae0
  | E_binary (Add, ae1, ae2) -> `Add (ae1, ae2)
  | E_binary (Sub, ae1, ae2) -> `Sub (ae1, ae2)
  | E_binary (Mul, ae1, ae2) -> `Mul (ae1, ae2)
  | E_binary (Div, ae1, ae2) -> `Div (ae1, ae2)
  | _ -> assert false
;;

let destruct_prob_term pe =
  match pe.exp_desc with
  | E_var x -> `Var x
  | E_prob (a, b) -> `Prob (a, b)
  | E_unary (Not, pe0) -> `Inv pe0
  | E_binary (Mul, pe1, pe2) -> `Mul (pe1, pe2)
  | _ -> assert false
;;

let ty_of_arith_term ae =
  match ae.exp_type with
  | `T_int -> `T_int
  | `T_real -> `T_real
  | _ -> assert false
;;
