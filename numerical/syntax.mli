type +'ty expression

type ty =
  [ `T_bool
  | `T_int
  | `T_prob
  | `T_real
  ]

type ty_bool = [ `T_bool ]

type ty_arith =
  [ `T_int
  | `T_real
  ]

type ty_prob = [ `T_prob ]
type formula = ty_bool expression
type arith_term = ty_arith expression
type prob_term = ty_prob expression

val mk_bool_var : string -> formula
val mk_int_var : string -> arith_term
val mk_real_var : string -> arith_term
val mk_prob_var : string -> prob_term
val mk_bool : bool -> formula
val mk_int : int -> arith_term
val mk_float : float -> arith_term
val mk_frac : Mpqf.t -> arith_term
val mk_prob : int * int -> prob_term
val mk_not : formula -> formula
val mk_inv_prob : prob_term -> prob_term
val mk_negate : arith_term -> arith_term
val mk_add : arith_term -> arith_term -> arith_term
val mk_sub : arith_term -> arith_term -> arith_term
val mk_mul : arith_term -> arith_term -> arith_term
val mk_mul_prob : prob_term -> prob_term -> prob_term
val mk_div : arith_term -> arith_term -> arith_term
val mk_lt : arith_term -> arith_term -> formula
val mk_le : arith_term -> arith_term -> formula
val mk_ge : arith_term -> arith_term -> formula
val mk_gt : arith_term -> arith_term -> formula
val mk_bool_eq : formula -> formula -> formula
val mk_arith_eq : arith_term -> arith_term -> formula
val mk_prob_eq : prob_term -> prob_term -> formula
val mk_bool_ne : formula -> formula -> formula
val mk_arith_ne : arith_term -> arith_term -> formula
val mk_prob_ne : prob_term -> prob_term -> formula
val mk_and : formula -> formula -> formula
val mk_or : formula -> formula -> formula
val mk_nondet : formula

val refine
  :  ty expression
  -> [ `Bool of formula | `Arith of arith_term | `Prob of prob_term ]

val destruct_formula
  :  formula
  -> [ `Var of string
     | `Bool of bool
     | `Not of formula
     | `Lt of arith_term * arith_term
     | `Le of arith_term * arith_term
     | `Ge of arith_term * arith_term
     | `Gt of arith_term * arith_term
     | `Eq of ty expression * ty expression
     | `Ne of ty expression * ty expression
     | `And of formula * formula
     | `Or of formula * formula
     | `Nondet
     ]

val destruct_arith_term
  :  arith_term
  -> [ `Var of string
     | `Int of int
     | `Float of float
     | `Frac of Mpqf.t
     | `Negate of arith_term
     | `Add of arith_term * arith_term
     | `Sub of arith_term * arith_term
     | `Mul of arith_term * arith_term
     | `Div of arith_term * arith_term
     ]

val destruct_prob_term
  :  prob_term
  -> [ `Var of string
     | `Prob of int * int
     | `Inv of prob_term
     | `Mul of prob_term * prob_term
     ]

val ty_of_arith_term : arith_term -> ty_arith
