open Core
open Result.Let_syntax
open Action_types
open Action_ops

let tycheck_expr ctx =
  let rec aux e =
    let loc = Untyped.expr_loc e in
    let ret e' = Ok (e' :> Typed.rhs_expr) in
    match Untyped.destruct e with
    | `Var x ->
      let%bind ty = tyctx_get ctx x in
      ret (Typed.mk_var ~loc x ty)
    | `Bool b -> ret (Typed.mk_bool ~loc b)
    | `Int n -> ret (Typed.mk_int ~loc n)
    | `Real r -> ret (Typed.mk_real ~loc r)
    | `Frac f -> ret (Typed.mk_frac ~loc f)
    | `Prob (a, b) -> ret (Typed.mk_prob ~loc (a, b))
    | `BinOp (bop, e1, e2) ->
      let%bind e1' = aux e1 in
      let%bind e2' = aux e2 in
      (match Typed.refine e1', Typed.refine e2' with
      | `ArithExpr ae1, `ArithExpr ae2 -> ret (Typed.mk_bin_op ~loc (bop, ae1, ae2))
      | _ -> Error (Static_error ("incorrect operand types", loc)))
    | `RelOp (rop, e1, e2) ->
      let%bind e1' = aux e1 in
      let%bind e2' = aux e2 in
      (match Typed.refine e1', Typed.refine e2' with
      | `ArithExpr ae1, `ArithExpr ae2 -> ret (Typed.mk_rel_op ~loc (rop, ae1, ae2))
      | _ -> Error (Static_error ("incorrect operand types", loc)))
    | `And (e1, e2) ->
      let%bind e1' = aux e1 in
      let%bind e2' = aux e2 in
      (match Typed.refine e1', Typed.refine e2' with
      | `BoolExpr be1, `BoolExpr be2 -> ret (Typed.mk_and ~loc (be1, be2))
      | _ -> Error (Static_error ("non-Boolean predicate", loc)))
    | `Or (e1, e2) ->
      let%bind e1' = aux e1 in
      let%bind e2' = aux e2 in
      (match Typed.refine e1', Typed.refine e2' with
      | `BoolExpr be1, `BoolExpr be2 -> ret (Typed.mk_or ~loc (be1, be2))
      | _ -> Error (Static_error ("non-Boolean predicate", loc)))
    | `Not e0 ->
      let%bind e0' = aux e0 in
      (match Typed.refine e0' with
      | `BoolExpr be0 -> ret (Typed.mk_not ~loc be0)
      | _ -> Error (Static_error ("non-Boolean operand", loc)))
    | `Negate e0 ->
      let%bind e0' = aux e0 in
      (match Typed.refine e0' with
      | `ArithExpr ae0 -> ret (Typed.mk_negate ~loc ae0)
      | _ -> Error (Static_error ("non-numeric operand", loc)))
    | `Nondet -> ret (Typed.mk_nondet ~loc)
  in
  aux
;;

let tycheck_data_action ctx = function
  | Untyped.DWeaken es ->
    Result.try_with (fun () ->
        Typed.DWeaken
          (List.map es ~f:(fun e ->
               Result.ok_exn
               @@ let%bind e' = tycheck_expr ctx e in
                  match Typed.refine e' with
                  | `ArithExpr ae -> Ok ae
                  | _ ->
                    Error
                      (Static_error ("numeric expression expected", Typed.expr_loc e')))))
  | Untyped.DSkip -> Ok Typed.DSkip
  | Untyped.DAssign (x, e) ->
    let%bind e' = tycheck_expr ctx e in
    let%bind ty = tyctx_get ctx x in
    if Typed.assignable ~from_ty:(Typed.expr_ty e') ~to_ty:ty
    then Ok (Typed.DAssign (x, e'))
    else
      Error (Static_error ("cannot assign to variable " ^ x.txt ^ " due to types", x.loc))
  | Untyped.DScore (e, flag) ->
    let%bind e' = tycheck_expr ctx e in
    (match Typed.refine e' with
    | `ArithExpr ae -> Ok (Typed.DScore (ae, flag))
    | _ -> Error (Static_error ("numeric expression expected", Typed.expr_loc e')))
  | Untyped.DAssume e ->
    let%bind e' = tycheck_expr ctx e in
    (match Typed.refine e' with
    | `BoolExpr be -> Ok (Typed.DAssume be)
    | _ -> Error (Static_error ("Boolean expression expected", Typed.expr_loc e')))
  | Untyped.DSample (x, dist) ->
    let dist_ty =
      match dist with
      | `Int _ -> `TyInt
      | `Real _ -> `TyReal
    in
    let%bind ty = tyctx_get ctx x in
    if Typed.assignable ~from_ty:dist_ty ~to_ty:ty
    then Ok (Typed.DSample (x, dist))
    else
      Error
        (Static_error
           ("connot draw a sample to variable " ^ x.txt ^ " due to types", x.loc))
;;
