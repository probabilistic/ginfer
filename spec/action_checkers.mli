open Action_types
open Action_ops

val tycheck_expr
  :  Typed.rhs_ty tycontext
  -> Untyped.expr
  -> (Typed.rhs_expr, exn) Result.t

val tycheck_data_action
  :  Typed.rhs_ty tycontext
  -> Untyped.data_action
  -> (Typed.data_action, exn) Result.t
