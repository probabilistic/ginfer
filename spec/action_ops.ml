open Core
open Action_types

type 'a tycontext = 'a String.Map.t

let tyctx_empty = String.Map.empty

let tyctx_add x ty ctx =
  match String.Map.add ctx ~key:x.txt ~data:ty with
  | `Ok ctx' -> Ok ctx'
  | `Duplicate -> Error (Static_error ("duplicate variable " ^ x.txt, x.loc))
;;

let tyctx_add_exn x ty ctx = tyctx_add x ty ctx |> Result.ok_exn

let tyctx_get ctx x =
  match String.Map.find ctx x.txt with
  | Some ty -> Ok ty
  | None -> Error (Static_error ("unknown variable " ^ x.txt, x.loc))
;;

let tyctx_get_exn ctx x = tyctx_get ctx x |> Result.ok_exn
let tyctx_scope ctx = String.Map.keys ctx
let tyctx_to_alist ctx = String.Map.to_alist ctx

let tyctx_restrict ctx vars =
  let vars = List.map vars ~f:(fun var -> var.txt) in
  String.Map.filter_keys ctx ~f:(List.mem vars ~equal:String.equal)
;;

let string_of_rel_op = function
  | LE -> "<="
  | LT -> "<"
  | GE -> ">="
  | GT -> ">"
  | EQ -> "="
  | NE -> "<>"
;;

let string_of_type = function
  | `TyReal -> "real"
  | `TyInt -> "int"
  | `TyBool -> "bool"
  | `TyProb -> "prob"
;;

let rec print_arith_expr fmt ae =
  match Typed.destruct_arith ae with
  | `BinOp (Add, ae1, ae2) ->
    print_arith_expr fmt ae1;
    Format.fprintf fmt " + ";
    print_arith_expr1 fmt ae2
  | `BinOp (Sub, ae1, ae2) ->
    print_arith_expr fmt ae1;
    Format.fprintf fmt " - ";
    print_arith_expr1 fmt ae2
  | _ -> print_arith_expr1 fmt ae

and print_arith_expr1 fmt ae =
  match Typed.destruct_arith ae with
  | `BinOp (Mul, ae1, ae2) ->
    print_arith_expr1 fmt ae1;
    Format.fprintf fmt " * ";
    print_arith_expr2 fmt ae2
  | _ -> print_arith_expr2 fmt ae

and print_arith_expr2 fmt ae =
  match Typed.destruct_arith ae with
  | `Negate ae0 -> Format.fprintf fmt "-%a" print_arith_expr3 ae0
  | _ -> print_arith_expr3 fmt ae

and print_arith_expr3 fmt ae =
  match Typed.destruct_arith ae with
  | `Var (x, _) -> Format.fprintf fmt "%s" x.txt
  | `Int n -> Format.fprintf fmt "%d" n
  | `Real r -> Format.fprintf fmt "%g" r
  | `Frac f -> Format.fprintf fmt "%a" Mpqf.print f
  | _ -> Format.fprintf fmt "(%a)" print_arith_expr ae
;;

let rec print_bool_expr fmt be =
  match Typed.destruct_bool be with
  | `And (be1, be2) ->
    print_bool_expr1 fmt be1;
    Format.fprintf fmt " and ";
    print_bool_expr fmt be2
  | `Or (be1, be2) ->
    print_bool_expr1 fmt be1;
    Format.fprintf fmt " or ";
    print_bool_expr fmt be2
  | _ -> print_bool_expr1 fmt be

and print_bool_expr1 fmt be =
  match Typed.destruct_bool be with
  | `RelOp (rop, ae1, ae2) ->
    print_arith_expr fmt ae1;
    Format.fprintf fmt " %s " (string_of_rel_op rop);
    print_arith_expr fmt ae2
  | _ -> print_bool_expr2 fmt be

and print_bool_expr2 fmt be =
  match Typed.destruct_bool be with
  | `Not be0 -> Format.fprintf fmt "!%a" print_bool_expr3 be0
  | _ -> print_bool_expr3 fmt be

and print_bool_expr3 fmt be =
  match Typed.destruct_bool be with
  | `Var (x, _) -> Format.fprintf fmt "%s" x.txt
  | `Bool true -> Format.fprintf fmt "true"
  | `Bool false -> Format.fprintf fmt "false"
  | `Nondet -> Format.fprintf fmt "demon"
  | _ -> Format.fprintf fmt "(%a)" print_bool_expr be
;;

let print_prob_expr fmt pe =
  match Typed.destruct_prob pe with
  | `Var (x, _) -> Format.fprintf fmt "%s" x.txt
  | `Prob (a, b) -> Format.fprintf fmt "prob(%d,%d)" a b
;;

let print_rhs_expr fmt rhs =
  match Typed.refine rhs with
  | `BoolExpr be -> print_bool_expr fmt be
  | `ArithExpr ae -> print_arith_expr fmt ae
  | `ProbExpr pe -> print_prob_expr fmt pe
;;

let print_data_action fmt act =
  match act with
  | Typed.DSkip -> Format.fprintf fmt "skip"
  | DWeaken _ -> Format.fprintf fmt "weaken()"
  | DScore (ae, _) -> Format.fprintf fmt "score(%a)" print_arith_expr ae
  | DAssign (x, e) -> Format.fprintf fmt "%s := %a" x.txt print_rhs_expr e
  | DAssume be -> Format.fprintf fmt "assume(%a)" print_bool_expr be
  | DSample (x, dist) ->
    Format.fprintf
      fmt
      "%s ~ %s"
      x.txt
      (match dist with
      | `Int d -> d.Dist.dist_name
      | `Real d -> d.Dist.dist_name)
;;

let smt_of_arith_expr =
  let rec inner ae =
    match Typed.destruct_arith ae with
    | `Var (x, ty) ->
      (match ty with
      | `TyInt -> Smt.mk_ivar x.txt
      | `TyReal -> Smt.mk_rvar x.txt)
    | `Int n -> Smt.mk_const n
    | `Real r -> Smt.mk_const_r r
    | `Frac f -> Smt.mk_const_r (Mpqf.to_float f)
    | `BinOp (bop, ae1, ae2) ->
      let se1 = inner ae1 in
      let se2 = inner ae2 in
      (match bop with
      | Add -> Smt.mk_add se1 se2
      | Sub -> Smt.mk_minus se1 se2
      | Mul -> Smt.mk_mult se1 se2)
    | `Negate ae0 -> Smt.mk_neg (inner ae0)
  in
  inner
;;

let smt_of_bool_expr =
  let rec inner be =
    match Typed.destruct_bool be with
    | `Var (x, _) -> Smt.mk_eq (Smt.mk_ivar x.txt) (Smt.mk_const 1)
    | `Bool true -> Smt.mk_true ()
    | `Bool false -> Smt.mk_false ()
    | `And (be1, be2) -> Smt.mk_and (inner be1) (inner be2)
    | `Or (be1, be2) -> Smt.mk_or (inner be1) (inner be2)
    | `Not be0 -> Smt.mk_not (inner be0)
    | `Nondet -> Smt.mk_bfvar ()
    | `RelOp (rop, ae1, ae2) ->
      let se1 = smt_of_arith_expr ae1 in
      let se2 = smt_of_arith_expr ae2 in
      (match rop with
      | LE -> Smt.mk_le se1 se2
      | LT -> Smt.mk_lt se1 se2
      | GE -> Smt.mk_ge se1 se2
      | GT -> Smt.mk_gt se1 se2
      | EQ -> Smt.mk_eq se1 se2
      | NE -> Smt.mk_not (Smt.mk_eq se1 se2))
  in
  inner
;;

let rec degree ae =
  match Typed.destruct_arith ae with
  | `BinOp (Add, ae1, ae2) | `BinOp (Sub, ae1, ae2) -> max (degree ae1) (degree ae2)
  | `BinOp (Mul, ae1, ae2) -> degree ae1 + degree ae2
  | `Negate ae0 -> degree ae0
  | `Var _ -> 1
  | `Int _ | `Real _ | `Frac _ -> 0
;;
