open Action_types

type 'a tycontext

val tyctx_empty : 'a tycontext
val tyctx_add : string loc -> 'a -> 'a tycontext -> ('a tycontext, exn) Result.t
val tyctx_add_exn : string loc -> 'a -> 'a tycontext -> 'a tycontext
val tyctx_get : 'a tycontext -> string loc -> ('a, exn) Result.t
val tyctx_get_exn : 'a tycontext -> string loc -> 'a
val tyctx_scope : 'a tycontext -> string list
val tyctx_restrict : 'a tycontext -> string loc list -> 'a tycontext
val tyctx_to_alist : 'a tycontext -> (string * 'a) list
val string_of_rel_op : rel_op -> string
val string_of_type : Typed.rhs_ty -> string
val print_arith_expr : Format.formatter -> Typed.arith_expr -> unit
val print_bool_expr : Format.formatter -> Typed.bool_expr -> unit
val print_prob_expr : Format.formatter -> Typed.prob_expr -> unit
val print_data_action : Format.formatter -> Typed.data_action -> unit
val smt_of_arith_expr : Typed.arith_expr -> Smt.expr
val smt_of_bool_expr : Typed.bool_expr -> Smt.expr
val degree : Typed.arith_expr -> int
