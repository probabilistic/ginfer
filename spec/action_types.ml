exception Static_error of string * Location.t

type 'a loc = 'a Location.loc =
  { txt : 'a
  ; loc : Location.t
  }

type bin_op =
  | Add
  | Sub
  | Mul

type rel_op =
  | LE
  | LT
  | GE
  | GT
  | EQ
  | NE

type ty =
  [ `TyBool
  | `TyInt
  | `TyReal
  | `TyProb
  | `TyTop
  ]
[@@deriving equal]

type sexpr =
  { exp_desc : sexpr_desc
  ; exp_info : ty
  ; exp_loc : Location.t
  }

and sexpr_desc =
  (* local variables *)
  | EVar of string loc
  (* literals *)
  | EBool of bool
  | EInt of int
  | EReal of float
  | EFrac of Mpqf.t
  | EProb of int * int
  (* arithmetic expressions *)
  | EBinOp of bin_op * sexpr * sexpr
  (* comparison expressions *)
  | ERelOp of rel_op * sexpr * sexpr
  (* short-circuit expressions *)
  | EAnd of sexpr * sexpr
  | EOr of sexpr * sexpr
  (* negation expressions *)
  | ENot of sexpr
  | ENegate of sexpr
  (* nondeterministic-choice *)
  | ENondet

module Untyped = struct
  type expr = sexpr

  let mk_expr exp_desc exp_loc = { exp_desc; exp_info = `TyTop; exp_loc }
  let mk_var ~loc x = mk_expr (EVar x) loc
  let mk_bool ~loc b = mk_expr (EBool b) loc
  let mk_int ~loc n = mk_expr (EInt n) loc
  let mk_real ~loc r = mk_expr (EReal r) loc
  let mk_frac ~loc f = mk_expr (EFrac f) loc
  let mk_prob ~loc (a, b) = mk_expr (EProb (a, b)) loc
  let mk_bin_op ~loc (bop, e1, e2) = mk_expr (EBinOp (bop, e1, e2)) loc
  let mk_rel_op ~loc (rop, e1, e2) = mk_expr (ERelOp (rop, e1, e2)) loc
  let mk_and ~loc (e1, e2) = mk_expr (EAnd (e1, e2)) loc
  let mk_or ~loc (e1, e2) = mk_expr (EOr (e1, e2)) loc
  let mk_not ~loc e0 = mk_expr (ENot e0) loc

  let mk_negate ~loc e0 =
    match e0.exp_desc with
    | EInt n -> mk_expr (EInt (-n)) loc
    | EReal r -> mk_expr (EReal (-.r)) loc
    | EFrac f -> mk_expr (EFrac (Mpqf.neg f)) loc
    | _ -> mk_expr (ENegate e0) loc
  ;;

  let mk_nondet ~loc = mk_expr ENondet loc
  let expr_loc e = e.exp_loc

  let destruct e =
    match e.exp_desc with
    | EVar x -> `Var x
    | EBool b -> `Bool b
    | EInt n -> `Int n
    | EReal r -> `Real r
    | EFrac f -> `Frac f
    | EProb (a, b) -> `Prob (a, b)
    | EBinOp (bop, e1, e2) -> `BinOp (bop, e1, e2)
    | ERelOp (rop, e1, e2) -> `RelOp (rop, e1, e2)
    | EAnd (e1, e2) -> `And (e1, e2)
    | EOr (e1, e2) -> `Or (e1, e2)
    | ENot e0 -> `Not e0
    | ENegate e0 -> `Negate e0
    | ENondet -> `Nondet
  ;;

  type data_action =
    | DWeaken of expr list
    | DSkip
    | DAssign of string loc * expr
    | DScore of expr * bool option ref
    | DAssume of expr
    | DSample of string loc * [ `Int of Dist.int_dist | `Real of Dist.real_dist ]
end

module Typed = struct
  type 'ty expr = sexpr
  type bool_ty = [ `TyBool ]

  type arith_ty =
    [ `TyInt
    | `TyReal
    ]

  type prob_ty = [ `TyProb ]

  type rhs_ty =
    [ `TyBool
    | `TyInt
    | `TyReal
    | `TyProb
    ]
  [@@deriving equal]

  type bool_expr = bool_ty expr
  type arith_expr = arith_ty expr
  type prob_expr = prob_ty expr
  type rhs_expr = rhs_ty expr

  let expr_loc e = e.exp_loc

  let expr_ty e =
    match e.exp_info with
    | `TyBool -> `TyBool
    | `TyInt -> `TyInt
    | `TyReal -> `TyReal
    | `TyProb -> `TyProb
    | `TyTop -> assert false
  ;;

  let refine e =
    match e.exp_info with
    | `TyBool -> `BoolExpr e
    | `TyInt | `TyReal -> `ArithExpr e
    | `TyProb -> `ProbExpr e
    | `TyTop -> assert false
  ;;

  let mk_expr exp_desc exp_info exp_loc = { exp_desc; exp_info; exp_loc }
  let mk_var ~loc x ty = mk_expr (EVar x) (ty :> ty) loc
  let mk_bool_var ~loc x ty = mk_var ~loc x ty
  let mk_arith_var ~loc x ty = mk_var ~loc x ty
  let mk_prob_var ~loc x ty = mk_var ~loc x ty
  let mk_bool ~loc b = mk_expr (EBool b) `TyBool loc
  let mk_int ~loc n = mk_expr (EInt n) `TyInt loc
  let mk_real ~loc r = mk_expr (EReal r) `TyReal loc
  let mk_frac ~loc f = mk_expr (EFrac f) `TyReal loc
  let mk_prob ~loc (a, b) = mk_expr (EProb (a, b)) `TyProb loc

  let mk_bin_op ~loc (bop, e1, e2) =
    let ty =
      match e1.exp_info, e2.exp_info with
      | `TyInt, `TyInt -> `TyInt
      | `TyInt, `TyReal | `TyReal, `TyInt | `TyReal, `TyReal -> `TyReal
      | _ -> assert false
    in
    mk_expr (EBinOp (bop, e1, e2)) ty loc
  ;;

  let mk_rel_op ~loc (rop, e1, e2) = mk_expr (ERelOp (rop, e1, e2)) `TyBool loc
  let mk_and ~loc (e1, e2) = mk_expr (EAnd (e1, e2)) `TyBool loc
  let mk_or ~loc (e1, e2) = mk_expr (EOr (e1, e2)) `TyBool loc
  let mk_not ~loc e0 = mk_expr (ENot e0) `TyBool loc

  let mk_negate ~loc e0 =
    let ty =
      match e0.exp_info with
      | `TyInt -> `TyInt
      | `TyReal -> `TyReal
      | _ -> assert false
    in
    mk_expr (ENegate e0) ty loc
  ;;

  let mk_nondet ~loc = mk_expr ENondet `TyBool loc

  let assignable ~from_ty ~to_ty =
    match from_ty, to_ty with
    | `TyBool, `TyBool
    | `TyInt, `TyInt
    | `TyInt, `TyReal
    | `TyReal, `TyReal
    | `TyProb, `TyProb -> true
    | _ -> false
  ;;

  let destruct_bool e =
    match e.exp_desc with
    | EVar x when equal_ty e.exp_info `TyBool -> `Var (x, `TyBool)
    | EBool b -> `Bool b
    | ERelOp (rop, e1, e2) -> `RelOp (rop, e1, e2)
    | EAnd (e1, e2) -> `And (e1, e2)
    | EOr (e1, e2) -> `Or (e1, e2)
    | ENot e0 -> `Not e0
    | ENondet -> `Nondet
    | _ -> assert false
  ;;

  let destruct_arith e =
    match e.exp_desc with
    | EVar x when equal_ty e.exp_info `TyInt -> `Var (x, `TyInt)
    | EVar x when equal_ty e.exp_info `TyReal -> `Var (x, `TyReal)
    | EInt n -> `Int n
    | EReal r -> `Real r
    | EFrac f -> `Frac f
    | EBinOp (bop, e1, e2) -> `BinOp (bop, e1, e2)
    | ENegate e0 -> `Negate e0
    | _ -> assert false
  ;;

  let destruct_prob e =
    match e.exp_desc with
    | EVar x when equal_ty e.exp_info `TyProb -> `Var (x, `TyProb)
    | EProb (a, b) -> `Prob (a, b)
    | _ -> assert false
  ;;

  type data_action =
    | DWeaken of arith_expr list
    | DSkip
    | DAssign of string loc * rhs_expr
    | DScore of arith_expr * bool option ref
    | DAssume of bool_expr
    | DSample of string loc * [ `Int of Dist.int_dist | `Real of Dist.real_dist ]
end

let create_sample_dist_exn ~loc name params =
  match name, params with
  | "runif", [ { exp_desc = EReal a; _ }; { exp_desc = EReal b; _ } ]
  | "UnifR", [ { exp_desc = EReal a; _ }; { exp_desc = EReal b; _ } ] ->
    `Real (Dist.real_unif a b)
  | "ber", [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ } ]
  | "Ber", [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ } ] ->
    `Int (Dist.int_ber a b)
  | ( "bin"
    , [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ }; { exp_desc = EInt n; _ } ] )
  | ( "Bin"
    , [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ }; { exp_desc = EInt n; _ } ] )
    -> `Int (Dist.int_bin a b n)
  | "geo", [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ } ]
  | "Geo", [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ } ] ->
    `Int (Dist.int_geo a b)
  | ( "nbin"
    , [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ }; { exp_desc = EInt n; _ } ] )
  | ( "Nbin"
    , [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ }; { exp_desc = EInt n; _ } ] )
    -> `Int (Dist.int_nbin a b n)
  | "pois", [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ } ]
  | "Pois", [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ } ] ->
    `Int (Dist.int_pois a b)
  | ( "hyper"
    , [ { exp_desc = EInt n; _ }; { exp_desc = EInt r; _ }; { exp_desc = EInt m; _ } ] )
  | ( "Hyper"
    , [ { exp_desc = EInt n; _ }; { exp_desc = EInt r; _ }; { exp_desc = EInt m; _ } ] )
    -> `Int (Dist.int_hyper n r m)
  | "unif", [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ } ]
  | "Unif", [ { exp_desc = EInt a; _ }; { exp_desc = EInt b; _ } ] ->
    `Int (Dist.int_unif a b)
  | _ -> raise (Static_error ("unsupported distribution or incorrect signature", loc))
;;

let () =
  Location.register_error_of_exn (function
      | Static_error (msg, loc) -> Some (Location.errorf ~loc "%s" msg)
      | _ -> None)
;;
