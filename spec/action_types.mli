exception Static_error of string * Location.t

type 'a loc = 'a Location.loc =
  { txt : 'a
  ; loc : Location.t
  }

type bin_op =
  | Add
  | Sub
  | Mul

type rel_op =
  | LE
  | LT
  | GE
  | GT
  | EQ
  | NE

module Untyped : sig
  type expr

  val mk_var : loc:Location.t -> string loc -> expr
  val mk_bool : loc:Location.t -> bool -> expr
  val mk_int : loc:Location.t -> int -> expr
  val mk_real : loc:Location.t -> float -> expr
  val mk_frac : loc:Location.t -> Mpqf.t -> expr
  val mk_prob : loc:Location.t -> int * int -> expr
  val mk_bin_op : loc:Location.t -> bin_op * expr * expr -> expr
  val mk_rel_op : loc:Location.t -> rel_op * expr * expr -> expr
  val mk_and : loc:Location.t -> expr * expr -> expr
  val mk_or : loc:Location.t -> expr * expr -> expr
  val mk_not : loc:Location.t -> expr -> expr
  val mk_negate : loc:Location.t -> expr -> expr
  val mk_nondet : loc:Location.t -> expr
  val expr_loc : expr -> Location.t

  val destruct
    :  expr
    -> [ `Var of string loc
       | `Bool of bool
       | `Int of int
       | `Real of float
       | `Frac of Mpqf.t
       | `Prob of int * int
       | `BinOp of bin_op * expr * expr
       | `RelOp of rel_op * expr * expr
       | `And of expr * expr
       | `Or of expr * expr
       | `Not of expr
       | `Negate of expr
       | `Nondet
       ]

  type data_action =
    | DWeaken of expr list
    | DSkip
    | DAssign of string loc * expr
    | DScore of expr * bool option ref
    | DAssume of expr
    | DSample of string loc * [ `Int of Dist.int_dist | `Real of Dist.real_dist ]
end

module Typed : sig
  type +'ty expr
  type bool_ty = [ `TyBool ]

  type arith_ty =
    [ `TyInt
    | `TyReal
    ]

  type prob_ty = [ `TyProb ]

  type rhs_ty =
    [ `TyBool
    | `TyInt
    | `TyReal
    | `TyProb
    ]
  [@@deriving equal]

  type bool_expr = bool_ty expr
  type arith_expr = arith_ty expr
  type prob_expr = prob_ty expr
  type rhs_expr = rhs_ty expr

  val expr_loc : rhs_expr -> Location.t
  val expr_ty : rhs_expr -> rhs_ty

  val refine
    :  rhs_expr
    -> [ `BoolExpr of bool_expr | `ArithExpr of arith_expr | `ProbExpr of prob_expr ]

  val mk_var : loc:Location.t -> string loc -> rhs_ty -> rhs_expr
  val mk_bool_var : loc:Location.t -> string loc -> bool_ty -> bool_expr
  val mk_arith_var : loc:Location.t -> string loc -> arith_ty -> arith_expr
  val mk_prob_var : loc:Location.t -> string loc -> prob_ty -> prob_expr
  val mk_bool : loc:Location.t -> bool -> bool_expr
  val mk_int : loc:Location.t -> int -> arith_expr
  val mk_real : loc:Location.t -> float -> arith_expr
  val mk_frac : loc:Location.t -> Mpqf.t -> arith_expr
  val mk_prob : loc:Location.t -> int * int -> prob_expr
  val mk_bin_op : loc:Location.t -> bin_op * arith_expr * arith_expr -> arith_expr
  val mk_rel_op : loc:Location.t -> rel_op * arith_expr * arith_expr -> bool_expr
  val mk_and : loc:Location.t -> bool_expr * bool_expr -> bool_expr
  val mk_or : loc:Location.t -> bool_expr * bool_expr -> bool_expr
  val mk_not : loc:Location.t -> bool_expr -> bool_expr
  val mk_negate : loc:Location.t -> arith_expr -> arith_expr
  val mk_nondet : loc:Location.t -> bool_expr
  val assignable : from_ty:rhs_ty -> to_ty:rhs_ty -> bool

  val destruct_bool
    :  bool_expr
    -> [ `Var of string loc * bool_ty
       | `Bool of bool
       | `RelOp of rel_op * arith_expr * arith_expr
       | `And of bool_expr * bool_expr
       | `Or of bool_expr * bool_expr
       | `Not of bool_expr
       | `Nondet
       ]

  val destruct_arith
    :  arith_expr
    -> [ `Var of string loc * arith_ty
       | `Int of int
       | `Real of float
       | `Frac of Mpqf.t
       | `BinOp of bin_op * arith_expr * arith_expr
       | `Negate of arith_expr
       ]

  val destruct_prob : prob_expr -> [ `Var of string loc * prob_ty | `Prob of int * int ]

  type data_action =
    | DWeaken of arith_expr list
    | DSkip
    | DAssign of string loc * rhs_expr
    | DScore of arith_expr * bool option ref
    | DAssume of bool_expr
    | DSample of string loc * [ `Int of Dist.int_dist | `Real of Dist.real_dist ]
end

val create_sample_dist_exn
  :  loc:Location.t
  -> string
  -> Untyped.expr list
  -> [ `Int of Dist.int_dist | `Real of Dist.real_dist ]
