open Action_types

type aux_arith_expr =
  [ `Var of string loc * Typed.arith_ty
  | `Int of int
  | `Real of float
  | `Frac of Mpqf.t
  | `BinOp of bin_op * aux_arith_expr * aux_arith_expr
  | `Negate of aux_arith_expr
  | `IfEQ of (aux_arith_expr * aux_arith_expr) * aux_arith_expr * aux_arith_expr
  | `IfRel of
    (rel_op * Typed.arith_expr * Typed.arith_expr) * aux_arith_expr * aux_arith_expr
  | `IfNondet of aux_arith_expr * aux_arith_expr
  ]

let numeralize_bool_expr : Typed.bool_expr -> aux_arith_expr =
  let rec aux be =
    match Typed.destruct_bool be with
    | `Var (x, _) -> `Var (x, `TyInt)
    | `Bool true -> `Int 1
    | `Bool false -> `Int 0
    | `RelOp (rop, ae1, ae2) -> `IfRel ((rop, ae1, ae2), `Int 1, `Int 0)
    | `And (be1, be2) ->
      let ae1 = aux be1 in
      let ae2 = aux be2 in
      `IfEQ ((ae1, `Int 1), `IfEQ ((ae2, `Int 1), `Int 1, `Int 0), `Int 0)
    | `Or (be1, be2) ->
      let ae1 = aux be1 in
      let ae2 = aux be2 in
      `IfEQ ((ae1, `Int 1), `Int 1, `IfEQ ((ae2, `Int 1), `Int 1, `Int 0))
    | `Not be0 ->
      let ae0 = aux be0 in
      `IfEQ ((ae0, `Int 1), `Int 0, `Int 1)
    | `Nondet -> `IfNondet (`Int 1, `Int 0)
  in
  aux
;;

let numeralize_prob_expr : Typed.prob_expr -> aux_arith_expr =
  let aux pe =
    match Typed.destruct_prob pe with
    | `Var (x, _) -> `Var (x, `TyReal)
    | `Prob (a, b) -> `Frac (Mpqf.of_frac a (a + b))
  in
  aux
;;
