open Action_types

type aux_arith_expr =
  [ `Var of string loc * Typed.arith_ty
  | `Int of int
  | `Real of float
  | `Frac of Mpqf.t
  | `BinOp of bin_op * aux_arith_expr * aux_arith_expr
  | `Negate of aux_arith_expr
  | `IfEQ of (aux_arith_expr * aux_arith_expr) * aux_arith_expr * aux_arith_expr
  | `IfRel of
    (rel_op * Typed.arith_expr * Typed.arith_expr) * aux_arith_expr * aux_arith_expr
  | `IfNondet of aux_arith_expr * aux_arith_expr
  ]

val numeralize_bool_expr : Typed.bool_expr -> aux_arith_expr
val numeralize_prob_expr : Typed.prob_expr -> aux_arith_expr
