open Core

type 'a dist =
  { dist_name : string
  ; dist_range : 'a option * 'a option
  ; dist_pmf : 'a -> (float, Mpqf.t) Either.t
  ; dist_mom : int -> (float, Mpqf.t) Either.t
  }

type int_dist = int dist
type real_dist = float dist

let cache_computing f =
  let cache = Hashtbl.create (module Int) in
  fun k -> Hashtbl.find_or_add cache k ~default:(fun () -> f k)
;;

let moments_of_imp_dist ?(nseries = 1000) ran pmf =
  cache_computing (fun k ->
      match ran with
      | None, None -> assert false
      | None, Some r ->
        let series = Array.create ~len:nseries 0.0 in
        for i = 0 to nseries - 1 do
          let x = r - i in
          series.(i) <- pmf x *. Float.int_pow (Float.of_int x) k
        done;
        Gsl_caller.sum_accel (Array.to_list series)
      | Some l, None ->
        let series = Array.create ~len:nseries 0.0 in
        for i = 0 to nseries - 1 do
          let x = l + i in
          series.(i) <- pmf x *. Float.int_pow (Float.of_int x) k
        done;
        Gsl_caller.sum_accel (Array.to_list series)
      | Some l, Some r ->
        let acc = ref 0. in
        for i = l to r do
          acc := !acc +. (pmf i *. Float.int_pow (Float.of_int i) k)
        done;
        !acc)
;;

let moments_of_imp_dist_exact ran pmf =
  cache_computing (fun k ->
      match ran with
      | l, r ->
        let acc = ref (Mpqf.of_int 0) in
        for i = l to r do
          acc := Mpqf.(add !acc (mul (pmf i) (Mpqf.of_int (Int.pow i k))))
        done;
        !acc)
;;

let int_ber pa pb =
  let p = Float.(of_int pa / of_int pb) in
  { dist_name = Format.sprintf "Bernoulli(%g)" p
  ; dist_range = Some 0, Some 1
  ; dist_pmf =
      (fun k ->
        Either.second (if k = 0 then Mpqf.of_frac (pb - pa) pb else Mpqf.of_frac pa pb))
  ; dist_mom = (fun _ -> Either.second (Mpqf.of_frac pa pb))
  }
;;

let int_bin pa pb n =
  let p = Float.(of_int pa / of_int pb) in
  let ran = Some 0, Some n in
  let pmf k = Gsl_caller.ran_binomial_pdf k p n in
  let cache = moments_of_imp_dist ran pmf in
  { dist_name = Format.sprintf "Binomial(%g, %d)" p n
  ; dist_range = ran
  ; dist_pmf = (fun k -> Either.first (pmf k))
  ; dist_mom = (fun m -> Either.first (cache m))
  }
;;

let int_geo pa pb =
  let p = Float.(of_int pa / of_int pb) in
  let ran = Some 1, None in
  let pmf k = Gsl_caller.ran_geometric_pdf k p in
  let cache = moments_of_imp_dist ran pmf in
  { dist_name = Format.sprintf "Geometric(%g)" p
  ; dist_range = ran
  ; dist_pmf = (fun k -> Either.first (pmf k))
  ; dist_mom = (fun m -> Either.first (cache m))
  }
;;

let int_nbin pa pb n =
  let p = Float.(of_int pa / of_int pb) in
  let ran = Some 0, None in
  let pmf k = Gsl_caller.ran_negative_binomial k p (Float.of_int n) in
  let cache = moments_of_imp_dist ran pmf in
  { dist_name = Format.sprintf "NegativeBinomial(%g, %d)" p n
  ; dist_range = ran
  ; dist_pmf = (fun k -> Either.first (pmf k))
  ; dist_mom = (fun m -> Either.first (cache m))
  }
;;

let int_pois mua mub =
  let mu = Float.(of_int mua / of_int mub) in
  let ran = Some 0, None in
  let pmf k = Gsl_caller.ran_poisson_pdf k mu in
  let cache = moments_of_imp_dist ran pmf in
  { dist_name = Format.sprintf "Poisson(%g)" mu
  ; dist_range = ran
  ; dist_pmf = (fun k -> Either.first (pmf k))
  ; dist_mom = (fun m -> Either.first (cache m))
  }
;;

let int_hyper n r m =
  let n1, n2, t = r, n - r, m in
  let ran = Some (max 0 (t - n2)), Some (min t n1) in
  let pmf k = Gsl_caller.ran_hypergeometric k n1 n2 t in
  let cache = moments_of_imp_dist ran pmf in
  { dist_name = Format.sprintf "HyperGeometric(%d, %d, %d)" n1 n2 t
  ; dist_range = ran
  ; dist_pmf = (fun k -> Either.first (pmf k))
  ; dist_mom = (fun m -> Either.first (cache m))
  }
;;

let int_unif a b =
  let ran = Some a, Some b in
  let pmf k = if a <= k && k <= b then Mpqf.of_frac 1 (b - a + 1) else Mpqf.of_int 0 in
  let cache = moments_of_imp_dist_exact (a, b) pmf in
  { dist_name = Format.sprintf "DiscreteUniform(%d, %d)" a b
  ; dist_range = ran
  ; dist_pmf = (fun k -> Either.second (pmf k))
  ; dist_mom = (fun m -> Either.second (cache m))
  }
;;

let real_unif a b =
  let ran = Some a, Some b in
  let pmf x = if Float.(a < x) && Float.(x < b) then 1. /. (b -. a) else 0. in
  { dist_name = Format.sprintf "Uniform(%g, %g)" a b
  ; dist_range = ran
  ; dist_pmf = (fun k -> Either.first (pmf k))
  ; dist_mom =
      cache_computing (fun k ->
          let acc = ref 0.0 in
          for i = 0 to k do
            acc := !acc +. (Float.int_pow a i *. Float.int_pow b (k - i))
          done;
          Either.first (!acc /. Float.of_int (k + 1)))
  }
;;
