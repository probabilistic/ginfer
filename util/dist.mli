open Core

type 'a dist =
  { dist_name : string
  ; dist_range : 'a option * 'a option
  ; dist_pmf : 'a -> (float, Mpqf.t) Either.t
  ; dist_mom : int -> (float, Mpqf.t) Either.t
  }

type int_dist = int dist
type real_dist = float dist

val int_ber : int -> int -> int_dist
val int_bin : int -> int -> int -> int_dist
val int_geo : int -> int -> int_dist
val int_nbin : int -> int -> int -> int_dist
val int_pois : int -> int -> int_dist
val int_hyper : int -> int -> int -> int_dist
val int_unif : int -> int -> int_dist
val real_unif : float -> float -> real_dist
