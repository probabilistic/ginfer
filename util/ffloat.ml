open Core

module type FLOAT = sig
  type t

  val zero : t
  val one : t
  val minus_one : t
  val infinity : t
  val neg_infinity : t
  val abs : t -> t
  val neg : t -> t
  val ( + ) : t -> t -> t
  val ( - ) : t -> t -> t
  val ( * ) : t -> t -> t
  val ( / ) : t -> t -> t
  val ( >= ) : t -> t -> bool
  val ( < ) : t -> t -> bool
  val ( > ) : t -> t -> bool
  val ( <= ) : t -> t -> bool
  val of_int : int -> t
  val of_float : float -> t
  val pp : Format.formatter -> t -> unit
  val is_zero : t -> bool
  val of_mpqf : Mpqf.t -> t
  val of_mpfrf : Mpfrf.t -> t
  val to_int : t -> int
  val to_float : t -> float
  val to_mpqf : t -> Mpqf.t
end

module Std_float = struct
  include Float

  let pp fmt f = Format.fprintf fmt "%.8f" f
  let is_zero f = abs f < 1e-8
  let of_mpqf = Mpqf.to_float
  let of_mpfrf = Mpfrf.to_float ~round:Mpfr.Zero
  let to_mpqf = Mpqf.of_float
end

module Mpq_float = struct
  type t = Mpqf.t

  let zero = Mpqf.of_int 0
  let one = Mpqf.of_int 1
  let minus_one = Mpqf.of_int (-1)
  let infinity = Mpqf.of_float 1e100
  let neg_infinity = Mpqf.of_float (-1e100)
  let abs = Mpqf.abs
  let neg = Mpqf.neg
  let ( + ) = Mpqf.add
  let ( - ) = Mpqf.sub
  let ( * ) = Mpqf.mul
  let ( / ) = Mpqf.div
  let ( >= ) a b = Mpqf.cmp a b >= 0
  let ( < ) a b = Mpqf.cmp a b < 0
  let ( > ) a b = Mpqf.cmp a b > 0
  let ( <= ) a b = Mpqf.cmp a b <= 0
  let of_int = Mpqf.of_int
  let of_float = Mpqf.of_float
  let pp = Mpqf.print
  let is_zero f = Mpqf.cmp f zero = 0
  let of_mpqf f = f
  let of_mpfrf = Mpfrf.to_mpqf
  let to_int f = Mpqf.to_float f |> Float.to_int
  let to_float f = Mpqf.to_float f
  let to_mpqf f = f
end
