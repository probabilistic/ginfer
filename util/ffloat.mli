module type FLOAT = sig
  type t

  val zero : t
  val one : t
  val minus_one : t
  val infinity : t
  val neg_infinity : t
  val abs : t -> t
  val neg : t -> t
  val ( + ) : t -> t -> t
  val ( - ) : t -> t -> t
  val ( * ) : t -> t -> t
  val ( / ) : t -> t -> t
  val ( >= ) : t -> t -> bool
  val ( < ) : t -> t -> bool
  val ( > ) : t -> t -> bool
  val ( <= ) : t -> t -> bool
  val of_int : int -> t
  val of_float : float -> t
  val pp : Format.formatter -> t -> unit
  val is_zero : t -> bool
  val of_mpqf : Mpqf.t -> t
  val of_mpfrf : Mpfrf.t -> t
  val to_int : t -> int
  val to_float : t -> float
  val to_mpqf : t -> Mpqf.t
end

module Std_float : FLOAT with type t = float
module Mpq_float : FLOAT with type t = Mpqf.t
