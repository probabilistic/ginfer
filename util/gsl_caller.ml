open Core

let ran_bernoulli_pdf k p = Gsl.Randist.bernoulli_pdf k ~p
let ran_binomial_pdf k p n = Gsl.Randist.binomial_pdf k ~p ~n
let ran_geometric_pdf k p = Gsl.Randist.geometric_pdf k ~p
let ran_negative_binomial k p n = Gsl.Randist.negative_binomial_pdf k ~p ~n
let ran_poisson_pdf k mu = Gsl.Randist.poisson_pdf k ~mu
let ran_hypergeometric k n1 n2 t = Gsl.Randist.hypergeometric_pdf k ~n1 ~n2 ~t

let sum_accel series =
  let res =
    Gsl.Sum.Trunc.accel (Array.of_list series) (Gsl.Sum.Trunc.make (List.length series))
  in
  res.res
;;
