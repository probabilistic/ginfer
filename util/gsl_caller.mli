val ran_bernoulli_pdf : int -> float -> float
val ran_binomial_pdf : int -> float -> int -> float
val ran_geometric_pdf : int -> float -> float
val ran_negative_binomial : int -> float -> float -> float
val ran_poisson_pdf : int -> float -> float
val ran_hypergeometric : int -> int -> int -> int -> float
val sum_accel : float list -> float
