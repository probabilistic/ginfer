open Core
open Ffloat

module type SOLVER = sig
  type lp_manager
  type lp_var = int [@@deriving sexp, compare]
  type lp_flt

  val create_lp_manager : unit -> lp_manager
  val new_lpvar : lp_manager -> lp_var

  val add_lprow_array
    :  lp_manager
    -> ?k:lp_flt
    -> (lp_var * lp_flt) array
    -> [> `Ge | `Le ]
    -> unit

  val change_objective_coefficients : lp_manager -> (int * lp_flt) array -> unit
  val initial_solve : lp_manager -> [ `Maximize | `Minimize ] -> unit
  val status : lp_manager -> int
  val set_log_level : lp_manager -> int -> unit
  val primal_column_solution : lp_manager -> lp_flt array
  val output_stats : lp_manager -> unit
end

module type SOLVER_STD = SOLVER with type lp_flt := float
module type SOLVER_MPQ = SOLVER with type lp_flt := Mpqf.t

module Make_solver (Flt : FLOAT) (S : Lp_common.SOLVER with type flt := Flt.t) :
  SOLVER with type lp_flt := Flt.t = struct
  type lp_manager =
    S.t * int ref * (S.column array * int ref * int ref) * (S.row array * int ref)

  type lp_var = int [@@deriving sexp, compare]

  let dummycol =
    { Lp_common.column_obj = Flt.zero
    ; column_lower = Flt.zero
    ; column_upper = Flt.zero
    ; column_elements = [||]
    }
  ;;

  let dummyrow =
    { Lp_common.row_lower = Flt.zero; row_upper = Flt.zero; row_elements = [||] }
  ;;

  let maxcols = 1000
  let maxrows = 1000

  let flush_cols (st, _, (cols, ncols, _), _) =
    if !ncols > 0
    then (
      S.add_columns st cols !ncols;
      ncols := 0)
  ;;

  let flush_rows ((st, _, _, (rows, nrows)) as t) =
    if !nrows > 0
    then (
      flush_cols t;
      S.add_rows st rows !nrows;
      nrows := 0)
  ;;

  let create_lp_manager () =
    ( S.create ()
    , ref 1
    , (Array.create ~len:maxcols dummycol, ref 0, ref 0)
    , (Array.create ~len:maxrows dummyrow, ref 0) )
  ;;

  let new_lpvar ((_, _, (cols, ncols, rcols), _) as t) =
    let col =
      { Lp_common.column_obj = Flt.zero
      ; column_lower = Flt.neg_infinity
      ; column_upper = Flt.infinity
      ; column_elements = [||]
      }
    in
    if !ncols = maxcols then flush_cols t;
    cols.(!ncols) <- col;
    incr ncols;
    incr rcols;
    !rcols - 1
  ;;

  let add_lprow_array ((_, _, _, (rows, nrows)) as t) ?(k = Flt.zero) arr o =
    let row =
      { Lp_common.row_lower =
          (match o with
          | `Le -> Flt.neg_infinity
          | _ -> k)
      ; row_upper =
          (match o with
          | `Ge -> Flt.infinity
          | _ -> k)
      ; row_elements = arr
      }
    in
    if !nrows = maxrows then flush_rows t;
    rows.(!nrows) <- row;
    incr nrows
  ;;

  let change_objective_coefficients ((st, _, _, _) as t) pairs =
    flush_rows t;
    let obj =
      S.(
        force_sync st;
        objective_coefficients st)
    in
    Array.iter pairs ~f:(fun (idx, coef) -> obj.(idx) <- coef);
    S.change_objective_coefficients st obj
  ;;

  let set_direction st dir =
    match dir with
    | `Maximize -> S.set_direction st Lp_common.Maximize
    | `Minimize -> S.set_direction st Lp_common.Minimize
  ;;

  let initial_solve ((st, log_level, _, _) as t) dir =
    flush_rows t;
    set_direction st dir;
    (* S.write_mps st "debug.mps"; *)
    S.solve_with_log_level st !log_level
  ;;

  let status (st, _, _, _) =
    match S.status st with
    | Lp_common.Optimal | Lp_common.Suboptimal -> 0
    | _ -> 1
  ;;

  let set_log_level (_, log_level, _, _) l = log_level := l
  let primal_column_solution (st, _, _, _) = S.primal_column_solution st

  let output_stats (st, _, _, _) =
    Format.printf
      "lp rows: %d@.lp cols: %d@.lp nzs: %d@."
      (S.number_rows st)
      (S.number_columns st)
      (S.number_elements st)
  ;;
end

let lp_solvers_std = Hashtbl.create (module String) ~size:2
let lp_solvers_mpq = Hashtbl.create (module String) ~size:2

let iter_lp_solvers_std ~f =
  Hashtbl.iteri lp_solvers_std ~f:(fun ~key:name ~data:solver -> f ~name ~solver)
;;

let iter_lp_solvers_mpq ~f =
  Hashtbl.iteri lp_solvers_mpq ~f:(fun ~key:name ~data:solver -> f ~name ~solver)
;;

let get_lp_solver_std name = Hashtbl.find lp_solvers_std name
let get_lp_solver_mpq name = Hashtbl.find lp_solvers_mpq name

let () =
  Lp_common.iter_lp_backends_std ~f:(fun ~name ~backend ->
      Hashtbl.set
        lp_solvers_std
        ~key:name
        ~data:(module Make_solver (Std_float) ((val backend)) : SOLVER_STD));
  Lp_common.iter_lp_backends_mpq ~f:(fun ~name ~backend ->
      Hashtbl.set
        lp_solvers_mpq
        ~key:name
        ~data:(module Make_solver (Mpq_float) ((val backend)) : SOLVER_MPQ))
;;
