module type SOLVER = sig
  type lp_manager
  type lp_var = int [@@deriving sexp, compare]
  type lp_flt

  val create_lp_manager : unit -> lp_manager
  val new_lpvar : lp_manager -> lp_var

  val add_lprow_array
    :  lp_manager
    -> ?k:lp_flt
    -> (lp_var * lp_flt) array
    -> [> `Ge | `Le ]
    -> unit

  val change_objective_coefficients : lp_manager -> (int * lp_flt) array -> unit
  val initial_solve : lp_manager -> [ `Maximize | `Minimize ] -> unit
  val status : lp_manager -> int
  val set_log_level : lp_manager -> int -> unit
  val primal_column_solution : lp_manager -> lp_flt array
  val output_stats : lp_manager -> unit
end

module type SOLVER_STD = SOLVER with type lp_flt := float
module type SOLVER_MPQ = SOLVER with type lp_flt := Mpqf.t

val iter_lp_solvers_std : f:(name:string -> solver:(module SOLVER_STD) -> unit) -> unit
val iter_lp_solvers_mpq : f:(name:string -> solver:(module SOLVER_MPQ) -> unit) -> unit
val get_lp_solver_std : string -> (module SOLVER_STD) option
val get_lp_solver_mpq : string -> (module SOLVER_MPQ) option
