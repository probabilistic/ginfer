open Core

exception Smt_failure

let () =
  Caml.Printexc.register_printer (function
      | Smt_failure -> Some "analysis failure: SMT failed"
      | _ -> None)
;;

type model = Z3.Model.model
type expr = Z3.Expr.expr

let cfg = [ "model", "true"; "proof", "false"; "unsat_core", "false" ]
let ctx = Z3.mk_context cfg

let gen_free_var =
  let cnt = ref 0 in
  fun () ->
    let res = "_" ^ Int.to_string !cnt in
    incr cnt;
    res
;;

let mk_const n = Z3.Arithmetic.Integer.mk_numeral_s ctx (Int.to_string n)
let mk_const_r r = Z3.Arithmetic.Real.mk_numeral_s ctx (Float.to_string r)

let mk_const_f f =
  Z3.Arithmetic.Real.mk_numeral_nd
    ctx
    (Mpqf.get_num f |> Mpzf.to_string |> Int.of_string)
    (Mpqf.get_den f |> Mpzf.to_string |> Int.of_string)
;;

let mk_true () = Z3.Boolean.mk_true ctx
let mk_false () = Z3.Boolean.mk_false ctx
let mk_bvar x = Z3.Boolean.mk_const_s ctx x
let mk_bfvar () = Z3.Boolean.mk_const_s ctx (gen_free_var ())
let mk_ivar x = Z3.Arithmetic.Integer.mk_const_s ctx x
let mk_rvar x = Z3.Arithmetic.Real.mk_const_s ctx x
let mk_add e1 e2 = Z3.Arithmetic.mk_add ctx [ e1; e2 ]
let mk_minus e1 e2 = Z3.Arithmetic.mk_sub ctx [ e1; e2 ]
let mk_mult e1 e2 = Z3.Arithmetic.mk_mul ctx [ e1; e2 ]
let mk_div e1 e2 = Z3.Arithmetic.mk_div ctx e1 e2
let mk_mod e1 e2 = Z3.Arithmetic.Integer.mk_mod ctx e1 e2
let mk_neg e = Z3.Arithmetic.mk_unary_minus ctx e
let mk_eq e1 e2 = Z3.Boolean.mk_eq ctx e1 e2
let mk_lt e1 e2 = Z3.Arithmetic.mk_lt ctx e1 e2
let mk_le e1 e2 = Z3.Arithmetic.mk_le ctx e1 e2
let mk_gt e1 e2 = Z3.Arithmetic.mk_gt ctx e1 e2
let mk_ge e1 e2 = Z3.Arithmetic.mk_ge ctx e1 e2
let mk_max e1 e2 = Z3.Boolean.mk_ite ctx (mk_gt e1 e2) e1 e2
let mk_and e1 e2 = Z3.Boolean.mk_and ctx [ e1; e2 ]
let mk_or e1 e2 = Z3.Boolean.mk_or ctx [ e1; e2 ]
let mk_not e = Z3.Boolean.mk_not ctx e

let check hyps goal =
  let solver = Z3.Solver.mk_simple_solver ctx in
  Z3.Solver.add solver (mk_not goal :: hyps);
  let res = Z3.Solver.check solver [] in
  match res with
  | Z3.Solver.UNSATISFIABLE -> true
  | _ -> false
;;

let fit forms =
  let solver = Z3.Solver.mk_simple_solver ctx in
  Z3.Solver.add solver forms;
  let res = Z3.Solver.check solver [] in
  match res with
  | Z3.Solver.SATISFIABLE -> Option.value_exn (Z3.Solver.get_model solver)
  | _ -> raise Smt_failure
;;

let eval_bvar model x =
  Option.value_exn (Z3.Model.eval model (mk_bvar x) true) |> Z3.Boolean.is_true
;;

let eval_ivar model x =
  Option.value_exn (Z3.Model.eval model (mk_ivar x) true)
  |> Z3.Arithmetic.Integer.numeral_to_string
  |> Int.of_string
;;

let eval_rvar model x =
  Option.value_exn (Z3.Model.eval model (mk_rvar x) true)
  |> Z3.Arithmetic.Real.numeral_to_string
  |> fun s ->
  try
    let n = Int.of_string s in
    `Int n
  with
  | _ ->
    (try
       let f = Mpqf.of_string s in
       `Frac f
     with
    | _ ->
      let r = Float.of_string s in
      `Real r)
;;

let print_model fmt m = Format.fprintf fmt "%s" (Z3.Model.to_string m)
let mk_abs e = Z3.Boolean.mk_ite ctx (mk_ge e (mk_const 0)) e (mk_neg e)
