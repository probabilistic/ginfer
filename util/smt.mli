type model
type expr

val mk_const : int -> expr
val mk_const_r : float -> expr
val mk_const_f : Mpqf.t -> expr
val mk_true : unit -> expr
val mk_false : unit -> expr
val mk_bvar : string -> expr
val mk_bfvar : unit -> expr
val mk_ivar : string -> expr
val mk_rvar : string -> expr
val mk_add : expr -> expr -> expr
val mk_minus : expr -> expr -> expr
val mk_mult : expr -> expr -> expr
val mk_div : expr -> expr -> expr
val mk_mod : expr -> expr -> expr
val mk_neg : expr -> expr
val mk_eq : expr -> expr -> expr
val mk_lt : expr -> expr -> expr
val mk_le : expr -> expr -> expr
val mk_gt : expr -> expr -> expr
val mk_ge : expr -> expr -> expr
val mk_max : expr -> expr -> expr
val mk_and : expr -> expr -> expr
val mk_or : expr -> expr -> expr
val mk_not : expr -> expr
val check : expr list -> expr -> bool
val fit : expr list -> model
val eval_bvar : model -> string -> bool
val eval_ivar : model -> string -> int
val eval_rvar : model -> string -> [ `Real of float | `Frac of Mpqf.t | `Int of int ]
val print_model : Format.formatter -> model -> unit
val mk_abs : expr -> expr
